# Capitulo 4

## Parte 1

![](capitulo_4/img/c1.jpg)

Buzucoily regresó a su casa con el corazón roto y permaneció ahí por tres días.

Yo pensé que no habría ningún otro evento aparte de este, pero…

-Yunyun ha estado algo rara últimamente.

“Buenos días Megumin. Ven toma esto.”

Yunyun me entrego una caja de almuerzo tras haber entrado al salón.

Fue tan inesperado que no sabía ni cómo responderle.

Así que solo sostuve la caja del almuerzo y me forcé a mí misma a decir algo.

“¿Qué sucede? Es que acaso tú ¿Estás enamorada de mí? Este es un cambio demasiado repentino. Para que tú te comportes como una esposa amorosa, siento que…”

“¡¿Qué pasa con eso de esposa amorosa?! Oye ¡¿Qué es lo que estás diciendo?! Yo simplemente no intento retarte el día de hoy ¡por lo que simplemente te entrego el almuerzo! En otras palabras, ‘¡Ya te entregué el almuerzo, así que no me molestes más!’ ¿entendiste?”

… ¿Qué?

“Por la manera en que lo dices, pareces sugerir que yo soy una persona despreciable que te quitaría ese almuerzo por cualquier forma necesaria si es que tu no me lo dieras.”

“Megumin es una persona despreciable- pero ya que yo también te molesto a diario, no es que yo sea mejor que tú.”

Yunyun lo dijo de golpe. Y justo cuando estaba considerando que responderle, el profesor encargado entro al salón.

El ruido del salón se desvaneció. El profesor se dirigió al pódium.

“Buenos días. He escuchado que el monstruo de hace algunos días, del cual se sospechaba que fuera un subalterno del Dios Demonio, ha aparecido dentro del Hogar del Clan de Magos Carmesí. La situación actual demanda ser precavidos.”

Tras escuchar eso el ruido volvió al salón.

Dejando de lado a los monstruos similares al ‘oso de un solo golpe’, monstruos normales escaparían tras ver la silueta de un miembro del Clan de Magos Carmesí. Era definitivamente poco natural que los monstruos se adentraran a la villa.

“Por tanto, incluso aunque las preparaciones no están completas, hemos decidido traer más gente para reformar el sello por la fuerza. El ritual se llevará a cabo a partir de mañana por la tarde hasta la mañana del día siguiente. Si es que llegamos a fallar, el Hogar del Clan de los Magos Carmesí, se vería inundado por los subalternos del Dios Demonio- Aunque por supuesto nosotros tenemos nuestros propios medios para lidiar con algo así.

Una vez que el ritual de comienzo, ustedes no deberán de andar por ahí, solo quédense en sus casas.”

El usualmente frívolo profesor mostró su raramente vista expresión seria.

Yo nuca le había puesto mucha atención a nuestro profesor antes. Ahora tras ver esto, el profesor se ve inesperadamente maduro.

“Muy bien. Primero que nada, anunciare los resultados de sus pruebas. Como es costumbre, ¡los tres primeros lugares recibirán una poción de aumento de habilidad! ¡Pasen adelante a recibirla cuando escuchen su nombre!… ¡Tercer lugar Nerimaki!”

Mientras que escuchaba al profesor, yo examinaba mi tarjeta de aventurero al mismo tiempo.

Ehehehe… solo me faltan cuatro puntos más.

Solo cuatro puntos más y podre aprender la Magia de Explosión de mis sueños.

“Segundo lugar, ¡Arue!”

El profesor seguía hablando mientras que yo sostenía mi tarjeta con una sonrisa.

… … Segundo lugar, ¿Arue?

“Primer lugar, ¡Megumin! Buen trabajo. Ven y toma tu poción.”

Me levante tras escuchar mi nombre y voltee a un lado mío.

Mire a Yunyun, quien apretaba su puño y lucia preocupada.

“La primera clase es sobre crear equipo que se vea genial- Tal como el parche para el ojo de Arue. Dicho equipamiento aumenta nuestra personalidad y consume 1 punto de habilidad. Por ejemplo, guantes sin dedos o un pañuelo. Ahora ¡Todas diríjanse al salón de manualidades!”

Después de que el profesor saliera del salón, yo puse a la vista mi nueva poción de aumento de habilidad y puse mi silla cerca de la de Yunyun.

Yunyun evito mi mirada torpemente. No dije nada y simplemente puse mi poción frente a ella.

“… Oye, ¡di algo! Si tú sigues así ¡el ambiente se volverá muy pesado!”

Hasta que tuvo suficiente, Yunyun se levantó y golpeo el pupitre.

Entonces inmediatamente se sentó de regreso a su lugar con arrepentimiento. Su enojo se disipo antes de lo normal.

“… Entonces diré algo. Los méritos de Yunyun son, ‘ser buena cocinando’, ‘excelente estudiante’ y ‘no tiene presencia’. Bueno, entonces, ¿Qué te sucedió esta vez?”

“Oye, ¡el último mérito es un poco extraño! ¡¿No tengo presencia?! Además de que ¡mis méritos no pueden ser tan pocos!”

Levante la poción frente a Yunyun, que estaba enrojecida y parecía no poder dejar pasar lo que le había dicho antes.

“Tu dijiste antes que no querías un encuentro hoy, así que ¿Qué piensas hacer ahora? Si recuerdo bien, tu dijiste que necesitas 3 puntos de habilidad más para aprender magia avanzada. A mí aún me faltan 4 puntos… ¿está realmente bien así? Finalmente vas delante de mí y podrás graduarte antes que yo. Tu no quieres ser superada por mí en el último momento ¿verdad? Oye, oye… ¿Qué es lo que harás?”

Yunyun escucho mi provocación y me miro con sentimientos encontrados…

“Ya lo dije antes, hoy, uh, no hay necesidad de un encuentro… tu deberías de tomarte tu poción rápidamente.”

“… En verdad. Entonces no queda de otra. Por tanto, me comeré el almuerzo también.”

Por alguna razón tras verme comer el almuerzo y tomarme mí poción ella parecía aliviada.

-Tal como parece, Yunyun está muy extraña últimamente.

***

“Oye, ¿ya escuchaste? Se rumorea que ¡el candidato a héroe ha llegado!”

Las clases de la mañana terminaron y fueron seguidas del descanso de la tarde.

Funifura menciono este rumor mientras que comíamos nuestros almuerzos en nuestros pupitres.

Rodeado de poderosos monstruos, el hogar del Clan de Magos Carmesí, está situado un sitio remoto y poco desarrollado.

¿Por qué vendría aquí un candidato a héroe?

El candidato a héroe es termino colectivo que se les da a las personas que recibieron poderes especiales de los dioses. Ellos usualmente tienen nombres extraños.

El rumor dice que además de sus extraños nombres, sus personalidades, actitudes y apodos son diferentes de los demás.

“¡Lo sé, lo sé! ¡Lo conocimos ayer! Él es un chico apuesto, supuestamente está aquí para reclutar miembros para su equipo que busquen derrotar al rey demonio. ¡Parece ser que necesitan a un mago poderoso!

Ah, ¿Porque tuvo que venir ahora? De haber venido más tarde cuando haya aprendido magia, ¡yo definitivamente lo hubiera acompañado!”

Dijo Dodonko, mientras suspiraba con pesar.

… Hm. El candidato a héroe es un chico apuesto.

Yo aún no puedo usar magia, así que aún no puedo unirme a algún equipo. Pero como candidata a Archí-Maga, es posible que conozca a este candidato a héroe algún día.

Después de todo, los poderosos tienden a atraerse unos a los otros.

Como dice el dicho, ‘Aves del mismo plumaje vuelan juntas’.

Yunyun le pregunto a Dodonko por curiosidad.

“El candidato a héroe… ¿Cómo es el? ¿Acaso es muy fuerte?”

“Él tiene a dos chicas acompañándolo, y usa una poderosa espada mágica. Es apuesto. Y he escuchado que su ocupación es maestro de la espada… Creo que su nombre es… Mitsurugi, ¿eso creo?”

Un maestro de la espada con una poderosa espada mágica.

Muchos monstruos poderosos viven alrededor de la casa del Clan de Magos Carmesí.

Él debe de ser realmente fuerte para poder pasar por la zona de peligro.

“Ya veo. Este hombre probablemente ande por aquí por algún tiempo, ¿verdad? En ese caso, creo que le permitiré que se una a mi equipo después de que aprenda magia.”

Después de escucharme, Dodonko volteo a decirme.

“Él dijo que se iría el día de hoy. Si él se pudiera quedar más días, definitivamente le hubiera pedido que me llevara con él.”

Eso era lamentable.

Como un candidato a héroe, él debe de ser una excelente persona, alguien que pueda superar cualquier obstáculo y hacer historia.

Yo intentare convertirme en la maga de un grupo de aventureros. En tal caso, el hacer equipo con un candidato a héroe es la mejor opción.

Un equipo con un candidato a héroe, uno que pueda enfrentar cualquier obstáculo directamente, lleno de justicia y admirado por todos.

Finalmente, yo usaría mi magia para barrer al rey demonio- o lo que sea- y dejar mi marca en la historia.

Y después de derrotar al rey demonio, yo me convertiría en la nueva reina de los demonios.

Reina Megumin-¡!

“Megumin ¿me estas escuchando?”

“Estaba pensando en algo muy importante, así que no te escuche. ¿Qué es lo que pasa?”

Justo cuando estaba disfrutando mi fantasía, Yunyun me trajo de nuevo a la realidad.

Funifura y Dodonko ya estaban hablando de algo diferente ahora.

Yunyun las miraba con preocupación de vez en vez y se dirigió a mí con un tono apenado.

“Oye, Megumin, ¿Podrías hacerme compañía por un rato, después de la escuela? Tengo algo que decirte…”

## Parte 2

Otra cosa aparte de que Yunyun fallo en obtener el tercer lugar en los resultados del examen, no pasó nada extraño hoy. Después de la salida de la escuela, Yunyun y yo íbamos de camino a casa.

Yunyun dijo que tenía algo que decirme, aun así, ha permanecido en silencio todo este rato. Entonces finalmente dijo.

“… Oye, Megumin, ¿Qué tipo de relación es la de ‘amigos’ exactamente…?”

Tras escuchar este tema inesperadamente serio, dejé de caminar y puse mi mano en mi rostro. Trataba de contener mis lágrimas.

“Oye, espera, espera, Megumin. ¿Qué es lo que pasa? ¡¿Es que dije algo que te hiciera llorar?! ¡Por favor, dime!”

“No. Yo sabía que Yunyun era extremadamente solitaria, pero no pensé que a hasta el punto de que ni siquiera sabría que son amigos…”

“¡Yo lo sé! ¡De alguna manera creo que lo sé! ¡Salir de compras juntas, jugar juntas, etc.! – ¡Eso no es de lo que yo hablaba!”

Yunyun me contesto enojada, pero se calmó rápidamente.

“Oye, Megumin. Tu siempre me engañas o haces trampas, pero nunca vas tras mi dinero, ¿verdad? -Tu únicamente pones alguna expresión de que esperas de que te invite o andas a mi alrededor durante la hora del almuerzo para que lo comparta contigo.”

“Por supuesto. Sobre eso ¡yo tengo principios! Si yo realmente fuera por tu dinero, yo no me quejaría incluso si tu quisieras mi cuerpo como pago.”

“No lo quiero. ¡¿Por qué clase de persona me tomas?!

De cualquier forma, yo también creo que la ‘amistad’ es algo que no debería ser controlado por el dinero, pero… yo recientemente me encontré con cierto problema… Funifura dijo que su hermano pequeño estaba realmente enfermo…”

No sé cuál sea la situación de la familia de Funifura, pero he escuchado que ella tiene un hermano mucho menor que ella y que lo consiente mucho.

“Y, por tanto, ella quería comprarle medicina, pero ella no tenía el suficiente dinero. Si yo le diera dinero esta vez ¿sería maleducado de mi parte…? Yo siento que lo amigos deben ayudarse mutuamente cuando tienen problemas, pero me odiarían si solo les doy el dinero…”

“… ¿Funifura te pidió dinero?

Después de escuchar eso, Yunyun agito su mano rápidamente.

“Ah, no, no. Yo solo la escuche quejándose acerca de que no tenía suficiente dinero para comprar la medicina. Dodonko dijo que le gustaría ayudarla. Así que yo pensé que podría darle algo de dinero…”

Ahh, esta niña es tan crédula como siempre.

Creo puedo comprender de alguna forma los eventos recientes hasta ahora.

Esas dos personas, de dudosa reputación, repentinamente se volvieron cercanas a Yunyun. Yo siempre creí que era sospechoso.

Dodonko intencionalmente dijo que quería ayudar a Funifura enfrente de Yunyun.

Eso era para crear el ambiente que sugiera, ‘Esto es lo que los amigos hacen’.

Seguramente antes de preguntarme, Yunyun ya había sentido que algo estaba mal con eso.

Pero el sentimiento de ser odiada por sus ‘amigas’, la solitaria Yunyun estaba por rendirse.

“Si yo estuviera en tu posición, yo le ofrecería ayuda no-monetaria. Después de todo yo no tengo dinero.”

“… ¿Ayuda no-monetaria?”

“Si… por ejemplo, ponerme una máscara y robar la farmacia junto con mis amigas.”

“Oye, ¡darles dinero es probablemente más apropiado que eso!”

Yo moví mi dedo frente a Yunyun ligeramente.

“Los amigos no simplemente se ofrecen así mismos. Una parte de la amistad es experimentar dificultades juntos, pero no es tan simple como compartir el dolor.”

“En otras palabras, si Megumin tiene hambre, no debería darle de comer, ¿sino que sufrir de hambre con ella?”

“…… No, no, no. Eso es un asunto totalmente diferente… Pero creo que, si Yunyun puede aceptarlo, darles dinero estaría fuera de cuestión. La amistad no debería de comprarse con dinero, pero los amigos aun así deberían ayudarse sin el dinero. Por cierto, yo soy súper pobre.”

Yunyun sonrió y dijo.

“Tu acabas de sugerir algo sutilmente, ¿verdad? -Bien, entiendo, haré lo crea correcto.”

…… Esta chiquilla es demasiado honesta, es fácil adivinar qué es lo que ella hará.

Incluso aunque ella siente que es sospechoso, ella aun así ha decidido que no puede ignorarlo.

Ella probablemente les dará el dinero mañana temprano o después de la escuela.

Técnicamente esto no tiene nada que ver conmigo, pero mañana-

… Nuestra conversación termino y caminamos en silencio. Esto hasta que nos encontramos con mi vecino NEET Buzucoily.

“Ah, Sr. Buzucoily, ¡qué bueno verlo!”

“Ugh, Buzucoily, ¿Qué haces aquí? Escuche que estabas en casa sin salir desde que te rechazo Soketto.”

“¡Megumin! ¡Shhh!”

“No, tu ¡Shhh! ¡Es inclusive más doloroso! Además, yo no me declare, así que no puede considerarse como que fui rechazado.”

Después de decir aquellas patéticas escusas, Buzucoily repentinamente se puso serio.

“Me di cuenta ahora de que, el mundo está esperando que mi poder despierte. Así que ahora no es momento para estar caliente… últimamente, ha habido testigos que confirman que los monstruos han estado apareciendo en las cercanías de la villa. Estos parecen ser los lacayos del dios demonio. Yo creo que ahora la villa necesita de mi poder, así que tomé la iniciativa y salí a patrullar.”

Para decirlo simplemente, este NEET esperaba olvidar su amor roto, así que el simplemente salió a dar un paseo ya que no tenía nada más que hacer.

“Escuche acerca de eso. Los NEET sin que hacer de la villa, han sido organizados para formar una guardia o algo.”

“No la llames guardia. Nosotros tenemos un nombre genial. Es la ‘Unidad de Guerrilla Contra la Armada del Rey Demonio’.”

La armada del rey demonio nos teme demasiado como para acercarse a esta villa. ¿Cómo es que ellos esperan librar una guerra de guerrillas?

Pero darle un nombre genial y glorioso a una guardia es típico del Clan de Magos Carmesí.

“Escuche que los adultos en la villa han tenido problemas con estos monstruos. El profesor también dijo que ellos volverían a poner a la fuerza el sello mañana. Eso es demasiado problemático. ¿Porque simplemente no liberan el sello del dios malvado y lo destruyen con el poder combinado de la villa?”

El Hogar del Clan de Magos Carmesí es el lugar donde Archí-Magos de primer nivel se congregan.

Inclusive los países cercanos no se atreven a interferir con este lugar.

Si el Clan de Magos Carmesí trabajara unido, debería ser fácil derrotar al dios malvado.

“No, parece ser que alguien más lo había sugerido, pero parece ser que el dios malvado fue traído aquí para permanecer sellado.”

“¡¿Ahaaa?! ¡Nunca había escuchado de eso antes! ¡¿Por qué es que hicieron eso?! ¿Porque nuestros ancestros hicieron algo tan problemático e inútil?”

Yunyun grito, mientras Buzucoily precia inseguro.

“Por supuesto, fue porque, decir que el dios malvado se encuentra sellado aquí es bastante genial… de cualquier manera, todo mundo estuvo de acuerdo en rehacer el sello. Actualmente hay otras cosas peligrosas selladas en este lugar, tales como ‘El Arma Prohibida que Puede Destruir el Mundo’ y ‘La Diosa Sin Nombre de la Venganza que Perdió sus Seguidores’.”

“Aunque escuchar eso hace que me duela el estómago, estoy un poco interesada en esa arma prohibida. Bueno no es como si no entendiera como se sienten los demás aldeanos.”

“¡¿Tu puedes entenderlos?! Por cierto, yo no puedo entender a ninguno de ustedes. ¡¿En verdad soy rara?! ¡¿es que acaso algo está mal con mi percepción?!”

“Así es” x2

“¡¿?!”

***

-Al siguiente día, yo arrastraba mi mochila en donde llevaba a Kuro a la escuela. Era un poco más temprano que de costumbre. Tal como esperaba- Vi tres rostros familiares de camino a la escuela.

“Gracias, ¡Yunyun! ¡esto es realmente una gran ayuda! ¡Te lo pagare!”

“No es nada, ¡no es necesario que me agradezcas! Ah, entonces, quieren que vayamos juntas a la escuela…”

Esas tres eran Funifura, Dodonko y Yunyun.

Funifura sonreía mientras tomaba algo de Yunyun.

“Ah… Lo-, lo siento es solo que debo llevar el dinero inmediatamente.”

“Ah, sí, si claro… lo siento, no me percate de eso… entonces vámonos en la escuela.”

Yunyun sonrío y camino hacia la escuela.

Poco después, la figura de ella encogiendo los hombros mientras caminaba reflejaba tristeza.

Funifura y Dodonko al ver que Yunyun se había ido, murmuraron algo.

“Mí, con- consciencia…”

“Me lastima…”

“Ehehehe… en tal caso no debieron de haber hecho eso.”

“¡¿?!” x2

Tras escuchar mí voz atrás de ellas, ambas brincaron asustadas y rápidamente voltearon.

“¡¿Megumin?! ¡¿Qué haces aquí?!”

“Nu- nuestra conversación con Yunyun… ¡¿Que tanto escuchaste?!”

“Que tanto escuche, he…”

Yo les hablaba mientras salía de los arbustos, en los que me oculte.

“Yo escuche todo, empezando por como ustedes chantajeaban a Yunyun con ‘si no quieres que se sepa tu vergonzosa historia, se obediente y has cosas pervertidas con nosotras’.”

![](capitulo_4/img/c2.jpg)

![](capitulo_4/img/c3.jpg)

“¡No hubo tal chantaje! ¡Nosotras no lo hicimos!”

“¡Porque mencionas cosas pervertidas! Tú, ¡Por quien nos tomas!”

Mi pequeña broma provoco una serie de respuestas embarazosas.

“Espera, uh… yo solo tome dinero prestado de Yunyun. De hecho, mi hermano pequeño, se encuen…”

“Es verdad, es verdad. El hermanito de Funifura está enfermo, así que ella necesitaba dinero para la medicina. Nosotras no teníamos suficiente. Así que dejamos que Yunyun nos ayudara un poco.”

“Ah, eso es terrible… tanta frialdad conmigo. ¿Porque es que no se acercaron a mí?”

“¡¿Eh?!” x2

Sorprendidas por mis palabras, ambas gritaron.

“¿Qué? ¿Es tan sorprendente que yo quiera ayudar a las personas en necesidad? O es que ¿ustedes dos están buscando pelea?”

“No, ¡no…! No queríamos decir eso, pero, uh… ¿No eres pobre?”

“Es verdad, es verdad. Incluso si estamos cortas de dinero, ¡no podríamos tomar prestado de Megumin!”

“Mueran.”

Arroje mi mochila y me aliste para atacar. La cara de ambas, se puso blanca del miedo.

“Entonces, ¿Cómo es que esperas ayudarnos?”

“Es verdad, es verdad. Incluso si te lo pedimos, ¿en verdad nos prestarías dinero?”

“¡¿Cómo sería posible que yo les prestara dinero?! ¿Pero qué tonterías estás diciendo? Si lo que ustedes quieren es pedir dinero, entonces necesitan encontrar a la persona correcta.”

“¡Tú, tu maldita…!!” x2

Me voltearon a ver bastante enojadas, casi como sí la vena del rostro se les fuera a reventar. Pero no es como si estuviera bromeando.

“Ambas cálmense y escuchen. Tu querías dinero con el fin de comprar medicina. Pero para adquirir medicina no necesariamente necesitas dinero. Así que básicamente está bien, siempre y cuando tu obtengas las medicinas, ¿verdad?”

“¡Eh…! Uh, eso es verdad…”

“Entonces ¿Es que tú tienes alguna otra forma de obtener la medicina?”

Yo me reí con confianza.

“Déjenmelo a mí, la genio número uno del Clan de Magos Carmesí.”

Tras ver mi confianza, ellas se voltearon a ver una a la otra con inquietud.

## Parte 3

Bueno, yo se los prometí, pero ¿Cómo es exactamente que yo voy a obtener la medicina?

Quizás pueda llevar a Komekko a la farmacia y que convenza al encargado con su coquetees.

Usando su naturaleza de Femme Fatale hermana pequeña, es posible obtener algo de la nada.

“¡Kuro! Kuro, ¡espabila! ¡¿Pero qué te paso?! ¡¿Porque es que cojeas?! ¡¿Es que acaso Megumin, venia agitando su mochila por todos lados, olvidándose de que tu venias adentro?!”

Ignore a Yunyun quien abrazaba a Kuro y armaba una escena.

Y me concentre en planear como obtener la medicina.

Finalmente, el flojo profesor encargado, entro al salón. Paso asistencia como era usual…

“Ah, el ritual para volver a poner el sello del dios malvado comenzara esta tarde. Ya lo había mencionado ¿verdad? Déjenme aclárales algo, fallar es imposible. Como medida precautoria, yo inclusive he preparado ‘esa cosa’ que he mantenido oculta por tanto tiempo…

De cualquier forma, yo preferiría no usar ‘esa cosa’ de ser posible. Ya que la posibilidad de éxito del ritual es de más del 90%, lo más probable es que no haya oportunidad de usarlo. Sería mejor resolver la situación tranquilamente…”

El profesor encargado acaba de dar un discurso que inevitablemente activaría el evento que garantiza que fallaran.

Su inquietud sugería que el profesor probablemente desee que el ritual falle.

Entonces ‘esa cosa’, que nadie se molestó en preguntar que era, podría ser usado.

“De cualquier forma, así son las cosas. vayan derecho a sus casas después de la escuela el día de hoy. Todas deben de ir a sus casas antes de la tarde.

Bueno pasemos a lo que sigue. La siguiente clase será ‘Creación de Artículos Mágicos’ ¡Todas al laboratorio! ¡Ahora!”

Después de su anuncio el profesor salió rápidamente del salón. En ese momento-

Repentinamente tuve una epifanía. ¡Creación de Artículos Mágicos…!

-El laboratorio fue construido, bajo la escuela. Con el propósito de almacenar agentes peligrosos y varios artículos explosivos… o tal vez no.”

Yo escuche que la razón fue ‘Los Magos que Realizan Dudosos Experimentos Bajo Tierra son Geniales’.

El orden de lugares en el laboratorio es aleatorio, pero yo siempre me siento en la fila de enfrente.

El profesor encargado, se dirigió sin ganas al podio.

“Empezaremos la lección sobre Creación de Artículos Mágicos. Crear artículos mágicos y pociones mágicas han sido siempre una importante fuente de ingreso para los magos. Dominar esta técnica es en definitiva ventajosa… Megumin, es bueno que estés entusiasmada, pero no tienes porque acercarte tanto.”

“Lo siento, esta es mi clase favorita.”

El profesor me hizo gestos con la mano para que regresara a mi lugar y levanto un bote para pócima.

“Ya hemos hecho esto varias veces, pero reforzar las bases es importante. El día de hoy, crearemos una pócima simple de salud… sí, Megumin, ¿Por qué es que levantas tú mano? ¿Tienes alguna pregunta?”

“Olvídese de esas pócimas baratas. Enséñeme algo más difícil con lo que pueda ganar mucho dinero.”

“Bien, tú serás mi asistente por esta lección. Te pondré a trabajar hasta que dejes de decir esas tonterías.”

¡Qué irrazonable!

-Mientras trabajaba de asistente de mala gana, el profesor empezó la clase.

“Todas, usen sus ingredientes preferidos para crear la poción mágica. Si esta les resulta, pueden tratar cambiar la formula. Diferentes proporciones de ingredientes pueden afectar la potencia de la poción. Experimenten y creen su propia y única fórmula.”

Como asistente, estaba encargada de entregar el equipo y los ingredientes a mis compañeras. Entonces, recordé mí plan inicial.

“Profesor, tengo una pregunta. ¿Se puede crear una poción para curar enfermedades?”

“¿Una poción mágica para curar enfermedades? Es posible, pero ese tipo de pociones son bastante difíciles de hacer. Además, el costo por crear dicha poción es bastante elevado, aunque se vende bien. Básicamente, sirve para hacer buen dinero.”

“Finalmente entiendo cómo me ve el profesor… esto no es por hacer dinero. Es porque hay alguien enfermo, que yo personalmente, quiero hacer algo de medicina para ayudar.”

Tras escuchar eso, el profesor acaricio su barbilla y dijo.

“… en tal caso, puedes utilizar cualquier ingrediente que quieras. Esta es la fórmula para esa poción. Tómala… De cualquier forma, es sorprendente que Megumin quien es codiciosa, egoísta y lo suficientemente despiadada como para matar monstruos sin miramientos tendría un corazón gentil.”

“Ahora en verdad puedo comprender completamente como me ve el profesor.”

Yo en silencio juraba para mí misma que este profesor me las ‘pagaría’ después de la graduación y examine la formula. Reuní los ingredientes para la poción…

Hígado de Draco de Fuego, raíz de Mandrágora, Farfetch… (Nova: ¿Un pokemon?)

“Oye, Megumin, ¿Que estás haciendo con esos ingredientes? ¿Qué paso con la poción de salud? En estos momentos Kuro está bastante débil. De ser posible, prepara un poco para esta pobre cosita…”

Yunyun me hablaba, mientras que ella me veía reunir los ingredientes. Parecía preocupada.

“Este es un secreto entre yo y una amiga, así que no hay forma de que le diga a mi rival Yunyun.”

“¡Ah! Qué, ¡De que se trata esto! ¡¿Te estas vengando por lo que paso recientemente?!”

Mientras ignoraba a Yunyun, yo iba poniendo los costosos ingredientes en el mortero.

“Suficiente. Entonces yo misma salvare a Kuro…”

Voltee a ver por un momento a Yunyun. Ella puso a Kuro, que se encontraba inconsciente, sobre la mesa y muy animada empezó a hacer una poción de salud.

“El sistema educativo de mi familia es del tipo Espartano. Así que por favor no consientas demasiado a mi gato.”

“Ponerlo en una mochila y agitarlo por todos lados no es Espartano, ¡es tortura! ¡Tú deberías cuidarlo más!”

Yunyun dijo eso un poco enojada mientras acariciaba gentilmente a Kuro.

Pesé a que Yunyun parecía tan preocupada, yo no creo que este gato pueda morir tan fácilmente.

Después de todo, puede subirse a los hombros de uno y frecuentemente muestra una actitud arrogante. Solo es un gatito, pero aun así es selectivo con su comida. No se siente como un gato. ¿Habrá sido porque se adaptó a las duras condiciones de vivir con mi familia?

“De cualquier manera, ¡más acción, menos charla! ¡Es momento de trabajar! ¡Ahora les mostrare mi técnica especial de creación de pociones! ¡Véanme aniquilar a ese bastardo, conocido como ‘enfermedad’! ¡Hahahahaha…!”

“Meg-, Megumin. No sé qué es lo que estás haciendo, pero no estás haciendo veneno ¿Verdad? ¡Creo que escuche algunas palabras peligrosas como ‘aniquilar’!”

Yunyun permaneció desconfiada al lado mío, en lo que yo realizaba esta difícil poción-

Primeramente, muela el hígado seco del Draco de Fuego hasta que quede como polvo.

Después, use la raíz de Mandrágora que posee una gran fuerza vital para…

“¡Ugh! ¡El mortero de Megumin está sacando chispas! Oye, ¡¿Qué estás haciendo?!”

“¡Ah! ¡Además se está quemando esta parte! Ahahah, ¡Fuego! ¡Fuego! Profesor, ¡profesor!”

“¡Crear Agua!”

Mientras ignoraba los gritos de angustia de la gente a mi alrededor yo tome el cuchillo para cortar la raíz de mandrágora…

“Oigan, ¡Hay algunas raíces de mandrágora corriendo por ahí! ¡¿Quién uso las raíces de mandrágora?!”

“¡¿…?! Uh, la planta que Megumin está cortando es raíz de mandrágora… ¿verdad? …”

Seguía ignorado el ruido que hacían todos a mi alrededor y empecé a cortar la raíz de mandrágora que estaba forcejeando conmigo. Puse los pedazos en el recipiente… pero aún no es suficiente.

Debo capturar esas raíces de mandrágora que se escaparon…

“Aquí, ¡capture una! No sé qué es lo que estás haciendo, pero luce muy interesante.”

Arue mantenía a la raíz de mandrágora fuertemente agarrada por las hojas, presionando el parche de su ojo con su otra mano. Ella sonrió maliciosamente y me paso la raíz de mandrágora.

“Estoy haciendo una muy difícil poción para curar enfermedades. Arue, ¿Tu ya acabaste tu poción? Si estas libre, ven y ayúdame.”

“No hay problema. Okay, lo primero será cortar este chico en pedacitos…”

“Escucha, Arue. Tú debes de presionarlo así, ¡o si no este chico se retorcerá muy fuerte! Es un monstruo tipo planta después de todo, ¡así que no hay necesidad de tener piedad! ¡Agh! Que molesto, ¡deja de retorcerte! ¡Se obediente y déjame cortarte!”

Después de que Arue se me unió, nosotras continuamos con la difícil tarea. Yunyun nos volteaba a ver con una expresión pálida.

“Wah… Wahhh…”

Bajo la mirada triste de Yunyun, nosotras eventualmente terminamos con las raíces de mandrágora. Para que, finalmente pasamos a el último ingrediente.

Una linda criatura parecida a un pato que carga un cebollín constantemente como su fuente de alimento – El Farfetch.

¡Necesito al Farfetch-!

“¡No te dejare! ¡No puedo dejar que continúes con esto!”

Yunyun grito repentinamente y lo arrebato de mi mano para detenerme.

“¿Qué es lo que haces? No interrumpas mi trabajo.”

Yunyun movía su cabeza vigorosamente y me dijo con lágrimas.

“Pero, pero- este, ¡este lindo Farfetch será…!”

![](capitulo_4/img/c4.jpg)

Las compañeras que me rodeaban me veían con una expresión de pena.

Incluso el desconcertado Farfetch giro su cabeza para voltearme a ver con sus grandes ojos redondos.

Si, en verdad es muy lindo.

Pero pese a que es muy lindo…

“Megumin, este puede parecerte lindo, pero aún sigue siendo un monstruo. Existen muchos monstruos en este mundo que parecen inofensivos, pero en verdad son bastante espantosos. Deberías de conocer a la ‘chica de la tranquilidad’ ¿verdad? Es un monstruo que vive cerca de la villa, este monstruo puede activar el instinto de protección de un humano, para hacer que uno no la quiera abandonar y con eso que finalmente muera de hambre. Sin importar que lindo pueda ser un monstruo, uno no debe de tener piedad.”

“¡Eso puede ser verdad! Pero, ¡pero!”

Arue puso su mano sobre el hombro de Yunyun quien aún trataba de discutir y le dijo.

“Tranquila. Megumin, ¿Qué parte del Farfetch es el que necesitas como ingrediente? Si son las entrañas, entonces tendremos que matarlo. Si no…”

Tras escuchar esto, Yunyun tímidamente volteo a verme.

“Lo que necesito del Farfetch es su cebollín… puedes estar tranquila ahora, ¡¿verdad?!”

Sonreí para hacer que se tranquilizara.

Yunyun se tranquilizó y exhalo, para después devolvérmelo.

“Después de todo, los cebollines tienen propiedades curativas. Hay muchos usos para estos cebollines. Tu puedes comerlos, enrollarlos o hasta introducírtelos.

Además, los cebollines del Farfetch son cebollines peleadores, ¿verdad?”

Arue le hablaba mientras que le quitaba algunos cebollines al Farfetch.

Yo veía a Arue cortar los cebollines mientras conversaba con Yunyun.

“En verdad… ¿Qué clase de persona creen que soy? A mí también me causan ternura las criaturas lindas. De ser posible, yo tampoco quería matarlo.”

“Tú-, tú tienes razón. Lo siento, yo te juzgue mal, ¡Megumin! Qué bueno… todo por que escuche que al matar a un Farfetch te da muchos puntos de experiencia y que además puede prepararse en muchos platillos deliciosos…”

“……”

¿Un monstruo raro que te da muchos puntos de experiencia?

¿Comida deliciosa?

“Lo siento mucho. Después de todo, como puede usarse para hacer medicina, incrementar tus puntos de experiencia, además de servir de comida. Yo creí que Megumin diría ‘En verdad matar tres pájaros de un tiro’ y entonces…”

“¡Mano de la Explosión!”

Sofoque a el Farfetch y el gimió miserablemente mientras que yo lo estrangulaba sin piedad del cuello.

Examine mi tarjeta de aventurero y note que había ganado dos niveles en un instante, así como dos puntos de habilidad.

Yo le mostré rápidamente mi tarjeta a la perpleja Yunyun, y le dije muy animadamente.

“¡Megumin subió de nivel!”

“Megumin, ¡Tú gran idiota!

¡Ahhhhhh!”

## Parte 4

“¿Qué pasó? Asesina de Farfetch, ¿Tienes algún problema conmigo?”

“Megumin, tú deberías de disculparte con Yunyun. El incidente de esta mañana la dejo profundamente molesta. Incluso ahora ella sigue llorando por él.”

Yo les dije a Funifura y a Dodonko que me vieran detrás de la escuela.

Ellas me dijeron eso en cuanto llegué.

“Llámame asesina de Farfetch de nuevo y voy a molerte.

Y acerca del alboroto de esta mañana que hizo que el modo de toda la clase fuera tan pesado, incluyendo el de Yunyun, fue fundamentalmente por ustedes. ¿Acaso saben cuánto he hecho por ustedes?”

Tras escucharme ambas voltearon a verse…

“No me digas que…”

“La pócima que estas sosteniendo es…”

“Así es, yo hice esta pócima para curar enfermedades.”

Ambas mostraron una expresión de disgusto.

“Sé que tienes que ser precavida, pero esta poción fue hecha siguiendo la fórmula del profesor, así que no debería de haber ningún problema. Yo use algunos ingredientes de más, pero solo servirían para hacer la pócima más potente. Vamos, vamos, no tienen por qué ser reservadas.”

“Uh…”

Funifura tomo la poción de mala gana. Ella parecía bastante preocupada.

“Ahora no tienen por qué tomar el dinero de Yunyun. Así que pueden devolverlo.”

“¡Eh! Espera, espera. ¡Aún no sabemos si esta pócima es realmente efectiva…!”

Sin esperar a que terminara de hablar, yo la interrumpí.

“Eso no tiene nada que ver conmigo. Por cierto, el que el hermano de Funifura este realmente enfermo o no, tampoco tiene nada que ver conmigo.”

Les dije eso, de manera clara y concisa, causando que se quedaran en silencio.

“Uh… no, eso es…”

“No, no… Él, ¡él está enfermo! ¡El hermanito de Funifura está realmente enfermo!”

Dijo Dodonko fuertemente, defendiendo a Funifura que estaba sin palabras.

Pero eso era irrelevante para mí.

“Sobre lo que les estoy hablando es, sobre manipular a una pobre niña solitaria y sin amigos, para sacarle dinero. La inteligencia de esa chica es segunda solo tras la mía. Ella no es ninguna idiota. Inclusive yo creo que evento es sospechoso, así que definitivamente ella también lo cree.”

Decía mientras me movía a la fuerza hacia adelante. Ellas se pusieron pálidas y contestaron rápidamente.

“¡Lo sé, lo sé! ¡Yo le regresare el dinero! Oye, tú-, ¡tus ojos se están poniendo rojo brillante!”

“No lo tomes tan enserio. ¡Eso-, eso realmente asusta!”

Ellas suplicaban por piedad, mientras que sacaban el dinero de Yunyun.

Ugh, que pena. Parece ser que yo en verdad me enoje mucho.

Cuando los miembros del Clan de Magos Carmesí entran en estado de excitación, sus ojos carmesíes comienzan a brillar.

Tras este incidente, mi imagen como ‘Reina de Hielo’ esta arruinada.

“… Con esto queda. Yo le regresare el dinero a Yunyun. Si ustedes quieren acercársela porque de verdad quieren ser sus amigas, entonces supongo que está bien. Pero si ustedes simplemente quieren explotar su amabilidad y credulidad- Les advierto que mejor no lo hagan. O después de que yo aprenda magia, ustedes serán mi primer objetivo.”

“Está bien, ¡lo entiendo! Pero, aunque hallas dicho eso, ¡tus ojos aún están brillando! ¡Pues que tanto quieres a Yunyun!”

“No molestaremos más a ninguna en el futuro. Ni tampoco interferiremos con ustedes. ¡Las dos podrán estar tan cariñosas y acarameladas como quieran…!”

Funifura y Dodonko dijeron eso ansiosamente.

“… Debieron de haber malentendido algo. Mi relación con Yunyun no están buena… de hecho no somos realmente amigas.”

“Está bien, bien. No necesitas explicarnos más.”

“Entonces de nuevo, para que tu hagas tanto por ella. Si eso no es una amiga, ¿entonces qué es?”

Las dos empezaron a abanicarse el rostro. Como diciendo “¡Qué calor! ¡Qué calor!

“Si quieren saber cuál es nuestra relación, esta sería… puramente, simplemente, rivales…”

“Bien, bien. No expliques más. Te entendemos. Solo que, para alguien más, se vería completamente ‘Yuri‘.” (Nova: Yuri, es un término japonés utilizado para clasificar relaciones románticas entre dos individuos de sexo femenino, ya sea en anime, manga o cualquier otro medio de comunicación)

“Los ojos de Megumin están completamente carmesí. Esta es en verdad una de las debilidades del Clan de Magos Carmesí- no podemos mentir en esa condición.”

……

“Bien, por ahora nos retiramos. Pero no deberías confiarte solo porque ahora eres la Numero 1.”

“Cierto, cierto. Mientras que ustedes estén de cariñosas, nosotras las sobrepasaremos. Y si yo me convierto en la Numero 1, ¿quizás tu querida esposa se convierta en mi rival en tu lugar? Así que, si quieren estar de cariñosas, será mejor que lo hagan ahora…”

Ni siquiera me moleste en escuchar sus ladridos de perdedoras hasta el final. Simplemente las golpee.

“¡Espera! ¡Ahah! No lances golpes hacia la poción que fue tan difícil de hacer. ¡Tú eres demasiado malvada! ¡Despreciable! No, ¡detente…!”

“¡Esta persona ni siquiera puede leer el ambiente! ¡Eso era solo un discurso para hacer ver nuestra derrota genial…! Ah, no espera, ¡no…!”

***

Tras derrotarlas exitosamente, yo regrese al salón, planeaba llevarme mi mochila y a Kuro.

Para ese momento solo quedaba Yunyun en el salón.

“… ¿Qué es lo que haces aquí sola?”

“¡¿Qué pasa con ese tono sarcástico?! ¡Estoy esperándote! ¡¿A dónde fuiste, abandonando a Kuro aquí?!”

Parecía que quería acompañarme a casa.

“A ningún lugar en especial. Solo tenía algunos asuntos pendientes con Funifura.”

Sin darme cuenta, regresar juntas a casa se volvió algo cotidiano. Recordé lo último que dijo Funifura.

… Bueno, después de todo, los subalternos del dios demonio aún siguen rondando la aldea. Así que debemos poner en pausa nuestra rivalidad.

Sí, no es que seamos amigas, pero en esta época de peligro debemos…

Yunyun aún suena enojada, pero también más tranquila.

Probablemente por haber esperado sola aquí todo este tiempo.

“Es raro que tuvieras asuntos pendientes con Funifura. Pero bueno, será mejor que nos apresuremos a regresar a casa. Después de todo el profesor dijo que el ritual para reforzar el sello del dios malvado empezara esta tarde, debemos apresurarnos…”

“Ten, es para ti.”

Mientras que Yunyun alistaba sus cosas para irnos, yo le di el dinero que recibí de Funifura.

Yunyun sostuvo la bolsita del dinero, ella parecía perpleja.

Mi trabajo esta echo.

Tome mi mochila y trate de poner a Kuro adentro, pero por alguna razón, Kuro se resistía a entrar en la mochila.

El uso sus garras para aferrarse a mi hombro y se resistía vigorosamente.

“Oye, este dinero…”

“Es de parte de Funifura, el asunto con la medicina de su hermano se resolvió, así que te regresa tu dinero. Felicidades.”

Le respondí a Yunyun, mientras que forcejeaba para quitarme las garras de Kuro de mi hombro.

Esto, este animal… ¡parece que no ha olvidado lo de esta mañana…!

Mientras que yo libraba una fiera batalla contra Kuro, Yunyun dijo…

“Oye, Megumin. ¿Acaso hiciste algo por el hermano de Funifura? Como por ejemplo… hacer alguna pócima para curar enfermedades o algo así…”

Ella pregunto tímidamente.

¿Vieron, Funifura y Dodonko? La inteligencia de esta chica es segunda solo tras la mía.

“Yo soy realista, ¿Por qué crees que haría algo que no me beneficiara?”

“Ya que lo dices así, ¡tienes razón, de hecho, es súper convincente!”

……

“¡Oye!, ¡¿Porque te vas sin decir nada?! Yo era la que te estaba esperando todo este tiempo, ¡No me dejes atrás!”

– Tras dejar la escuela, el sol estaba a punto de ponerse.

Ya estaba atardeciendo.

Yunyun rápidamente me dio alcance por atrás.

Ya que Kuro se negó a entrar a la mochila, lo deje en mi hombro y caminamos a casa.

“Oye, Megumin. ¿En verdad no hiciste nada?”

“Tú, sospechas demasiado. Incluso si hice una pócima para el hermano de Funifura, nadie se quejaría, ¿verdad? ¿No es bueno eso?”

“¿Entonces el alboroto de esta mañana fue…?”

Preferí permanecer en silencio y aumenté el paso.

Yunyun rápidamente alcanzo mi paso.

Entonces se puso a mi lado y me dijo…

“…Oye, Megumin. Te gustaría ir para allá, solo de paseo… no-, no es que lo haga para pagarte, simplemente es un impulso… ya que tengo el dinero de regreso, déjame invitarte algo.”

Voltee a verla, ella se veía feliz y sonriente.

…Mi inteligente rival ya ha de haberse echó una idea general de lo que sucedió.

## Parte 5

“Yo dije que invitaba. Ugh, sí, en verdad dije que te invitaba.”

Salíamos del café, con intención de regresar a nuestras casas.

Yunyun examino su bolsa de dinero y suspiro profundamente.

“Gracias por la invitación. Nunca había comido tanto en toda mi vida. Tal parece que no puedo comer nada más el día de hoy.”

“¡Eso es genial! Ahh, en verdad… aunque yo dije que podías ordenar cualquier cosa…”

Escuchaba las quejas de Yunyun, mientras caminábamos lentamente por el camino pintado de rojo por el sol que se ocultaba.

“Huugh… es realmente agonizante tener que caminar después de haber comido tanto. ¿Qué tal si descansamos un poco primero? Déjame digerir un poco lo que comí.”

“¡En verdad… en verdad…! ¿Por qué insististe tanto en comer tanto? ¡Pero que desesperada estabas…!”

Lleve a Yunyun al parque que hay en el hogar del Clan de Magos Carmesí.

Lo llamábamos parque, pero solo había un pozo, algunos bancos y techos para la lluvia.

Bajé a Kuro de mi hombro y me acosté en la banca.

“¡Meg-, Megumin! ¡Tú falda se subió! – ¡Tus pantaletas se pueden ver! Ah, ¡en verdad…!”

Yunyun arreglo mi falda diligentemente.

“Yunyun se convertirá en una buena esposa algún día. Después de que nos graduemos, ¿No quieres cuidar de mí como tu concubina? Incluso si solo me das arroz simple todos los días, yo aun así seguiré diciendo, ‘¡es delicioso!’”

“¡N-, No! ¿Por qué haría eso? ¿Tú crees que yo sería feliz simplemente porque tu dirías que mi comida es deliciosa? … diciendo simplemente todos los días, que es deliciosa… todos los días… Ugh, ugh…”

Tal como la súper perdedora que es, Yunyun empezó a pensar seriamente en ese dilema.

Ella siempre ha sido así.

Tal vez por eso Funifura dijo que era Yuri.

“Hablando de graduaciones. ¿Qué harás Yunyun, cuando yo me gradué? Para graduarme solo necesito un punto de habilidad más.”

“Eh, ¿Por qué? Megumin, ¿no dijiste el otro día que aún te faltaban cuatro puntos más para aprender magia? Aún después de la poción de aumento de habilidad de ayer, a ti aún deberían de faltarte tres puntos, justo como a mí… ¡¡Ahaha!!”

Yunyun entendió mientras hablaba.

“¡El Farfetch de esta mañana! Después de matar al Farfetch, ¡tú nivel…!”

“Sí, matar al Farfetch incremento mi nivel por dos. Y con la pócima de aumento de habilidad, he ganado tres puntos de habilidad más. Ahora solo me falta un punto más para aprender magia. Así que es probable que me gradué después del siguiente examen.”

Kuro se subió a mi estómago mientras que estaba recostada en la banca.

Este animal es demasiado descarado.

-Yunyun hablo triste y melancólica.

“Por- porque es así… no podremos graduarnos juntas… yo intencionalmente baje un poco para ir junto con Megumin…”

Yunyun dijo tristemente-

-Yo brinqué de la banca.

Ignore a Kuro que rodo por mi estómago y fue a dar al piso, para interrogar a Yunyun.

“¿Qué es lo que dijiste? ¿Dices que ajustaste tus puntos de habilidad solo para que nos graduáramos juntas? Así que el resultado de la prueba anterior no fue un accidente. Tú intencionalmente no quedaste dentro de las primeras tres, para evitar recibir la pócima de aumentó de habilidad, ¿verdad?”

“¡¿?!”

Yunyun tembló un poco, “¡Oh no!” – Es lo que parecía decir su expresión.

No hay necesidad de que confiese, esa reacción es más que suficiente.

“¡Pero qué tontería has hecho! Si lo que tu querías hacer era graduarte conmigo, lo único que tenías que hacer era simplemente, ¡no aprender magia después de acumular suficientes puntos! ¿No sabias de ese truco? Tampoco me dijiste que querías graduarte junto conmigo. Solo hiciste todo esto secretamente por tu parte- ¡Pero qué torpe eres!”

“¡Pero, pero es que! Megumin, quien siempre estuvo por delante, ¡repentinamente estaba atrás de mí! ¡Yo siempre pensé que Megumin se graduaría antes que yo…!”

“¡Ah! ¡¿Acabas de decir que me superaste?! ¡Sueña con eso! ¡Tú nunca me has superado! Seré honesta contigo. ¡Yo nunca he tenido la intención de aprender magia avanzada como el resto de ustedes! ¡Lo que yo quiero aprender es magia muchísimo más poderosa que sobrepase a la magia avanzada! ¡Mira mi tarjeta de aventurero! ¡Tengo más que suficientes puntos para aprender magia avanzada desde hace mucho!”

Yo estaba sobre-excitada, así que le mostré mi tarjeta a Yunyun frente a su cara. Yunyun se le quedo viendo como si quisiera tragársela completa.

“De ver- ¡en serio es así…! Entonces… Megumin es mejor que yo, ¡como esperaba…!”

“Uh… hm, correcto. Soy realmente buena. Así que no creas que puedes superarme fácilmente.”

Yunyun sonrió felizmente, yo no sabía cómo reaccionar ante eso.

Como esperaba, ella deseaba que su rival fuera la mejor.

“Entonces… siento que intencionalmente cortara mis puntos. Por cierto, una magia más poderosa que la magia avanzada… ¿es que tú quieres aprender ‘Magia de Impacto’? o tal vez es ‘Magia de Detonación’…

“Magia de Explosión”

…… Tras escuchar eso, Yunyun repentinamente quedo en silencio.

“Uh, tu acabas de decir, ¿Qué? ¿Creo que escuche ‘Magia de Explosión’…?”

“Correcto, es Magia de Explosión. La que es conocida como la magia más poderosa de todas.”

Después Yunyun permaneció otro momento en silencio-

“¡Pero de que estás hablando! Magia de Explosión, ¿En verdad esa Magia de Explosión? ¿La Magia de Explosión que es considerada como inútil? La que requiere la mayor cantidad de puntos de experiencia de todos los trabajos. Y que incluso si es aprendida, la mayoría de la gente no puede activarla por insuficiencia de mana. E incluso si es activada, el mago sería incapaz de moverse por agotamiento de mana………”

“Sí, esa Magia de Explosión”

Yo asentí vigorosamente.

Yunyun tomo un gran respiro y-

“¿Es que eres estúpida? ¡Pero qué estás diciendo, Megumin! ¡¿Qué vas a hacer con esa magia?! Una magia que básicamente no puede ser usada por casi nadie. Que incluso si es activada a la fuerza, solo es posible hacerlo una vez por día. ¡Una magia con los peores requerimientos y sin uso práctico! ¿Pero qué estás pensando?

Tal parece que la diferencia entre un genio y un idiota es solo un paso. Megumin, si tu persistes en esto, es que eres una idiota, ¿estás bien con eso?”

“Incluso si es Yunyun, yo no aceptare ser llamada idiota. ¡O es que acaso que quieres sufrir!… De cualquier forma, entiendo lo que quieres decir. Yo ya había investigado acerca de la Magia de Explosión a fondo. Y me atrevo a decir que yo soy la que mejor entiende la magia de Explosión en toda la aldea.”

“Si lo entiendes tan bien, ¿Por qué es que aun así quieres aprenderla? ¡Si se trata de Megumin, estoy segura de que podrás aprender una gran cantidad de magia avanzada, acumular una gran cantidad de experiencia y finalmente convertirte en una Archí-maga que dejara su huella en la historia…! Oye, al menos dime ¿Cuál es la razón?”

No podía comprender porque es que Yunyun se había agitado tanto. Después de todo esta es una decisión personal.

“Obviamente, eso es porque yo amo la Magia de Explosión.”

Yo le di mi más sincera respuesta.

Parece ser que Yunyun creyó que yo tenía razones más complicadas. Tras escuchar mi respuesta ella abrió sus ojos y dijo…

“…Como esperaba, Megumin no es una genio, es una idiota.”

“¡Te acababa de advertir que no me llamaras idiota!”

Después de eso, yo le lance un golpe a Yunyun.

***

“¡Ha… ha…! ¡Yo gane…! ¡Mi primera victoria contra Megumin…!”

Yunyun estaba encantada, su rostro se veía completamente colorado.

¡Maldición…! Mi primera derrota contra Yunyun.

“Bueno, no es como si hubiera usado todo mi poder. Ya que no puedo usarlo hasta la luna llena.”

“¡Eso no es posible porque tú no eres un demonio! ¡Así que solo admite tu derrota!”

Yo me pelee contra Yunyun en el parque por un rato.

Aunque eventualmente Yunyun me presiono hasta el final.

Mi cuerpo que desprendía calor y sudor, fue presionado contra el frío suelo. Se sentía confortable.

El sol estaba a punto de ponerse, hacía que los alrededores se obscurecían. En el momento que ambas nos encontrábamos jadeando en el parque.

En un momento de impulso, yo me comprometí con ella a pelear a puño limpio, en lo cual yo tenía la desventaja.

“Haa… está bien admito que perdí. Ya quítate de encima. Acepto la derrota. Yo perdí, ¿está bien?”

Así que Yunyun me libero obedientemente.

“…Hu. Yo acepte la derrota. Haa. Después de comer tanto era natural que perdiera. Pensándolo bien, yo ni siquiera use la mitad de mi verdadera fuerza.”

“¡Ahah! ¡Es muy bajo decir escusas después de que te soltara!”

Le dije a Yunyun mientras que me sacudía la tierra de las rodillas…

“Bien por ti, que ganaras antes de que parta de aquí a mi viaje. Un día Yunyun será la siguiente Jefa del Clan de Magos Carmesí. Sería bueno que mientras que yo viajo por el mundo para convertirme en una Archí-Maga, tú te conviertas en la siguiente jefa y pases tus días tranquilamente aquí en el Hogar del Clan de Magos Carmesí.”

“¿No puedes al menos felicitarme por ganarte solo esta vez? ¡Solo dices eso porque estas molesta por haber perdido! …además, tú… después de graduarte, ¿es tu intención salir de viaje?”

Cargue a Kuro que corrió hacia mis pies, luego le dije a Yunyun quien se veía intranquila.

“Correcto. Tengo la intención de salir de viaje. De hecho, hay una razón por la cual yo amo la Magia de Explosión. Y te revelare esa razón solo a ti.”

Kuro se sostenía firmemente de mi capa en mi hombro, quizás a ella realmente le guste ese lugar. Yo acaricié su cabeza y le dije a Yunyun algo que ni siquiera le había dicho a mis padres.

“Cuando yo era pequeña, yo fui atacada por un monstruo. Fue entonces cuando una nee-san, una maga que se encontraba de paso, uso la magia de explosión para derrotarlo. La devastación causada por esa magia fue tan grandiosa. Era un poder avasallador, una fuerza absoluta. Fue tan sorprendente que era merecedora del título de la magia más poderosa. Una vez que la has visto, no te interesa otro tipo de magia.”

Yo ahora no puedo recordar claramente la voz ni el aura de esta nee-san, pero la escena de la Explosión se encuentra grabada profundamente en mi mente.

Cada vez que lo recuerdo mi pecho se siente caliente y duele.

Yo no estoy interesada en los rumores como Funifura o Dodonko, no tengo una gran ambición como Yunyun, que trabaja muy duro para ser la siguiente jefa. Mi único deseó es volver a ver a esta nee-san y mostrarle mi magia de Explosión.

Para verla de nuevo agradecerle… y preguntarle-

-Mi magia de explosión que aprendí de ti… ¿Cómo estuvo?

Después de escuchar mi único sueño, Yunyun no parecía tan molesta como antes, sino más bien como de entendimiento. Ella suspiro.

“Ya que es esa la razón, entonces no hay nada que pueda decirte. Pero una vida como mago que usa ‘Explosión’ será realmente difícil. Con la cantidad de mana que tienes, es probable que puedas activarla, pero después de eso no podrás hacer nada por falta de mana. Ir de aventura suena genial, pero si tú estás sola, ¿no serás presa fácil del ataque de los monstruos después de que no te puedas mover? ¿Tienes alguna manera de encontrar compañeros de viaje?”

“Yo soy como Yunyun que no tiene amigos. ¿Qué puedo hacer?”

“¡¿Cómo puedes decir eso sin sentir pena?! Oye, incluso si tú sales en busca de aventura, no será inmediatamente después de aprender magia, ¿Verdad? ¿Tu aún te quedaras en el pueblo por un tiempo?”

“Hm, sí. Yo no puedo simplemente abandonar a mi hermanita. Yo pienso que primero subiré mi nivel, cerca del pueblo. Después de un tiempo, cuando mis preparaciones estén terminadas, dejare el pueblo.”

Yunyun se relajó.

“Yunyun, tú te quedaras para poder suceder como jefe, ¿verdad? Después de todo la posición de jefe es hereditaria.”

“Si, yo siento que seré la siguiente jefa, pero primero debo acumular la experiencia necesaria antes de eso. Actualmente, Megumin a cuidado de mi, pero en un futuro, yo…”

En el momento que Yunyun iba a continuar Kuro que estaba tumbado en mi hombro, reacciono intempestivamente a un pequeño ruido.

Yo volteé a ver-

“¡Ah, esto es raro! ¡Un Farfetch salvaje! Y pensar que aparecería dentro del pueblo…”

El Farfetch estaba chapoteando en el estanque del parque, que estaba atrás de nosotras.

Es delicioso, aun así, por alguna razón desconocida, nunca era atacado por otros monstruos. Ese era el atributo único del Farfetch.

Alguna vez un biólogo de monstruos dijo que, su linda apariencia despertaba el instinto de protección de los otros monstruos.

El Farfetch dejo el estanque y camino hacia nuestra dirección con pasos bamboleantes.

Para evitar asustarlo, Yunyun se hizo a un lado lentamente.

Ella lo volteo a ver gentilmente y continuo la conversación.

“…hasta ahora Megumin a cuidado de mí, pero un día, yo me convertiré en la maga más poderosa de todo el pueblo y seré capaz de proteger a todas las delicadas formas de vida como al Farfetch…”

“¡Gah!”

No era mi intención dejar escapar al Farfetch. Sin esperar a que Yunyun terminara de hablar. Yo me lance y estrangule del cuello al Farfetch.

Levante por el cuello al Farfetch y…

“¡Megumin ha capturado la cena de esta noche!”

“Megumin, ¡tú gran idiota ahhhh!”

Yunyun lloraba mientras me lanzaba golpes.

– ¡Segunda Ronda!

## Parte 6

“… En verdad, todo es tú culpa Yunyun. El cielo ya está casi completamente obscuro ahora.”

“¡¿Mía?! ¡¿Es mi culpa?! ¿Cómo es que Megumin puede matar sin piedad a tales adorables y lindas criaturas? Aunque yo te creo capaz de todo, ¡A un así es demasiado brutal! ¡es que no puedes tener un poco de compasión!”

Yunyun caminaba enojada por su cuenta enfrente de mí.

Después de eso, debido a la Segunda Ronda, todo alrededor se puso obscuro.

Las lámparas mágicas alrededor del pueblo se encendieron, iluminando el camino como si fuera de día.

“El resultado del día de hoy fue, una derrota y una victoria. En otras palabras, queda en empate. Así que lo contamos como que no pasó nada. No te importa ¿verdad?”

“Pero, ¡Claro que me importa! ¡es un hecho que yo gane una vez! Como la que siempre pierde, no importa si pierdo de nuevo. Pero esto no puede considerarse como empate o cancelado. Una victoria y una derrota ¡eso es todo! Eh, eh. Lo debo de escribir en mi diario -El día de hoy yo vencí a Megumin.”

“También escribe como es que te presione contra el suelo sin poder hacer nada después de eso.”

“¡Yo no admitiré eso! ¡Y tú no deberías de usar siempre a Kuro como escudo!… ¿Por qué es tan cercano a ti, si siempre abusas de él?”

Perpleja, Yunyun volteo a ver a Kuro, quien meramente se estiro sobre mi hombro y bostezo.

En verdad es un gato extraño.

Bajo circunstancias normales, no sería extraño que ya hubiera huido o que se enojara.

Yo felizmente tome el Farfetch como mi premio de victoria y me apresure a casa.

Pese a que yo estaba satisfecha después de lo que me invito Yunyun, mi hermanita aún se moría de hambre.

Debo apresurarme a casa y cocinar este pato para que ella cene.

Yunyun me miró fijamente, mientras que tarareaba una canción y sostenía al pato.

“¿Es Megumin realmente una chica? ¿Dónde está tu gentiles, consideración y apariencia?”

¿Es Yunyun realmente la hija del jefe? ¿Dónde está el noble gusto del Clan de Magos Carmesí?

Después de un breve momento, ambas nos quedamos calladas y nos sonreímos maliciosamente una a la otra.

El resultado del día de hoy fue una victoria y una derrota.

Porque no lo decidimos ahora mismo- pero Yunyun repentinamente dio la vuelta.

“Ahh… en verdad. ¿Por qué siempre es así cuando estoy con Megumin?”

“¡Esa es mi línea! ¿Por qué me encuentras problemas todos los días?”

“Uh… eso, es…”

Yunyun trato de disimular caminando adelante, mientras me ignoraba, cuando yo me le acercaba con una sonrisa.

Yo continúe sonriendo maliciosamente mientras seguía detrás de Yunyun.

“… ¡Qué molesta! ¡Y pensar que finalmente el día de hoy logre una hermosa victoria! ¿No estas conforme si no haces esto todos los días? Estoy segura qué cuando tu formes un equipo en el futuro, ¡solo les causaras problemas!”

“Pero ¿qué es lo que dices? Después de que aprenda Magia de Explosión yo seré la que cause la mayor cantidad de daño del equipo. Los subordinados del rey demonio no estarán a mi altura. Con la rara y excepcional genio del Clan de Magos Carmesí, el equipo será famoso a lo largo del mundo. ¡Será un equipo de elite formado solo por gente con profesiones avanzadas…!”

Yo compartí con Yunyun mi futuro imaginario. En ese momento-

-Dang, dang, dang. Agudos sonidos resonaron por todo el hogar del Clan de Magos Carmesí.

Si no mal recuerdo esa campana solo se toca en situaciones de emergencia.

¿Qué sucedió? –Yo voltee a ver…

Un incontable número de monstruos voladores podían verse en el cielo oscuro.

¡Tal parece que buscan algo, se dispersaban y buscaban por todas partes…!

“Meg-… Meg-, ¡Megumin! ¡Eso! ¡No es eso-!”

“Tra-, tran-, tranquila Yunyun. ¡El profesor dijo que ellos sellarían por la fuerza al dios malvado! Incluso ese irresponsable profesor dijo que, si fallaban, ¡Aun tienen ‘esa cosa’ para usar! ¡Así que este asunto, se resolverá rápidamente!”

O más bien, nuestro profesor esperaba que el sello fallara.

Así que no hay nada de qué preocuparse.

Yo incluso pensé que nuestro profesor arruinaría el ritual del sello a propósito.

Kuro, que estaba echado en mi hombro, se escondió así mismo en la mochila, que había rechazado anteriormente.

Aunque parezca un gato rudo, aun así, temía a los monstruos.

“¡Oye… Meg- Megumin!”

Yo cerré mi mochila con Kuro. Pero en eso, Yunyun jalo mi manga.

Ella miro al cielo con una expresión pálida.

“Tú ya… ¿Te diste cuenta de que ellos están volando hacia nuestra dirección?”

Ella apunto a los monstruos que volaban hacia a nosotros.

“¡Corramos ahora! ¡Mi casa está cerca de aquí! Yunyun si algo me pasa, no te molestes conmigo. Y no mires atrás. Yo dejare este lugar en tus manos, ¡Vayámonos ya…!”

“¡Id-Idiota! ¡Que es lo que dices! Como podría abandonar a Megumin y… y… eh, ¿uh? ¡¿Qué fue lo que dijiste?! ¡¿Oye, que acabas de decir?!”

Yunyun, que noto mi intención de disimular las cosas, protesto muy enojada y casi lloraba.

Yo ignores sus palabras, volteé a ver y descubrí que los monstruos se nos acercaban.

“¡Yu- Yunyun! ¡¿Cuántos puntos de habilidad tienes?! ¡¿Aún no tienes suficientes para aprender magia avanzada?!”

“¡Claro que no! Si Megumin se da por vencida sobre aprender Magia de Explosión y en su lugar aprende magia avanzada, ¡estos monstruos no tendrán oportunidad! Oye, ¡Solo hazlo!”

Yunyun me grito. Pero eso era la única cosa en la que yo no me puedo comprometer.

La razón por la que yo me esforcé tanto hasta ahora era para que pudiera aprender Magia de Explosión.

“¡Estamos acabadas si es que nos atrapan…! ¿…Eh?”

“… Tal parece que ellos meramente nos pasaron volando.”

Algunos monstruos nos ignoraron y volaron hacia alguna otra parte.

Exhale de alivio, volteando a ver dónde sonaba la campana.

Entre cada campanada, una luz blanca resplandecía por el obscuro cielo.

Seguramente eran los adultos del pueblo que vieron la oportunidad para ventilar y liberar su poder.

Los monstruos serán suprimidos por ellos pronto, pero aun así debemos apresurarnos a llegar a casa.

“Yunyun, ven a mí casa y quédate esta noche.”

“¡¿Eh?! ¿Quedarme en tu casa? ¡¿En verdad está bien?!”

“No hay problema. O es que aún con todos esos monstruos, ¿Tú intentas regresar a casa sola? Tu puedes usar mis pijamas, pero si llegas a quejarte sobre el área del pecho o que son muy pequeñas, haré que duermas desnuda.”

“Yo, ¡Yo no diría algo así! ¡Yo puedo soportar esas trivialidades!”

Yunyun no negó que la ropa tal vez sea muy pequeña o el área del pecho muy ajustada.

Aún que justo ahora quería el inicio de la Tercer Ronda, ahora no era el momento para eso.

“Mi casa está justo adelante, apurémonos. Hoy solo estará mi hermanita en casa. Incluso si ella tiene hambre, ella vigilara obedientemente la casa con la puerta y las ventanas cerradas. Aún que es una vieja y maltrecha casa, si las ventanas y la puerta están cerradas adecuadamente, incluso esos monstruos podrán- ”

Entrar – O es lo que quería decir.

-Y entonces vi la puerta delantera completamente arruinada.

Dejé caer la mochila que estaba en mi mano y dije en voz baja…

“……………… ¿Komekko?”
