# Interludio 3: Señora Aqua, ¡Yo No Me Rendiré!, 2da Parte

Algo de gran importancia ocurrió el día de hoy.

Hoy, yo dibuje un grafiti sobre la estatua de la diosa Eris, solo por diversión como es usual. Y mientras que era perseguida por esos malvados miembros del culto a Eris…

Una ‘Lolita mágica’ apareció de repente y me salvo, cuando estaba a punto de ser capturada por esos seguidores de Eris.

¿Pero qué sucedió?

¿Habrá sido esto una recompensa por mi vandalismo constante de la estatua de la diosa Eris?

“¡Mi nombre es Megumin! La maga número uno del Clan de Magos Carmesí, ¡Aquella que usa la magia de Explosión! ¡Hmph! Y ya que estoy aquí, ¡yo no puedo ignorar esto!”

Esta chica maga dijo eso y después hizo una pose genial. Ella era definitivamente del tipo que me gustan.

Tan linda. Como un ángel, ella estaba completamente en mi zona de bateo.

¡¿Pero que es esta chica?! ¿Un ángel?

“Oye, ¡¿En verdad eres del Clan de Magos Carmesí?! Espera-espera, ¡cometes un error! ¡Nosotros somos las victimas!”

“¡Ca-Calmémonos primero! ¡Qué tal si hablamos de esto!”

Los dos seguidores de Eris, buscaban escusas rápidamente…

“Pero que pena. Ustedes podrían haber engañado a la gente estúpida, pero ante mis ojos carmesí, ¡esas escusas no sirven!”

Yo pensé qué quizás fuese un ángel. Y resulto ser que en verdad era un ángel.

Ella creía en mí incondicionalmente. Estaba tan feliz. ¡Quería llevarla conmigo a casa!

No, oh cielos, oh cielos. ¡A penas puedo contenerme a mí misma de abrazarla aquí mismo frente a todos!

Ya me lo había advertido la policía apenas la semana pasada. Las cosas no terminaran bien, si simplemente voy a abrazarla, así de la nada.

Primero debo calmarme.

“No, ¡Quizás deberías ir a que te revisen tus ojos carmesíes!”

Uno de los tipos se quejaba. Si, ¡ahora es el momento!

“Sí, nosotros pertenecemos al culto a Eris de esta ciudad… ¡¿Ah?! ¡Esto es malo!”

Yo aventé la mano que me estaba sosteniendo y escape rápidamente hacia un callejón.

Por fortuna esos dos miembros del culto a Eris, no me siguieron.

Yo los espié desde el callejón y descubrí que la Lolita que me rescato, se encontraba asustada.

Pero que linda.

“¡Oye, como piensas compensarnos por esto! ¡Esa mujer era del culto a Axis! ¡Ella pinto un grafiti, sobre la estatua de la señora Eris, de nuestra iglesia!”

Tras ser regañada por ese hombre enojado, la Lolita se atemorizo.

La próxima vez, yo vaciare una gran cantidad de Tokoroten Slime en el buzón de ese tipo.

“Y antes de eso, ella tomo todo el pan, que nuestro culto había preparado para alimentar a los pobres.”

Yo también soy pobre, así que tenía derecho de tomarlo.

Aunque la verdadera desafortunada era mi vida amorosa.

Ya que ese apuesto joven de la espada mágica me abandono, yo esperaba que esta Lolita me amara.

“Sobre eso. Uhm… yo en verdad lo siento… Yo acabo de llegar a esta ciudad recientemente…”

Los dos creyentes de Eris, se iban acercando a la pobre lolita mientras que ella trataba de explicarse.

En ese momento, yo voltee a ver al final de la calle.

Después de todo, yo tenía bastante experiencia y conocía muy bien esta ruta.

A esta hora, ese tipo definitivamente estará patrullando esta área… ¡Ahí esta!

Yo lo tomé de la mano y con lágrimas en los ojos, hice mi acusación.

“¡Por favor ayúdeme! Los miembros del culto a Eris que estaban persiguiéndome, ¡repentinamente fueron a abusar de una pobre niña…!”

“¡¿Miembros del culto a Eris?! No creo, ellos nunca harían algo así…”

Yo ignore su sospecha y señale hacia donde estaban esos creyentes de Eris.

“¡Policía! ¡Son esas personas!”

-Señora Aqua, ¡Yo no me rendiré!
