## Parte 7

“Oh, wow. Eso en verdad cambio mi impresión- No, ¡no es que dudara de ti antes! Desde un principio, ¡yo creía que tú eras una archí-maga bastante poderosa!”

Después de haber agotado mí mana, yo descansaba en mi asiento junto a la ventana, el cual se convirtió en el asiento de honor. El líder de la caravana, no dejaba de alabarme.

Después de haber extinguido a Anis con mi magia de explosión, subieron a los heridos a las diligencias y partimos hacia la ciudad de Axel una vez más…

“Por cierto, cuando los miembros del Clan de Magos Carmesí se ponen serios, realmente dan miedo. ¡En ese momento yo realmente creía que las montañas iban a partirse!”

“La magia se activó en medio del aire, ¡Pero aun así dejo un pequeño cráter en el suelo! ¿No fue eso demasiado poder? ¿Qué clase magia fue esa? He escuchado que los magos que pueden usar magia avanzada son raros- ¿podría ser que se tratara de magia avanzada?”

Yo respondía su sinfín de preguntas.

En realidad, lo quería, era descansar apropiadamente, pero este sentimiento de exaltación tampoco estaba nada mal.

Yunyun quien también había agotado su mana, estaba sentada junto a mí y volteaba a verme insatisfecha. Esa era la expresión que yo buscaba.

“Y esa demonesa era tan poderosa. Pero, aun así no pudo sobrevivir a esa enorme explosión.”

Hablaba un aventurero con su mano herida, mientras que la presionaba mostrando signos de dolor.

-Los demonios son habitantes del infierno.

Así que, incluso si sus cuerpos físicos son destruidos en este mundo, ellos no son derrotados por completo.

Yo he escuchado que algunos demonios súper poderosos, pueden usar ‘residuos’ para preparar un alma sustituta y revivir instantáneamente. Este era un poder tan grande, que raya con lo que llamarías hacer trampa.

Pero aun así, es muy poco probable que nos volvamos a topar con Anis en este mundo.

“Esa onee-san demonio, no era simplemente poderosa. Ella era grande en otros aspectos también…”

……

Un aventurero empezó este tema sobre la apariencia de Anis.

… Cuando vaya en búsqueda de compañeros a Axel, debo de encontrar a un hombre honesto que no me acose sexualmente.

Pensaba eso mientras que relajaba mi cuerpo.

“Oye, Megumin. Tú en verdad eres poderosa. Tú incluso derrotaste a ese tipo de demonio…”

Yunyun me dijo eso con una voz que solo yo pude escuchar.

“Claro que soy poderosa. Después de todo, yo soy la maga número uno de todo el Clan de Magos Carmesí.”

Yunyun se veía insatisfecha, pero al mismo tiempo sonreía felizmente.

“… Te diré, sobre lo que hablamos ayer por la noche… mejor olvídalo.”

Me dijo eso y sonrió agriamente.

Ahora que lo menciona, ayer ella me invitaba a hacer algo. No puedo recordarlo bien. Tenía tanto sueño ayer que yo simplemente le di por su lado…

Así que en silencio trate de recapitular los eventos de la noche pasada. Yunyun entro en pánico de inmediato, como si hubiera malinterpretado algo.

“¡Y-yo, no me refería a eso! ¡No es que me disguste estar junto a Megumin! Nada de nada… es solo que si formamos un equipo, yo siento que solo te estorbare, así que…”

Yunyun miro hacia arriba, con sus ojos llenos de determinación.

“Entrenare muy duro. Espera hasta que aprenda magia avanzada, entonces tendremos un duelo… Y…”

Ella dijo algo más, pero su voz fue tan suave que no pude escucharle que dijo.

Comparado con la magia avanzada, lo que ella realmente necesita son habilidades sociales.

Pero…

“No tengo ningún problema. Tendremos nuestro duelo entonces. Además, no importa que tan duro entrenes, tu terminaras llorado por la derrota.”

“¡Dilo mientras puedas! Cuando me haga más fuerte, ¡estoy segura que la que vendrá pidiendo la revancha será Megumin!”

Discutíamos mientras que nos recargábamos en nuestros asientos. Una feliz risa se podía escuchar de los asientos al lado opuesto.

Se trataba de la señora que nos ofreció los dulces y su hija.

Finalmente tuve la oportunidad de actuar y aun así me muestro tan patética frente a ella.

Yunyun y yo la miramos con un poco de vergüenza.

“Oye, poderosa maga onee-chan.”

La pequeña niña nos sonrío y dijo.

“¡Gracias por salvar a mami y a todos los demás!”

… Yunyun y yo nos volteamos a ver.

¿A caso el ser aventureras, es el tipo de trabajo en el que recibes el agradecimiento y adoración de la gente común?

Sí así es, tengo la motivación de trabajar duramente en esta ciudad.

“Por cierto, Megumin…”

Yunyun me dijo casualmente.

“En ese momento, ¿no sentiste una cantidad exorbitante de mana que provenía de Axel? Era como… así es. Como un dios usando magia de nivel milagroso.”

… Yo también estaba bastante interesada.

“¿Qué fue eso? Después de ese instante, la sensación se perdió. O más bien, ¿no fue el momento demasiado oportuno? Yunyun ¿no estabas tú orando antes de eso? quizás algún dios sintió deseos de ayudarnos.”

“¡¿Eh?! Pero… ¿ahora que debo de hacer? En ese momento, yo ore a todos los dioses… incluyendo el dios malvado y el dios de la destrucción… yo llame a cada dios que pude pensar…”

……

“Bu-bueno, olvídalo. Al final nos salvamos.”

“E-eso es verdad, pero estoy bastante preocupada por lo que paso en la ciudad…”

Mire a Yunyun, quien se encontraba pensando profundamente, hasta que poco a poco regrese a ver por la ventana.

Nuestro carruaje acababa de entrar a la ciudad y circulaba lentamente por razones de seguridad.

En las calles pavimentadas con piedras, el carruaje se movía hacia adelante con el sonido del golpeteo de las rocas con las ruedas.

En ese momento.

Pude observar a un joven con los ojos brillantes, que veía como si todo fuera nuevo para él. También había una hermosa chica de cabello azul, que tenía la boca abierta como si algo la hubiera impactado.

Esas dos personas debían de ser un poco mayores que yo.

“… Este es un mundo paralelo… Oye, ¡En verdad es un mundo paralelo! ¿Eh? Es real, ¿en verdad es real? ¿Yo en verdad voy a usar magia o empezare a aventurarme en este mundo?”

La voz de este joven podía escucharse a través de la ventana abierta.

Tal parece que esas dos personas también acababan de llegar a Axel.

“Ah… Ah… Ah… Ahh Ah Ah…”

![](cap_5/img/c3.jpg)

La chica temblaba mientras que lloraba suavemente de desesperación.

… ¿Qué le pasa a estos dos?

¿Es molesto o desconcertante?

“¡Orejas de animal! ¡Aquí hay orejas de animal! ¡Y orejas de elfo! ¡¿Se trata de un elfo?! Tan pequeña, ¡debe de ser! Adiós, ¡vida de encierro! ¡Hola mundo paralelo! Ya que así es esta mundo, ¡tengo confianza de poder trabajar afuera!”

“Ahahahahaha… Ahahahahahahah… ¡Ahahahahahaha!”

El joven gritaba algo que no entendía… o más bien, ni siquiera trataba de entender.

El temblor de la chica se hizo más severo.

“Oye, ¡eres demasiado ruidosa! ¡La gente creerá que soy amigo de una mujer loca! Por cierto, ¿no es momento de que entregues algo? Mira mi apariencia. ¡Estoy vistiendo un traje de deportes! ¡Estoy vistiendo un traje deportivo en un mundo paralelo de fantasía! Deberías de seguir la trama de los juegos y darme algo de equipo básico…”

“¡¡AHAHHAHAHAHA!!”

La chica empezó a ahorcar al joven.

“¡Ugh! ¡¿Qu-que estás haciendo?! ¡Detente! Lo entiendo. Ya pensare yo en algo sobre el equipo básico. Lo siento. Si tú estás tan indispuesta, entonces regresa. Ya pensare en algo por mí mismo.”

El joven agito su mano por la molestia.

“¡¿Pero qué estás diciendo?! ¡¡Yo estoy en problemas porque no puedo regresar!! ¡¿Qué es lo que hare ahora?! Oye, ¡qué puedo hacer! ¡¿Qué hare a partir de ahora?!”

… No se los detalles, pero será mejor no involucrarme con estas dos personas.

Escuchaba sus quejidos mientras que regresaba la vista a adentro del carruaje.

¿Qué are a partir de ahora?

Mire a Yunyun quien se había acomodado para dormir junto a mí.

Rodeada por el ruido de afuera, decidí que también dormiría…
