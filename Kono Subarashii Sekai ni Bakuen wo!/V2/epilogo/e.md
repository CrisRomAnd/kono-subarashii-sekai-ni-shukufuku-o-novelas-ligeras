# Epílogo

![](epilogo/img/c1.jpg)

-En cierto hotel de la ciudad de Axel.

El líder de la caravana, preparo un cuarto para mí como agradecimiento por haber derrotado a la demonesa.

Yo arrastre mi cansado cuerpo y caí rendida en el cuarto que me prepararon, entonces simplemente colapse en la cama.

… Estoy tan cansada.

Después de agotar mí mana, mi cuerpo se siente muy cansado y repentinamente me siento con sueño.

Este cansancio no era meramente por haber agotado mi mana.

Si lo pienso bien, pasaron muchas cosas después de haber partido del Hogar del Clan de Magos Carmesí.

¿Pasaban cosas así mientras que yo vivía tranquilamente en el pueblo?

En el mundo exterior ocurren demasiados eventos ‘anormales’.

En este corto tiempo he experimentado demasiadas dificultades.

… Pero incluso así, tengo muy buenas memorias. O debería de decir, he encontrado gente muy rara-

Mientras me recostaba en la cama, sentía como algo caminaba en mi espalda.

Debe de ser mi desvergonzado familiar, tomando ventaja de su cansada ama.

Me moví rápidamente, sujete a Chomsuke, que caminaba en mi espalda y la jale bajo las sabanas.

En ese momento, debido a mis movimientos, algo se salió de mi equipaje cuando callo de la cama.

Se trataba de un libro de ilustraciones. Mientras que seguía recostada tome el libro.

-Era acerca de una famosa leyenda de hace algún tiempo.

En cierto lugar, había un joven genio.

Ese joven poseía un talento inimaginable. Él podía volverse más fuerte con solo un poco de combate.

Los aventureros admiraban y temían a aquel joven.

Ese joven siempre estaba solo.

En ese momento.

Un equipo de valientes aventureros lo invitaron a que se les uniera.

Pero el joven les dijo.

“Si tengo una ventaja tramposa, entonces no necesito compañeros. Yo puedo ser un aventurero solitario y quedarme con todo el botín. ¡Viva el jugar solo!”

Ese joven era fuerte a tal grado.

El joven era tan fuerte que venció a la armada del rey demonio en repetidas ocasiones por sí solo.

En su desesperación, el rey demonio se dio cuenta que no era contrincante para el joven en una confrontación directa. ¿Cómo vencería a ese joven entonces?

En ese momento el rey demonio se percató que el joven nunca se unió a ningún grupo.

Uno de los líderes del ejercito del rey demonio le dijo entonces al joven, “¡Un héroe solitario y sin amor es una broma! ¡Los héroes usualmente tienen compañeros con los que trabajan juntos duramente para derrotar al rey demonio! Tú ni siquiera tienes compañeros. ¿Por quién y para quien peleas? ¡¿Por qué no mejor te unes al ejército del rey demonio?! Tenemos muchos beneficios.”

El líder del ejército del rey demonio le pregunto al joven que regresara cuando le tuviera una respuesta. Así que el joven se retiró obedientemente.

Al final, el joven ataco de nuevo el castillo del rey demonio y se confronto contra el líder del ejército del rey demonio.

“Yo no soy solitario, si no que un gran y poderoso jugador individual. Y no es que no pueda hacer amigos, sino más bien es que no los necesito. Porque sé que los compañeros pueden volverse una responsabilidad… ¡Y que es eso de los beneficios! ¡Crees que seré engañado! ¡Las cosas nunca terminan bien cuando uno hace un trato con el rey demonio! ¡¡Yo peleo por la humanidad y por la paz!! No hablare contigo más. ¡Mi meta es la cabeza del rey demonio! Te dejare escapar, ¡así que piérdete!”

Al decir eso, el joven apunto al líder del ejército del rey demonio

El líder del ejército del rey demonio entonces le contesto, “Eso hubiera sonado genial si tú lo hubieras dicho entonces.”

-Al final, el líder del ejército del rey demonio también fue eliminado.

Este valiente joven se lanzó hasta la parte más profunda del castillo del rey demonio.

Nada parecía poder detenerlo. Al final, el llego frente al rey demonio-

Desde hace mucho tiempo, las confrontaciones entre el héroe y el rey demonio son de uno contra uno.

Pero de ese lado…

Como si violara las reglas, había un gran grupo de lacayos que se negaban a retirarse incluso frente al gran héroe, con solo deseo de proteger al rey demonio.

-Cerré el libro de ilustraciones y lo puse con cuidado de vuelta en el equipaje.

Será que ese joven genio, que siempre peleo solo, ¿Se libraría de todos los obstáculos a la fuerza como yo lo hice?

Él no tendría ningún miembro de su familia con él, ¿Cómo una arrogante pero linda hermanita como la mía?

-Se trataba de una historia que todo mundo conocía, la historia donde el joven se convertía en el rey demonio al final.

No pude resistir el deseo de dormir ni por un momento más y cerré mis ojos.

¿Podre encontrar excelentes compañeros en esta ciudad?

Y si los encuentro, ¿Cómo serán?

De ser posible…

Quisiera conocer gente, como el equipo de valientes que se le acercó al joven-

**– FIN DEL VOLUMEN 2 –**

![](epilogo/img/c2.jpg)

![](epilogo/img/c3.jpg)
