# Capítulo 1: La Explosiva Búsqueda de Empleo de la NEET


## Parte 1

![](cap_1/img/c1.jpg)

Ha pasado algo de tiempo desde el alboroto con los lacayos del Dios Malvado.

Yunyun y yo aprendimos magia, nos graduamos de la escuela y tomamos caminos diferentes.

Ya que Yunyun aprendió magia intermedia en ese entonces, ella se unió al grupo de auto-defensa para de esta forma saliera a cazar monstruos y eventualmente pudiera aprender magia avanzada.

Mientras que yo-

“Buenos días, ¡Hermana NEET! ¡Aliméntame ahora!”

“Komekko no me llames hermana NEET. ¿De quién aprendiste esa palabra?”

Yo pasaba mis días sin hacer nada siendo llamada NEET por mi hermanita.

“Escucha Komekko. Yo no soy una NEET. Un NEET es basura que no quiere trabajar. Yo estoy buscando empleo, pero no he podido encontrar uno. Así que técnicamente no soy una NEET.”

“Entonces ¿Qué eres?”

……

“¿Busca-buscadora de empleo…?”

“Hermana NEET, ¡Ya quiero comer!”

“¡Ko-Komekko!”

Sostuve mi cabeza por el dolor, después de ser habitualmente llamada por este raro mote, por mi hermana la cual lo hacía sin ninguna mala intención.

-En el hogar de los Magos Carmesí, a excepción de algunas personas que administran las tiendas necesarias, la mayoría de la gente trabaja en la elaboración de la especialidad local.

La especialidad del Hogar del Clan de Magos Carmesí. Se trata de artículos mágicos y pócimas de la más alta calidad, hechos usando nuestro naturalmente poderoso mana.

Por ejemplo, la pócima de aumento de habilidad que Yunyun y yo bebíamos como si fuera agua. Si se vendiera fuera del pueblo, podría alcanzar precios de millones de eris.

Me entere de eso después de graduarme y me molesto mucho.

¿Por qué no guarde al menos un bote entonces?

El Clan de Magos Carmesí puede crear una gran cantidad de artículos mágicos de gran calidad. Después de todo, Archí-Mago es una profesión avanzada, la cual no es fácil de conseguir para los magos.

Aun así, la gente de este pueblo tiene el potencial de convertirse en Archí-magos desde el momento en el que nacen.

Los artículos mágicos creados por estos magos de élite, son lo que soportan la economía del pueblo.

“… Seria grandioso si hubiera algún taller que quisiera emplearme el día de hoy…”

Suspiraba mientras que preparaba el almuerzo de Komekko.

***

-Dejar el pueblo y encontrarme con la persona que me enseño la Magia de Explosión.

Esa es mi meta.

Para convertirme en una aventurera fuera del pueblo, primero debo ir a una ciudad.

Pero fuera del pueblo se encuentra un bosque lleno de monstruos poderosos.

Después de usar magia de Explosión, yo me quedare inmóvil, así que no podría ir a una ciudad sola.

Hay un grupo de teletransportadores que se ganan la vida llevando a personas a lugares distantes. Para ellos sería posible mandarme a una ciudad…

“Un solo boleto para Alcanretia, la ciudad del agua y fuentes termales, cuesta 300,000 eris. Yo solo tengo 4,000 eris en este momento… Habrá algún trabajo con el que pueda hacer dinero…”

Mire mi bolso de dinero y suspire profundamente.

Yo quería ir a Axel, la ciudad donde los aventureros principiantes se reúnen.

Pero debido a que era una ciudad de novatos con solo monstruos débiles cercanos, Axel no era un destino popular para los miembros del Clan de Magos Carmesí que pueden usar magia avanzada.

Para realizar una teletransportación de larga distancia, uno debe ir primero al destino objetivo y dejar un punto de salvado. De los teletransportistas locales, ninguno tiene a Axel como una locación disponible.

Así que, para llegar a Axel, primero debo teletransportarme a Alcanretia y de ahí proseguir a pie o a carreta hasta mi destino.

Primero, debo encontrar un trabajo para pagar la cuota de teletransportación…

-Mientras pensaba en esto, me percate que uno de nuestros vecinos se acercaba del otro lado.

“Que hay, Megumin, ¿buscas trabajo hoy también? ¿O te rindes con eso de trabajar y te unirás a nosotros, la Unidad Guerrillera Anti-Armada del Rey Demonio? Puedes unirte a nosotros y a Yunyun para defender la paz del pueblo.”

“No, no… pero y pensar que esa niña introvertida en verdad se les uniría.”

“Sí, ella es bastante animada. Ella dice que ella protegerá a sus compañeros esta vez y que aprenderá magia avanzada.”

Aunque el nombre del grupo “Los Asesinos Muertos de Ojos Rojos” apenaba a Yunyun, ella se unió a estos NEETS de la fuerza de auto-defensa con el objetivo de entrenar y aprender magia avanzada.

Ella entrena sin descanso con el objetivo de ganar suficiente experiencia con Buzucoily y los otros, quienes son todos magos de élite, a pesar de ser NEETs.

“Por cierto, Buzucoily, ¿Está realmente bien para ti el no trabajar? Tus padres siempre se están quejando de eso.”

“Mis padres y la sociedad, me ven con malos ojos actualmente, pero pronto aparecerá una gran batalla en donde podamos usar todo nuestro poder. Para prepararnos para eso, yo debo de entrenar constantemente por mí mismo.”

Después de decir eso Buzucoily apretó su puño bajo sus guantes, los cuales rechinaron debido por lo ajustado.

“Hasta luego, Megumin. Como una compañera NEET, cuenta con mi apoyo para cualquier problema no-financiero que tengas.”

“Yo, ¡Yo no soy una NEET! ¡yo estoy buscando empleo!”

Pese a mi rápida contestación, Buzucoily ya había volteado y se marchaba despidiéndose.

Hay dios, hay dios…

Así que, bajo los ojos de Komekko, yo soy igual a ese tipo.

Sin importar que, ¡debo de encontrar empleo hoy…!

En el hogar del Clan de Magos Carmesí, crear artículos mágicos era lo que dejaba mejor sueldo.

Por tanto, después de graduarme, yo visite un montón de talleres para entrevistarme…

“La entrevista de hoy es en la sastrería de Chekera… ¡Hoy seguro será…!”

Yo golpee mis mejillas con mis palmas para darme ánimo a mí misma y camine hacia el lugar de la entrevista.
