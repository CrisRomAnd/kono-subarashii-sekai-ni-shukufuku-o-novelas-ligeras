## Parte 6

-Zesta dijo, mientras que abría las puertas de la catedral.

“… ¿Qué clase de broma es esta?”

“No- ¡no es ninguna broma! Oficial del más alto rango del culto a Axis, Señor Zesta. Mis superiores lo han convocado. Por favor acompáñenos a la estación de policía.”

Una mujer caballero, acompañada por varios miembros de la policía, nos dieron la bienvenida en cuanto llegamos a la iglesia.

Dos policías tomaron a Zesta por ambos lados, con intención de llevárselo.

Zesta no pudo responder a tiempo.

“Pero, ¿qué están haciendo? ¡Ni siquiera le han dicho de qué lo acusan! ¡Eso es demasiado irrazonable! Este tipo ha estado conmigo todo este tiempo. Puedo ser su testigo de ser necesario.”

Yo proteste, mientras que bloqueaba la salida…¡!

“¡Señor Zesta! ¡¿Pero qué crímenes cometió esta vez?! ¡Ya le habíamos dicho varias veces que no abusara de la nueva auto-indulgencia!”

“¿Es por el acoso sexual a las lindas sacerdotisas del culto a Eris?”

“Hace algún tiempo, el grito en la oficina general, ‘¡¿Quién está mejor capacitado para fungir como salvavidas de la alberca que yo, que sirvo a la diosa del agua?! ¡Déjenme proteger a esas pequeñas niñas!’ ¿Quizás fue por eso?”

“O quizás fue por ese sermón que dio sobre ‘Esta bien para una mujer comprar ropa interior masculina, ¡pero un hombre comprando ropa interior femenina es altamente criticado! ¡Eso es inequidad de género!’.”

Tras escuchar los comentarios del resto de los miembros del culto, yo me hice a un lado.

“Por favor, sigan adelante.”

“Gracias por su cooperación.”

Mientras que la mujer caballero me agradecida e intentaba salir de la iglesia…

“Señorita Megumin. ¡¿No cree que es demasiado abandonarme de esa forma?! ¡¿Acaso no éramos camaradas que disfrutaron no hace mucho del fetiche de ser sermoneados en público!?”

“¡No digas eso, que puede malinterpretarse! Y por favor no me llames camarada. ¡En verdad, esto es estresante!”

Después de liberarse de los policías, Zesta me atosigo y se dirigió a la mujer caballero.

“De cualquier forma, ¿qué están haciendo? El arresto está limitado a uno por día. ¡Esa ha sido nuestra vieja tradición! Y a mí ya me llamaron la atención los policías el día de hoy.”

“¡¿Y desde cuando esa ha sido nuestra tradición?!… Señor Zesta, por favor tómeselo seriamente. Esta vez no será una simple llamada de atención como acostumbramos.”

“En otras palabras, ¿Esta vez es el fetiche de la carcelera?”

“Ah. ¡En verdad! Hablar contigo, me vuelve loca.”

La mujer caballero se sacudió fuertemente la cabeza, al estallar su enojo.

“La administración de las aguas termales de esta ciudad, son manejadas por el culto a Axis que adoran a la diosa del agua… y desde ayer ha ocurrido que, a habido múltiples quejas de los hoteles, sobre la calidad de las aguas termales.”

Dijo eso mientras que veía fríamente a Zesta.

“¿…? Oh, había algo de eso en la montaña de reportes que recibí. Ya que estaba ocupado con el acoso sexual… digo enfrentándome a los paganos y salvando a las ovejas descarriadas, deje eso a un lado… pero bueno, vayamos a investigar a la fuente de las aguas termales.”

Escuchando las palabras de Zesta, ella le contesto.

“Eso no será necesario.”

La mujer caballero le dijo fríamente mientras que le mostraba un documento.

“Por favor vea esto. ¡Usted está acusado de introducir ilegalmente una sustancia extraña!”

“¿Violación y dejarlo a dentro…? ¡¿Pero qué acusación tan despreciable?!”

“¡Lea bien lo que está escrito!… Hace dos días, el encargado de la administración de esta ciudad, recibió un reporte de uno de nuestros más cercanos aliados, el Clan de Magos Carmesí. Hay una muy poderosa adivina en el hogar de los Magos Carmesí. Usted sabe de eso ¿verdad?”

… esa debe de ser Soketto.

“La adivina predijo lo siguiente, ‘El desastre descenderá sobre Alcanretia. Cuando las aguas termales de sufran cambios no naturales, tengan cuidado del administrador de las aguas termales. Ese hombre es un subalterno del Rey Demonio.’ Nuestra situación actual es exactamente ‘cuando las aguas termales sufran cambios no naturales’. Y el actual administrador de las aguas termales es usted, ¡quien es acusado de complotar con los demonios para dañar esta ciudad!”

“¡Pero que está diciendo, jovencita! Las doctrinas de nuestra iglesia son ‘Exterminar a los demonios’ y ‘Derrotar al Rey Demonio’. ¡¿Qué complotamos con los demonios?! ¡¿De esa boca, salieron esas palabras?! ¡Déjame besarla para limpiarla!”

“¡De-deténgase! ¡O añadiré ‘dificultar el trabajo de la policía’ a sus crímenes! ¡Le dije que se detenga! Ustedes, llévense a este hombre, ¡rápido…! Ah, alto. ¡Nooooooo!”

Después de que Zesta tiro a la mujer caballero, él fue retenido, mientras que el resto de la gente del culto le demandaba a la policía que revelara lo que estaba pasando.

Después de ser liberada en el último momento, la mujer caballero, ahora con lágrimas en los ojos, se alejó lo más posible de Zesta.

“¡Ha… ah…! ¡Cómo le iba diciendo! Las predicciones del Hogar del Clan de Magos Carmesí, nunca se han equivocado. Tomando eso en consideración y le sumamos el comportamiento usual de ustedes, no creo que haya necesidad de decir nada más. En resumen, necesitamos interrogar a este administrador pervertido acerca de la situación actual. Y dependiendo de lo que diga, es probable que necesitemos interrogar a otros miembros también.”

Cecily empezó a presionar a la mujer caballero que empezaba a calmarse poco a poco.

“¡Por favor espere! El señor Zesta es sin duda un pervertido sin remedio. Cada mañana, el muy excitado va las regaderas a tratar de espiar, mientras que yo me pregunto si debería de golpear su cabeza lo suficientemente fuerte como para que se le arregle. Pero dejando a un lado el grado de perversión del Sr. Zesta, Tú dijiste ¿qué traicionaríamos a la Señora Aqua, para aliarnos a los demonios? Eso es imposible, ¡mujer de pechos de vaca! ¡Déjame quitarte esos excesos de grasa!”

“¡Detente! ¡Porque todos ustedes tienen esa disposición a ser ofensores sexuales sin que les importe el género! Oigan, ¡Ya llévense a este pervertido de aquí! ¡Rápido! El resto de ustedes escuchen. Antes de que el interrogatorio de este hombre termine… en verdad… oye, ¡detente! Te dije que es suficiente. Vámonos de aquí ahora mismo… Ah, ¡¿Alto?! ¡Detente!”

“¡Todo esto es un complot, tramado por esos del culto a Eris, ellos tienen miedo de mi liderazgo y me tendieron una trampa usando a esta mujer sin cerebro…!”

La mujer caballero, parecía que estaba a punto de llorar, mientras que Cecily le jalaba la blusa. Pero aun así ella consiguió escapar exitosamente, llevándose consigo a Zesta que seguía protestando ruidosamente-

***

Después de que se llevaran a Zesta, el resto de los miembros del culto que se quedaron en la iglesia no sabían qué hacer.

“Ahora que es lo que vamos a hacer… Sin el señor Zesta ¿Qué será de este culto?”

Cecily fruncía el ceño, mientras que murmuraba para ella misma.

“El señor Zesta es el oficial de más alto rango, ¿cierto? Si no está ¿Qué problemas pueden surgir? Bueno, mientras todo esto pasa, yo les ayudare en lo que pueda. Hasta que el señor Zesta regrese, ¿protejamos esta iglesia?”

Yo le di unas cuantas palmadas a Cecily en la espalda, para darle ánimos.

“Pe-pero… ah, es verdad. No podemos seguir así por siempre. Primero ¡Dividamos las tareas cotidianas del señor Zesta entre nosotros…! …aunque, antes que eso… ¿Qué es lo que el señor Zesta, hace usualmente?”

Con su espíritu levemente mejor, Cecily le pregunto al resto de los miembros del culto.

“… Las confesiones, son manejadas por especialistas. ¿No se trataba de la administración financiera del culto?”

“Las finanzas las maneja la secretaria. Curar al desvalido… eso también lo manejan especialistas. Creo que el señor Zesta, básicamente, solo se pasea por ahí. De hecho, yo realidad nunca lo he visto usando magia.”

“Incluso cuando el predica por las calles, nunca es para esparcir las doctrinas del culto. Lo que predica generalmente es sobre equidad de género, lo cual es una excusa para implementar una ley que apoye los baños mixtos.”

Después de que los miembros del culto, mencionaron esto, uno tras de otro, ellos luego permanecieron callados por un momento.

“… Entonces, ¿Qué es lo que el señor Zesta en realidad hace?”

Mientras que Cecily murmuraba para ella, el resto de los miembros del culto expresaron sus dudas.

“Señorita Megumin. El problema se resolvió solo. Incluso si no está el señor Zesta, no ocurrirá nada de importancia. De todos modos, gracias por su preocupación.”

“¡¿Eh- está realmente bien así?! ¡¿No es después de todo su representante?! Además, que, esa mujer caballero sospecha de todo el culto a Axis. ¿En verdad ésta bien así?”

Los miembros del culto, que estaban a punto de marcharse. Tras escuchar mis palabras voltearon y fruncieron su ceño.

“Eso es verdad. No es raro que arresten al señor Zesta. Así que eso no es el problema. Lo que si nos molesta es que, nos acusen de ayudar a los demonios a los cuales la Señora Aqua odia. Además, que dada nuestra usual buena conducta, ¿Por qué tendríamos que ser tratados como sospechosos?”

Al escuchar eso, Yunyun y Cecily temblaron un poco.

Pero al ver su expresión, creo que puedo entender un poco.

“… ¿Yunyun?”

“¡¿Qué- que pasa?!”

Le hable a Yunyun que tartamudeo mientras que miraba a lo lejos.

“… Incluso si tú no puedes aceptar el cómo es el señor Zesta, tu no acusarías a un hombre inocente, ¿verdad?”

“¡Yo- yo no lo hice! Yo no he acusado a nadie. Además, incluso si no puedo manejar bien a ese viejo, no es que lo odie a tal grado…”

Mire con sospecha a Yunyun que parecía un poco nerviosa.

“¿Entonces porque estas temblando? Así como estas ahora, te pareces a mi hermanita cuando quería aprender magia de fuego rápidamente, para así poder deshacerse de la evidencia de que ella mojo la cama.”

“¡¿Komekko intento hacer algo como eso?! No- ¡No! Eso… yo solo dije…”

Yunyun junto sus manos y empezó a jugar con sus dedos.

“… De hecho, antes de que saliera del hogar del clan de magos carmesí, Soketto la adivinadora, me pidió ayuda. Y bueno… ella dijo que había predicho que un desastre caería sobre esta ciudad en un futuro. Y ya que yo venía a Alcanretia, ella me pidió que entregara una carta que contenía su predicción… después de eso…”

Ella dijo eso y después agacho la cabeza en forma de disculpa-

