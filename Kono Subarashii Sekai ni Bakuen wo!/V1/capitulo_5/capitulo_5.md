# Capitulo 5

## Parte 1


![](capitulo_5/img/c1.jpg)

En mi estrecha y mal gastada casa, se escucha el sonido de alguien buscando algo en la casa violentamente.

Eso es por decir que alguien se encuentra saqueando la casa.

Por decir alguien, me refiero naturalmente a un monstruo.

Y dentro de la casa debería de estar también –

“Ko-Ko-… Komekko… Komekko debe estar- ”

“Meg-Megumin, ¡tranquilízate! ¡Tra-ta-tranquilízate!”

Yunyun me sostuvo y sacudió del hombro, mientras que yo vacilaba frente a la maltrecha puerta.

“…¡¡Komekko!!”

“¡Tranquilízate! Megumin, ¡Solo cálmate un poco!”

Las palabras de Yunyun me sacaron de mi trance y mi cerebro comenzó a funcionar rápidamente.

Es verdad, lo primero que hacer en una situación así es tranquilizarse.

“¡No hay problema! Mi hermana es la reencarnación del demonio de la gula, ¡Astaroth! Cuando ella se encuentre en peligro, su sello se rompería y ella finalmente estaría lista para conquistar el mundo junto a mí.”

“¡Lo que te dije es que te tranquilizaras! Ya cálmate, ¡Megumin!”

*¡Zas!*

Después de recibir una bofetada de Yunyun, yo recobre mi compostura.

“Duele, duele, duele… no ahora no es el momento de decir esas cosas, ¡Yunyun! ¡Komekko aún debe de estar en la casa! ¡Debemos entrar a rescatarla! Esa niña sabe mejor que yo como funciona el mundo, ¡así que ella no se convertiría en la comida de unos monstruos tan fácilmente! ¡Vamos!”

“…Co-Como es que ella puede ser tan positiva de repente…”

Yunyun me siguió por detrás, mientras sacaba la daga de su cinturón y murmuraba algo. La hoja plateada de la daga brillaba en la oscuridad.

“Las acciones extrañas de Yunyun solo son útiles en momentos como este.

Yunyun, es tu turno cuando llegue el momento crítico, ¿está bien? Después de todo yo no tengo un arma”

“¿¡Extraña!? ¡¿Qué quieres decir con extraña?! Oye, ¡¿Estoy haciendo algo extraño?! Dime”

“Más que extraño- Solo la rara de Yunyun llevaría una daga a la escuela porque a ella le gusto. No importa cuánto te guste la daga, aun así, es ridículo.”

“¡Ugh…! Yo, yo no puedo responder a eso… ¡es verdad! ¿Pero qué es lo que pasa? ¡Tuve una sensación de incomodidad justo ahora…!”

Puse mi dedo índice en mi boca, para señalarle a la ruidosa Yunyun. Yo entre silenciosamente a la casa, de la cual se seguían escuchando ruidos de “crash, crash”.

No se escuchan gritos de angustia de Komekko desde la casa.

El peor escenario se reflejó en mi mente, pero mi hermanita es una gran persona así que ella debe de estar bien – Intente convencerme a mí misma de esto para poder permanecer tranquila…

-Tras atravesar la puerta delantera, quede frente a frente con el monstruo.

Un monstruo de características reptiliana con un pico de ave, se encontraba frente a nosotras.

“………….Ah, ¡ahhhhhhhh! ¡Yunyunyunyunyunyun! ¡Yunyunyun! ¡Yunyun!”

“Espera. ¡No me empujes! Y ¿porque mi nombre se volvió tan extraño? Espera ¡espera!”

“¡Ahhh!”

“Está tratando de intimidarnos, solo nos está intimidando, Yunyun. ¡Por favor usa tu daga para apuñalarlo!”

“Incluso si tú me lo pides, matar a ese monstruo es un poco…¡!”

El monstruo lanzo un rugido después de vernos discutiendo entre nosotras.

Yunyun tomo rápidamente una pose con su daga plateada, pero no mostraba ninguna fuerza. Sus rodillas temblaban y parecía que iba a llorar en cualquier momento.

El monstruo debió pensar que era fácil lidiar con Yunyun así que estiro sus brazos para atraparnos-

“¡Ahora!”

Empuje a Yunyun fuertemente, de manera que la daga se le encajara al monstruo por el estómago. Este lanzo un chillido de agonía.

“¡Gahhhh!”

“¡Kyaaaaa!”

Yunyun grito junto al monstruo mientras que lo apuñalaba.

Tome la daga de Yunyun mientras ella seguía gritando y me abalance al monstruo que se encontraba herido el cual rodaba en el piso cerca de la puerta. Sosteniendo la daga con las dos manos, yo apuñale al monstruo por su garganta.

“¡Eres demasiado ruidosa Yunyun! Nosotras pertenecemos al Clan de Magos Carmesí que incluso el rey demonio teme. ¿Cómo puedes dudar cuando nos encontramos enfrentándonos a mon…struos…?”

¿…………?

Sucedía algo extraño con el monstruo que acababa de apuñalar.

O más bien…

“… ¿Acaba de desaparecer?”

“¿Co-cómo?”

Después de que lo apuñala por la garganta el monstruo se retorció por el suelo un momento, para después desaparecer en una nube de humo negra.

¿Por qué desapareció el cuerpo?

En ese momento me di cuenta de que la casa había quedado en silencio de nuevo.

Tal parece que el único monstruo buscando en mi casa, era ese.

¿Pero porque es que buscaba algo en una pobre casa con apenas nada de comida?

No lo más importante ahora es -¡!

“… Sí, ¡Komekko! Komekko, ¡¿Dónde estás?! ¡Soy yo, tu hermana!”

“¿Ko-Komekko? Komekko, ¡¿dónde estás?!”

Yunyun y yo buscamos por toda la casa, pero Komekko ni contesto ni apareció.

En una búsqueda más detallada, no parecía haber rastros de sangre, lo que significaba que ella probablemente escapo de la casa.

O fue capturada por los monstruos…

“Yunyun, ¡Ella está afuera! ¡Mi hermanita debió de ir afuera! Yo iré a buscar a mí hermanita. Yunyun quédate aquí solo en caso de que ella regrese a casa. Has una barricada en la entrada con la puerta rota y lo que encuentres. Ah, y préstame tu daga.”

Le decía a Yunyun en lo que me dirigía a la salida. Yunyun me tomo del cuello.

“¡No-no! Si sales en esas condiciones, Megumin, ¡solo serás comida para los monstruos! ¡Yo te acompañare!”

Bien…

“Eres bastante arrogante, solo porque ganaste por pura suerte un encuentro antes. Bien, vayamos. Pero mi condición es que cuando los monstruos aparezcan, Yunyun tomara responsabilidad completa.”

“¡¿Eh?! Eso, eso es… mmm…”

Nosotras discutíamos mientras que salíamos de la casa…

-Y nos encontramos con un monstruo rasgando la mochila que deje a la entrada.

“Kuro está-¡! ¡Acabo de recordar que tu pusiste a Kuro en la mochila!”

Yunyun grito en desesperación.

El monstruo que estaba rasgando la mochila se percató de nosotras.

“Ya es demasiado tarde para la bola de pelo, ¡dejémosla atrás! ¡Se ha sacrificado por nosotras, así que erigiré una tumba por ella! No hay problema la bola de pelo siempre estará con nosotras ahora. Sí, ¡vivirá por siempre en nuestros corazones…!”

“¡Aún está viva! Mira cuidadosamente, esa pequeña ¡aún está viva! ¡Tú te rindes demasiado rápido!”

Dijo Yunyun señalando la mochila mientras que me sostenía del cuello en lo que yo trataba de escapar del monstruo.

En verdad, Kuro se arrastró sigilosamente para salir de la mochila rasgada.

Pero el monstruo se dio cuenta de esto, aunque se dedicó meramente a observar silenciosamente a Kuro sin intención de lastimarlo.

Aún más, el monstruo se dio cuenta de nuestra presencia desde hace rato, pero parecía no prestarnos importancia.

“En cualquier caso, ¡esta es una rara oportunidad! ¡Esa bola de pelo debió de despertar el instinto de protección del monstruo! ¡Debemos aprovechar esta oportunidad para escapar y buscar a mi hermana…!”

“¡Espera! Sé que estas preocupada por Komekko, pero por favor ¡salva a Kuro también!”

“Pero ¿qué estás diciendo? Si alejamos a Kuro de ese monstruo que parece tan obsesionado con él, ¡tengo la premonición de que ese monstruo nos perseguirá!”

Tras escuchar eso, Yunyun puso una expresión como la de una niña que actúa tímida frente a sus padres para que adopten a un perro callejero.

…..Ahhh, ¡pero que irritante!

“¡Prepárate para huir a toda prisa! Yo recuperare esa bola de pelo que mi hermanita alimento con tanto ahínco.”

Claro que fue ‘alimentada con mucho ahínco’ para convertirse en una fuente de alimento de emergencia – Pero es mejor no decirle eso a Yunyun en este momento.

La expresión de Yunyun cambio como si floreciera. Yo sostuve la daga y llame la atención por detrás del monstruo.

-Al mismo tiempo, el monstruo reacciono.

Alcanzo a Kuro con sus dos manos y lo levanto.

Kuro no trato de resistirse, se veía realmente dócil con el monstruo que lo cargaba.

¡Cuando el monstruo extendió sus alas para volar al cielo…!

“Mi hermanita tiene grandes expectativas con su crecimiento. Sí esa bola de pelo es tomada, ¡Mí hermanita me odiaría! ¡Prueba mi técnica de lanzamiento de cuchillo!”

“¡Ahh!”

Yo use toda mi fuerza para lanzar la daga… que voló en dirección opuesta.

“… Increíble. Este monstruo inclusive está protegido por una barrera mágica…”

“¡No tiene tal barrera! No importa como lo vea, ¡Fue Megumin la que lanzo así de mal la daga!”

“¡Ahora no es el momento de tener esta discusión! ¡Kuro…!”

“¡Aunque es verdad lo que dijo Megumin! Pero, ¡Aun así tengo este sentimiento de descontento! –”

Mientras que nosotras discutíamos, el monstruo tomo a Kuro y voló por los cielos hacia algún lugar…

-Viendo a Kuro siendo llevado hacia algún lugar, dije en una extraña voz baja.

“Esa bola de pelo debió ser un mensajero del cielo. Y ahora simplemente regresa a su hogar. Así que deja de llorar, y ¡mándalo tranquilamente de regreso…!”

Casi llorando, Yunyun recobro la daga y dijo.

“¡No le añadas ningún pasado extraño para que dejes morir a Kuro! Que haremos, ¡se han llevado a Kuro! ¡ese monstruo debió haberse llevado a Kuro a su nido para poder comérselo! ¡¿Qué podemos hacer?!”

“Hm. Tranquilicémonos primero. Kuro no se resistió cuando el monstruo se lo llevo, así que debería estar bien. Esa bola de pelo recibió el entrenamiento espartano de mi familia, así que debería ser capaz de sentir el peligro.”

“¡Oye! ¡¿Cómo es que tú lo educaste?! ¡¿Abusaste de él?!”

Yunyun me tomo de los hombros y me sacudió.

Sin importar que Komekko es más importante que Kuro.

“Dejemos a Kuro por ahora. La que importa es Komekko. Mi hermana es muy valiente y experimentada. No la críe para que fuera una chica ingenua que llore por cualquier cosa y sea asesinada. Ella debe de estar escondida en algún lugar justo ahora…”

“Eso- ¡es verdad! Oye, Megumin ¿Sabes de algún lugar donde pueda estar Komekko?”

Un lugar donde podría haber ido Komekko…

No tengo ni idea. Pero siento que algo que dijo Komekko antes es preocupante.

Que fue, que sería…

“No es que tenga pruebas, pero estoy preocupada acerca de algo. Después de todo, cuando yo era niña, yo era justo como Komekko…”

Yo frecuentemente me escapaba de casa, provocando que mis padres me regañaran.

– En el momento que iba a decir eso… cierto recuerdo apareció en mi memoria.

Recuerdo que cuando era pequeña, yo una vez jugué con el sello del dios malvado como si fuera un juguete.

“……Ah.”

“……..¿? ¿Qué pasa Megumin?”

Recientemente, cuando yo le preguntaba a Komekko, a donde iba a jugar, ella me respondía.

“He encontrado un juguete nuevo, ¡así que yo seguí jugando con el! Hermanita ¿Tú también quieres jugar?”

…Eso es.

…Eso es en verdad…¡!

“Ah… Jajaja No-no-no-no-no me digas que eso…¡!”

“¡¿Qué?! ¡¿Qué paso Megumin?! Porque de repente tu entraste en pánico…”

Ahora todo tiene sentido. Finalmente, todo tiene sentido.

Mi familia es pobre, no tenemos juguetes.

Pero Komekko dijo que jugaba con uno.

Es posible que mi hermanita recibiera un juguete de algún vecino, pero lo más probable es que…¡!

## Parte 2

– La Tumba del Dios Malvado estaba ubicado a las afueras del pueblo.

Iluminado por lámparas mágicas, la tumba daba una sensación siniestra por la noche.

Los monstruos continuaban rondando sobre el cielo. Cuando nosotras llegábamos a la tumba.

Los monstruos nos ignoraron mientras que corríamos ahí. Parecía que buscaban algo.

“Oye, Megumin, Komekko no vendría a un sito así…”

No le conteste a Yunyun que preguntaba tan dudosa. Nos ocultamos en los arbustos para ver lo que sucedía en la tumba.

“… Ella está aquí.”

“… Ella en verdad está aquí.”

Komekko sostenía las piezas del rompecabezas mientras estaba parada sin expresión frente a la tumba. No sabía lo que estaba intentando.

Yo reconocí las piezas que sostenía Komekko, como los fragmentos del sello.

¿Por qué es que esta niña está sosteniendo eso en este lugar?

Mientras consideraba eso, seguí la línea de visión de Komekko…

“… Qué bueno, Yunyun. La bola de pelo, también está bien.”

“¿Cómo es que estas tan calmada? ¡¿No es esta situación bastante terrorífica?!”

Komekko sostenía las piezas del rompecabezas, frente a él monstruo qué sostenía a Kuro.

“¡Pe-pe-pe-pero ahora que haremos! ¡Y donde están los adultos del pueblo!”

“Viendo la situación actual, no parece que nuevos monstruos vayan a aparecer de la tumba. Los adultos deben de haber confirmado eso y han de haber partido a echar los monstruos que estaban dentro del pueblo.”

Desde temprano, los pobladores han estado disparando magia de colores por el cielo.

Si la situación fuera diferente, yo estaría viendo los ‘fuegos artificiales’ desde mi casa junto con Komekko y Yunyun.

“Hm. Calmémonos. Como puedes ver, solo hay un enemigo y parece ser que está concentrado en Komekko. Estará bien si procedemos con cuidado. Después de todo, ya he matado a uno.”

“Ah, ya veo. Tal parece que tú también te diste cuenta.”

Yunyun empezó a calmarse mientras que me veía.

Entonces Komekko puso las piezas en el suelo, cuando repentinamente elevo los brazos en dirección al monstruo.

“… ¿Qué está haciendo Komekko?”

“Probablemente está tratando de intimidar al monstruo. Tal parece que ella quiere arrebatarle a Kuro del monstruo.”

Frente a los avances de Komekko, el monstruo retrocedió.

Incluso Kuro, que estaba siendo cargada por el monstruo, empezó a temblar.

“Tal parece que el monstruo fue suprimido por Komekko. Y aunque quiero ver como se desarrollan las cosas… Vamos Yunyun. Te encargo ser la carnada.”

“Bien… Oye, ¡Espera! ¿Por qué es que yo soy la carnada?”

“La torpe Yunyun, no podría matar al monstruo de un solo golpe ¿verdad? Entonces dame la daga… no la lanzare esta vez. ¡Rápido dámela!”

En lo que nosotras peleábamos por la daga, ella repentinamente volteo al cielo y exclamo.

“¡No, no! Esta vez yo lo apuñalare, así que esta vez Megumin será la carnada… ¡Oye! ¡espera!”

Viendo de cerca, había cinco monstruos que descendían cerca de Komekko.

“No-no hay problema ¿verdad? Megumin tú ya has de haber pensado en un plan ¡¿verdad?!”

“Por supuesto, en un momento tan crítico como este, es seguro que mi poder escondido despierta o que alguien llegara al rescate.

Por tanto, lo único que necesitamos hacer es temblar y gritar como chicas desamparadas…”

“Megumin, ¡¿Qué estás diciendo?! ¿Y porque es que tienes esa apariencia? ¡Tus ojos se ven llorosos! Oye, ¡será que tú eres sorpresivamente mala para manejar las crisis repentinas!”

Komekko era totalmente ajena a mí, mientras que yo perdía cordura. Ella empezó a hacer ruidos intimidatorios a los monstruos que la rodeaban.

“¡Gao!”

“-Oye, Megumin. ¿Komekko realmente estará bien? ¡Esa niña trata de buscar pelea contra varios monstruos! Y aun así no parece tener miedo, en lugar de eso ¡por que los monstruos parecen temerosos!”

Mi hermanita puede que sea realmente una gran persona.

Dejando de lado a Kuro que la trata como a su némesis, ¿porque los otros monstruos le temen?

Es como si hubieran sido infectados por el miedo de Kuro.

“Eh, ¡ahora no es el momento de esperar!… en verdad no quiero hacer esto…”

Yo saque mi carta de triunfo de mi bolsillo.

Mirándola, Yunyun dijo.

“¿Tu tarjeta de aventurero?… ¡¿Ah?! Megumin, ¡tu…!”

-Exactamente. Tengo que elegir entre el menor de los males.

Si tengo que elegir entre la vida de mi hermana y la magia de explosión, ¡entonces por supuesto! –

“¡Alto ahí!”

Komekko y los monstruos voltearon a verme.

Yo sostenía mi tarjeta de aventurero, mientras salía de los arbustos.

“¡Mi nombre es Megumin! La más grande genio del Clan de Magos Carmesí que usa ¡Magia Avanzada! ¡Dejen a mi hermanita en paz!”

“Ah, ¡hermana! ¡Ellos se llevaron mi comida!”

“Komekko ¡¿Qué comida?! ¡¿Quieres decir Kuro?!”

Yunyun salió también de los arbustos sosteniendo su daga.

Ambas arruinaron mi entrada genial.

“…Oye, Megumin, ¿en verdad piensas aprender magia avanzada? Pero si tú amas tanto a la Magia de Explosión…”

“Una genio como yo puede fácilmente ganar esos puntos cazando monstruos. No importa cuánto tiempo me tome – Incluso si me lleva docenas de años, ¡yo nunca me rendiré con la Magia de Explosión!”

… Eso dije, pero mi corazón se sentía pesado.

Incluso aunque yo necesite enfrentarme a los monstruos ahora.

Los monstruos cambiaron su objetivo y lentamente comenzaron a acercarnos y rodearnos.

Uno de ellos extendió sus alas e intento atacarnos desde el cielo.

-Mi mano temblaba mientras que sostenía mi tarjeta de aventurero.

Después de todo era mi sueño de infancia, no era fácil desprenderse de eso.

Pero no había otra forma de salvar a mi hermanita.

… No hay problema solo tendré que trabajar más dudo desde ahora.

Me convencí a mí misma de eso y tomé mí tarjeta de aventurero -¡!

“Tu voz y tu cuerpo está temblando. No te atreves a hacerlo ¿verdad?”

Yunyun puso su daga de nuevo en su cintura.

Ella saco su tarjeta de aventurero y la sostuvo igual que yo.

“Pero que es lo que tu-”

–intentas hacer?

Justo cuando iba a terminar de preguntar…

“¡Relámpago! –”

Fui interrumpida por el encantamiento de Yunyun.

***

Iba siguiendo las luces de la calle que parecían estrellas, mientras corría tomando la mano de Komekko.

“Hermana, ¡Yunyun es muy poderosa! El rayo paso ‘chaz’-”

Quizás fuera porque estaba muy emocionada, pero Komekko presionaba mi mano con mucha fuerza.

“Sí, es muy poderosa. Pero debido a eso ¡ella me ha superado ahora! ¡Pensé que ella se quedaría de lado, completamente perdida…!”

Yo compartía mis quejas con Komekko, al mismo tiempo que corríamos para buscar a los adultos del pueblo.

-Yunyun aprendió magia intermedia.

Después de aprender magia intermedia, ella tendrá que graduarse.

Así que ella no podrá disfrutar ahora de los privilegios de ser un miembro inmaduro del Clan de Magos Carmesí – el cual es un método fácil de obtener la muy rara pócima de aumento de puntos de habilidad.

Si Yunyun se decide a aprender magia avanzada, ella tendrá que pelear contra monstruos con tal de ganar la experiencia necesaria para subir de nivel.

Aprender magia intermedia consume 10 puntos de habilidad.

Para recobrar esos puntos Yunyun debe de incrementar la misma cantidad de niveles correspondientes.

Además de que mientras más alto sea tu nivel, es más difícil subir al siguiente.

Ya que Yunyun tiene un nivel bajo, ella debería de poder subir rápidamente de nivel.

Pero sin importar que tan rápido lo haga, aun así, le llevaría al menos un año para ganar esos 10 niveles.

Así que, por el siguiente año, mi rival seria vista como un miembro incompetente del Clan de Magos Carmesí.

Eso, aunque ella ha trabajado tan duro como hija del jefe del pueblo y siempre haya archivado excelentes resultados.

“Hermana, ¿estás llorando?”

“¡No estoy llorando! Solo me siento irritada, ¡lo que causa que mi mana fluya desde mis ojos!”

Yunyun uso magia de relámpago sobre la cabeza del monstruo que sostenía a Kuro y dijo.

-Yo salvare a Kuro, tú toma a Komekko y ve a buscar a los adultos del pueblo-

Debido a que yo estaba indecisa sobre aprender magia avanzada, Yunyun tuvo que graduarse de ser una aprendiz de maga.

Y esa niña, que no le gusta matar, pudo de hecho liberar su magia sin contenerse de esa forma.

Pese a que normalmente es torpe, ella seguro acude cuando alguien necesite protección.

La postura de mi rival es realmente deslumbrante-

“!¿…? Hermana ¿Que sucede? ¿Te cansaste de correr?”

Komekko estaba desconcertada mientras se me quedaba viendo, por haber dejado de moverme.

Mi auto-proclamada rival está peleando sola, contra esos monstruos ahora mismo.

Mi auto-proclamada rival es una persona solitaria sin amigos y que me molesta a todo el tiempo.

– Si huyo ahora, para alcanzar mi sueño, yo nunca estaré calificada para tener un encuentro contra mi auto-proclamada rival de nuevo.

“Komekko ¿Quieres a tu hermana?”

“¡La quiero!”

Komekko me sonrío al responder.

“… ¿Incluso aunque tu hermana no pueda usar la magia más poderosa y convertirse en la mejor maga?”

“Yo lo haré en lugar de ella, ¡así que no hay problema!”

Komekko continúo sonriendo mientras que respondía.

A su joven edad ella ya está aspirando en convertirse en la más poderosa. En verdad, ella se convertirá en una gran persona.

“… Komekko, voy a ir a salvar a Yunyun, así que tú…”

Mientras que hablaba, yo volteaba al cielo para encontrar el área de batalla más cercano.

Había algunas luces brillantes siendo disparadas desde algún lugar cercano.

Me agaché para poder hablar de frente con Komekko y le dije.

“Tú debes de correr hacia donde esa luz fue disparada. Los adultos del pueblo deben de estar ahí. Esos monstruos voladores parecen estar buscando algo, así que no se mostraran hostiles hacia ti. Para alguien que pudo llegar a la tuba en medio de esta confusión, tu estarás bien. Evita la luz de las lámparas y cosas similares, tú debes de pasar completamente desapercibida-”

“¡No quiero! ¡Yo quiero quedarme contigo, hermana!”

“…Escucha con cuidad, yo voy a la batalla, por lo que no importa que tan fuerte o genial sea, siempre existe la posibilidad de que pierda. Así que…”

Mientras que yo trataba de persuadirla, mi hermana cerro sus puños y dijo duramente.

“¡Yo también quiero pelear! ¡Yo misma tomare de vuelta la comida que me arrebataron!”

Ella dijo estas preocupantes, pero aun así confiables palabras.

– Di vuelta para regresar por el camino que tomamos y volví a instruir a mi hermana.

“¡Escucha con cuidado! ¡No debes de separarte de mi lado!”

“¡Entendido!”

“¡No te lances contra el monstruo que tomo a Kuro! ¡Yo te ayudare a recobrarlo! ¿Has entendido?”

“¡Bien! ¡Yo tratare de no lanzarme contra el!”

“¡No! No trates, ¡tú absolutamente no debes lanzarte contra el!”

“¡Entendido!”

¿En verdad así está bien?

Honestamente, no me siento muy convencida…

Pero tratar de alejar a la persistente de Komekko puede ser más peligroso.

…Ya tomé mí decisión.

No me rendiré sobre la magia de explosión. Incluso aunque me tarde alguno años o décadas. Yo absueltamente la aprenderé.

Este es solo un pequeño rodeo.

Correcto, solo uno pequeño-

## Parte 3

“¡Espada de Viento!”

Yunyun grito y movió su brazo hacia abajo creando una ráfaga de viento.

El viento se transformó en la hoja de una espada y corto a uno de los monstruos en el cielo.

Normalmente magia de nivel intermedio, no sería capaz de generar tanto daño. Así que ese efecto extra debe ser porque la magia de Yunyun es originalmente muy poderosa. Ella en verdad es solo segunda después de mí.

En cuanto regresamos, la escena frente a nosotras es Yunyun peleando desesperadamente.

“Hermana, ¿No vas a ir?”

“Espera, Komekko. Tu brillante hermana acaba de pensar en algo. No hay necesidad de la magia avanzada. Es suficiente con el hecho de que sobrevivamos este encuentro.”

Le dije mientras que observábamos el encuentro.

…No es que me retractara de mi decisión.

Si magia intermedia es suficiente para pelear contra estos monstruos como Yunyun lo está haciendo, ¿Por qué he de desperdiciar valiosos puntos en aprender magia avanzada?

Yunyun se encuentra por ahora con la ventaja.

Y ya que los monstruos que derrota no dejan atrás su cuerpo y simplemente se desvanecen, es imposible determinar cuántos ha matado. Pero mientras huíamos había seis de ellos. Ahora solo queda uno.

Yunyun encontraba al frente protegiendo a Kuro que estaba en sus pies.

“… Pero esto es terrible. Yunyun está a punto de derrotar a todos los monstruos.”

“¿? ¿No es algo bueno que los derrote a todos?”

“¡Claro que no! Si lo hace, mi decisión de regresar será…”

En ese momento.

Mi deseo se cumplió y siete monstruos más descendieron del cielo nocturno.

Bien, ahora puedo salir a escena de manera genial y salvarla, de esa manera le pagare ¡por todo lo que ella ha hecho por mí!

“Mi nombre- ”

“¡Mi nombre es Komekko! Cuidar la casa es mi trabajo. ¡Yo soy la Femme Fatale Hermanita Pequeña Número Uno del Clan de Magos Carmesí!”

Komekko me interrumpió y se presentó así misma antes que yo pudiera hacerlo.

“¡Komekko! Tu… ¡porque sigues interrumpiendo mi glorioso debut!”

“¡No me disculpare!”

“¡Ko-Komekko!”

“¡Oigan! ¡¿Por qué siguen aquí?! ¿No le dije que huyeran?”

Yunyun tenía la mirada fija en los monstruos que se acercaban mientras que nos gritaba.

Yo le dije.

“Tú crees que yo le permitiría a mi auto-proclamada rival deberle una deuda de honor y escapar sola?”

“¡No crees que ya es tiempo para que dejes de decir ese prefijo de ‘auto-proclamada’! Después de todo, ¡ahora soy toda una maga ya que he aprendido magia! ¡Yo soy diferente de la falsa maga Megumin!”

“Faaa- ¿Falsa Maga?! Como te atreves, ¡Tú maga intermedia!”

“¡No me llames ‘maga intermedia’ como si fuera un producto inferior del Clan de Magos Carmesí!”

Mientras discutíamos, uno de los monstruos que descendía antes repentinamente le lanzo un golpe a Yunyun.

Aunque ella estaba discutiendo conmigo, Yunyun seguía observando el movimiento de los enemigos. Ella tomo a Kuro y giro por el suelo rápidamente para evitar el ataque.

Entonces, ella saco la daga con su otra mano y se la lanzo al monstruo.

Quizás fuera por pura suerte, pero la daga voló directo a la garganta del monstruo.

“Uff- ¡¡!!”

El monstruo que fue herido, soltó un ruido como el de un laúd y colapso mientras que sostenía su propia garganta. Entonces desapareció en una nube de humo negro.

Tras ver eso el resto de los monstruos se lanzaron hacia ¡Yunyun!

“¡Tal parece que estás en aprietos ‘Maga Intermedia’ Yunyun! ¡Ahora es el momento para que la Maga Avanzada Megumin se encargue de sacar la basura de un solo golpe!”

“¡¿Ehh?! Megumin, ¡pero que estás diciendo ahora! ¡Tienes idea del porque aprendí magia intermedia…!”

Yunyun se levantó rápidamente y levanto una mano hacia el cielo.

“A partir de ahora, no te llamare ‘auto-proclamada’ nunca más. ¡Y te tratare como a mi rival oficial! Y por tanto no intento deberle una deuda de honor ¡A mi rival! Además ¿En que estabas pensando? ¿Querías graduarte primero para ampliar nuestra distancia? Si tu habías dicho primero que querías graduarte conmigo, ¡ahora podemos hacerlo…!”

“¡¡Bola de Fuego!!”

“¡¿Eh?! Espera…¡!”

Yunyun no espero a que terminara mi discurso y disparo una bola de fuego a los monstruos que se acercaban.

Ese hechizo debió de haberlo combinado con todo su mana restante. Los monstruos que golpeo directamente con su bola de fuego, crearon una explosión tan grande que uno no creería que se trataba de magia intermedia.

El sonido de la explosión fue tan grande que hizo eco a través del cielo.

Siete monstruos cayeron del cielo e instantáneamente se redujeron a cenizas.

Al mismo tiempo, después de confirmar que todos los enemigos fueron derrotados, Yunyun quedo de rodillas sobre el suelo, posiblemente por haber usado todo su mana.

Me apresure para llegar a su lado.

“Con esto… ¡Megumin no tendrá por qué aprender magia avanzada…!”

Ella dijo eso con una expresión que hacia alarde de su victoria.

“… ¿Por qué tuviste que hacerlo tú? ¿No fuiste tú la que se oponía a que yo aprendiera Magia de Explosión? ¿¡Qué te hizo cambiar de opinión!?”

Le decía mientras que sostenía su cabeza en mi hombro.

“No-no, no he cambiado mi opinión al respecto… incluso ahora me opongo a que aprendas magia de explosión, pero siento que sería una pena que dejaras tu sueño por esta razón… ¡y…y además! Me costó mucho trabajo que me debieras una. ¡No dejare que me pagues tan fácilmente!

Después de todo, ¡No hay muchas oportunidades para hacer que me debas un favor!”

“Entonces, ya que tú no te puedes mover debido a que te quedaste sin mana, si yo te llevo a casa, eso cancelara mi deuda de honor, ¿Correcto?”

“¡¿Eh?!”

Ya que decidí llevar a la fuerza a Yunyun a su casa, Komekko se apresuró y tomo a Kuro.

Yo en verdad espero que la razón por la que ella tenía su mirada fija en Kuro mientras que tenía los ojos rojos brillantes fuera porque ella estaba feliz de ver a Kuro sin heridas.

“¡Oye, Megumin! Yo aprendí magia intermedia para ayudarte, aun así, tú dices que llevarme a casa a rastras cancelaría tu deuda. ¡Eso es demasiado!”

“Eres muy ruidosa. Ya que no te puedes mover por agotar tu mana. Si yo te dejara en ese lugar, tú te convertirías en la comida de la siguiente ola de monstruos ¿correcto? En otras palabras, yo soy tu salvadora… Vez es similar a lo que tu hiciste antes.”

“¡Eso es sofismo! Yo arriesgue mi vida al pelear contra esos monstruos, en cambio Megumin solo…”

Yunyun estaba sosteniéndome fuertemente. Pero súbitamente detuvo sus protestas.

Seguí su mirada y quedé sin palabras también.

“Hermana, ¡muchas de esas cosas voladoras vienen para acá! Oye ¿Se pueden comer? ¿Puedo comérmelos?”

Mientras mirábamos el enjambre de monstruos que podía obscurecer el cielo. Komekko decía algo y brincaba de alegría.

***

Siento como si hubiera estado corriendo por todos lados el día de hoy.

“Meg-Megumin, ¡Me estas lastimando! ¡La punta de mis pies se siente como si se fuera a separar!”

Yunyun se quejaba con lágrimas mientras que la llevaba en mi espalda.

“¡No seas llorona! Ya que soy un poco más bajita que tú, ¡No se puede hacer nada! Además ¿Quién te dijo que crecieras tan alta? Sí es que duele tanto, entonces camina por ti misma.”

“¡Entonces yo llevare los pies de Yunyun!”

Escapamos por el camino más obscuro. Yo tome a Komekko y cargue a la exhausta Yunyun en mí espalda.

“¡Duele! ¡Duele! Komekko ¡espera un poco! Si tu levantas mis piernas de esa manera, mi postura terminara como la de un camarón frito… ¡!”

“Estamos en una situación desesperada y aun así ¡pero qué demonios es lo que ustedes dos están haciendo! ¡No te muevas abruptamente en mi espalda! ¡O te tirare aquí para abandonarte!”

En lo que yo me quejaba, muchos monstruos que cubrían el cielo pasaron sobre nosotros.

… ¿Porque es que tantos monstruos se congregaron aquí?

Como si fuera para contestarme, varias luces y rayos de origen mágico resplandecían en el cielo uno tras de otro. Sin darnos cuenta, la cantidad de magia que veíamos se incrementó.

Tal parece que acortamos la distancia entre nosotros y los magos del pueblo.

En otras palabras, los monstruos no se congregaban, sino que eran llevados a aquí.

“Parece que los monstruos están usando esta área para reagruparse.”

“¿¡En otras palabras, los subalternos del dios demonio se están siendo concentrados en la tumba del dios demonio?! ¿Por qué están haciendo eso…?

Ahh, porque hay demasiados monstruos, así que quieren atraerlos a aquí y sellarlos a todos de una sola vez…”

… Así que eso es. Los adultos probablemente quieren sellarlos a todos juntos o simplemente usar una magia súper poderosa para destruirlos.

Si es así, entonces debemos dejar este lugar rápidamente.

Pese a que la situación es urgente.

“… Yunyun, ahora es el momento ideal para esa famosa frase. Tú puedes decir fuertemente las clásicas líneas de ‘Solo déjenme atrás. ¡Ustedes váyanse primero!’”

“No, no, no, ¡No me abandonen! ¡Megumin dijo antes que cargarme a casa seria su forma de pagar su deuda de honor!”

¡Porque tuve que decir tales palabras innecesarias…!

En medio de los monstruos horribles gruñidos se escuchaban, así que yo lo di todo para poder sacar a Yunyun y escapar.

Incluso si aprendía magia avanzada, sería imposible lidiar contra tantos monstruos.

Yo ore en silencio para que nos vieran y nos escondimos en la sombra de la lámpara de la calle.

En ese momento.

“¡Miau!”

Kuro maulló en los brazos de Komekko.

-El maullido fue muy débil, aun así, los monstruos que rondaban en el cielo volaron hacia nuestra dirección.

Tras ver el comportamiento de los monstruos ¡yo tuve una epifanía!

“¡Komekko! ¡Arroja esa bola de pelo hacia el cielo!”

“¡¿Qué estás diciendo?! Megumin, Pero ¿qué dices?”

“Me costó mucho trabajo recuperar mi comida, ¡así que no puedo simplemente tirarla!”

“Komekko ¡Tú también! ¿Pero qué estás diciendo?”

… Como es que terminamos así.

Porque no me di cuenta antes.

La razón por la que los monstruos atacaron mi casa es probablemente, por culpa de Kuro.

La vez que estábamos en el entrenamiento a campo abierto, también.

Los monstruos ignoraron a las otras estudiantes y se lanzaron contra mí, que estaba cargando a Kuro.

Recientemente, Komekko ha salido a jugar con los fragmentos del sello del dios demonio.

Komekko, repentinamente trajo a esta bola de pelos un día.

Al mismo tiempo, en el pueblo se empezaron a dar reportes de testigos que aseguraban haber visto a los subalternos del dios demonio.

Hay solo una posible conclusión a la que apuntan todas estas pistas-¡!

“Ahah, ¡que dolor de cabeza! ¡Mí cabeza! Si pienso en algo más ¡el mecanismo de auto-protección de mi cerebro se activará!”

“Oye, Megumin ¡¿pero qué tonterías estás diciendo?! Deja de evadir la realidad ¡entendido!”

Tras escuchar las palabras de Yunyun, yo recobre un poco de mi sanidad y reevalúe la situación.

Tal parece que la mirada de todos los monstruos voladores esta fija en nosotras.

Mi intención era abandonar a Kuro y correr, pero…

Con esa aura de persona importante, Komekko cargo a Kuro y dijo con una sonrisa.

“Hermana, ¡esta es una buena oportunidad! ¡Atrapemos a uno y llevémoslo a casa!”

Mire a mi hermana con esa mirada resplandeciente que tenía y baje a Yunyun de mi espalda. Mire al cielo y saque mi tarjeta de aventurero.

“¿Meg-Megumin?”

Yunyun pregunto desconcertada desde el suelo.

No muy lejos de aquí, hechizos mágicos resplandecían por el cielo.

Debo de aprender magia avanzada para poder ganar algo de tiempo.

Si yo libero un hechizo de magia avanzada aquí, entonces los adultos del pueblo correrían hacia aquí inmediatamente.

“Hermana ¿Algo anda mal? ¡Tus ojos se ven más rojos que lo usual!”

Claro que se veían rojos.

Después de todo, mis emociones estaban a flor de piel.

“Yunyun, toma a Komekko y escapa.”

Yo voltee al cielo y acumule todo el mana de mi cuerpo.

Incluso si nunca he usado magia, yo puedo controlar el flujo de mi mana, debido a la naturaleza del Clan de Magos Carmesí.

Los lacayos del dios maligno no parecen descender, probablemente porque piensan que estamos usando a Kuro como rehén.

Pero siento que no permanecerán mucho tiempo solo observando. Después de todo, ellos están un punto crítico ahora. Si se les da la oportunidad, ellos atacaran de inmediato.

-Por ejemplo, si yo usara magia- esto definitivamente crearía una apertura.

No hay problema, yo ya tomé una decisión.

“Meg-Megumin. Siento que ellos solo están observando la situación. ¡Solo esperemos a que los adultos lleguen…!”

Yo no me arrepentiré, simplemente tendré que trabajar aún más duro en el futuro.

“¡Hermana! ¡Tus ojos…!”

Aun cargando a Kuro, Komekko volteo a verme preocupada.

Yo acaricie su cabeza gentilmente y le dije que todo estaría bien.

Entonces tome mi última instancia y saque mi tarjeta de aventurero.

-Mire a mi tarjeta y quede congelada.

En ese momento, me reí fuerte e incontrolablemente.

“¿Qu-Qué sucede? Megumin, ¿¡es que finalmente te volviste loca!?”

“¡Mi hermana se rompió!”

“¡Pero que groseras! ¡qué cosas dicen las dos!”

Incluso cuando les conteste, mi mirada no dejaba la tarjeta de aventurero.

-Tengo suficientes puntos de habilidad.

Yo tengo suficientes puntos de habilidad para aprender Magia de Explosión…

## Parte 4

Incluso aunque yo misma sé que es tonto, yo aun así insisto en aprender esa magia.

“¡Mi hermana está crujiendo!”

“¡Megumin! ¡Megumin! ¿Qué te pasa? Dime ¿Qué te pasa? ¿Qué magia avanzada es la que estas usando? Cuando los demás usan magia, ¡nunca les ha ocurrido algo así! Oye espera, ¿eso es magia?”

Desde que yo era pequeña, yo he memorizado el encantamiento y he practicado recitando esta magia a diario.

La atmósfera que nos rodeaba cambio en el momento que reunía mi mana e iba recitando el encantamiento.

Conmigo en medio, la atmósfera cambio mientras se llenaba de energía estática y se volvía brumoso.

Después de todo, esta es la primera vez que uso magia. También es la primera vez que voy a usar Magia de Explosión, conocida como la magia más difícil de manejar.

Yo no podía controlar completamente todo mi poder, así que un poco se escapó a los alrededores e interfirió con el ambiente.

-Mientras que recitaba el encantamiento para la Magia de Explosión, yo iba recordando muchas cosas.

Cosas que pasaron después de que a mí me faltara solo un punto para aprender Magia de Explosión.

Después de que yo me peleara contra Yunyun en el parque, yo estrangule a un Farfetch.

En ese momento, mi nivel se incrementó y mis puntos de habilidad fueron suficientes.

Sintiendo el cambio en el ambiente, los lacayos del dios demonio rugieron en confusión.

Yo podía sentir como mi mana era consumido con cada línea del encantamiento que recitaba.

Incluso aunque yo tengo confianza en la cantidad de mana que poseo, aun así, yo sude en frío por la cantidad que usaba.

Ya que la magia de Explosión consume una gran cantidad de mana, si el mago no tiene suficiente, la magia no puede ser activada a pesar de haberse aprendido.

Las palabras del libro cruzaron por mi mente, pero como miembro del Clan de Magos Carmesí, es imposible que yo no pueda usar esta magia. Yo saque esos pensamientos negativos de mi mente y continúe con el encantamiento.

Finalmente complete el encantamiento de la magia-

Había una pequeña esfera de luz en mí mano.

… Esta echo.

Para crear esta pequeña esfera de luz, he estado trabajando tan duro desde pequeña y ahora finalmente lo he logrado.

Yo aún no poseo un báculo mágico para potenciar mi poder mágico.

Al liberar la magia de Explosión en estas condiciones, lo más probable es que su poder sea solo de la mitad de lo que normalmente seria.

Pero, aun así.

“Yunyun, Komekko, Agáchense.”

Yo aun así tenía confianza de que podía eliminar a todos estos monstruos de un solo golpe.

Yunyun acerco su debilitado cuerpo junto a Komekko y la abrazo mientras se tiraban al piso.

Parece que ella entendió lo que estaba a punto de hacer.

La esfera brillante en mi mano se sentía caliente como el fuego. La sensación del poder comprimido era reconfortante.

No hay problema. Es seguro que yo puedo controlar esto apropiadamente.

Yo continuaba animándome a mí misma, observando hacia el cielo.

La Magia de Explosión que yo siempre quise aprender.

La Magia de Explosión de mis sueños.

La Magia de Explosión que lo destruye todo.

Ni un dragón, ni un demonio, ni siquiera un dios o rey demonio, puede sobrevivir a un ataque directo.

El último recurso de la humanidad.

La escena que presencie en mi infancia, aún la podía ver vívidamente en mí memoria, tan clara como si hubiera sido ayer. Esta vez, yo personalmente-

“¡Mi nombre es Megumin! La genio número uno del Clan de Magos Carmesí, ¡Aquella que usa la Magia de Explosión! Al fin- ¡yo al fin he logrado la magia de mis sueños! ¡Nunca olvidare este día! … ¡Cómanse esto!”

Yo abrí mis dos ojos y levante la esfera en dirección al cielo, entonces exclame.

“Explosión— ¡¡!!”

La luz de mi mano salió disparada hacia el centro del enjambre de monstruos.

La luz desapareció como si fuera engullida por el abismo.

Un segundo después, unos vistosos y gloriosos fuegos artificiales florecieron en cielo nocturno-¡! 

![](capitulo_5/img/c2.jpg)

“¡Ahh! ¡Grrrraaaa!”

“¡¡–¡!”

“¡Jajajaja! Eso es. ¡Eso es lo que yo quería ver! ¡Qué explosión tan gloriosa! ¡Qué poder tan destructivo! ¡Qué desbordante sentimiento!”

Yunyun gritaba en desesperación mientras que abrazaba a Komekko.

Yo ignoraba la violenta corriente de viento y el rugido explosivo, mientras que reía con todas mis fuerzas.

La onda expansiva rompió los arboles bajo la Explosión. Yo fui arrojada al piso.

Los monstruos que se encontraban en el cielo, fueron devastados por la explosión de vientos de poder mágico y dirigido por la irresistible y abrumadora violencia. Todos ellos desaparecieron.

Yo estaba recostada en el suelo observando al cielo.

No podía moverme debido al agotamiento mágico, así que solo podía ver el humo desvanecerse después de lo sucedido.

Cuando el humo finalmente aclaro, la gran cantidad de monstruos ya habían desaparecido por completo.

“… Qué, pero qué fue… ¿Eso es la magia de Explosión…? Eso está más allá de las definiciones de ‘poderoso’ o ‘fuerte’…

Incluso sin el control y los efectos de aumento de un báculo mágico, esto es poder. No hay duda de porque se le llama la magia más poderosa… yo puedo de alguna forma, de alguna manera porque Megumin esta tan obsesionada con la Magia de Explosión.”

Tras presenciar el efecto destructivo de la Magia de Explosión, Yunyun solo suspiro.

No intento responder, simplemente me quede acostada sin moverme.

Un solo uso y agoto completamente con mi reserva de mana e incluso consumió mi fuerza física.

Después de usar esta magia, el mago se vuelve completamente inútil.

Eso significa que, para convertirme en aventurera, yo debo tener compañeros que puedan defenderme cuando quede completamente exhausta.

Siempre pensé que estaría bien por mí misma.

Pero hay cosas que no puedo hacer sola.

Debo recordar la experiencia de hoy. Yo definitivamente debo de atesorar a mis compañeros.

Escuche voces desde las lejanías. Los adultos del pueblo sonaban muy ansiosos. Yo me imaginaba la apariencia de mis futuros compañeros que aún no he conocido…

“¡¡Ahh!! ¡Mi hermana mando a volar toda la carne de pájaro!”

***

-Algunos días después.

Después de que los adultos que vieron la magia de explosión llegaron, la situación se puso bastante caótica.

Después de todo, la hija del jefe del pueblo y yo estábamos desmayadas en el suelo, mientras que Komekko se encontraba cerca de nosotras cargando a Kuro.

A mí me llevaron inconsciente a mi casa. Y al día siguiente Yunyun y yo reportamos lo que sucedió a nuestro profesor.

Lo que les dije a los adultos fue que, cuando llegamos a mi casa encontramos la puerta rota, Komekko estaba desaparecida y Yunyun me acompaño a buscar a Komekko.

Como resultado, una nueva pregunta circulo por los adultos del pueblo.

“… Oye, Megumin ¿Qué haremos ahora?”

“……”

Sin mostrar ningún tipo de expresión Yunyun me pregunto. Yo no sabía que decirle.

El profesor nos indicó que la escuela celebraría nuestra graduación la próxima semana. No era necesario que fuéramos a la escuela hasta entonces.

Así que estos últimos días nos la hemos pasado sin hacer nada en el parque cercano.

“… Oye, Megumin.”

Ya que me llamo, voltee a verla.

Ella también volteo y se me quedo viendo bastante cerca-

“… Megumin …. ¡Que es lo que vamos a hacer!”

Yo cerré mis ojos, tape mis oídos con mis manos y me agache.

“¡No es momento para pretender que no escuchaste nada! ¡Qué vamos a hacer! ¡El sr. Buzucoily dijo que el sello de la ‘La Diosa de la Venganza sin Nombre’ también se liberó! ¡El sello estaba exactamente localizado donde Megumin lanzo su hechizo! Y ahora ¡Nadie sabe dónde está la diosa que quedo libre del sello! ¿Qué vamos a hacer? Oye, ¡que es lo que vamos a hacer!”

Yunyun me sujetaba fuertemente de mis hombros para agitarme, yo seguía pretendiendo no había escuchado nada.

Aunque yo simplemente quería evitar el tema, escuche una línea que debía de ser corregida.

“Yunyun, espera un poco. La forma en que dijiste eso, puede fácilmente malinterpretarse por las personas como qué ‘Yo soy la responsable de que el sello se haya roto’.”

Le dije a Yunyun que no dejaba de molestarme.

“Pero eso no es un error ¿verdad? Sr. Buzucoily había dicho que ¡muchas cosas peligrosas estaban selladas en ese lugar! Soltar el poder de la Magia de Explosión en el cielo sobre ese lugar, ¡era obvio que los sellos se romperían!”

“Pero la gente del pueblo tiene otra explicación. El Dios Malvado que se liberó del sello peleo contra la diosa que acaba de despertar. Al final la diosa gano y acabo con todos los lacayos del dios malvado con una gran explosión. Después de eso ella voló a alguna parte…”

“¡Eso es completamente mentira! ¡Todo lo que sucedió fue culpa de la magia de Megumin!”

Ninguno de los pobladores piensa que fue Komekko la responsable de romper el sello del dios malvado.

Ni tampoco saben que yo aprendí la magia de Explosión.

Solo nuestro profesor sabe que hechizos aprendimos.

Si los pobladores se enteran que yo aprendí magia de Explosión y que Yunyun aprendió magia intermedia, ellos se sentirían muy decepcionados.

El profesor lo entendió así, por lo que acordó mantenerlo en secreto.

Tal parece que nuestro profesor encargado, el cual siempre creímos que era un inútil, se preocupa por sus estudiantes.

Y, además-

“Yunyun, el día de hoy podría…”

“¡No! ¿Porque necesitas preguntar? Las cosas finalmente se acaban de calmar, o es qué ¡¿intentas crear disturbios de nuevo?! Solo han pasado unos pocos días desde que usaste Magia de Explosión así que aún puedes resistir ¡¿Verdad?! … Incluso si tu pones esa expresión tan triste, ¡aun así es imposible! ¡Lo estoy diciendo por nuestro bien!”

Yunyun dijo eso, aunque parecía dudar un poco.

Solo tiene pocos días desde que yo probé el sentimiento de la magia de explosión.

Aun así, Yunyun me ha prohibido usarla.

La razón es que si yo libero el poder de la magia de explosión cerca del pueblo, seguramente causaría otro disturbio en el pueblo, después de que nuestro profesor lograra encubrir las cosas.

Sí, yo entiendo la razón.

Pero pese a que lo entiendo…

“Yunyun, tu deberías saber cuan tan grande es mi amor por la magia de explosión ¿verdad?”

“Si, entiendo que tu amor por la magia de explosión podría ser vista por otras personas como una manía obsesiva.”

Sí sabes que es así, entonces debe ser fácil de comprender.

“Escucha Yunyun. Para explicarte cuan tan grande es mi amor por la magia de explosión… Si yo tuviera que escoger entre ‘Usar Magia de Explosión y tener una comida simple al día’ y ‘Comer tres comidas al día con postre incluido, pero sin Magia de Explosión al día’ yo sin dudarlo elegiría comer una comida simple y usar Magia de Explosión. Tras lo cual yo me comería las dos comidas restantes más el postre. Tanto así yo amo la Magia de Explosión.”

“Eh… ¿la glotona de Megumin podría en verdad…? Eh, ¡¿Uh?! Oye, ¿podrías repetir lo que acabas de decir? ¡Presiento que acabas de decir algo bastante extraño!”

Yunyun rápidamente me interrogo de nuevo, pero es claro que entiendo que usar Magia de Explosión nos crearía muchos problemas.

Yo le rasque la cabeza a la bola de pelos que tenía en mis pies.

“Mmm… creo que podré soportarlo un poco más. Una vez que no pueda soportarlo más, yo viajare y usare la Magia de Explosión para devastar el mundo más allá del pueblo.”

“Tú, ¡sería mejor que no hagas eso! ¡No lo digas ni como broma!”

Me levanté y decidí cambiar de tema.

“Es bueno que nadie resultara herido en el incidente. Aun que lo que los pobladores crean sea diferente de la verdad, está bien si pueden aceptarlo.”

Levante a Kuro del suelo.

Yunyun miro a Kuro y me pregunto con sentimientos encontrados.

“… Oye, Megumin. ¿Qué es Kuro exactamente? ¿Por qué es que Kuro atraía a todos esos monstruos? ¿Tiene que ver con el dios malvado? Y ¿Cómo es que se rompió en primer lugar el sello del dios malvado? ¿En verdad paso lo que dice la gente del pueblo? Sobre que un turista que iba pasando nos jugó una broma…”

Yunyun no dio con la raíz del problema.

Después de todo nadie creería que una niña rompió el sello por pura curiosidad.

Si yo no hubiera hecho lo mismo de niña, yo tampoco sospecharía de Komekko.

Después de interrogarla a fondo en casa, encontré que efectivamente fue ella la que rompió el sello.

Quería llamarle la atención, pero no pude hacerlo cuando ella inocentemente tomo las piezas del sello y me pregunto si quería jugar.

El único daño que sufrió mi casa, fue la puerta de enfrente. Pero como no fue nada serio, me mantuve en silencio.

-La pregunta es, ¿qué hacer con esta bola de pelo?

“Esta bola de pelo es tan sin vergüenza. Un gatito debería de actuar más lindo.”

Kuro, al cual buscaron y cuidadosamente cargo uno de los lacayos del dios malvado.

Su verdadera identidad sea probablemente…

“Oye, Megumin. ¿Acaso lo mantendrás en tu casa? Ah, um… La expresión de Komekko era un poco…”

Yunyun dejo de hablar a la mitad de lo que quería decir.

Pero, claro que entendí a qué se refería.

“¿Qué hacer? En verdad, si lo dejo en casa, con Komekko un día de estos ella se lo comería. Pero en estos momentos, ni dárselo a alguien más, ni dejarlo ir parecen buena idea…”

Levante a Kuro con mis dos manos y lo puse a frente a mis ojos. No se resistió.

Yunyun miro a Kuro y pensó en un plan.

“¡Claro! ¿Por qué no estableces oficialmente un contrato como tu familiar? Si fuera el familiar de su querida hermana, Komekko probablemente no…”

Ella dudo mientras que hablaba.

Claro que entiendo lo que significaba.

Por mi hermanita, que vive por sus instintos, tal razonamiento no aplica.

Pero, un familiar…

“… La Maga que controla al Dios Malvado… No suena nada mal.”

“¿…? Megumin, ¿que acabas de decir?”

Parece que Yunyun no alcanzo a escuchar lo que murmuraba para mí.

“Yo decía que, dejarlo ser mi familiar no era una mala idea.”

Parece ser que me libre de esa.

Le sonreí a esta bola de pelo con grandes antecedentes.

Yunyun se relajó y soltó un suspiro de tranquilidad. En ese momento me vino esto a la mente.

“Sí, si va a ser mi familiar, ese nombre temporal no es suficiente.”

“¡¿Eh?! ¡¿No puedes usar Kuro como su nombre oficial?!”

“No, para el tener ese nombre tan ‘simplón’ sería demasiado triste.”

“¡Nombre ‘simplón’!”

Ignore a Yunyun que quedo impactada. Debo de pensar muy bien su nombre.

En ese momento Kuro de repente giro su cuerpo.

Como si digiera, ‘Kuro está bien’.

“Mira, a Kuro parece gustarle su nombre. Y, además, aún es un gatito, cambiar su nombre de repente podría confundirlo.”

Yunyun insistía en que el nombre que le dio era mejor. Pero yo ya había pensado en un buen nombre.

“¡Esta decidido!”

Les dije con confianza.

Al contrario de Yunyun que parecía preocupada.

“Oye, Megumin. Kuro es una gata, ¿entendiste? Considera eso primero y dale un nombre lindo…”

Yo la interrumpí.

Mire a mi familiar que estaba frente a mí y le anuncie.

“-Tu nombre será Chomusuke. Así es Chomusuke.”

El familiar que usualmente se porta arrogante y que posiblemente tenga una muy terrible forma verdadera-

Se sacudió como nunca antes.
