# Extra

## Como Pasar un Solitario Día de Descanso

“Ohh, ¿Acaso no se trata de Megumin? ¿Qué pasa con ese nivel de acoso? ¿En verdad te preocupa tanto Yunyun?”

“¡¿?!”

Al ser llamada repentinamente desde la espalda, yo di un salto de la sorpresa y voltee a ver de quien se trataba.

“…Ahh, solo eres tú Arue, ¡no me sorprendas de ese modo! Además, yo no estoy acosándola, simplemente observo a mi autoproclamada rival.”

Parada atrás de mí, se encontraba mi compañera de clases con el parche en el ojo, Arue.

***

-Hoy no teníamos escuela.

Salí a caminar por la calle ya que tenía tiempo libre y me encontré con Yunyun, quien también parecía tener mucho tiempo libre, caminando distraídamente, así que decidí seguirla.

“Estoy bastante segura de que a eso es lo que se llama ‘acosar’. Bueno, yo también tengo tiempo libre, así que, ¿Te molestaría si me uno a ti?”

Y así fue como Arue y yo comenzamos a observar el día de descanso de nuestra compañera de clases sin amigos.

-Una señorita agito su mano en dirección de Yunyun, quien se había detenido a descansar al lado del camino en una banca y mostraba una gran sonrisa.

En respuesta, Yunyun, tras un momento de sorpresa, mostró una sonrisa rígida y le regreso el saludo…

“Lo siento mucho ¿llevas mucho esperándome?”

“En realidad no, yo también acabo de llegar.”

Un hombre que se encontraba por ahí se le acerco a la señorita.

Al ver eso, Yunyun rápidamente desvió su cara enrojecida.

Aquella señorita no venía a encontrarse con Yunyun, sino que con el hombre que estaba tras de ella.

Ya que ellos dos se sentaron al lado de Yunyun para conversar, Yunyun murmuro ‘¿Por qué se tarda tanto…? ¿Debería de ir yo a buscarla…?’ y otras frases similares. Entonces, ella dejo el área rápidamente.

“…Megumin, ¿Te molestaría explicarme que es lo que ella decía para sí misma?”

“Ella quería alejarse en el momento en el que aquella señorita llego, pero también quería evitar arruinar el habiente entre esos dos. Probablemente se trate de un pequeño acto que hizo en consideración.”

“Ahh, ¿Qué será esta sensación de hormigueo que siento en mi pecho?”

***

-Por la tarde.

Pensé que Yunyun iría a la aldea para comer, pero en ese momento, ella caminaba en círculos frente a la entrada del merendero, en vez de entrar.

“¿Pero qué es lo que ella hace? Megumin ¿te molestaría explicarme?”

“Ella probablemente quiere entrar, pero tal parece que en estos momentos hay muchos clientes y el comedor se encuentra bastante ocupado, así que ella está siendo considerada y espera afuera. Una vez que los clientes disminuyan y los empleados estén más libres, estoy segura que ella… Ahh, ¡ella ha entrado!”

Yunyun finalmente entro al comedor al ver que un gran grupo de clientes se marchaba.

“En vez de pasar por el problema de esperar a fuera, ella simplemente debió de haber ido a otro lado.”

“Probablemente se deba a que ella comerá sola, así que quiere hacerlo en un lugar con mucha gente, pero por otra parte, ella es una cobarde considerada que duda en entrar a dichos lugares, así que muy probablemente se contenía.”

A lo lejos, vi a Yunyun sentarse frente a la barra del comedor y silenciosamente comer lo que ordeno.

Ya que los clientes regulares y el dueño, reían fuertemente por la charla, Yunyun mostraba una sonrisa forzada para evitar alterar el ambiente.

“…No puedo ver esto más, me lastima.”

“Ella aun es más desafortunada que esto. Si tan solo al ver hasta aquí te hace decir eso, ¿estas segura de que podrás manejar lo que sigue?”

***

-Tras terminar de comer, Yunyun regreso a pasear sin algún destino en mente, por toda la aldea.

“¿Hmm? ¿No esa nuestra compañera de clase?”

“Ohh, tienes razón, se trata de Nerimaki. Tal parece que ella se encuentra en medio de su práctica de poses.”

Nerimaki practicaba celosamente sus poses características en un área abierta en medio de la aldea.

Ella rápidamente noto la presencia de Yunyun y fue a conversar algo con ella.

“…Tal parece que ella quiere que Yunyun le ayude a practicar.”

Tras recibir algunas instrucciones de parte de Nerimaki, Yunyun modelo algunas poses.

Tras un breve momento de consideración, Nerimaki le dio algunas instrucciones más.

Tal parece que ella estaba en el medio de desarrollar una nueva pose personal.

Finalmente se decidió en una pose que le agrado y entonces la practico por ella misma.

“Así que entonces, ella no rechazaría una petición, ¿aunque a ella le desagrade?”

“Tal parece que ella es el caso típico, de las que serán fácilmente engañadas por hombres malintencionados. Estoy segura de que, siempre y cuando se postren ante ella para suplicarle, ella incluso accedería a salir con algún viejo sospechoso… Ohh, tal parece que se retiran ahora mismo.”

Yunyun, cuyo rostro se encontraba completamente ruborizado de la vergüenza, se despidió y entonces se dirigió a otra parte.

***

-Entonces, ella, quien había caminado distraídamente e indefensa todo este tiempo, repentinamente se comportó alerta de lo que la rodeaba.

Por un momento, yo pensé que habíamos sido descubiertas, pero tal parece que ese no era el caso.

Tras confirmar que no había nadie alrededor, ella se arrodillo frente a un arbusto.

Entonces, ella esparció lo que parecían ser los sobrantes de su almuerzo-pedazos de pescado frito-por todo el piso.

Al mismo tiempo, el arbusto se agito un poco y de él salió un gato.

“…Acaso ¿ella lo alimenta? Eso es lindo de su parte.”

“No espera un poco. Mira, ella parece estar diciéndole algo…”

Ocultándonos en un arbusto cercano, nosotras nos quedamos en silencio y escuchamos atentamente a lo que Yunyun le decía.

“…Ella, ella acaba decirle ‘¿Podemos ser amigos?’ a ese gato. Megumin, yo simplemente no puedo soportar seguir viendo eso. ¿No podemos simplemente salir ahora y jugar con ella?”

“¡E-espera! Si salimos ahora, ¡entonces se dará cuenta de que lo escuchamos todo! Una vez lo sepa, estoy bastante segura de que ella no dudara en cometer suicidio de la pena. Simplemente pretendamos no haber escuchado nada y retirémonos por… ¡Ahh!”

El gato con el cual Yunyun había estado hablando, rápidamente tomo el pescado y regreso al arbusto de donde había salido.

Hablando con honestidad, tan solo de verla, yo sentía la necesidad de llorar.

“Megumin… ¡Megumin-!”

“Ya lo sé, ¡Ya lo vi por mí misma! Deja de agitarme de los hombros. De momento salgamos de aquí, ¡Para poder regresar desde otro lado! Entonces podremos pretender que la encontramos de casualidad y jugaremos con ella por un rato, ¿está bien…?”

Y justo en el momento en el que estábamos por decir ‘vámonos’-

“¿Ohh? Oye, es Yunyun.”

“En verdad es ella. ¿Qué es lo que haces aquí?”

“Ah-, Funifura-san, Dodonko-san, ¡b-buenas tardes! Verán, ahí un gato que se oculta en un arbusto de por ahí… dejando eso de lado ¿Qué es lo que ustedes hacen aquí?”

Viendo que les voltearon la pregunta, las dos cruzaron miradas por un breve momento.

“Bueno, como puedes ver, Dodonko y yo pensábamos jugar un poco de bádminton.”

“Mhm, es verdad. Ahh, si estas libre Yunyun, ¿Por qué no te nos unes?”

El rostro de Yunyun pareció iluminarse ante esta invitación, pero entonces su mirada se desvió hacia las ‘dos’ raquetas…

“Y-yo… lo siento mucho, tengo algo más que hacer…”

Tras decir eso, ella se alejó dando largos pasos, completamente desanimada y con su cara viendo al piso…

“Oye ¡Explícame lo que Yunyun acaba de hacer!”

“Ella estaba súper feliz al ser invitada a jugar, pero entonces, ella se dio cuenta de que si se les unía, no habría suficientes raquetas. A pesar de la cantidad de tiempo libre que ella tiene, ella decidió ser considerada, y así…”

Antes de que pudiera terminar mi explicación, Arue salto del arbusto donde nos ocultábamos.

Claro está, yo la seguí.

“¡Ustedes tres! ¡¿No les gustaría jugar un juego para 5 personas con nosotras?!”
