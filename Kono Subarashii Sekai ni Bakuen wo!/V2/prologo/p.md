# Prólogo


![](prologo/img/c1.jpg)

![](prologo/img/c2.jpg)

Mi hermana se volvió independiente recientemente así que ya no duerme conmigo. Pero inesperadamente ella se metió bajo mis cobijas el día de hoy.

“-Pero entonces el joven dijo, “Si yo tengo una ‘ventaja’, entonces no tengo la necesidad de compañeros. Yo seré un jugador solitario y me quedare con todo el botín. Jugador solitario, ¡Hurra!” Y de hecho, él es lo suficientemente poderoso para hacer eso…”

El objetivo de Komekko quizás haya sido que le leyera un libro de ilustraciones.

Ella abrazaba a la ración de comida de emergencia de nuestra familia, Chomsuke y se acostó aun lado mío.

“¿Hermana, que es una ‘ventaja’?”

Komekko me pregunto mientras trataba de abrir sus cansados ojos.

“Tener una ‘ventaja’ puede significar ‘hacer trampa’ o ‘ser un pillo’. Es un término frecuentemente ocupado por aquellos que tienen nombres extraños.

Básicamente, es un poder tan fuerte que debería ser considerado que es contra las reglas.”

“Oh.”

Komekko siguió escuchándome atentamente de nuevo y yo seguí leyéndole.

“El joven era muy poderoso y continuo repetidamente venciendo a los lacayos del rey demonio por si mismo.”

Esta es una muy famosa-

“El Rey Demonio en su desesperación se dio cuenta de que no le sería posible vencer al joven en un encuentro directo. Entonces ¿Cómo vencerlo? El Rey Demonio entonces se dio cuenta de que el joven nunca se unía a ningún equipo.”

-Bien conocida leyenda de hace mucho tiempo.

“Los sirvientes del Rey Demonio le dijeron al joven, ‘¡Un solitario y sin amor héroe es una broma! Los héroes normalmente cooperan con sus compañeros para superar las dificultades y entonces ¡vencer al Rey Demonio! Tu ni siquiera tienes compañeros, así que ¿Por quién y porque peleas contra nosotros? De ser así porque mejor no te nos unes. Nosotros tenemos muchos beneficios.’ Los sirvientes le dijeron entonces que regresara una vez que lo pensara mejor. Y el joven se marchó obedientemente.”

Finalmente, ya bastante adormilada, Komekko descanso su cabeza en mis hombros mientras yo seguía leyendo el libro.

“Finalmente, el joven se lanzó a atacar el castillo del Rey Demonio de nuevo y les dijo a los sirvientes ‘Yo no soy solitario, yo soy un solista. Y no es que no tenga amigos, solo que no los necesito, esto es porque sé que los compañeros solo se volverán una responsabilidad… además ¡qué beneficios! ¡Ustedes creen que me engañaran tan fácil! ¡Un trato con el Rey Demonio nunca podría terminar bien! ¡Yo peleo por la humanidad y por la paz! No desperdiciare más el tiempo con ustedes, mi meta es ¡la cabeza del Rey Demonio! Por esta vez los dejare ir, ¡piérdanse!’ Eso fue lo que joven les dijo a los sirvientes mientras que los señalaba. Los sirvientes le respondieron ‘Si hubieras dicho eso, la primera vez, entonces te hubieras escuchado genial’ -Los sirvientes fueron destruidos al final.”

Para este momento Komekko estaba usando mi brazo como almohada, respirando suavemente.

Con mucho cuidado de no despertar a Komekko continúe leyendo ese familiar libro para mí misma.

“El valiente joven entro hasta lo más profundo del castillo del Rey Demonio. Nada podía detenerlo. Hasta que finalmente el llego frente al Rey Demonio-”

![](prologo/img/c3.jpg)

![](prologo/img/c4.jpg)
