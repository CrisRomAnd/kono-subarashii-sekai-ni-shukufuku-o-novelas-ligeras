# Interludio 2: Komekko y el Señor Hoost

“Oh, llegas un poco tarde el día de hoy. Vamos continúa resolviendo el rompecabezas.”

Como de costumbre, Hoost estaba sentado frente a la tumba del dios malvado.

“Pensé que solo necesitabas resolver un rompecabezas para romper el sello. Y pensar que habían más… Hey, Komekko, el sello de la Señora Wolbach debe de ser liberado el día de hoy. Te lo encargo mucho Komekko.”

“Bien.”

Resolví un rompecabezas antes y otro más difícil apareció en el pedestal.

“Mira, te traje mucha comida deliciosa el día de hoy. Básicamente, di caza a las criaturas que había fuera de la villa y las cociné. No es que pueda entrar a la villa a comprar ingredientes, después de todo. Así que confórmate con esto.”

“…(Inhala)”

“… ¡¿Debemos continuar así hasta que el sello sea roto?! Por cierto, ¿Por qué vienes a aquí? ¿A dónde va tu madre? ¿Es que no tienes amigos?”

“Mamá usualmente no está en casa. No hay nadie en la villa de mi edad, así que no tengo amigos. Y ya que no tengo tampoco juguetes, vengo aquí a jugar.”

“… ya veo. Bueno, hasta que tu liberes el sello, yo te haré compañía y hablare contigo. Este nuevo rompecabezas es más difícil, así que probablemente tome más tiempo. Ya que la villa se ha puesto más vigilante, no es posible que venga todos los días. Pero cuando venga definitivamente te traeré más comida.”

Después de decir eso, Hoost miro mí mano…

“¿No hay mucho progreso?”

“Sí.”

Hoost me miro, yo pasaba mi saliva y me pregunto.

“… ¿Quieres esto?”

“… Sí.”

Así que descubrió que estaba robándole miradas a la carne que cocinaba. Hoost me lanzo una sonrisa malvada.

“Para un demonio, un contrato es algo absoluto que debe cumplirse. Te dije esto antes, debes mostrar progreso con el rompecabezas para que te de la comida.”

Dejé de mover las piezas del rompecabezas y volteé a ver a Hoost.

“Hoost es tan genial.”

“Aun que me halagues, no te la daré.”

“… No he comido alimento solido por más de tres días.”

“¡Tú te comiste la criatura que cace ayer! Es inútil no te la daré inclusive si tú lloras y suplicas.”

“Yo me convertiré en el Archí-Mago que eventualmente superara a mi hermana. Así que es mejor que no me hagas enojar…”

“¿Dónde es que una chiquilla como tú aprendió ese discurso?… ¡Cómo demonio que soy, no seré amenazado por ti!”

Dijo Hoost mientras reía.

“… no pienso claramente por el hambre, así que por favor dame comida. Te lo suplico, señor Hoost.”

“¡Ahh! En verdad, ¡no puedo contigo! Vamos, come rápido, Komekko.”

Aunque la comida que Hoost preparo no tenía ninguna sazón, aun así, era deliciosa.

Después de que yo comiera una gran pieza de carne, él me dijo.

“Bueno, ya hemos dejado claro quién es el amo y quién es el siervo. Y ya que estas satisfecha, ahora se buena niña y escucha al señor Hoost. Resuelve el rompecabezas para romper el sello.”

“… estoy muy llena, quiero dormir.”

“¡Tú-! ¡No bromees! Tú estabas comiendo tan energéticamente apenas hace un momento y ahora sin vergüenza ¡dices eso! Hey, hey, hey, ¡Komekko!”

“Porque comí mucho, no tengo ganas de hacer nada.”

“… Por-, por favor, sea más responsable, Señorita Komekko.”

“¡Ahh! En verdad, ¡no puedo contigo!”

“A pesar-, aunque yo haya dicho lo mismo antes, ¡Tu no deberías de copiar las frases de otras personas!”
