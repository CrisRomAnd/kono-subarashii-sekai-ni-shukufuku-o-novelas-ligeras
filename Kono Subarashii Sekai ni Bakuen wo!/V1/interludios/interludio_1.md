# Interludio 1; El Rompecabezas Insoluble y el Sello del Dios Demonio
 
 Llegue al patio de juegos que frecuentemente visito.

Inclinarse frente a la tumba es…

“¡Qué goblin tan grande!

“… Hey, chiquilla, no me compares con mísero goblin.”

“No soy una chiquilla. Mi nombre es Komekko.”

“¿Ese es tu nombre?… Hey, Komekko, ¿Qué es lo que haces aquí? Esta es la tumba donde la otra mitad de la Diosa Malvada, la Señora Wolbach, se encuentra sellada. ¿Es que tu familia no te advirtió que no te acercaras a este lugar?”

“Lo hicieron, pero mi hermana me dijo que los Miembros del Clan de Magos Carmesí no deben ceder a demandas irrazonables.”

“… ¿es así? Que problemático, pensar que no tengo otra alternativa más que eliminar a esta chiquilla…”

Extendiendo un par de alas como de murciélago, el no-goblin grande y negro se encogió de hombros.

“¡No soy una chiquilla! Soy Komekko. Señor no-goblin ¿Qué hace usted aquí?”

“Señor no-goblin… Hey, ¡Chiquilla, mira detenidamente! Tengo grandes cuernos y alas ¡La marca del mal! Además, los goblins ¡No tienen un cuerpo tan musculoso como el mío! Yo soy el ayudante de la Diosa Maligna la Señora Wolbach. Soy el Demonio Mayor el Señor Hoost. ¡Más te vale recordarlo!”

“¡Tan Genial!”

Viendo a Hoost con sus alas extendidas, levanto mis brazos y lo animo.

“Oh, ohhh, esto es… tienes buen gusto. Normalmente silenciaria a cualquier testigo, pero te dejare ir como una excepción. Con la condición de que no le digas a nadie acerca de mí. Cualquier cosa que pase aquí, debe permanecer en secreto. Considera esto como un trato especial y agradécelo.”

“Muchas gracias.”

Aunque no sé muy bien que está pasando, le agradezco de cualquier manera. Me siento frente a Hoost y le doy palmaditas a su gran pierna. “¡Pat! ¡Pat! ¡Pat!”

“Pero que extraña chiquilla… No importa, tengo cosas importantes que hacer. No me molestes ¿Has entendido?”

Hoost volteo su rostro a otra dirección y empezó a entretenerse con algo frente a la tumba… ¡Ah!

![](interludios/img/in1.jpg)

“¡Ese es mi rompecabezas!”

“¿Eh? Esto no es tuyo. Este es un objeto precioso que puede romper el sello de la Señora Wolbach… ¡Hey, hey, hey, tu…!”

Junto la piezas del rompecabezas. Hoost se muestra sorprendido por alguna razón.

“¡Tú eres bastante competente! Mientras me ocultaba de los pobladores, no pude resolver el rompecabezas inclusive después de varios meses… Muy bien, ¡Ahora sé qué debo hacer! Hey, ¡Dame eso!”

“Ahaha, ¡Hoost me arrebato las piezas!”

“Eh, no me importa lo que digas. Pero no menciones mi nombre con tanta familiaridad, chiquilla. Tu deberías llamarme ¡Señor Hoost! Ahora, la Señora Wolbach podrá recuperar todo su poder. A partir de ahora ella podrá destrozar todo a su paso como antes… Uh, que pasa con esto, es extraño…”

Después de arrebatarme la piezas del rompecabezas Hoost trato de acomodarlas en el pedestal frente a la tumba, pero fallo repetidamente. Finalmente se encogió de hombros y volteo a verme.

“…Hey, Komekko, haré una excepción especial y te dejare jugar con este rompecabezas. Tu puedes continuar.”

“Tengo hambre, así que no quiero jugar con el rompecabezas ahora. Señor Hoost tómese su tiempo con él.”

“…Aunque me avergüenza decir esto, puedes dejar de decirme Señor Hoost. Yo encontrare comida para ti, así que por favor ayúdame a resolver el rompecabezas, Komekko.”

“…….”

“…Por favor, Komekko.”

“Entendido.”

Tras escuchar mi respuesta, Hoost batió de mala gana sus alas y se fue volando a algún lado.

Mientras lo veía irse volando, levante las piezas del rompecabezas una vez más-
