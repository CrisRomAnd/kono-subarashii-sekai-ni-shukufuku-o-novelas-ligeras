## Parte 6

Después de que la fiesta de despedida termino, todo mundo se marchó.

Yunyun me dijo que me acompañaría a casa.

¿Es acaso que me está tratando como a una niña que se perdería fácilmente?

En el oscuro camino de regreso.

Yunyun me dijo suavemente.

“Megumin, ¿Tu regresaras al pueblo después de que te encuentres a la Onee-san encapuchada?”

Ella tartamudeara ligeramente, como si se esforzara para que sonara animada.

Yo le conteste.

“No. Yo no regresare al pueblo. Después de salir al mundo, yo debo de volverme súper fuerte junto con mi equipo. Yo quizás incluso derrote al rey demonio actual y después me convierta en el siguiente Rey Demonio. Cuando eso suceda, yo dejare que Yunyun se convierta en una líder del ejercito del rey demonio.”

“¡Yo no quiero eso! ¡¿Por qué te tienes que convertir en la villana?! De cualquier forma, eso es imposible ya que solo puedes usar magia de explosión.”

Yo no quería escuchar esos comentarios tan realistas.

“… Yo partiré del pueblo mañana temprano, así que si quieres despedirte, levántate temprano.”

“¡¿Por qué haría algo como eso?! Oye, ¿en verdad te vas mañana? ¡Komekko aún es muy pequeña…! Si se trata de ella estará probablemente bien…”

“Esa niña es más independiente que yo. Y ya hice arreglos con los vecinos. Además, mis padres no están siempre fuera de casa.”

Comparado con eso.

“¿Puedo dejarte esta bola de pelo a ti? Yo aún siento que no sería buena idea llevarla conmigo.”

Chomsuke aún se encontraba pegada en mi hombro y parecía renuente a despegarse. Yo trataba de pasársela a Yunyun.

“… Incluso después de que tú le pusiste ese nombre, tu ahora quieres separarte de ella…”

Yunyun acaricio a Chomsuke con simpatía.

“Además, ¿no es muy cercana a ti, Megumin? Y yo siento que hay pocos lugares más peligrosos que el hogar del Clan de Magos Carmesí. Tu talvez debas de llevarla contigo.”

“… Bueno, ella puede actuar como la carnada o como suministro de emergencia, así que no es como si me fuera inútil…”

“¡No lo hagas! ¡¿Porque tus ideas siempre son salvajes y viciosas?!”

Mientras que discutíamos, mi casa estuvo a la vista.

Si es que Yunyun no vendrá mañana, entonces deberíamos de despedirnos ahora.

“Yunyun, ¿tú te convertirás en la jefa del pueblo después de que aprendas magia avanzada?”

“Sí, pero no lo hare inmediatamente después de que la aprenda. Yo creo que será en un futuro aún distante…”

Por tanto…

Yunyun parecía preocupada mientras que ella murmuraba algo.

Ya que ella carecía del coraje para hablar, ella intento varias veces decir algo pero se rindió a medio camino.

¿Qué es lo que quería decir?

… Yo llegue a la puerta de mi casa.

“Entonces, Yunyun. Como mi auto-proclamada rival, por favor trabaja duro. Si tu te tardas demasiado, yo me convertiré en la Reina Demonio que reinara sobre todo el mundo. Y será demasiado tarde si tú quieres volverte una de las líderes en ese momento.”

“¡Yo nunca querré algo como eso! Además, si Megumin se convierte en la Reina Demonio, ¡Yo te derrotare!”

Yunyun refunfuño como siempre.

Ya parada frente a mi puerta, yo me sentí aliviada al escucharla decir eso.

“Entonces adiós.”

“… Sí, adiós.”

Yo le di una despedida bastante normal a Yunyun.

Aún sentía su mirada puesta en mí, mientras que volteaba.

Yo abrí la puerta.

***

“¡Bienvenida a casa, hermana! ¡Comamos la cena!”

Komekko corrió hacia donde estaba. Yo me separare de ella por un tiempo.

¿Sera que mi hermanita llore?

¿Qué es lo que hare si ella se pone a llorar y me ruega de que no me vaya?

“Komekko, tengo que decirte algo antes de que cenemos.”

“¿?”

Yo me senté justo frente a ella.

En ese momento, ella se sentó firmemente sobre el suelo.

Y le dije a Komekko que se veía desconcertada.

“Komekko. Yo saldré mañana de aventura.”

“Muy bien.”

…

“Komekko, es un viaje de aventura. Tu hermana partirá de viaje. Y por supuesto, no regresara por un tiempo. Por eso no podrás ver a tu adorada hermana por un largo tiempo.”

“¡Lo sé! ¡yo lo soportare!”

Que niña tan fuerte.

“Sí tú te sientes triste, tu puedes intentar hacer que me quede. Claro, yo ya me decidí, así que no me persuadirán. Por lo que sería inútil intentarlo.”

“¡Yo lo entiendo! ¡No intentare algo tan inútil!”

“Komekko. Tu hermana está feliz de que seas tan fuerte, pero también me lastima un poco.”

“Mi hermana es tan tsundere.”

“¡¿?!”

-Después de bañarme con mi hermanita, yo la invite a que nos durmiéramos juntas.

“Mi hermana es una chiquilla testaruda.”

“¡¿Ko-Komekko?! ¡¿Dónde aprendiste términos como tsundere y chiquilla testaruda?!”

“De nuestro vecino, Buzucoily.”

“De ese maldito NEET he.”

Yo debo darle su merecido antes de que parta mañana.

Komekko hábilmente hizo su cama en la sala. Después de que me metiera bajo la cobija, ella ya no dijo más ‘chiquilla testaruda’ y plácidamente se durmió conmigo.

Pero ¿qué sucedió? Yo siento que nuestros roles se cambiaron.

Como la hermana mayor, yo deseaba que ella atuviera algo testaruda.

En el cuarto oscuro.

Yo sostuve fuertemente su mano, bajo la cobija. Komekko me regreso el apretón.

“Komekko. Si algo sucede cuando yo no esté aquí, avísale a los adultos cercanos inmediatamente.”

“Sí, lo sé.”

Parte 6

Después de que la fiesta de despedida termino, todo mundo se marchó.

Yunyun me dijo que me acompañaría a casa.

¿Es acaso que me está tratando como a una niña que se perdería fácilmente?

En el oscuro camino de regreso.

Yunyun me dijo suavemente.

“Megumin, ¿Tu regresaras al pueblo después de que te encuentres a la Onee-san encapuchada?”

Ella tartamudeara ligeramente, como si se esforzara para que sonara animada.

Yo le conteste.

“No. Yo no regresare al pueblo. Después de salir al mundo, yo debo de volverme súper fuerte junto con mi equipo. Yo quizás incluso derrote al rey demonio actual y después me convierta en el siguiente Rey Demonio. Cuando eso suceda, yo dejare que Yunyun se convierta en una líder del ejercito del rey demonio.”

“¡Yo no quiero eso! ¡¿Por qué te tienes que convertir en la villana?! De cualquier forma, eso es imposible ya que solo puedes usar magia de explosión.”

Yo no quería escuchar esos comentarios tan realistas.

“… Yo partiré del pueblo mañana temprano, así que si quieres despedirte, levántate temprano.”

“¡¿Por qué haría algo como eso?! Oye, ¿en verdad te vas mañana? ¡Komekko aún es muy pequeña…! Si se trata de ella estará probablemente bien…”

“Esa niña es más independiente que yo. Y ya hice arreglos con los vecinos. Además, mis padres no están siempre fuera de casa.”

Comparado con eso.

“¿Puedo dejarte esta bola de pelo a ti? Yo aún siento que no sería buena idea llevarla conmigo.”

Chomsuke aún se encontraba pegada en mi hombro y parecía renuente a despegarse. Yo trataba de pasársela a Yunyun.

“… Incluso después de que tú le pusiste ese nombre, tu ahora quieres separarte de ella…”

Yunyun acaricio a Chomsuke con simpatía.

“Además, ¿no es muy cercana a ti, Megumin? Y yo siento que hay pocos lugares más peligrosos que el hogar del Clan de Magos Carmesí. Tu talvez debas de llevarla contigo.”

“… Bueno, ella puede actuar como la carnada o como suministro de emergencia, así que no es como si me fuera inútil…”

“¡No lo hagas! ¡¿Porque tus ideas siempre son salvajes y viciosas?!”

Mientras que discutíamos, mi casa estuvo a la vista.

Si es que Yunyun no vendrá mañana, entonces deberíamos de despedirnos ahora.

“Yunyun, ¿tú te convertirás en la jefa del pueblo después de que aprendas magia avanzada?”

“Sí, pero no lo hare inmediatamente después de que la aprenda. Yo creo que será en un futuro aún distante…”

Por tanto…

Yunyun parecía preocupada mientras que ella murmuraba algo.

Ya que ella carecía del coraje para hablar, ella intento varias veces decir algo pero se rindió a medio camino.

¿Qué es lo que quería decir?

… Yo llegue a la puerta de mi casa.

“Entonces, Yunyun. Como mi auto-proclamada rival, por favor trabaja duro. Si tu te tardas demasiado, yo me convertiré en la Reina Demonio que reinara sobre todo el mundo. Y será demasiado tarde si tú quieres volverte una de las líderes en ese momento.”

“¡Yo nunca querré algo como eso! Además, si Megumin se convierte en la Reina Demonio, ¡Yo te derrotare!”

Yunyun refunfuño como siempre.

Ya parada frente a mi puerta, yo me sentí aliviada al escucharla decir eso.

“Entonces adiós.”

“… Sí, adiós.”

Yo le di una despedida bastante normal a Yunyun.

Aún sentía su mirada puesta en mí, mientras que volteaba.

Yo abrí la puerta.

***

“¡Bienvenida a casa, hermana! ¡Comamos la cena!”

Komekko corrió hacia donde estaba. Yo me separare de ella por un tiempo.

¿Sera que mi hermanita llore?

¿Qué es lo que hare si ella se pone a llorar y me ruega de que no me vaya?

“Komekko, tengo que decirte algo antes de que cenemos.”

“¿?”

Yo me senté justo frente a ella.

En ese momento, ella se sentó firmemente sobre el suelo.

Y le dije a Komekko que se veía desconcertada.

“Komekko. Yo saldré mañana de aventura.”

“Muy bien.”

…

“Komekko, es un viaje de aventura. Tu hermana partirá de viaje. Y por supuesto, no regresara por un tiempo. Por eso no podrás ver a tu adorada hermana por un largo tiempo.”

“¡Lo sé! ¡yo lo soportare!”

Que niña tan fuerte.

“Sí tú te sientes triste, tu puedes intentar hacer que me quede. Claro, yo ya me decidí, así que no me persuadirán. Por lo que sería inútil intentarlo.”

“¡Yo lo entiendo! ¡No intentare algo tan inútil!”

“Komekko. Tu hermana está feliz de que seas tan fuerte, pero también me lastima un poco.”

“Mi hermana es tan tsundere.”

“¡¿?!”

-Después de bañarme con mi hermanita, yo la invite a que nos durmiéramos juntas.

“Mi hermana es una chiquilla testaruda.”

“¡¿Ko-Komekko?! ¡¿Dónde aprendiste términos como tsundere y chiquilla testaruda?!”

“De nuestro vecino, Buzucoily.”

“De ese maldito NEET he.”

Yo debo darle su merecido antes de que parta mañana.

Komekko hábilmente hizo su cama en la sala. Después de que me metiera bajo la cobija, ella ya no dijo más ‘chiquilla testaruda’ y plácidamente se durmió conmigo.

Pero ¿qué sucedió? Yo siento que nuestros roles se cambiaron.

Como la hermana mayor, yo deseaba que ella atuviera algo testaruda.

En el cuarto oscuro.

Yo sostuve fuertemente su mano, bajo la cobija. Komekko me regreso el apretón.

“Komekko. Si algo sucede cuando yo no esté aquí, avísale a los adultos cercanos inmediatamente.”

“Sí, lo sé.”

Mi hermanita es muy confiable, pero aún es pequeña.

Yo debo de encargársela a mis vecinos mañana.

“Y aunque él no es confiable, tu puedes discutir algunas cosas con Buzucoily, ya que el básicamente esta libre todos los días.”

“Lo sé. ¡Yo lo buscare cuando salga por comida!”

¿Sera que ese NEET tiene esa capacidad?

“Si te sientes sola, también puedes ir a la casa de Yunyun. Ella felizmente se encargará de ti. O más bien, ella se deprimirá si se queda sola por mucho tiempo, así que visítala de vez en cuando.”

“Muy bien. Lo entiendo…”

La voz de Komekko se iba volviendo más suave. Tal parece que el sueño la iba venciendo.

“… Dejare una nota para nuestros padres. Cuando regresen, recuerda dársela a ellos. Yo ya les había dicho antes que en cuanto juntara suficiente dinero me iría, así que ellos no se preocuparan, aunque no me vean.”

“Bien…”

Komekko sonaba como si estuviera a punto de dormirse.

Yo la abrazaba mientras que se iba durmiendo.

Quizás yo también tenga hermanitis.

Así que, quizás deba de tomar esta oportunidad para almacenar algo de poder de hermanita.

… En ese momento, Komekko dijo en la oscuridad.

“Hermana.”

“¿…? ¿Qué pasa?”

Mientras que aun la abrazaba, Komekko fuertemente me abrazo de regreso y me dijo suavemente.

“Regresa pronto.”

… No me importa tener hermanitis.

Yo abrase a Komekko hasta la mañana.

-Al día siguiente.

Yo me levante silenciosamente de la cama, con mucho cuidado para no despertar a Komekko quien aún dormía.

Me puse la túnica que me regalo Yunyun. Después me puse el parche para ojo que me dio Arue y me puse frente al espejo.

…Eh, nada mal. Este parche para ojo luce bastante bien.

Seguido de eso, me puse el sombrero y tomé mi báculo.

“Hermana, ¿a qué estás jugando?”

![](cap_2/img/c3.jpg)

Mientras hacia una pose frente al espejo, Komekko salió de repente y la imito.

Después de desayunar, yo revise cuidadosamente mi equipaje. Tal parece que no me faltaba nada. O más bien, había casi nada que pudiera llevarme de viaje desde un principio.

Cuando estaba a punto de dejar la casa.

“Hermana, ¡Olvidas algo!”

Había revisado a conciencia, pero aun así Komekko corrió hacia mí con algo en sus manos.

“¿Qué es?”

“¡Comida empaquetada!”

Komekko puso algo del tamaño de un ladrillo en mis manos.

… Tal parece que ella me preparo algo para almorzar.

Había muchas bolas de arroz adentro.

Al ver como Komekko hacia esto por mí justo antes de que partiera. Me hacía dudar sobre si debería cancelar el viaje y quedarme a vivir felizmente junto a Komekko.

“Además, te daré esto, de esa forma la chiquilla testaruda de mi hermana no se sentirá sola.”

Komekko me dijo algo bastante rudo, mientras que me daba el libro de ilustraciones que me hacía leerle frecuentemente antes de acostarse…

Ese debería de ser su tesoro.

Yo tenía una sonrisa amarga, mientras que guardaba el libro entre mis cosas.

Komekko vio lo que hacía y sonrío satisfecha.

“Hermana, ¡trabaja duro! ¡Debes de convertirte en la más fuerte!”

“… Lo sé. Yo te prometo que, ¡Un día me convertiré en la maga más poderosa!”

Después de actuar genial frente a mi hermanita, yo agite la capa de la túnica que me dio Yunyun.

Esto no esta tan mal.

Yunyun realmente me regalo algo bueno.

Komekko levanto su pequeño puño al aire y dijo.

“¡Tú debes de vencer al Rey Demonio!”

“¿Al Rey Demonio? Bueno, yo dije algo como eso antes, pero…”

“¡Debes vencerlo!”

“… Yo me esforzaré en eso.”

Mientras que forzosamente le contestaba. Komekko sonrió satisfecha.

