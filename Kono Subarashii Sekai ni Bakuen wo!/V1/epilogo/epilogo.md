# Epilogo

![](epilogo/img/c1.jpg)

La encargada de la tienda me urgía a que me sentara en una silla en este obscuro establecimiento.

Este negocio es la casa de adivinación propiedad de la adivinadora Soketto.

Justo cuando creí, que todo había terminado, Soketto repentinamente me llamo.

“… Vaya, en verdad que pásate por varios incidentes, Megumin.”

Soketto me sonrió, yo no sabía qué hacía aquí.

Por “algunos incidentes” debe estarce refiriendo a mí siendo perseguida por los lacayos del dios demonio, ¿verdad?

“… Será mejor que le compres a tu hermana un juguete, en el futuro.”

“¡¿?!”

Tras ver mi expresión de sorpresa, Soketto soltó una carcajada.

“No te preocupes, no le diré a nadie. Los adivinadores usualmente son muy buenos guardando secretos, ¿comprendes verdad? Pero yo no te llame aquí para hablar de eso.”

Ella levantó su mano para alcanzar su bola de cristal y suspiro fuertemente.

“¿Entonces porque me llamaste a aquí? Ah, ¿es debido a Buzucoily?

Es que acaso ¿ese estúpido NEET hizo algo de nuevo? Incluso si de alguna forma yo conozco a ese estúpido NEET de algún lado, por favor no la tomes contra mí.”

“No es eso, el algunas veces ronda por mi tienda, pero no es tan problemático.”

Soketto me hizo señas con la mano, esperando que me acercara.

“El día de hoy… quiero adivinar el futuro que te depara.”

Soketto sonrió.

“¡Adivinar! ¿Adivinar qué? Solo para que lo sepas, yo no estoy interesada en temas amorosos.”

“Ah, eso es lamentable. Pero no se trata de eso. Yo simplemente quiero ver qué es lo lograras en el futuro. Basada en mi intuición como adivinadora, tu lograras cosas extraordinarias.”

“¿Por pura curiosidad? Bueno, está bien. Pero si llegas a ver algo desafortunado que me ocurrirá en el futuro, por favor no me digas.”

“Jajaja, el futuro se puede cambiar. Ayudarte a evitar malestares futuros, es parte de mi trabajo.”

Soketto, felizmente inyectó mana en su bola de cristal.

“… Hmm. Primeramente, tu intentaras irte del pueblo, con dirección a una ciudad llamada Axel. De esa forma. Tú encontrarás con todo tipo de problemas ahí y finalmente conocerás unos buenos compañeros. ¿Estas personas serán realmente excelentes… exce… lentes…? Um, esto, estas personas… su personalidad será un poco… mmm… excelente… Ah, ¿eh…? Es la juventud… eh…”

“¡¿Qué?! Continúa hablando ¡estoy muy preocupada! ¿Qué pasa con mis compañeros? Ellos son excelentes, respetables y gentiles, ¿verdad?”

Soketto silenciosamente volteo a un lado.

“Dar sugerencias para evitar futuros problemas es tu trabajo, ¡¿O no?! ¡Vamos escúpelo!”

Yo tomé a Soketto de los hombros y la sacudí. Repentinamente su expresión cambio.

“… Eso es, eh. En verdad, Megumin será muy afortunada por conocer a tan grandiosos compañeros.”

“¡¿Cómo es?! Tu dijiste antes que el futuro podía cambiar, ¿verdad? Ahora tengo que reconsiderar si debo ir a Axel o no…¡!”

Soketto sonrió entrecortadamente y me dijo.

“No sería bueno que ese futuro cambie. Uh, no te diré más por ahora.”

“¡Por favor dime! ¡En verdad estoy muy preocupada!”

Soketto ignoro mi pregunta y me sonrió felizmente.

“… Todos ustedes completaran grandes hazañas. Después de eso tu regresaras al Hogar del Clan de Magos Carmesí por cierto desastre, el cual probablemente tenga que ver contigo y tus compañeros.”

“Esta adivinación parece extremadamente abstracta… yo había escuchado que las adivinaciones de Soketto eran bastantes específicas y certeras.”

Nuevamente Soketto sonrió entrecortadamente y tembló un poco.

“Ese desastre que ocurrirá en el Hogar del Clan de Magos Carmesí, también debe de tener que ver conmigo. Un adivinador nunca puede predecir cosas que estén relacionadas con uno mismo. Si el evento tiene que ver conmigo, la bola de cristal no me mostrara nada.”

Ella toco la superficie de la bola de cristal ligeramente.

Si uno pudiera predecir su propio futuro, entonces el adivinador podría hacer lo que quisiera y cambiar el futuro acorde a lo que desee.

Tal parece que la realidad no es tan simple.

Un desastre que tenga que ver con Soketto…

El alboroto de esta vez, o todas las cosas peligrosas que estaban selladas- Simplemente hay demasiados riesgos potenciales y no hay suficientes pistas.

“¿Podría ser una invasión de la armada del rey demonio? Después de todo Buzucoily incluso reunió a los NEETs para formar un extraño grupo.”

Ella debe de estar hablando acerca de la llamada Unidad Armada, Guerrilla Anti-Rey Demonio.

… ¿Eh?

Cuando Soketto intento adivinar, el futuro amor de Buzucoily, la bola de cristal no mostró nada.

Si tuviera que ver con Soketto misma, ¿la bola de cristal mostraría algo así…?

“Buzucoily no es un mal tipo. Si el encontrara un trabajo, sería algo bueno. Entonces el cambiara sus extraños hábitos de acosar a la gente. Estoy segura de que su compañera destinada debería aparecer… Megumin, ¿Qué pasa? ¿Por qué de repente sonríes de esa forma?”

“No, nada. Simplemente creo que es una pareja demasiado dispareja.”

¿Dispareja? – Tras escuchar mis palabras, Soketto giro su cabeza desconcertada.

“En cualquier caso, puedo decirte lo siguiente. Por favor espera con ansias tu vida en Axel… además… escucha, incluso si eres objeto de un poco de acoso sexual, no lo tomes demasiado en serio.”

“Yo seré asaltada sexualmente ¡¿Por quién?! ¡¿Son mis compañeros los que me acosaran sexualmente?! Oye, ¿en verdad pueden considerarse compañeros excelentes?”

-Después de eso yo deje la casa de adivinación, Chomusuke que se había clavado con sus uñas en mí hombro, se columpiaba hacia atrás y hacia adelante.

Aunque sus uñas me lastimaban un poco, yo podía soportarlo.

Antes incluso de que yo empiece mi viaje, he recibido este tipo de noticia desconcertante.

Quizás también pueda quedarme en este pueblo como una NEET…

“Bueno, primero que nada, debo quedarme en el pueblo hasta juntar suficiente dinero para viajar. Puedo pensar en lo demás después.”

Me esforcé para convencerme a mí misma, mientras bajaba a Chomusuke de mi hombro.

Si es que encuentro algunos compañeros descarados, lo único que tengo que hacer es dejarlo pasar. Con eso debería bastar.

-Mis metas son dos principalmente.

Primero que nada, dejar que esa Nee-san presencie mi magia de explosión.

Y la otra-

Este mundo está lleno de monstruos, criminales e inclusive demonios.

Yo espero probar mis poderes contra ellos.

Probar que yo, que he aprendido magia de explosión, soy la más fuerte.

Sin importar que el oponente sea el dios malvado o el rey demonio-

En ese momento, por alguna razón Chomusuke tembló en mis brazos.

Yo sostuve a Chomusuke arriba y dejé que mis ojos color carmesí se encendieran.

Sí, sin importar lo poderosos que sean, yo les lanzare mi Explosión-¡!

