
# Interludio 5

“Oh, Komekko”

“Oh, Hoost”

Yo estaba frente a la tumba del dios malvado, cuando en ese momento, Hoost llegó volando, sosteniendo algo.

“Tanto tiempo sin vernos… si ¿qué pasa? Tal parece que sucedió algo en el área alrededor de la tumba. Incluso hay una parte en donde los árboles se volaron. ¿Pero qué pasó aquí?”

“Escuche que el dios malvado revivió, lo que hizo que la diosa sin nombre despertara. Ellos pelearon y el dios malvado al final fue destruido.”

Hoost dejó caer al piso lo que sea que haya estado cargando.

Era una jaula, que dentro contenía una gallina con sus pollitos.

“Tú, ¡¡Debes de estar bromeando!!”

“Eso es lo que los adultos dicen.”

Tras escuchar eso, Hoost bajo su cabeza deprimido.

Pero a mí me interesaba más la gallina que cacareaba en la jaula.

“Por qué el sello de la Sra. Wolbach se… pero qué. Mientras que no estuve… ¿Eh? Pero esto es bastante extraño. Si la otra mitad de la Sra. Wolbach fue destruida, yo no podría permanecer en este mundo…”

Yo me senté frente a la jaula observando su interior, cuando Hoost gritó repentinamente.

“¡Es verdad! La otra mitad de la Sra. Wolbach ¡Debe de haber sobrevivido!

Se debe de estar ocultando e en algún lado. Debo encontrarlo rápido y protegerlo…”

Hoost miró hacia dónde yo estaba.

“… Uhm, así son las cosas. Yo debo de irme. Es probable que nunca nos volvamos a ver… yo ya no necesito esta ofrenda, puedes quedártela.”

“Bien, yo me comeré a la mamá gallina y Chomsuke puede comerse a los pollitos.”

“¡No te los comas! Espera, creo que esto no es bueno para el crecimiento sano y moral de un niño. Creo que mejor los llevo conmigo. Por cierto ¿qué rayos es ese Chomsuke?”

“Es una bestia mágica de color negro, así de grande. ¿Quieres verla?”

“¿Quién querría? Probablemente solo sea un gato ¿verdad? Al menos dale un nombre adecuado… en verdad, que le pasa a esta gente del Clan de Magos Carmesí…”

Hoost extendió sus alas.

“… ¿Te vas?”

“¿Ah? ¿No escuchaste lo que dije? Yo necesito encontrar a la Sra. Wolbach… Que. No me mires con esa expresión tan triste. Así son las cosas, además tú perteneces al Clan de Magos Carmesí, ¿verdad?”

Yo asentí.

“Yo tengo este presentimiento de que tú, te convertirás en una gran maga. Si mi contrato con la Sra. Wolbach termina… en ese momento, si tú puedes invocarme, yo estableceré un contrato de familiar contigo.”

“¡¿En verdad?!”

“¡Tú deberás de poder invocarme primero! Hmm. Pero la probabilidad de invocar con éxito a un demonio de alto nivel como yo, es muy baja. Es probable que tú no puedas invocarme…”

Hoost se quedó inmóvil en el aire volando con sus alas, giró su cabeza y dudo por un momento, entonces regreso al suelo y se agachó para quedar de mi nivel.

“Eso es lo que sucedería normalmente… pero siento que tú tienes el potencial para convertirte en una demonologa. Quién sabe, quizás en verdad puedas invocarme.”

“¡Trabajaré duro para eso!”

Hoost dijo, “Pero aún no es posible, así que…” el bajo mi entusiasmo y me acarició fuertemente la cabeza, revolviéndome el cabello.

“Entonces, nos vemos ¡Komekko! ¡Debes dar lo mejor de ti para convertirte en una gran maga! Por cierto ¡mi nombre es Hoost! El gran demonio que atiende a la Diosa Demoníaca la Sra. Wolbach, ¡Hoost!”

“¡Mi nombre es Komekko! Cuidar la casa es mi trabajo. Y soy ¡La Femme Fatale hermana pequeña número uno del Clan de Magos Carmesí! ¡Además de que seré la que al final controle a Hoost!”

Yo ondeé mi capa e hice una pose para Hoost.

Hoost se río fuertemente, extendió sus alas y voló lejos del pueblo.

Yo vi volar lejos a mi primer amigo quien frecuentemente se enojaba, pero aun así siempre me traía comida.

Y me despedí de él hasta que su silueta desapareció más allá del horizonte.
