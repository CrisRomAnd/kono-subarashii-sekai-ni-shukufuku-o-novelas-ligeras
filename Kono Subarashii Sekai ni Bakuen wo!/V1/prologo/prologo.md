![](prologo/img/pp1.jpg)

![](prologo/img/pp2.jpg)



# Prólogo

“¡Explosión!”

Una persona vistiendo una capucha muy baja termino su encantamiento tranquilamente.

En contraste con su tono calmado, el poder del hechizo fue devastador.

El sonido de la explosión desplazo al viento y vino con una ráfaga abrazadora.

La bestia negra y gigante que estaba persiguiéndome desapareció sin ofrecer ninguna resistencia.

Y eso no fue todo. Mi sitio de juego sagrado también fue completamente destruido por el poderoso hechizo, volando los juguetes que tanto trabajo me costaron juntar.

Un poder abrumador que puede barrer con todo.

La fuerza más grande y destructiva. Más poderoso que cualquier otro hechizo.

Esta destrucción masiva fue realizada por un solo mago. Un solo disparo.

¿Qué clase de hechizo pudo ser?

Inclusive los adultos en la villa nunca han usado algo como eso antes.

La persona encapuchada camino hacia mí que me encontraba sorprendida y aturdida.

¿Estás bien? ¿Esta lastimada de alguna parte?

Se agacho y observo mi expresión.

Llevaba una túnica realmente desgastada, pero cuando se inclinó su amplio pecho se mostró prominentemente.

Oh wow, eso fue sorprendente. Bueno ese hechizo también fue increíble, pero eso fue mejor.

“¿Cómo puedo convertirme en alguien como tú?”

Esa fue la pregunta que surgió inmediatamente de mi boca. Inclusive olvide agradecerle primero.

Las palabras que mi madre me repetía recientemente flotaban en mi cabeza.

“Nuestra familia siempre ha tenido cuerpos delgados básicamente en todas las generaciones. Así que es mejor que te rindas pronto.” – — o algo así.

No voy a rendirme, no me rendiré hasta el final.

Con ese ánimo, cerré mi puño y observé sus curvas……

La persona encapuchada parecía que sonreía mientras me miraba.

“¿Cuál es tu nombre, pequeña?”

“Soy Megumin”

![](prologo/img/p1.jpg)

Después de un corto silencio. Esta Onee-san me pregunto entre cortadamente.

“…………………….. ¿Es una especie de apodo?”

“Es mi nombre real.”

La Onee-san se quedó callada nuevamente mientras se calmaba así misma y entonces ella dijo…

“¿Tú quieres convertirte en alguien como yo…? Hmm………Si comes más, estudias más y te conviertes en una archí-maga; Estoy bastante segura……”

Convertirme en una archí-maga y entonces tener pechos grandes.

¡Convertirme en una archí-maga y entonces tener pechos grandes!

“Si, si te conviertes en una archí-maga, un día tú también podrás usar ese hechizo. Pero honestamente no te recomiendo ese hechizo.”

Parece que la persona encapuchada dijo algo más, pero.

Yo ya había caído en el sueño de convertirme en archí-maga.

“Pero…”

La persona encapuchada acaricio mi cabeza mientras murmuraba. “Archí-mago, archí-mago”, mientras miraba alrededor.

“Oye pequeña, ¿viste algún otro adulto por aquí? Debe haber alguien que removiera el sello de la tumba… porque los fragmentos del sello están aquí y no podría liberarse automáticamente…”

La persona encapuchada entonces inclino su cabeza y tomo un fragmento que se encontraba junto a mi pie.

“Ese hombre aún debería estar por aquí. Es un misterio quien me libero… bueno no es que importe, preguntarte no me ayudará a dar con él.”

Mientras hablaba, ella camino al centro del hoyo gigantesco que resultó del hechizo.

Una bestia negra se encontraba tirada ahí, apenas respirando.

Ella puso su mano sobre la cabeza de aquella bestia.

“Por favor duerme un poco más, mi otra mitad. Este mundo es aún demasiado pacífico para que tu despiertes…”

La persona encapuchada susurro así, mientras su mano parecía absorber algo de la bestia, mientras brillaba.

Mientras que la bestia se encogía y encogía.

Finalmente quedó del tamaño de un gato y su forma fue difusa y desapareció.

“Muy bien, después de esto… ¿Eh…? ¿Qué haces ahora pequeña?”

Yo estaba levantando los juguetes que quedaron esparcidos por el suelo.

Entones ella se volteó a mí.

“Estoy recogiendo mis juguetes. Mi familia es muy pobre y estos son todos los que tengo.”

“¿Ah? No, no. Estos no son juguetes. Estos son usados para sellar al horrible y malvado demonio. Son muy importantes… ¿Hmmm?”

Junte los fragmentos que había levantado, pero algunos faltaban.

Necesito tres piezas más.

“Onee-san, tu hechizo hizo volar tres piezas, ¿podemos buscarlas juntas?”

“No, ¡NoNoNo! ¡Esto es demasiado raro! ¡Cómo es que tú armaste esas piezas tan fácilmente! ¡Es absolutamente extraño! Incluso aquellos oráculos pasaron por mucho trabajo en hacerlo difícil, como es que esto sucedió…”

La persona encapuchada se mantuvo frotando su barbilla, mientras parecía confundida.

“¿Cuándo es que viniste a jugar a esta tierra prohibida pequeña?

¿No te dijeron los adultos de la villa que no te acercarás aquí?”

“Mi mamá dijo, si las personas te dicen ‘Es peligroso ahí’, ‘No hay nada ahí’ o ‘No te acerques a eso’ sobre algún lugar, eso siempre significa qué hay tesoros escondidos en ese lugar.”

“¡¡¡Qu-Queeeee……!!!”

La persona con la capucha parecía nerviosa y asustada.

Después de eso se paró junto a mí y puso su mano sobre mi cabeza.

“Bueno, aún que aún hay muchas cosas que no sé qué pasaron, pero parece que tengo que agradecerte. ¿Tienes algún deseo, pequeña? Puedo no parecerlo, pero en verdad soy una maga misteriosa con un gran poder. Sin importar el tipo de deseo, puedo hacer uno realidad.”

“¿Deseo?”

“Si, deseo. No temas solo dilo. Sin importar lo que…”

“¡Conquistar el mundo!”

“…Yo, lo siento, no puedo hacer eso.” ¿Qué pasa con ella? Esta niña quizá se convierta en una gran persona… “Emmm, ¿no tienes algún otro deseo?”

La línea de su capucha era muy baja, pero aun así pude ver el gesto de su boca.

“Entonces, ¡Conviérteme en una chica con pechos grandes!”

“N, No, eso también es imposible. ¿Cuántos años tienes? No estás en la edad de preocuparte por eso todavía.”

Parece que eso tampoco va a funcionar.

Entonces…

“¡Hazme el rey demonio!”

“Yo, lo, ¡lo siento! Déjame corregir mis palabras. En vez de una niña tan talentosa como tú, yo soy solo una simple maga con algo de poder. Solo capaz de cumplir un simple deseo.”

La persona con la capucha se disculpó conmigo, mientras una gota de sudor recorría su rostro.

Mire entonces los fragmentos de las piezas en mis manos.

“…A mi juguete aún le faltan tres piezas, ¿puedes encontrarlas por mí?”

“¡Espera! No, aún puedo cumplir un deseo un poco más grande…

Y ¡no lo trates como un juguete! Ese chico negro de hace un momento ¡lo acabo de sellar de nuevo! Así que no vuelvas a este lugar de nuevo, ¡¿lo entiendes?! ¿No tienes algún otro deseo? Algo un poco más grande…”

La persona con la capucha parecía un poco avergonzada y me miraba tímidamente.

…Algo un poco más grande.

Bien entonces—

“¡Por favor enséñame ese hechizo!”

![](prologo/img/p2.jpg)

**Nota**: Esto es lo que dice el texto de la ilustración de arriba de izquierda a derecha…

FUNIFURA: Mi nombre es Funifura, la que tiene los sentimientos más fuertes por su hermano pequeño entre todo el Clan de los Demonios Carmesí, ¡Y portadora del título de Brocon!….. brocon….

DODONKO: ¡Mi nombre es Dodonko, la mejor del Clan de los Demonios Carmesí…! La mejor…. ¿qué era?…

YUNYUN: Mi nombre es Yunyun… La destinada a convertirse en la líder del Clan de los Demonios Carmesí….

KOMEKKO: ¡Mi nombre es Komekko! La encargada de cuidar la casa cuando todos están fuera, ¡y la más grande y precoz hermana pequeña del Clan de los Demonios Carmesí!

MEGUMIN: ¡Mi nombre es Megumin! La mayor genio del Clan de los Demonios Carmesí, ¡y la que está enamorada de la magia de la explosión!

NERIMAKI: Mi nombre es Nerimaki, hija del mas grandioso dueño de una tienda de licores del Clan de los Demonios Carmesí, ¡y aquella que aspira a ser la propietaria de un bar!

ARUE: Mi nombre es Arue. La más desarrollada físicamente dentro del Clan de los Demonios Carmesí, ¡y aquella que aspira a convertirse en una escritora!

