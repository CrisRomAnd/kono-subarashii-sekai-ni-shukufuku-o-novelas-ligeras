## Parte 2

Después de que la furibunda de Yunyun se calmara, nos fuimos a caminar por la ciudad por una hora.

Mientras que caminaba delante de mí, ella de vez en vez volteaba como si quisiera decirme algo.

“Meg-Megumin… sobre…”

“… Ya que te saliste de la iglesia del culto a Axis, tu ahora no tienes ningún lugar a donde ir, así que no sabes dónde te quedaras- ¿verdad?”

“…… sí…”

Yunyun asintió un tanto avergonzada.

“Señorita Megumin, ¡por favor no se enoje con la señorita Yunyun! Mire, ¡Mire su rostro todo colorado! Ah, ¡Ella es tan linda! ¡¡Pero que adorable!! No hay problema. Onee-san no es ninguna extraña, así que puedes llamarme casualmente por mi nombre, llámame Cecily onee-chan, ¡¡entendido!!”

Cecily, que por alguna razón nos estaba siguiendo, quedo conmovida por lo que escucho y fue a abrazar a Yunyun.

Yo le dije a Cecily…

“Entonces, Onee-san. ¿Estas libre?”

“¡Llámame Cecily onee-chan!”

“Onee-san, hay algo que me gustaría preguntarte… La mujer caballera dijo que este culto es responsable por la administración de la fuente de las aguas termales y que los hoteles que reciben estas aguas se habían estado quejando desde ayer.”

“Sí. Yo decidí no mirar al pasado y por tanto no lo recuerdo bien, pero tal parece que había dichas quejas.”

¿Estará esta onee-san realmente bien…?

“Eh… por tanto, yo creo que deberíamos de visitar los hoteles con aguas termales que levantaron una queja. Pese a que sucedió un cambio no natural en las aguas termales, quizás no sea a lo que se refería la adivinación. Primero debemos investigar lo que realmente sucedió. Quizás no sea el caso, pero talvez haya sido un mero accidente.”

“… Ya veo. ¡Pero les apostaría 10,000 eris a que el señor Zesta es un criminal!”

“Se-señorita Cecily, ¡eso es demasiado!”

Yunyun le reclamo, cuando aún estaba siendo abrazada.

![](cap_4/img/c2.jpg)

Pero por cual hotel deberíamos de empezar…

… ¡¿Eh?!

“¿Qué sucede? Parece que hay cierta conmoción por allá.”

“Hm- ¿? Oh, ¿No es ese, uno de los miembros del culto?”

Delante de nosotras había un hombre que habíamos visto antes en la iglesia.

“-Yo no creí que algo así pudiera suceder… no, el señor Zesta siempre ha causado problemas, pero él nunca se aliaria con la armada del rey demonio…”

“Pues… creo que puedo entender tus sentimientos. Pero después de ver la conducta usual de los miembros del culto a Axis, es imposible que creamos en su inocencia fácilmente- ¡puedo entender lo que siente el resto del pueblo!”

“No, no es que sea increíble a tal extremo. ¿Cómo debería decírtelo? Yo siento que, si el culto a Axis está involucrado, entonces… no puedes culpar al resto del pueblo, de creer que él lo hizo…”

Nosotras escuchamos su conversación. Tal parece que era acerca del arresto señor Zesta.

Pero el contenido era un tanto extraño.

“Como un miembro del culto a Axis, me siento avergonzado por lo que el señor Zesta hizo. El señor Zesta arruino nuestra reputación… no, Zesta. Pero ¡Yo definitivamente la restaurare!”

“Pues yo siento que no habrá mucha diferencia si no está el señor Zesta. Por cierto, ¿Puedo irme ahora? Necesito ir a regar los campos de arroz…”

Tal parece que un miembro del culto a Axis intercepto a un granjero que iba pasando e intento adoctrinarlo.

“¡Regar los campos de arroz! ¿De dónde cree que viene el agua?… ¡Así es! Viene de la lluvia, misma con la que nos bendice la Señora Aqua, la diosa del agua. ¿Y qué tipo de ciudad es esta?… ¡Así es! Esta es Alcanretia, ¡la ciudad del agua y las fuentes termales! Esta es la ciudad, ¡la cual es patronada por la Señora Aqua! En otras palabras, ¡es lo más natural del mundo que todos sus habitantes se unan al culto a Axis! Vamos, ¡tú también deberías afiliarte al culto a Axis…!”

“Ah, como granjero, yo siempre he sido un miembro del culto a Axis. Y siempre he estado agradecido con la Señora Aqua.”

“Oh, ¿así que así es? Eso hace las cosas más simples entonces. De hecho, debido al arresto del señor Zesta, surgió la necesidad de elegir al nuevo oficial de más alto rango. Esto se decidirá por medio de votos. Cada miembro del culto tiene el derecho a votar. Así que…”

El tema dio un giro extraño ahora.

“Yo tengo en mi posesión ahora, la ropa interior, de una hermosa sacerdotisa del culto a Eris, la cual fue obtenida cuando la estaban secando en el patio trasero… ¿Entiendes lo que trato de decirte?”

“… Tú eres un imbécil. O mejor debería de decir ‘tú eres un verdadero miembro del culto a Axis’.”

Este miembro del culto a Axis saco las pantaletas y la sostuvo de ambas esquinas para podérselas mostrar mejor al granjero.

“¿Hm? Estas pantaletas…”

“Ah, ¿habrá algún miembro del culto a Axis que sea merecedor de mi voto? …Oh, debió haber sido cosa del destino que te conociera aquí el día de hoy. Dime ¿Cuál es tu nombre?”

“Mi nombre… Ah, tengo una pluma, pero no papel… bien, lo escribiré en estas pantaletas, ¡de esa forma puedes tenerlas como una carta de presentación!”

“Mmm… si es una carta de presentación, ¡pues que se le va a hacer! Ya veo, ya veo creo que tendré que aceptarlas… Bien, ¡lo recordare! ¡Estaba esperando que alguien como tu apareciera en el culto a Axis…!”

El miembro del culto y el granjero se dieron una mirada y sonrieron.

“Que la señora Aqua te bendiga.” x2

En ese momento Cecily y yo usamos toda nuestra fuerza para golpearlos por la espalda.

Originalmente nos acercamos solo para escuchar lo que decían.

Ambos cayeron después de ser asaltados.

“¡¿Pero… que están haciendo?!”

“Maldición, ¡¿Sera una emboscada de esos miembros del culto a Eris?!… ¡¿Qué, Cecily?! ¿Qué haces? ¡Esta es una rara oportunidad para obtener un voto!”

Ambos hombres se levantaron y protestaron.

“¡No es lo que estamos haciendo! Acaban de arrestar al señor Zesta. ¡¿Y qué hacen ustedes?!”

“¡Megumin tiene razón! ¡¿No se supone que ese tal Zesta es uno de los nuestros!?”

Y qué es lo que discuten, ¡mientras que sostienen un par de pantaletas! Ahh, ¡en verdad es inaceptable! Rápido, señorita Cecily, usted también dígales algo…”

“¡¿No son esas mis pantaletas?! ¡Qué fue todo eso de que pertenecían a una linda sacerdotisa del culto a Eris! Sera mejor que corrijas lo que dijiste, ¡Y digas que estas pantaletas pertenecen a una hermosa sacerdotisa del culto a Axis! ¡Y tú! Si quieres esas pantaletas, ¡mejor vota por mí!”

Quizás sea mejor si dejo atrás a esta persona…

Mientras que veía a Cecily tomar a esos dos hombres del cuello, tuve un mal presentimiento acerca de lo que iba a suceder.


