# Capítulo 4:  Salvadores de la Ciudad del Agua

## Parte 1

![](cap_4/img/c1.jpg)

-A la siguiente mañana.

Yunyun y yo nos quedamos en el culto. Caminamos en dirección del salón donde se reunían los miembros del culto a Axis.

“Recuerda, Yunyun. Tú solamente entregaste un mensaje de alguien del Hogar del Clan de Magos Carmesí, así que no tienes por qué culparte a ti misma. Enfréntalos con confianza.”

“¡E-enterado! Así es. Yo simplemente entregue una carta. Incluso si se llevaron a ese viejo para interrogarlo, eso… Ugh, hm… ¡no tiene nada que ver conmigo!”

Tras pensarlo un poco, Yunyun levanto su cabeza finalmente.

Después de que se llevaran a Zesta, esta niña a estado culpándose así misma…

“Ese es el tipo de resolución que necesitamos. Después de todo, si su conducta normal hubiera sido buena, al menos hubiera sido posible discutir sobre ello. Es su propia culpa que se lo llevaran tan fácilmente. Además, si prueban su inocencia, el será liberado rápidamente, así que no hay necesidad de preocuparse.”

Yunyun se decidió y dijo, mientras abría las puertas de la iglesia…¡!

“¡Si-sí! lo entiendo. Incluso si los miembros del culto me culpan- ¡sin importar lo que digan! ¡Yo permaneceré fuerte!”

“Oh, buen día, señorita Megumin y señorita Yunyun. ¿Durmieron bien?”

Cecily estaba tomando su desayuno, con su cabello aun hecho un desastre.

“Buenos días… ¿solo onee-san está de guardia? ¿A dónde fueron el resto de los miembros del culto a Axis?”

“Todo mundo salió temprano por la mañana. Ya que, si esto sigue así, el señor Zesta será removido de su posición como el más alto oficial, así que tendremos que decidir quién tomara el puesto. Para tomar esa decisión, necesitamos hacerlo por medio del voto de los miembros del culto a Axis. En otras palabras, todo mundo está ocupado preparando la elección.”

…Elección.

“Entonces… antes de la elección, ¿la gente estará reuniendo evidencia que pueda absolver a Zesta de sus supuestos crímenes?”

“¿? ¿Por qué haríamos algo tan problemático y aburrido? Pero no se preocupen. Si el no cometió ningún crimen, el regresara pronto. Pero si no regresa, también está bien. Después de nuestra discusión, nosotros concluimos que estaríamos mejor sin el señor Zesta, así que simplemente dejen que pase, lo que tenga que pasar…”

“¡¿Es enserio?! Señorita Cecily, quizás como la que entrego la carta, no debería de decir esto, pero ¡¿de verdad está bien?! ¿Está bien que no vayamos a rescatarlo?”

Yunyun hablo preocupada, mientras que veía como Cecily continuaba comiéndose su huevo, tranquilamente.

Ella dijo antes que permanecería fuerte, pero su sentido de la culpa era más fuerte.

“Inclufo si fu diches echo… la policía continúa investigando. El señor Zesta sin duda es el responsable de las aguas termales y es un hecho que esas quejas repetidas involucran su jurisdicción. Así que es mejor dejarlos investigar cuidadosamente al señor Zesta y que encuentren la verdad. Y es por eso que, incluso aunque la mayoría del culto se quedó despierto hasta ya tarde noche, ellos se levantaron temprano hoy para trabajar.”

Dijo Cecily, mientras continuaba con su desayuno.

“Y, además- ”

Ella se pasó el bocado que traía en la boca.

“Yo he escuchado que la poderosa adivina del clan de magos Carmesí siempre atina en sus predicciones. De acuerdo con los rumores, ella usa un sospechoso poder demoníaco para ver el futuro y así elabora una intrincada adivinación.”

Eso es demasiado.

“Las predicciones de Soketto, son en verdad acertadas… pero incluso así, alguien al menos, debería de buscar pruebas que demuestren la inocencia del señor Zesta… Ah, pero tú al menos crees en el ¿verdad? Después de todo, tú decidiste permanecer aquí, en lugar de buscar votos.”

“¡¿Yo?! Yo me quede dormida, debido a que me quede bebiendo toda la noche, festejando que arrestaron al señor Zesta. Así que en cuanto termine de desayunar, yo también saldré…”

“¡Eso es demasiado! Megumin, ¡que debemos de hacer! ¡Yo nunca pensé que el entregar esa carta, nos llevaría a esto…!”

Yunyun había entrado en pánico y estaba a punto de llorar. Pero ya que sus compañeros no tenían ninguna intención de salvarlo, no había nada que nosotras pudiéramos hacer.

En verdad, solo hace un momento, ella incluso dijo que este asunto, no tenía nada que ver con ella.

“No se preocupe, señorita Yunyun. En estos momentos, el señor Zesta seguramente está feliz por ser interrogado por esa mujer caballero. Y no sería correcto interrumpirle su diversión. En lugar de eso, ¿podrían ayudarme a mí? La paga será buena. Verán, debido a mi conducta en el pasado, los ciudadanos seguramente recuerdan mi rostro, así que serían un tanto represivos, incluso si me dirijo a ellos para una plática casual. Por tanto, ¡me gustaría que ustedes…!”

“¡¿Cómo si pudiéramos ayudarte?! Oye, Megumin, ¡tú también dile algo…!”

“¿De cuánto estamos hablando?”

“Déjame pensarlo… 10,000 eris por cada voto que me consigan…”

“……”

“Espera, te digo esperes. ¡Lo sé! Yunyun, lo sé, ¡así que deja de recitar tu hechizo!”

“¡O-onee-san, te dará algo de tocino! Así que por favor ¡tranquilízate ya!”
