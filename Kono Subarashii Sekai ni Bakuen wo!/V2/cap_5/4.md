## Parte 4

“¡Vamos, su grandeza! Por favor coma. ¡Al menos recupere algo de sus energías!”

“Mu-muchas gracias.”

“……”

El resto del viaje pasó sin incidentes. El sol se puso y fuimos rodeados por la oscuridad.

Los carruajes se detuvieron cerca del lago y acomodaron el campamento.

Yunyun fue invitada a la fogata del líder mercante.

“Es bastante raro ver a alguien de 13 años de edad, ser tan habilidoso en la magia. ¿Acaso esa es la norma en el Hogar del Clan de Magos Carmesí?”

“N-no, no del todo. Algunas de mis antiguas compañeras, pueden usar magia avanzada, así que en realidad yo estoy un poco abajo del promedio…”

“¡Magia avanzada! ¡Pueden usar magia de ese nivel a esa edad! ¡No cabe duda del porque hasta la armada del rey demonio tiene miedo del Clan de Magos Carmesí!”

Tal parece que el líder mercante le tomo aprecio a Yunyun.

El tomo un trozo de carne bastante apetitoso del hoyo de la barbacoa y se lo ofreció a Yunyun.

“……”

Yo puse a Chomsuke en mis rodillas mientras que veía a Yunyun a la distancia. Ella noto eso y me dijo…

“… Meg-Megumin, ¿quieres un poco? Es bastante sabroso.”

Siento que abre perdido si acepto la carne.

Yo no tuve oportunidad de actuar en la batalla previa, debido a que había otras personas en el área.

Si el campo de batalla hubiera sido más amplio, esto no hubiera sucedido.

Yo que soy la genio número uno del Clan de Magos Carmesí, ¡no puedo desmotivarme por solo una falla!

“Vamos pequeña, acércate y come también. Tú debes de comer más, para que puedas ser como tu hermana mayor.”

“…Nosotras somos del mismo grado.”

“¡¿Eh?! M-mis disculpas. Como digo esto… la señorita Yunyun parece más madura, así que…”

“Si usted tiene algún comentario acerca de mi crecimiento, por favor dígalo.”

En el momento en el que iba a discutir con el líder de los mercantes, un aventurero regreso de su ronda.

El venia equipado con una armadura de cuero y una daga, así que el probablemente se tratara de un ladrón.

“He completado la ronda. No hay señales de monstruos cercanos.”

Parece ser que, él fue el que silbo cerca de aquí hace poco.

“Asignen el mínimo número de personas para la guardia nocturna. Que el resto descanse temprano. Muchos de los monstruos temen al fuego, pero a la inversa, el fuego puede atraer otros monstruos más inteligentes. Pasa estas palabras al resto de los aventureros.”

Esa persona prendió una antorcha y partió para buscar al resto de los aventureros.

“Ah, ah. Será mejor que lo acompañe y de las ordenes directamente.”

El líder mercante añadió esas palabras y salió rápidamente hacia algún lugar con el aventurero que le acababa de entregar el reporte.

…Probablemente el temía que lo molestara y prefirió escapar.

El resto de los pasajeros rápidamente se quedaron dormidos en las diligencias. Tal parece que el viaje los había agotado.

Yunyun y yo nos quedamos solas frente a la fogata.

“…Oye, Megumin. Mí desempeño el día de hoy fue grandioso.”

Yunyun me dijo emocionada.

“Hehehe. Tu misma te estas elogiando sobre una pequeñez como esa.”

“¡N-no, no es verdad! No pensé en nada de eso. ¡No te me acerques!”

Ella trato de explicarse mientras que yo me le acercaba lentamente. Entonces, ella se recobró y dijo tímidamente.

“…Piénsalo. Cuando estábamos en el pueblo, yo era simplemente una chica que pasaba desapercibida…”

“Eso es verdad. Yo frecuentemente me preguntaba si es que estabas usando alguna técnica de invisibilidad.”

“Oye, espera… Olvi-olvidalo- el día de hoy, finalmente mostré de lo que soy capaz y mucha gente confiaba en mí, ¿no es así?”

Yunyun agacho su cabeza y me dijo felizmente.

…En ese momento note que Chomsuke que estaba en mi regazo, no apartaba la mirada de cierta dirección.

“Así que ahora y-yo tengo un poco de confianza en mí misma. De hecho, yo quería decir esto hasta que hubiera aprendido magia avanzada- Megumin, ¿de ser posible, uh… tú y yo… juntas…?”

Yo miraba hacia un punto en la oscuridad.

¿De qué se trataba?

Pensé en eso y me levante…

Algo agito sus alas y salió de la oscuridad, por donde Chomsuke estaba viendo.

“Jun-juntas formar un equipo… ¿Eh?”

Esa cosa que salió volando, tomo a Chomsuke y se desvaneció en la oscuridad…

“…Chomsuke me fue arrebatado tan fácilmente. ¿Sera este un nuevo método para golpear a alguien?”

“¡¡Oye!!”

La criatura que tomo a Chomsuke, no fue la única que salió de la oscuridad.

Seguido de un penetrante sonido de aleteo-

“¡Kyaaa! ¡Murciélagos gigantes! ¡Es una colonia de murciélagos gigantes!”

Del tamaño de buitres, los murciélagos gigantes, ¡atacaron bajo la luz de la fogata!

***

Yo levante a Chomsuke. Había cadáveres de murciélagos por doquier.

“Fiuu. Tuvimos suerte. Casi te llevaban para ser comido por esos murciélagos.”

“…”

Le dije a Chomsuke, mientras que Yunyun miraba con ojos inquisitivos. Ella respiraba con dificultad.

“Yunyun, la tuviste difícil, pero lo hiciste bien.”

“¡Tú también debiste de ayudar! En esta oscuridad, ¡los ataques de los otros aventureros tienen poca puntería! Ah, mí ma-ma-mana esta por…”

Yo también quería ayudar, pero de haber usado magia de explosión en la oscuridad, ¿Quién sabe cuanta gente hubiera resultado herida por accidente?

Yunyun se quedó pasmada y se sentó. En ese mismo momento, el líder de los otros aventureros se nos acercó.

“Señorita Yunyun, ¡Muchísimas gracias! ¡Usted nos salvó de nuevo! En este viaje hemos dependido tanto de usted. ¡En verdad no sé qué decirle…!”

“Es verdad, ¡usted es tan poderosa! ¿Cómo es posible que libere magia intermedia de tal poder?”

“Oye, te diriges a Axel para formar un equipo, ¿no es así? No le gustaría unirse a mi equipo…”

Todo mundo estaba ocupado alabando a Yunyun.

Pero ya que Yunyun había agotado su mana, a ella parecía no importarle.

“Ya es tarde. Y la señorita Yunyun parece que ha agotado todo su mana, ella se ve realmente cansada. Si le quieren decir algo, déjenlo para mañana. ¡Señorita Yunyun, por favor tome un buen descanso!”

Bajo las órdenes del líder, todos se dispersaron y fueron a descansar.

En ese momento, un aventurero, que estaba junto a Yunyun, me lanzo una mirada.

……

“Por cierto, ¿Qué fue lo que hizo esta niña? Ella tampoco hizo nada cuando fuimos atacados por los gusanos de tierra gigantes…”

“Idiota. Ella debió de estar protegiendo al resto de los pasajeros.”

“Pero incluso de ser así, ella debió de usar un poco de magia…”

Yo escuchaba esa crítica mientras que caminaba hacia Yunyun.

Pese a que se veía agotada, aun así ella me miraba con cierta satisfacción.

“Megumin, ¿Qué tal lo hice?”

…

Me cubrí con una cobija, cerré mis ojos y dormí a un lado de Yunyun. Solo una parte de mi rostro, de la nariz para arriba estaba expuesta.

No es que estuviera enojada o insatisfecha por no tener la oportunidad de actuar.

“¿Así que la magia intermedia también es aceptable? Yo pienso que resulta bastante útil.”

“Es verdad~”

“Oye, no armes un alboroto solo porque no has tenido la oportunidad de usar tu magia de explosión.”

¡Yo no estaba buscando pleito!

¡Definitivamente no!

“…Uh, te diré, si continuamos nuestra conversación de hace un rato… de ser posible, forma un equipo conmigo…”

Yo apreté mis dientes por mi propio sentido de incompetencia. Ni siquiera escuchaba a Yunyun y simplemente le respondí por inercia.

