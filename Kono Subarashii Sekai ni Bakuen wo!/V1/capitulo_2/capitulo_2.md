# Capítulo 2: El Solitario Maestro del Clan de Magos Carmesí

## Parte 1

![](capitulo_2/img/c1.jpg)

“¡Megumin! Debemos tener un encuentro hoy, ¿Entiendes?”

Debido a los efectos del control del clima de cierto profesor, hoy hace un buen clima con un cielo limpio y azul.

En cuanto entro al salón, soy inmediatamente atosigada por Yunyun.

Por alguna razón hoy está muy buen humor.

-Yo entiendo la razón.

La daga que compro hace algunos días, ahora la porta en su cintura.

Ella intencionalmente la toca algunas veces para llamar mi atención. Es tan molesta.

¿Es que espera que le diga que esa daga, le sienta bien?

No soy su novio. Y no tengo intención de poner tan problemática actitud.

“No tengo problema en aceptar. Pero no me queda ninguna pócima para apostar. Así que ¿Cómo lo haremos?”

“Apuesta… Si yo gano, Megumin tendrá que obedecer una orden mía…”

“Bien. La apuesta del encuentro será decidida por ti, entonces. ¿Qué te parece si usamos esa daga tan genial que llevas en tu cintura para decidir esto?”

“¿Usar esta daga? Me parece bien. Aunque no se bien de qué va, ¡Acepto!”

Llevo a la confiada Yunyun a mi lugar y pongo mi palma en el pupitre.

“Por favor usa tu daga para acuchillar el espacio entre mis dedos. Antes de que cuente hasta diez, si tú no has fallado los espacios, es tu victoria. Simple, ¿Verdad?”

“¡Espera! ¡Espera! No, no, ¡eso definitivamente no!”

“No hay problema, yo creo en la habilidad de Yunyun. Inclusive si soy acuchillada, soportare el dolor. Entonces ¡empecemos! ¡uno! ¡dos!”

“¡No importa! ¡Me rindo por el día de hoy!”

El escenario sigue sin cambiar también hoy.

“…Ah. Gracias por la comida. Hoy también sabe muy bien.”

“Si… una que otra vez, yo también quiero enfrentarme en serio…”

Yunyun dijo con voz entrecortada, mientras tomaba la caja de almuerzo de regreso.

“…Por cierto, Yunyun, ¿cuántos puntos más necesitas para aprender magia avanzada?”

“¿Puntos? Otros… tres puntos. Podre aprender magia avanzada con otros tres puntos. Para entonces… me graduare… ¿Cuántos puntos más necesitas tu Megumin?”

En la escuela del Clan de Magos Carmesí, un estudiante se gradúa una vez que ha aprendido a usar magia.

Tras escuchar la pregunta de Yunyun, veo mi tarjeta de aventurero, esta muestra que tengo 46 puntos de habilidad.

En la lista de técnicas, las palabras “Aprender magia avanzada requiere 30 puntos de técnica” se encuentran brillando. Pero lo que yo quiero aprender es Magia de Explosión. Para aprenderla aún necesito…

“Aún me faltan cuatro puntos, lo que significa que Yunyun te graduaras antes que yo.”

“¡¿Eh!? Espera, las calificaciones de Megumin siempre han sido mejores que las mías, ¿cómo es que tienes menos puntos que yo? Pero más importante… ¡¿Ah?!

¡Voy a graduarme sola…!”

En lo que Yunyun se mortificaba, el profesor encargado llego al salón.

El salón quedo en silencio. Estando en el podio, el profesor sostenía la lista de asistencia.

“Tomare la asistencia ahora.”

Mis compañeras respondían llegado su turno.

“… ¡Dodonko! ¡Nerimaki! ¡Funifura!”

Ya que solo hay once estudiantes en el salón mi turno llega rápidamente.

“¡Megumin!… y también ¡Yunyun!”

“¡Presente!… Profesor, ¿Qué fue eso? Usted dijo ‘también’ ¿Verdad? ¿Usted casi me olvida de nuevo?”

“Bien, bien, empecemos con la clase… o eso me gustaría decir. Últimamente los monstruos se han puesto más activos cerca de la villa. Por lo que el director me ha solicitado que dirija a los hikikomori… no, a las personas con mucho tiempo libre a cazar monstruos. Por lo que pueden irse a casa por la tarde y será auto-estudio en la biblioteca por la mañana.” 

**(Nota: Hikikomori, es un término japonés para referirse al 
fenómeno social que consiste en personas apartadas que han 
escogido abandonar la vida social; a menudo buscando grados 
extremos de aislamiento y confinamiento, debido a varios 
factores personales y sociales en sus vida)**

Yunyun pone sus ojos llorosos por ser ignorada por el profesor. Mientras este se marcha después de su explicación.

-Esta es la Aldea del Clan de Magos Carmesí que hasta el Rey Demonio teme.

Los monstruos raramente les ve activos por aquí.

Los monstruos que hay por los alrededores usualmente no se acercan a la villa…

Miraba los libros en la biblioteca, mientras aún me seguía molestando ese asunto.

Quisiera leer la secuela del libro que encontré hace algunos días “Rodas el Indomable” Volumen 2, “Rodas el Indomable” Volumen 2…

-Lo encontré.

“Yunyun, he estado buscando ese libro que tienes en tus manos. Tú ya tomaste varios libros, si no lo vas a leer inmediatamente ¿puedo tomarlo primero?”

Yunyun sostenía “Rodas el Indomable” Volumen 2 junto a otros libros.

“Uh… seguro, Megumin ¿lees este tipo de libros también? Aquí tienes.”

Yunyun me paso los libros de: “Tu puedes comunicarte inclusive con goblins” y “Hazte amigo de los monstruos”.

“¡¿Quién te dijo que quería estos?! El que yo quería es el de ¡’Rodas el Indomable’!”

“¿Eh? ¿Megumin te gusta este? Es un libro muy interesante. Yo ¡lo he leído varias veces ya! Volumen 2 ‘El Rey Falso Aparece’ tiene un final inesperado con el viejo rey y sus dos sirvientes saliendo de viaje…”

“Por favor, ¡no spoilers!… Por cierto, ¿Qué pasa con tu selección de libros? Los títulos son demasiado dolorosos… muy dolorosos.”

“No seas así Megumin. ¡Por qué me miras con esos ojos tan compasivos! Mira aquí, hasta los cactus tienen sentimientos ¿verdad? Eso quiere decir que es ¡posible hacerse amigo de las plantas…!”

… ¿Qué le pasa a esta chica?

“No puedo soportarte. Si tanto quieres tener amigos, solo retráctate de tu declaración sobre de que soy tu rival…”

“-Hey, Yunyun. ¿Sigues leyendo esos libros? Si tanto quieres amigas, yo también puedo ser tu amiga.”

Volteo a ver a la persona que nos interrumpió. Es la compañera que hablo antes con Yunyun… compañera…

¿Cómo se llama?

“¿No es Funikura o algo así? Los amigos no hacen intencionalmente, sino que naturalmente.”

“¡Mi nombre es Funifura! ¡Has el favor de recordar el nombre de tus compañeras!”

Yunyun se acercó de repente a la molesta Funifura.

“¿Qué fue lo que dijiste hace un momento? Que, ¡¿Qué fue lo que dijiste antes?!”

“Hey, estas demasiado cerca. ¡Tu rostro está muy cerca! Solo dije que podemos ser amigas…”

De frente a la cercana y de mirada seria Yunyun, Funifura lentamente retrocede, y se explica rápidamente.

Mientras la escucha, Yunyun asiente varias veces, avergonzada.

Oh… La siempre solitaria, Yunyun ¡finalmente consiguió una amiga!

Aunque mi preocupación por el futuro de Yunyun no era solicitada, ¡puedo sentirme más tranquila ahora…!

“Ugh… ¡No tengo experiencia, así que por favor guíame!”

“Hey, Yunyun, siquiera sabes ¿Qué significa ‘amigo’? ¿De verdad sabes?”

… Puedo… sentirme más tranquila… probablemente.

***

“Hey, ¡Yunyun! ¡Te daré una nueva! Es solo una banda para el cabello. Si la pierdes, ¡simplemente de daré una nueva! Megumin ya se disculpó, ¡no es un gran problema!”

“¡Pero, pero…! ¡Este era el primer presente que recibía de una amiga…!”

-Es imposible que me sienta mejor.

Funifura quería que Yunyun se viera mejor, así que le regalo una banda elástica para el cabello.

Yunyun estaba encantada por haber recibido un regalo. Así que regreso al salón y lo guardo cuidadosamente en su pupitre.

Entonces-

“¡¿Por qué Megumin busco en mi pupitre?! ¡¿Por qué la tomaste para jugar y disparaste mi banda elástica?! ¿Eres una niña de tres años?”

Me aburrí de estar leyendo así que regresé al salón antes. Tome la banda elástica, la estire y la dispare por la ventana. Eso fue todo.

-Me encontraba de rodillas en el salón de clases.

“No es así. Miren, hay un insecto en el árbol que esta fuera frente a la ventana. A mí me molestan bastante ese tipo de insectos así que pensaba en dispararle y tirarlo. Yo ya había usado todas mis ligas así que no tenía más alternativa que tomar prestadas las tuyas…”

“No es que no ‘tuvieras más alternativa’. Eso no es algo a lo que una chica debería estar jugando. ¡No te soporto…!”

Yunyun me miro, mientras agachaba mi cabeza y suspiro.

“¡Funifura, lo siento! Es raro que yo reciba un regalo así que originalmente mi intención era guardarlo en una caja fuerte en casa…”

“¡No era necesario eso! ¡Es ridículo! Las bandas para el cabello están hechas para usarse.”

Funifura dijo bruscamente, sintiendo un poco de miedo de Yunyun.

“Olvida eso. Casi es hora del almuerzo. Preguntémosle a Dodonko si quiere comer con nosotras.”

“¿Yo también puedo? Comer el almuerzo juntas, ¡Es como ser amigas!”

“Ya lo había dicho antes, ¡ya somos amigas!”

La soledad de Yunyun era una enfermedad que se desarrolló demasiado.

“Así es, casi es hora del almuerzo, vayamos rápidamente.”

A mí, que se me ordeno permanecer sentada firmemente en el piso, me levanto y me alisto para seguirlas…

“… Megumin, ¿Trajiste algo para el almuerzo?”

“No.”

“Así no funciona esto.”

Yo respondí y Funifura me rechazo.

… Oh dios.

Es algo bueno que Yunyun hiciera amigas.

Pero al mismo tiempo, la pobre de mí perdía una importante línea de suministros.

Mire preocupada a Yunyun quien se preparaba para marcharse con su caja de almuerzo.

“Megumin, hm… si es la mitad… Hey, ¡No me abraces! ¡Y tampoco me reverencies!”

-Pero, como el dicho dice, tres mujeres hacen un drama.

“¡Esa persona definitivamente está interesada en mí! ¿Pero qué haré?

También, tengo a alguien con el que hice una promesa de por vida en mi vida anterior. ¿Eso es adulterio entonces? Más o menos.”

“Eso no importa. La vida anterior no es esta vida. Mi pareja destinada esta ya está decidida… no, debería ser un guapo chico que se encuentre sellado en la parte más profunda de una mazmorra, así que debo apresurarme y aprender magia avanzada para ir a rescatarlo.”

¿Qué pasa con esta insufrible conversación?

“¿Es así? ¡Ustedes dos son increíbles!”

Yunyun sonrió nerviosamente y respondió sin mucha emoción.

Mientras comía el almuerzo con mis compañeras Funifura y Dodonko, el tema del amor nunca termina.

Parece que tienen su amor ideal y la realidad en una mezcla confusa.

“¿Qué hay acerca de ti Yunyun? Tu tipo… no, tu amante en tu vida anterior, ¿cómo era?”

Pregunto Dodonko mientras pinchaba su ensalada.

¿Yo? No tengo memorias de mis vidas anteriores… Una persona madura y estable que sea gentil conmigo y que me escuche…”

“Que poco romántica.”

“Muy poco romántica.”

“No se puede hacer nada Yunyun es extraña después de todo. Por cierto, en mi vida anterior debí ser un dios de la destrucción, así que no tuve un amante. Ah, estoy satisfecha, estuvo bueno.”

“¡¿Persona extraña?! Hey, ¿En verdad soy extraña? Además, ¡te dije que solo te daba la mitad! ¡¿Por qué te lo comiste todo?!”

## Parte 2

-De camino a casa de la escuela.

Fantástico, Yunyun finalmente tiene amigas. En algún momento llegue a sospechar que en tu vida anterior fuiste un isópodo gigante, y estarías sola por siempre. Me tenías preocupada.”

Le decía a Yunyun que se mostraba encantada.

“¡No es que yo quisiera estar sola!… Megumin, en tu boca. Ah, una chica debería ser más consciente de su apariencia. Tienes algo de salsa de soya en la boca.”

Yunyun, decía eso mientras que sacaba su pañuelo para limpiar mi boca, justo como lo haría mi madre.

“Solía pensar que Yunyun era una niña que no encajaba en la sociedad y que sería fácilmente engañada por un chico malo que pretendiera ser su amigo. Ahora me siento aliviada.”

“Yo solía pensar que Megumin era una niña que no tenía los medios para sobrevivir sola y que sería fácilmente engañada por un chico malo que la invitara a comer. Ahora me siento aliviada.”

Yunyun, que limpiaba mi boca y yo nos volteamos a ver una a la otra y nos separamos.

“Tú acabas de decir algo bastante rudo. ¿Quieres decir que yo soy una chica estúpida qué seguiría a un chico malo ciegamente solo por una comida?”

“Así es Megumin. Además, tú me subestimas. ¿Tú crees que soy una chica frívola que seguiría ciegamente a un chico malo solo para ser su amiga?”

Confronte a Yunyun a lado de la carretera sonriendo viciosamente.

“Ah, puedo imaginar fácilmente un futuro donde Yunyun esté siempre lista para el llamado de un chico inútil que le dijo ‘Seamos amigos’.”

“Yo también puedo imaginar a Megumin estando perdida por la falta de medios para sobrevivir, ¡rogándole a algún chico inútil que le de comida!”

… Parece ser que llego el momento de decidir las cosas con esta auto-proclamada rival.

Tomo una posición defensiva. Yunyun parece alerta.

En este momento tan decisivo, un repentino sonido nos interrumpe.

“¿Eh? Pero si no es Megumin, la vecina.”

Me volteo y miro. Se trataba del hijo del zapatero que vivía cerca de mi casa.

“¿Pero si no es Buzucoily? ¿Qué haces aquí?”

Para mí y Komekko, él es un vecino al que tratábamos como un hermano mayor. Pero todo lo que hacía era holgazanear todos los días, declarando que el necesitaba conservar su poder antes de que el mundo lo necesitara.

Era raro verlo pasearse afuera.

“Los monstruos se han vuelto activos últimamente e inclusive se han acercado a la villa. Yo me encontraba alejándolos. Ah, eso fue debido a que me dijeron ‘¡Ahora es el momento de usar tu energía almacenada!’, me encuentro emocionado.”

Oh, sí. El profesor menciono algo sobre dirigir a los hikikomoris de la villa a cazar monstruos.

Él fue simplemente obligado a trabajar, pero viendo su cara de satisfacción, decidí no arruinar su humor.

“Oh, inclusive hay monstruos que no ves usualmente. ¿Cuál era su nombre? …Y pensar que había tales monstruos cerca de la villa…”

Tras decir eso Buzucoily hizo contacto visual con Yunyun.

“¡Mío es el nombre de Buzucoily, un archí-mago que usa magia avanzada… el hijo y sucesor de la mejor zapatería en la villa del Clan de Magos Carmesí…! Tu debes de ser la hija del jefe de la aldea. Encantado de conocerte.”

“¡Ah! Sí, mi nombre es Yunyun. Un placer conocerte…”

Buzucoily hizo su exagerada auto-introducción.

Luciendo apenada, Yunyun bajo su cabeza y se presentó con la voz baja.

Es raro tener la oportunidad de presentarse uno mismo. Esta chica en verdad es extraña.

“¿Qué es lo que hacían? Hay una cierta tensión en el ambiente, como si una batalla fuera a explotar en cualquier momento.”

“¡Sí! ¡Esta mujer y yo estábamos a punto de tener un duelo a muerte, para determinar quién es la más fuerte de entre las dos!

Yunyun, ¡empecemos!”

“¡Espera! ¿No tendremos un duelo normal? ¡Yo aún no tengo ese tipo de determinación!”

***

Abrí la puerta mal cerrada de mi casa y saludé a la oscuridad.

“Estoy en casa.”

Siguiendo el ruido de unos pasos, una voz de bienvenida decía.

“¡Bienvenida a casa hermana!”

Komekko corrió hacia mí sonriendo.

Su rostro y túnica se encontraban sucias.

Parece que salió a jugar de nuevo.

“Komekko, no se ha dónde es que vayas, pero han aparecido monstruos cerca de la aldea. Alguien inclusive dijo que se encontró con uno extraño. Trata de no salir.”

“¡Muy bien!, tratare de no salir.”

“… Más precisamente debería ser ‘No salgas’.”

Komekko me paso una pieza de papel.

“¿…? ¿Qué es esto?”

“Una hermosa mujer, que parecía un poco enferma, vino hace un rato. Ella preguntaba si aquí vivía un artesano de objetos mágicos, yo le respondí que aquí no vivía ningún artesano de objetos mágicos. A lo que ella respondió con un ‘a sí’. Ella se marchó después de darme esto.”

¿…? Lo vi de cerca y parecía ser una orden por objetos mágicos.

En la esquina aparecía con una hermosa letra lo siguiente:

“Estoy impresionada por los tantos excelentes objetos mágicos que usted a creado. Por favor siga manteniendo la buena relación de negocios con mi tienda…”

También había elogios por el trabajo de mi padre en la parte de atrás.

…Mi padre es un artesano de objetos mágicos. Pero, aunque los objetos mágicos son muy poderosos, todos ellos son basura.

No sé de dónde vino esa tendera, pero ella probablemente confundió a mi padre con alguien más.

De no ser así, el sentido de negocios de esta tendera sería anormal.

“Sí. Si no estaba confundida, entonces debe de ser una burla. La localización de su tienda es en la ciudad para principiantes Axel. El nombre de su tienda es…”

-Antes de poder terminar de leer, Komekko jalo de mis mangas.

“Hermana, ¡Tengo hambre! ¡Prepara la cena! Ya tengo listos los ingredientes.”

“Bien, hoy te preparare algo para comer, pero no creo que queden muchos ingredientes, solo unos pocos vegetales.”

Komekko me llevo a la cocina. La olla, los sartenes y las especias, todo estaba preparado.

Inclusive había algunos vegetales ya cortados.

Estaba pensando en hacer una sopa de vegetales, cuando escuche un repentinamente un ligero sonido de una de las ollas.

“¿…?”

Quite la tapa de la olla. Lo que encontré fue…

![](capitulo_2/img/c2.jpg)

“…Komekko, espera a que engorde antes de comerlo.”

“¡Comparado con el día de ayer hoy está más gordo! ¿Cando podemos comerlo? ¿Mañana?”

“Esperemos unos días más.”

Alimente a Komekko mucho en el desayuno, así que creí que estaría bien dejar a Kuro en casa hoy. Tal parece que aún debo llevarlo a la escuela.

-Tras decidir eso, saque a Kuro de la olla.

## Parte 3

-Hoy es día no laborable y no hay escuela.

Hoy es el peor día de la semana. Para mí es un día insufrible.

La razón es…

“Hermana, ¡buenos días! No tienes clases hoy, ¿verdad? ¡Comamos juntas el desayuno!”

“…Komekko, madre debió de haber preparado el desayuno antes de salir. Puedes comerte mi parte también. Yo pienso conservar mis calorías, así que ¡no voy a hacer nada el día de hoy!”

Tras decir eso me acosté bajo las cobijas y me quedé inmóvil.

Pero Komekko no se rindió.

“Si no va a la escuela mi hermana no puede comerse el almuerzo de Yunyun, por eso ¡desayunemos juntas!”

…tras escuchar eso, salí de mala gana de la cama.

Al no haber escuela no puedo obtener comida de Yunyun.

Normalmente tomaría el almuerzo en la escuela, para poder dejarle mi porción a Komekko que aún está en crecimiento.

Así que mi gentil hermana me invita a compartir la comida durante los días de absuelto.

“No puede hacerse nada entonces. ¿Qué hay para desayunar el día de hoy?”

“Bolas de arroz y salmón”

“… es todo lo que podemos tener al final del mes. Pero a mí no me gusta el salmón, así que Komekko puedes tener mi parte.”

“…entones a comer se ha dicho”

Dijo Komekko, viendo a Kuro que salía de las cobijas conmigo. Parece ser que ella quiere comérselo.

Le doy palmaditas en la cabeza a mí gentil hermanita…

“…(glup)…”

…apuro a mi hermanita, para que vayamos a la cocina, pero ella solo está babeando y mirando a Kuro.

“Kuro, ¿quieres comerte la piel de mi salmón?”

“Miau”

“La piel es nutritiva, así que asegúrate de comértela toda”

Después de almorzar con Komekko. Consideraba que hacer el día de hoy.

-Mi padre es un artesano de objetos mágicos, pero todo lo que hace le queda imperfecto, así que el negocio no andaba bien. Inclusive si se vendían algo, era ya con un gran descuento así que mi familia siempre era pobre.

Al final de cada mes mis padres iban a vender los objetos mágicos al pueblo, dejándonos a mi hermana y a mí solas en casa.

Mi padre menciono que saldría esta mañana y que no regresaría por algunos días.

Y no debería de haber mucha comida restante en el refrigerador.

Por tanto…

“Komekko. Voy a la villa. Tal vez pueda encontrar algo para la comida…”

¡Kashh! (Mordida)

“Miauuu”

…Después del sonido de una mordida, sonó el débil llanto de Kuro.

“… ¿Acabas de morder a Kuro…de nuevo?”

“No sabe bien cuando esta crudo.”

-Tal parece que debo apresurarme.

“Komekko, actúa bien. Tu objetivo es el viejo. Yo usare mi gran carisma en el más joven.”

“¡Mi hermana es una Femme Fatale!”

“¡Eh, eh! Komekko, no es que yo quiera hacer esto, entiendes. Es solo que la situación en nuestro refrigerador es realmente terrible…”

-Mientras discutíamos sobre esto, el pichón* apareció.

Komekko abrazaba a Kuro suavemente como a un peluche. Yo tome su mano como lo haría una buena hermana y nos acercamos al pichón.

“Ah… ¿Pero si no es Buzucoily?, el apuesto chico de la zapatería. ¡Buenos días! Hace un buen clima el día de hoy.”

“… No tengo trabajo, así que no tengo dinero que puedan estafar”

Buzucoily se encontraba practicando poses para hacer una gran entrada en su jardín. El me advirtió con una sonrisa en el rostro, haciendo el primer movimiento antes de que pudiera decir más.

Y yo que pensé que solo era un inútil hikikomori, pero parece ser que no lo puedo subestimar.

“Sería incapaz de pedirle a mi respetado Buzucoily, quien cuido de mi desde pequeña, dinero prestado. Nosotras simplemente tenemos un poco de hambre.”

“¿Dónde está el respeto? ¡Ustedes simplemente quieren estafarme! Yo no tengo trabajo ni novia, así que no hay forma que yo robe comida de mi casa para ustedes. Lo siento, será mejor que encuentren alguien más…”

“Hermanito, ¡Tengo mucha hambre…!”

“¡Espera un poco, Komekko! ¡Tu hermano mayor ira a tomar algo de comida para ti justo ahora!”

Mientras veía como Buzucoily salía corriendo, le decía a mi hermanita que aún sostenía mi mano.

“… A partir de hoy, tu puedes llamarte a ti misma ‘Femme Fatale’ Komekko.”

“¡Mío es el nombre de Komekko! Cuidar la casa es mi trabajo. ¡Soy la numero uno del Clan de Magos Carmesí Femme Fatale Hermana Menor!”

-A partir de entonces, mi Femme Fatale hermana menor recibe muchos tributos.

***

“Megumin, ¿Lo sabes verdad?”

Por alguna razón, Yunyun siempre esta animada por las mañanas. Ella se acercó a mi tan pronto como entre al salón.

“Lo sé, es hora de almorzar. Te agradezco por la comida de hoy.”

“¡¿Qué tiene que ver la hora del almuerzo con cualquier cosa?! Es que mi derrota es la única posible conclusión… Eh, a ella ¿también? ¿Te refieres a la comida para Kuro? ¡¿Tengo que alimentar a Kuro también?!”

Mientras que Yunyun se encontraba gritando, yo intencionalmente tome a Kuro que en ese momento se encontraba en mi hombro.

“No me importa si te niegas. Pero… mi familia no es rica, así que, si no aceptas, lo más probable es que muera de hambre…”

“¡Entiendo! Solo tengo que alimentarla ¿verdad?, ¡Pero eso tendrá que esperar hasta que Megumin gane! Y si tengo que preparar su comida también, entonces ¡el contenido del duelo lo decidiré yo!”

“Sin ningún problema.”

“Si no estás de acuerdo… ¿Eh? ¿En verdad está bien?”

Se lo afirme a la incrédula Yunyun.

“No tengo problema. Yunyun puedes decir el contenido del duelo.”

“¡…!”

Tras escuchar esto, el rostro de Yunyun se ilumino. Ella secretamente mostró en su mano una V de victoria. Pero su expresión cambio de inmediato.

“Sera solo un encuentro. Tú no puedes decir que solo tengo derecho a determinar el contenido de uno de tres encuentros entendido.”

“Yo no me retracto de lo que digo. ¿Por quién me tomas?”

Conteste de inmediato, pero aun así Yunyun no relajo su guardia ante mí.

“Y no dirás escusas como… ‘La que has derrotado no es la yo verdadera’, ‘Esa no es mi verdadera fuerza’, ‘Solo serás la verdadera ganadora hasta que hayas derrotado mi segunda transformación’.”

“Aún recuerdas eso de hace tanto tiempo. Por cierto, yo tengo cuatro transformaciones, pero si ganas el día de hoy, admitiré mi derrota.”

Al escuchar esto, Yunyun finalmente se relajó y suspiro.

“¡Entonces… entonces! ¡El encuentro se definirá con vencidas*! ¡No puedo perder en esto con la físicamente débil Megumin!”

Yunyun puso su brazo sobre el pupitre, se arremangó las mangas y sonrío con confianza.

Yo puse a Kuro sobre el pupitre donde pudiera verlo Yunyun, arremangue mis mangas y aliste mi brazo.

Mientras sosteníamos nuestras manos, nuestra compañera alta y con un parche en el ojo, Arue, le pareció interesante y se acercó.

“Arue, llegas en buen momento. Tú serás el juez.”

“Seguro. Nadie puede hacer trampa frente a mí ojo demoníaco. Ahora ¡listos…!”

Tras mi solicitud, Arue removió el parche de su ojo de manera exagerada y se arrodilló de manera propia en el piso.

“Yunyun, si yo gano tendrás que alimentar tanto a Kuro como a mí. Esta vez no tengo ninguna pócima. Si tú ganas, ¿qué es lo quieres?”

“¿Eh? ¿Lo que quiero? Si, si, hm… entonces… junta-juntas… A partir de mañana en la mañana, tú vendrás a la escuela junto conmigo…”

Arue, que estaba sentada en el piso, puso su barbilla sobre el pupitre. Y sosteniendo el pupitre, ella gritó.

“¡Peleen!”

“¡Ha!”

“¿Eh?¡ Ahhhh! ¡Espera! ¡Ughhhh!”

Intenté actuar de inmediato para obtener una victoria rápida en el momento qué Arue anunciara el inicio. Pero Yunyun pudo aguantar.

Yunyun que es físicamente más fuerte que yo, estaba dando vuelta a la situación.

Ya que el ataque rápido no funcionó, ¡lo único que puedo hacer es…!

“Ahh… si esto sigue así, no podré comerme el delicioso almuerzo que preparó Yunyun. Y eso es lo que con más ansias espero durante el día…”

“¡¿!? ¡Aún-aún que digas eso! Hoy es el día, ¡debo ganarte Megumin! Yo quiero ganar el título de el mejor genio del Clan de Magos Carmesí.

No simplemente el de ‘la hija del jefe de la aldea’ si no qué quiero ganar otros títulos…”

Yunyun parece molesta por el trato que se le da como hija del jefe de la aldea.

Originalmente pensé que Yunyun me retaba a diario solo porque nadie más socializaba con ella…

Pero yo. No pienso renunciar a mí título de la mejor genio el Clan de magos Carmesí…!

“Ah. ¡Si esto sigue así, no solo yo, inclusive Kurosuke morirá de hambre…! Mi familia es demasiado pobre como para alimentar a Kurobei. Por mi amado Kurotaro, no puedo perder…”

“¡¿Ah!? Si es así…. si te importa tanto, ¡no cambies su nombre cada que se te dé la gana! ¡solo lo estás usando para tomar ventaja de mi amabilidad!”

Yunyun relajo un poco el brazo mientras que acorralaba su conciencia. ¡Entramos en un punto muerto!

Arue, anunció seriamente:

“¡De momento se encuentran iguales! ¡Solo quedan 30 segundos! Si ninguna logra ganar, ¡ambas partes morirán!”

“¡¿Espera que?!” X2

Me sorprendió la última adición a las reglas echa por Arue.

Kuro entonces camino sobre el pupitre hacia dónde estaba Yunyun.

Olfateo nuestros brazos temblantes y entonces empezó a actuar de manera linda frente a Yunyun.

“Alto… alto, Kuro. ¡Aunque yo gane aun así te alimentaré….! Así que no actúes tan lindo.”

“¡Una mascota le pertenece a su amo! si yo no tengo nada que comer, le robare la comida a Kuro. Será mejor que entiendas eso primero.”

“¡Despreciable!”

“La ganadora es Megumin”

El anuncio lo da Arue, mientras levanta mi mano.

## Parte 4

“Hace algún tiempo, yo lidere a los hikikomori… no, a un grupo de valientes hombres para subyugar a los monstruos que había cerca de la villa. Lo saben ¿verdad? Como resultado ahora no hay monstruos fuertes en las cercanías. Ignoramos a los monstruos más débiles, limpiando él área solo de los peligrosos. La lección del día de hoy es combate en campo abierto. Y será en el ahora área limpia y segura, cerca de la villa, usaremos el método tradicional del Clan de Magos Carmesí para subir de nivel. ¡Todas, reúnanse en el campo! ¡Formen tres grupos de tres y uno de dos! ¡Vamos ya!”

Después de pasar lista, el profesor nos explicó lo que trataría la lección de hoy y de inmediato salió. El salón se volvió ruidoso.

Las alumnas se reunieron en base a qué tan cercanas eran unas con otras. Yunyun se sentó en su pupitre, cruzando miradas conmigo.

“¿Qué sucede? Mi auto-proclamada rival, Yunyun.”

“¿Auto-proclamada? No, no es eso… es verdad… el profesor nos dijo que formáramos grupos…”

“Así es. Tenemos que formar grupos. ¿Y eso que?”

Intencionalmente estaba usando un tono de voz distante. Yunyun parecía nerviosa.

… en verdad.

Como de costumbre, ella no puede reunir valor suficiente para invitarme a formar un equipo.

Bueno, me comí su almuerzo así que decidí tomar la iniciativa.

“Megumin. Si aún no tienes equipo ¿Quieres que formemos uno?”

Quería acercarme a Yunyun, pero Arue repentinamente me hablo.

Recordé que el grupo podía ser de tres.

“Me parece bien, hagámoslo.”

“¡¿?!”

Yunyun escuchó nuestra conversación, primero la vi confundida y luego agitada.

Arue y yo volteamos a verla.

Después de un momento, Yunyun nos miró y tímidamente dijo, “Hm, uh, Megumin, yo también…”

En ese momento antes de que acabara de hablar.

“Yunyun, ¿Quieres hacer equipo con nosotras?”

“Ah, claro. Tu siempre te quedas al final ¿verdad? Solo únete a nuestro equipo.”

Funifura y Dodonko invitaron a Yunyun.

Las dos sonrieron y se acercaron al pupitre de Yunyun.

Des pues de ser invitada por ellas, Yunyun volteo a mirarme, como si no estuviera segura de que hacer.

“Vamos, Yunyun. Todas somos compañeras después de todo.”

“Si, sí. Somos amigas después de todo ¿verdad?”

“¡¿?! ¡Amigas…! Sí, es verdad entonces…”

El termino amigas hizo que Yunyun se levantara avergonzada.

Que fácil de seducir. Que insegura de sí misma.

Tal como esperaba, en el futuro Yunyun será estafada por chicos malos.

Aun que parecía insegura, Yunyun finalmente cedió por Funifura y salieron juntas del salón.

“Ha esto se le llama poner los cuernos, verdad…”

“¡…!”

“Yo, ¡A mí no me pusieron los cuernos!”

![](capitulo_2/img/c3.jpg)

***

-La clase de hoy será al aire libre, dijo el profesor con su túnica ondeando por el viento.

“¡A Todas las presentes! Si tienen un arma propia, pueden usarla. Aquellas que no tengan ninguna, pueden usar alguna de estas para darle el golpe final a los monstruos.”

El profesor entonces señaló una pila de objetos en el suelo.

Ahí había una montaña de diferentes armas. Cabe mencionar que lo que más había eran…

“¡Profesor! Las armas son demasiado grandes. Probablemente no podamos cargarlas…”

Si, todas las armas eran masivas.

La gran espada (great sword) era más alta que Arue. La hoja del hacha era más amplia que mi cuerpo. La estrella de la mañana (morning star) lucia tan pesada que ni un ogro podría mover…

El profesor entonces levanto la gran espada con suma facilidad.

El profesor era físicamente débil y delgado, pero aun así, ¡pudo levantarla con una mano…!

“El truco es hacer que tu mana se inyecte en cada parte de tu cuerpo. Este método permite aumentar temporalmente las características físicas de los miembros del Clan de Magos Carmesí. Gracias a las lecciones anteriores, ustedes deben de saber lo básico de esta técnica. ¡Mientras ustedes lo deseen podrán usar este poder!”

Tras escuchar esto Arue dio un paso al frente y…

“… ¡Poder mágico! ¡Emana de mis arterias y concédeme la fuerza en mis extremidades!”

Después de que Arue gritó esas palabras ella levanto sin ayuda la gran espada que era más larga que su cuerpo.

“¡Oh!”x3

“¡¿Eh?! ¡Sorp-sorprendente…! Aunque es sorprendente, ¿esas líneas eran realmente necesarias?”

El resto de las estudiantes ignoraron las quejas de Yunyun y se abalanzaron a la pila de armas.

“Esta arma permanece sin daños, ¡aún tras recibir mi poder mágico…! ¡Eso amerita que yo te nombrare! Sí, de ahora en adelante ¡Tu nombre será…!”

Alguien levanto el hacha gigante con ambas manos y nombro el arma.

“¡Ha!… Esta es una buena espada, fue capaz de soportar mi movimiento especial. Bien, ¡Te confiare mi vida en un futuro…!”

Alguien agito una espada larga, mientras mostraba una sonrisa macabra.

Tras observar a las otras, yo tome un hacha gigante.

En base a mi mana, ¡yo debería ser capaz de hacerlo también…!

“…Ah, ¡no es suficiente mana…! ¡Enciéndete, mi poder mágico…! ¡Concédeme fuerza y gracia…!”

Tome el mango del hacha y me tambaleaba mientras la levantaba.

¡No hay forma de que mi mana sea insuficiente!

¡Yo soy la genio número uno del Clan de Magos Carmesí! ¡Es seguro que yo puedo completar esta tarea tan trivial…!

Mientras apretaba los dientes y levantaba el hacha. En ese momento Yunyun hablo junto a mí.

“Profesor, estas armas son falsas, ¿verdad? Parece que están hechas de madera pintada, así que son bastante ligeras…”

“Yunyun, menos cinco puntos”

“¡¿Ah?! Hey, ¡espere-profesor!”

Tire a un lado la pesada hacha y tome la espada de madera más pequeña que encontré.

-En el denso bosque a las afueras de la villa.

Todas estábamos paradas en línea frente al profesor. Cada una traía un arma de su elección.

Todas traíamos armas sin filo a excepción del arma de verdad que tenía Yunyun en sus manos.

Esa era la daga plateada que había comprado hace algunos días.

“¡Muy bien! Todas escuchen. Tal como les dije antes, todos los monstruos poderosos fueron erradicados hace poco tiempo. Así que, solo quedaron los más débiles, pero no por eso vayan a ser imprudentes. Yo usare magia para inhabilitarlos y ustedes les darán a los inmóviles monstruos el golpe final.”

Dijo el profesor mientras sostenía una gran espada ornamental.

“No debería de haber ningún problema. De suceder alguna emergencia solo échenme un grito. ¡Ahora retírense!”

Después de este anuncio él se fue a alguna parte.

Tras eso el grupo se dividió.

-En ese momento.

“¡Atadura congelante!”

Se escuchó una voz en la dirección donde el profesor corrió.

Arue y yo corrimos en esa dirección y encontramos…

“Oh…” x2

Tal como el dicho va, hasta un camello hambriento es más grande que un caballo. Después de todo nuestro profesor es un miembro del Clan de Magos Carmesí.

Una lagartija gigante se encontraba congelada del cuello hacia abajo y gruñía levemente.

Este era probablemente el trabajo de nuestro profesor.

“¡Atadura congelante!”

Se volvió a escuchar a nuestro profesor a la distancia.

El profesor se encontraba deshabilitando felizmente a todos los monstruos cercanos.

Arue y yo nos volteamos a ver.

“¿Puedo ir primero?”

Yo asentí para expresar mí consentimiento.

Arue levanto su gran espada ornamental con ambas manos.

“¡Deja que tu fuerza vital se convierta en la fuente de mi poder!”

La gran espada corto la cabeza de la lagartija. Entonces la lagartija congelada gimió y colapso en el suelo.

Arue reviso su tarjeta de aventurero y asintió con satisfacción.

Ella probablemente subió de nivel.

La Magia de Explosión que yo quiero aprender aún requiere de 4 puntos de habilidad.

Si hago esto concienzudamente ¡Tal vez pueda aprenderla hoy!

Mire a los alrededores, buscando monstruos pequeños para ganar puntos de experiencia. Repentinamente encontré a un grupo discutiendo sobre un conejo con cuerno. El conejo se encontraba congelado de manera similar del cuello para abajo. Yunyun sostenía su daga, frente al conejo mientras se quedaba inmóvil.

Tal parece que ella no podía matar sin piedad al conejo, el cual la miraba con ojos tristes mientras gruñía lastimeramente.

“Yunyun, ¡Mátalo rápido! ¡Debemos ir rápido a cazar al siguiente!”

“Vamos, Yunyun es la estudiante con el segundo mejor resultado. ¡Tú deberías ser nuestro modelo a seguir!”

Los otros dos miembros de su grupo presionaban a Yunyun, que no sabía qué hacer con su daga.

“Yo lo-lo siento, ¡cuando lo veo a los ojos…! Lo siento, ¡No puedo hacer esto!”

Yunyun agito su cabeza, mientras derramaba lágrimas. Ella trato de pasar su daga a alguna de las otras dos miembros de su grupo, pero nadie la tomo.

“¡No seas negligente durante un momento tan crucial! Nosotras como miembros del Clan de Magos Carmesí, ¡no somos debiluchas! Otras personas nos harán menos si esto se llega a saber.”

“¡Es cierto, es cierto! Además, no se puede mover, así que debería de ser fácil. ¡Muéstranos el poder de nuestra numero 2! ¡Acabalo…!”

“¡Entonces tu deberías ser la que lo acabe!”

Me situé detrás de Dodonko, quien estaba presionando a Yunyun y la empuje hacia adelante.

“¡¿Eh?! ¡Ahah!”

Arrebate la daga de las manos de Yunyun y la pase a manos de Dodonko.

Abrasé a Dodonko por detrás y guié a la sorprendida Dodonko hacia adelante.

Entonces…

“Ahora, Dodonko, ¡Acabalo! Convierte a este pobre conejo de grandes ojos en tu experiencia.”

“¡Espera! ¡Espera! Espera, Megumin, ¡ahórrame esto!”

“¿Por qué es que dudas? ¡Deja que este inocente conejo se convierta en tu poder…! No hay necesidad de molestar a nuestra número dos Yunyun. Déjame a mí, la numero uno ¡guiarte personalmente…!

“¡Espera! Detente, por favor, ¡detente! Si me empujas un poco más, ¡la daga lo apuñalara!… ¡está chillando! ¡está chillando!”

“Megumin, ¡Alto! ¡Dodonko está llorando! ¡Detente!”

En ese momento, justo cuando Funifura y Dodonko se encontraban peor.

“…Hey, deténganse todas. Hay un monstruo problemático aquí.”

Arue apunto hacia el bosque, advirtiéndonos.

Volteamos en la dirección que apunto y vimos a un monstruo.

Era un demonio con forma humanoide, con afiladas uñas en ambas manos, un cuerpo grande cubierto por un pelaje negro y un par de alas como de murciélago.

Su cabeza reptiliana tenía un pico. Y se mantenía observando a los alrededores.

Parecía ser fuerte, pero más importante, no estaba congelado.

Debemos escapar y avisarle al profesor…

-Pero este puso su atención en nosotras mientras tratábamos de escabullirnos.

## Parte 5


“Profesor, profesor, profesor-¡! “Profesor, profesor, profesor-¡!””

Funifura y Dodonko gritaban mientras corrían por sus vidas.

“Megumin, ¿Conoces a esa cosa? Parece estar concentrada solo en ti.”

“¿Cómo es posible que la conozca? Quizás forme parte de la vanguardia del Rey Demonio. Y me tema por mi poder… Por cierto, ¡¿Por qué me está persiguiendo?!”

“¡Debe ser la retribución por los actos malvados de Megumin! Hace algunos días, ¡Te vi robar la ofrenda de comida del altar a Eris!”

Arue y Yunyun decían mientras corrían a mi lado, mientras el monstruo nos perseguía.

Por alguna razón desconocida el monstruo solo me perseguía a mí, ignorando al resto.

Me preocupa lo que dijo Yunyun. Debió ver también al resto de los estudiantes. Quizás si sea mi retribución divina.

Para ese momento Arue y Yo ya nos habíamos desecho de nuestras pesadas armas.

El resto de los equipos, parecía ya haberse dado cuenta de nuestra situación, pero ¡nadie podía usar magia…!

Sentí que algo se movía en mi espalda. Y resulto ser Kuro el cual hundía sus garras en mí espalda para evitar caerse.

-Es hora de medidas desesperadas.

Tomo a Kuro y lo levanto frente al monstruo.

“No hay otra opción. ¡Te presento esta bola de pelos para ti! ¿Qué te parece? ¡Seguro que es más sabroso que yo! ¡Hasta mi hermana quiere comérsela!”

“Tú eres en verdad la mejor de la clase, mira que pensar en tal movimiento en ¡estas circunstancias!”

“¡Es que no tienes corazón Megumin! Ahora eres perseguida por los monstruos, ¡Por qué siempre haces cosas así!”

Inclusive después de ser reprendida por Yunyun, continúo levantando a Kuro lo más alto que puedo. Mientras que el monstruo vuela en círculos, descendiendo lentamente frente a nosotras.

Pese a que luce escalofriante, su comportamiento no es hostil.

Funifura y Dodonko ya han escapado. Por mi parte solo me queda enfrentarme al monstruo, mientras que el resto de mis compañeras miran a la distancia.

Sin decir ni una palabra Yunyun se coloca frente a mí. Ella sostiene su daga con ambas manos. Todo lo que se ve es su hoja plateada brillar.

Ella nos mantiene a Arue y a mí protectoramente a su espalda, mientras ella enfrenta al monstruo.

Solo Yunyun posee un arma real. Tal parece que la cobarde que no puede matar ni un conejito, quiere protegernos.

Junto a mí Arue revisa su tarjeta de aventurero. Probablemente esté considerando si debería aprender magia en este momento.

…Yo ya podría aprender magia avanzada. Pero si decido hacerlo. Tendría que rendirme sobre aprender Magia de Explosión…

Pero hasta la cobarde de Yunyun se muestra tan valiente. ¡Yo no puedo quedarme atrás…!

“Rayo de la Oscuridad, Golpea a mi Enemigo… ¡Relámpago Maldito!”

Seguido de la fuerte encantación, un golpe de luz obscura partió el pecho del monstruo.

El monstruo se colapsó en el sitio. Yo mire en dirección de la voz. Era el profesor, que se encontraba corriendo hacia nuestra dirección, mientras aun sostenía su gran espada.

El profesor, que siempre parecía estar mal de la cabeza, en estos momentos se veía tan confiable y genial.

-Mi compañera que trajo al profesor, entonces dijo:

“Profesor, usted ya había completado su encantación con tiempo de sobra, ¡¿Por qué tuvo que esperar hasta el último momento para liberar el hechizo?!”

“¿…? Obviamente fue para poderme mostrar de la forma ¡más genial posible!”

… No, para nada. Este profesor está completamente chiflado.

***

“¡Ya escuchaste! Dicen que el monstruo que ataco a Megumin, nunca había aparecido por esta área antes. De hecho, no debería de haber ningún tipo de monstruo volador en el área.”

Debido a la inesperada intervención del monstruo el entrenamiento al campo abierto, fue cancelado. Así que regresamos temprano a la escuela. Donde el rumor se esparció rápidamente en el salón.

Yo escuchaba ese tipo de conversaciones mientras jugaba con la cola de Kuro en mi pupitre.

En ese momento entro al salón el profesor, él tenía un semblante cansado. El profesor entonces subió al podio con desanimo.

“Escuchen, como mencione antes el sello de la tumba del Dios Demonio está a punto de romperse. Después de una investigación concluimos que el monstruo que apareció esta mañana durante la practica era probablemente un subalterno del Dios Demonio. En estos momentos estamos buscando los fragmentos del sello, pero nuestros esfuerzos hasta el momento han sido en vano. Debido a lo imperante de este asunto. Fui encomendado, para continuar con la cacería de monstruos… Por lo que las clases de la tarde están canceladas. Repito, hasta que sellemos de nuevo la tumba, no regresen a casa solas.”

Después de su anuncio el profesor salió del salón.

No tengo idea que se supone que es el Dios Demonio, pero es realmente bueno causando problemas.

“Megumin… hm… hoy…”

Yunyun quería decirme algo y me lanzaba miradas periódicamente.

En verdad, porque no simplemente me invita a regresar a casa juntas.

“……. Yunyun, regre-”

“Hey, ¡Yunyun regresemos a casa juntas! …tengo algo que decirte. Además, aun no me disculpado por haberte dejado atrás antes.”

Funifura interrumpió mi conversación repentinamente.

“¡¿Eh?! Ah… bien.”

La fácil de manipular Yunyun no la rechazo, simplemente asintió con la cabeza.

Si algún chico desconocido fuera a arrodillarse y suplicarle, ella probablemente también lo escucharía.

“Entonces, adi-, adiós Megumin, hasta mañana…”

Yunyun parecía un poco preocupada y solitaria, pero aun así las acompaño a casa.

….

Es algo bueno que la solitaria Yunyun haya echo amigas.

Pese a que es algo bueno, aun así, es un poco…

Siento una presencia a mis espaldas, así que volteo a ver. Arue pare estar diciendo algo.

“… cornuda.”

“Dilo de nuevo, y jugaré con tus alegres tetas.”

***

-Al día siguiente.

Llevo a Kuro conmigo a la escuela ya que desde temprano se me pego. Este gato sin vergüenza, simplemente se acomodó en mi hombro.

“Ah Megumin, buenos días… a ti también Kuro, buenos días.”

Usualmente, Yunyun me retaría muy animada desde el primer momento en que nos vemos, pero el día de hoy solamente me saludo.

“Buenos días… ¿Qué pasa contigo hoy? Normalmente en cuanto me vez, tú me retarías y te me acercarías como una bandida.”

“¡Yo no soy tan descontrolada!

Aunque puede parecer así, la forma en que lo dices debería ser… por ejemplo, rivales rentándose uno a otro…”

Dos personas se acercaron a Yunyun mientras dudaba.

Funifura y Dodonko.

“¡Yunyun buenos días! ¡Muchas gracias por lo de ayer! Como se dice, ‘¡solo conoces a tus verdaderos amigos en tiempos de crisis!’”

“¡Si, gracias! ¡Yunyun en verdad es confiable!”

“Ah, sobre eso… es genial poder ayudar a una amiga.”

Yunyun sonreía con su rostro rojo de vergüenza.

… ¿Que habrá sucedido?

“Todas, vayan a sus lugares. ¡Pasare lista!”

El profesor llego, mientras me preguntaba que paso con Yunyun.

-Después de pasar lista, el profesor seriamente empezó a escribir encantaciones en el pizarrón.

Se necesita más que puntos de experiencia, para aprender magia.

Primero que nada, debes de memorizar todas las encantaciones de la magia que quieres usar.

No hay demasiados hechizos de magia elemental y tampoco hay necesidad de recitar largos encantamientos. Sin embargo, la magia avanzada es diferente, esta requiere mucho esfuerzo para aprender.

Pero como la mejor estudiante que soy, yo ya he memorizado cada una de las encantaciones.

Yunyun al ser la segunda mejor, probablemente también los conozca todos.

Al no tener nada mejor que hacer, decido molestar un poco a Yunyun.

“Dime, ¿Qué hiciste con Funifura ayer?”

Corté la esquina de una hoja de mi cuaderno y escribí esta pregunta.

Forme una bola con la hoja y se la lance al pupitre de Yunyun.

Yunyun lo noto y leyó mi mensaje…

“Eso es un secreto entre amigas y no puedo decírselo a mi rival, Megumin.”

La respuesta que fue lanzada a mi pupitre, contenía ese mensaje.

… eso me molesto.

“Esta chiquilla solitaria, hace algunas amigas por un par de días y se vuelve arrogante.”

Le lance ese mensaje.

“Megumin siempre está fanfarroneando. Cuando de hecho ella tampoco tiene amigas.”

La respuesta vino con eso.

Lanzo una mirada a Yunyun y me la regresa con cierta satisfacción.

……

“Ya que ahora tienes amigas… Ya no hay necesidad de retarme más ¿verdad? Aunque me hace feliz que tengas amigas, también me hace sentir un poco solitaria…”

“Espera, lo siento, lo siento. ¡Yo no te retaba solo por eso!

Debido a que ayer sucedieron muchas cosas, hoy no estoy de buen humor…”

“Está bien, no es que importe. De cualquier forma, aunque siempre me quejaba de tener un encuentro contra Yunyun todas las mañanas, aun así, me hacía feliz. Y no era solo por la comida gratis.”

“¡No! De verdad ¡es enserio! ¡no me malentiendas! A mí me encanta tener encuentros contra Megumin también. Y preparar la comida a diario me hace feliz…”

“… es suficiente con que digas eso. Estoy segura de que, si no fuéramos rivales, seriamos buenas amigas.”

……

Después de lanzar ese último mensaje, no hubo más respuestas de Yunyun.

La volteé a ver. Yunyun se veía avergonzada y era incapaz de mover la pluma en su mano.

Vi el área cerca de la mano de Yunyun a la distancia. Donde se veía.

“Un día, yo quiero ser la amiga de Megumin…”

Ella se detuvo después de escribir eso.

Tras ver la expresión de Yunyun, le escribí otro mensaje en el papel, lo enrolle y se lo lance a ella.

Con el rostro completamente rojo, Yunyun vio el papel y levanto su rostro bruscamente.

Su expresión era expectante y sus ojos se pusieron llorosos. ¡Pero tras leer lo que decía el papel…!

“… ¿En verdad crees que yo diría algo así? ¡Tonta!”

-Yunyun se levantó de golpe, pateo la silla y se lanzó a mí llorando.

## Parte 6

-En el patio frente a la escuela.

Yunyun y yo aún seguíamos hablando.

“… no te soporto. ¿es que no puedes tomar una broma?”

“¡Eso no era una broma! ¡Nunca te perdonare! ¡Nunca!”

Debido a que Yunyun empezó a llorar y a golpearme, las dos fuimos castigadas y nos quedamos paradas en el pasillo el resto de la clase. Ahora tenemos entrenamiento de combate como parte de la clase de educación física.

“Ustedes las de allá, ¡son muy ruidosas! ¿Quieren ser castigadas durante esta clase también? Aunque ya hallan memorizado todas las encantaciones, ustedes no deberían de alterar la clase. Para ambas ¡menos 20 puntos! Esta lección es entrenamiento de combate. Pero el entrenamiento de hoy es un poco diferente… A ustedes dos, que se han estado lanzando miradas desde hace rato, les pregunto, ¿Qué es lo más importante, si quieren sobrevivir en una batalla hasta el final?”

Tras escuchar la pregunta. Yunyun dio un paso al frente.

Podía sentir que me observaba.

“¡Son compañeros! Si tú tienes buenos compañeros, ¡las posibilidades de sobrevivir aumentan! Claro que esto, excluye a los descerebrados, que no entienden que hay ciertas cosas de las que uno no puede hacer una broma.”

…Tu, ¡tú atrévete…!

“Si… entonces, la siguiente ¡Megumin! Para sobrevivir en una batalla hasta el final, ¿Qué es lo que más se necesita?”

“¡Es poder de fuego! Súper fuerte poder de fuego, que pueda volar lejos a las chicas solitarias que solo se quejan y hablan de compañeros. ¡Poder! ¡Poder que sobrepasa todo! En vez de actuar tímidamente para ganar compañeros, es mejor ser un mago solitario que este en lo más alto.”

“¡Ughhhhh…!”

Con los ojos llorosos, Yunyun volteo a verme.

El profesor considero nuestras respuestas y asintió levemente.

“¡Profesor! Díganos ¿Cuántos puntos?” x2

“Ambas 3 puntos. ¡Decepcionante! ¡Estoy tan decepcionado de ambas! ¡Ustedes dos pongan atención!… ¡phui!”

Este profesor de verdad escupió.

En verdad que este profesor me molesta, creo que incluso más que Yunyun.

El profesor ignoro nuestra insatisfacción.

“¡Arue! ¡Seguro que tu si entiendes! Tú eres diferente a esas dos imitaciones de miembros del clan de magos carmesí, que no tienen nada más que resultados en los exámenes.”

¡Imitación de miembro del Clan de Magos Carmesí!

Yunyun y yo enderezamos nuestras espaldas y rechinamos nuestros dientes.

Poco después de escuchar su nombre, Arue paso al frente.

Ella removió el parche de su ojo-

“Es el discurso inicial antes de la batalla. Si el monologo es bueno, aunque tengas una pésima arma y estés en desventaja numérica de uno a un millón, tu no perderás.

Inversamente, no importa que tan poderoso sea el Rey Demonio, si el activa alguna flag tal como: ‘Déjame mostrarte como es estar en el infierno’ o ‘La posibilidad de que me ganes es 0.1%’ el morirá fácilmente.”

“¡100 puntos! ¡Ten también te ganaste una pócima de aumento de habilidad! Espero que todas hayan memorizado la tradicional ‘Colección de Frases Famosas’ del Clan de Magos Carmesí. Entonces formen parejas y practíquenlas.”

Tras escuchar las instrucciones del profesor, todas las estudiantes formaron parejas.

Pero nuestra clase tiene 11 estudiantes.

Normalmente, yo me saltaría la clase de educación física, así que el número seria el justo. Pero hoy no estoy de humor para saltarme la clase.

Me levanté y le dije a Yunyun que estaba sentada a mí lado.

“… Yunyun, formemos un equipo. Funifura y Dodonko probablemente ya sean equipo. Por lo que tu solo serias un extra, ¿verdad?”

“… Bien, Megumin. ¡Formemos equipo, pero no creas que las cosas están zanjadas solo por practicar frases!”

Parece que tuvimos la misma idea.

“Profesor, me quede sin equipo. ¿Puedo hacer equipo con usted?”

“Oh, sin problema… entonces, ¡todo mundo comience!”

-De entre nuestras compañeras que realizaron sus impresionantes auto-presentaciones, solo Yunyun y yo nos quedamos viendo en silencio.

“Finalmente, es hora de acabar con esto. La más diligente será la que obtenga la victoria al final. Yo siempre he creído en eso. Aunque yo nací en una familia pobre, he avanzado un paso a la vez… Así que no perderé contra alguien que nació como hija en la casa del jefe del clan, ¡quien recibió educación de élite sin ningún esfuerzo! Te probare que la victoria no depende del talento natural, ¡si no del esfuerzo!”

“Aunque hasta el momento nunca te he vencido… pero, inclusive si la posibilidad de ganarte es muy pequeña… mientras que no sea cero, ¡yo nunca me rendiré!”

Ambas partes anunciamos nuestra determinación…

“…” x2

Después de eso nos quedamos calladas por un momento.

“… ¡Qué! Eso fue demasiado astuto de tu parte, ¡usar un dialogo como si fueras la protagonista! ¡Parecía como si yo fuera a perder! ¿No estabas diciendo antes que necesitabas compañeros? ¡Eso es incompatible con lo que acabas de decir!”

“¡Megumin, también! Antes, solo estabas hablando sobre poder de fuego, ¡Tu solo deberías de hablar como una villana! ¿Qué es eso, de ser una persona diligente? ¡Megumin es la genio natural aquí! Tu inclusive mencionaste los asuntos de mi familia también. Eso fue malvado.”

Usar el prólogo de la batalla es una forma de aumentar las posibilidades de la victoria.

Este método secreto, es tradicional del Clan de Magos Carmesí, pero no funciona con otros miembros del clan…

“¡Esto es demasiado problemático! Ya que estamos en la clase de ‘entrenamiento de combate’, ¡deberíamos dejar que nuestros puños sean los que hablen! No hay necesidad de una competición verbal.”

Yunyun anunció y tomo la iniciativa para atacar.

Para limitar mis movimientos, Yunyun se abalanzo a patear mi abdomen.

Ella estimo con precisión nuestra distancia, giro su cintura y…

“¡Miau!”

Tras escuchar el sonido que se emitía desde mi abdomen, Yunyun se detuvo.

Para ser precisa, no fue mi abdomen, sino que bajo mi ropa.

Kuro que estaba oculto entre mi ropa, fue pateado ligeramente por Yunyun, lo que causo que soltara un chillido.

“Ah…..Ah…”

Tras darse cuenta de la situación Yunyun entro repentinamente en pánico.

“Dime ¿Qué te pasa? Porque tan nerviosa. Si tu no atacas, entonces yo tomare la iniciativa.”

“Espera, espera, hey, ¡que esperes! ¡No pongas a Kuro bajo tu ropa! ¡No puedo atacarte así!”

Me acerque lentamente a Yunyun, mientras ella se ponía poco a poco más nerviosa.

“Yunyun hablaba sobre los compañeros hace poco ¿o no? Como puedes ver los compañeros no solo pueden ayudarte, algunas veces también pueden convertirse en renes, un lastre. ¡Pero por mi parte, yo pienso usar súper poder de fuego para destruir al enemigo junto con mis compañeros! Si tú puedes atacar ahora, ¡vamos adelante hazlo! ¡Si crees poder patear sin compasión al gatito que tú misma nombraste, entonces patea por todos los medios!”

“¡Eres despreciable!”

***

Después de obtener mi victoria en el duelo, caminamos juntas a casa.

“Megumin, ¡Tú nunca has peleado seriamente en nuestros encuentros!”

“No hay necesidad de quejarse después de un encuentro. ¡Yunyun es tan rencorosa!”

Yo originalmente intente regresar a casa sola, pero Yunyun uso rompimiento del sello en la tumba del dios malvado, como excusa para acompañarme. Aunque al final seguíamos discutiendo.

“Como sea, es culpa de Yunyun por no quererme decir que paso con esas dos. Eso solo aumenta mis dudas, haciendo el problema más grande. ¿Es que fue algo vergonzoso? Con que me digas solo un poco está bien.”

“¡No fue nada vergonzoso! Es solo que ellas me pidieron que lo mantuviera en secreto, ¡Así que no puedo decirle a nadie más! ¡Debo mantener a salvo el secreto de mi amiga!”

Ah… Esta chica es demasiado ingenua.

Es seguro que Yunyun va a ser engañada por algún tipo inútil en un futuro.

Por tanto, tengo que protegerla hasta entonces.

“… Olvídalo. Yunyun no quiero hablar cosas malas acerca de tus amigas, pero esas dos no tienen buena reputación. No estoy segura acerca de eso, pero al menos se cuidadosa.”

“Megumin eres demasiado desconfiada. ¿En qué clase de ambiente creciste para volverte tan desconfiada?”

“Debido a que mi familia tiene sus propias circunstancias, yo debo de ser más desconfiada que el resto. Nuestro sustento ya es de por sí difícil. Si llegara a ser engañada, toda la familia terminaría viviendo en las calles. Por ejemplo, según mi hermanita, una tendera elogio los maravillosos objetos mágicos creados por mi padre.”

“Uh, hm… Hasta yo pienso que eso es un engaño…”

Yunyun discretamente admitió que los productos de mi padre eran fallas, pero supongo que no se puede hacer nada.

Por ejemplo, un papiro mágico que ilumine toda el área una vez que fue leído.

Suena como un objeto conveniente, pero en un principio tú no puedes leer el papiro en la obscuridad. Si es que hubiera luz, este papiro sería inútil. El siempre crea ese tipo de objetos incomprensibles.

Vivir de tu pasatiempo no es algo malo, ¡pero al menos resuelve el asunto de las necesidades básicas primero!

… En fin, yo que decidí aprender la inútil magia de Explosión no estoy en posición de criticar a nadie.

Rápidamente, llegamos a mi casa-

“… yo pienso que no hay necesidad de que te aflijas por no tener amigas. Quizás haya alguien por ahí que entienda de verdad Yunyun.”

Le dije a la perpleja Yunyun y decidí regresar a casa…

Al llegar, descubro a un tipo sospechoso asechando cerca de mi casa.

“¡Hey, Megumin! ¿Quién es ese?”

Yunyun pregunto, mientras veíamos que el tipo se asomaba por la ventana para ver dentro de la casa.

Él es mi flojo vecino, el futuro sucesor de la zapatería, Buzucoily.

Si necesitara decirme algo, el simplemente hubiera venido directamente sin necesidad de esconderse por los alrededores.

“¿Qué estás haciendo?”

“¡Woah! Ah, ahah, Megumin… dios, te estuve esperando todo este tiempo. De hecho, tengo algo que discutir contigo. Pero ya es muy tarde hoy… mañana es domingo, así que no debería haber clases ¿verdad? Mañana por la mañana… si es posible, Yunyun podría acompañarnos también. Esto es algo que debe discutirse con chicas jóvenes, después de todo.”

Buzucoily dijo, mientras se rascaba la cabeza. Yunyun y yo solo nos volteamos a ver-

“-Estoy en casa.”

“Bienvenida de regreso, hermana.”

Komekko corrió hacia donde estaba.

“¿Tienes hambre?, te preparare algo.”

Le decía mientras sonreía, pero Komekko agito su cabeza.

“No tengo hambre. Ya comí bastante.”

… ¿Comió bastante?

No debería de haber comida de sobra en la casa.

Entre en la cocina mientras me preguntaba.

…Al entrar vi una gran pila de comida sobre la mesa. Estaba impactada.

“¿De dónde vino todo esto?”

Komekko me respondió seriamente.

“¡Mío es el nombre de Komekko! Cuidar la casa es mi trabajo. ¡Y soy la numero uno del Clan de Magos Carmesí Femme Fatale hermanita pequeña!”

Ella hacia su pose mientras decía esto.

Esta niña se convertirá en una gran persona en el futuro.


