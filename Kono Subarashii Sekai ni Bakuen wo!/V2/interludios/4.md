# Interludio 4: Señora Aqua, ¡Yo en Verdad fui Bendecida!

En la cocina del culto a Axis.

“Señorita Cecily, ¿Qué le sucede? Usted luce muy feliz.”

El señor Zesta me preguntaba con una sonrisa burlona, mientras que yo lavaba los trastes y sonreía como una diosa.

“Su usual sonrisa de criminal, luce especialmente desagradable el día de hoy.”

“Señor Zesta, estoy a punto de esparcir detergente en sus ojos… Hehehe. ¿Quiere saber por qué estoy tan feliz el día de hoy? ¡¿De verdad quiere saber?! Debería de decírselo~”

“…Iré a tomar un baño, usted acabe de lavar los trastes.”

“Señor Zesta, eso es demasiado. Usted fue el que pregunto primero.”

Yo tome la manga del señor Zesta quien estaba a punto de irse.

“…De hecho, yo tengo una cita con la señorita Megumin para bañarnos juntas.”

“¡¿?!”

Tras escuchar mi anuncio especial, el señor Zesta se detuvo y trago saliva.

“… Señorita Cecily. ¿Quiere decir, que usted se va a bañar con esa lolita, esa chica mágica y se van a lavar las espaldas la una a la otra?”

“Eso es lo que quiero decir. Esto es la recompensa de la señora Aqua, ¡por mi persistente atosigamiento a los seguidores de Eris!”

Por alguna razón, el señor Zesta sonrió con desdén, a mí que le anunciaba esto bastante emocionada.

“Señorita Cecily, ¡tal parece que usted lo ha olvidado! ¿Quién es la responsable de limpiar la fuente de las aguas termales el día de hoy? … así es. ¡es su turno señorita Cecily!”

“¡¿?!”

Las palabras del señor Zesta me arrojaron al infierno en un instante.

“¡Un honesto seguidor como yo, es el más adecuado para recibir la recompensa de la señora Aqua! Ya que la señorita Cecily estará ocupada y no podrá cumplir su promesa, yo iré a bañarme en su lugar…”

“Su lógica es demasiado extraña, ¡Señor Zesta! Si usted hace eso, ¡Usted será arrestado! ¡Claro, señor Zesta! ¿A usted le gusta la tokoroten Slime? De hecho, yo he estado acaparando una gran cantidad de tokoroten Slime sabor uva…”

Yo saque mi provisión secreta de tokoroten Slime de la cocina, planeaba usarla para sobornar a alguien para que tomara el día de limpieza en mi lugar.

El señor Zesta alzo su mano para detenerme a mí que trataba de levantar el saco de tokoroten Slime.

“Señorita Cecily, ágamos un trato.”

“¿Un trato eh?… usted desea mi voluptuoso cuerpo…¡!”

“No estoy interesado en su cuerpo, señorita Cecily. Eso no es a lo que me refería… ya que usted se bañara con la señorita Megumin- ¡¿Usted entiende lo que trato de decir, cierto?!”

Yo miré al señor Zesta que estaba ansioso por probar y entendí lo que quería.

“… Ya veo, ¡déjemelo a mí!”

“Señorita Cecily, ¡su perspicacia es remarcable! ¡Déjeme la tarea de limpiar a mí! ¡Yo la dejare impecable de limpio! Pero como pago- usted sabe, señorita Cecily. Usted debe… a la señorita Megumin… esto y eso… examine cuidadosamente…”

“¡Pero claro que lo sé, lo entiendo por completo, señor Zesta! No hay necesidad de detalles. ¡Yo he comprendido sus intenciones completamente! ¡Entonces, yo Cecily procederé a la batalla!”

Yo le di al señor Zesta un profundo agradecimiento.

“¡Está en sus manos, señorita Cecily! Ah… es verdad, señorita Cecily, yo nunca he limpiado la fuente de las aguas termales antes. Donde guardan el detergente usado para limpiar…”

Yo no pude contener mis emociones ni un momento más y simplemente ignoré al señor Zesta.

“Señorita Megumin, señorita Megumin, señorita Megumin, señorita Megumin, ¡Señorita Megumin! Espéreme. ¡Onee-chan definitivamente lavara cada parte de su cuerpo…!”

“Señorita Ce-Cecily, el detergente… ¡¿se trata de este?! Es este saco, ¡¿verdad, señorita Cecily?!”

Yo ignoré al señor Zesta y salí volando de la cocina.

En verdad, jamás hubiera creído que al señor Zesta también le interesara contar los cuadros de azulejo del piso.

Y yo que pensé que era la única que contaba las figuras del piso cuando limpiaba el baño.

Después de bañarme, yo le diré el número de figuras que he contado.

-Señora Aqua, ¡yo en verdad fui bendecida!
