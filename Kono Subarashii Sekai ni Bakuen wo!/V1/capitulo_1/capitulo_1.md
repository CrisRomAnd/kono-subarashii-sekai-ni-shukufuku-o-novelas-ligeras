# Capítulo 1: Los Magos de Ojos Carmesí

## Parte 1

![](capitulo_1/img/c1.jpg)

La escena de esa mañana era la usual.

El profesor encargado sostenía la lista con una mano, mientras pasaba asistencia.

“Empezare a llamarlos entonces... ¡Arue!,¡Kaikai!, ¡Sakiberi!”

Los estudiantes que eran llamados por el profesor, respondían acorde.

Todos eran chicas.

Esta escuela separa a los chicos de las chicas en diferentes clases.

Había solo once estudiantes en el salón, así que el profesor llego rápidamente a mi turno.

“¡Megumin!”

“Presente”

Después de mi respuesta el profesor asintió en concordancia.

“Muy bien todos están presentes. Entonces...”

“Pr-profesor”

La estudiante junto a mí levanto su mano, justo cuando el profesor se disponía a cerrar la lista. Ella se encontraba a punto de llorar.

“No ha dicho mi nombre aún...”

“¿Eh? Ah, ¡lo siento! Solo hay un nombre en la siguiente hoja. ¡Lo siento, lo siento! Entonces... ¡Yunyun!”

“¡Presente!”

![](capitulo_1/img/c2.jpg)

Yunyun cuyo cabello estaba atado con moños, respondió cuando se mencionó su nombre. Ella en lugar de enfadarse por haber quedado fuera, solo se sonrojó.

Está es una pequeña escuela situada en el Hogar de los Magos Carmesí.

Cuando llegan a la edad, todos los niños en la villa deben aprender conocimientos generales en esta escuela. A la edad de 12, ellos podrán obtener el trabajo avanzado conocido como “Archí-Mago” y comienzan a aprender magia.

El Clan de los Magos Carmesí, es naturalmente bendecido con una alta inteligencia y una vasta cantidad de mana. En casi todos los casos, permanecen en la escuela hasta que perfeccionan su magia.

Aquí, dominar la magia es suficiente para graduarte.

En otras palabras, todos en el salón aún no pueden usar magia.

Los estudiantes aquí acumulan puntos de habilidad (skill points) diariamente con el objetivo de dominar el tipo de magia que desean.

Diferentes tipos de magia requieren diferentes cantidades de puntos de habilidad.

Mientras más poderosa sea la magia, más puntos de habilidad requerirá.

Además de esto, el tipo de magia que los estudiantes de aquí desean aprender es sin lugar a duda-

Magia Avanzada.

Es a lo que todos los magos aspiran, la habilidad de usar todos los tipos de hechizos poderosos.

En el Hogar de los Magos Carmesí, dominar magia avanzada, es la única forma de ser oficialmente reconocidos como magos...

“Ahora anunciaremos los resultados de las pruebas. Como siempre los mejores tres recibirán la pócima de aumento de puntos. ¡Empezando por el tercer lugar! ¡Arue!”

Con mi visión periférica, vi a la estudiante que perezosamente paso adelante para obtener la pócima, entonces volteo a la ventana con aburrimiento.

“Segundo lugar, ¡Yunyun! Tu fama te precede como la hija del jefe, ¡Bien hecho! Continúa trabajando diligentemente.”

“Ah, ¡s-si!”

Mire a lado donde se encontraba Yunyun sonrojándose parada.

Además de matar monstruos para ganar puntos de experiencia y subir nivel, los puntos de habilidad solo pueden obtenerse bebiendo la rara ‘pócima de aumento de puntos’.

Por tanto, todo mundo peleaba para obtener la poción, en orden de aprender magia avanzada lo más pronto posible.

“Finalmente, en primer lugar, ¡Megumin!”

Habiendo oído mi nombre, me levanto a recibir la poción.

Yunyun, que se encuentra a un lado de mí, parece descontenta por esto.

“¡Tus resultados son bastante constantes! Siempre he creído que tienes suficientes puntos para aprender magia avanzada...

Pero no lo tomes en cuenta, ¡continúa trabajando duro!”

Después de tomar la poción y regresar a mi asiento, vuelvo a ver hacia la ventana.

Desde la ventana del segundo piso, uno puede ver más allá de la villa.

Sera que esa persona sin nombre que conocí de niña, ¿estará actualmente viajando felizmente?

Mientras que el profesor encargado se encontraba animando a otros estudiantes, yo saco discretamente una tarjeta del bolsillo de mí pecho.

Esa tarjeta es llamada “Tarjeta de Aventurero”. En la línea de trabajo está escrito “Archí-Mago”.

Nivel 1. Hay 45 puntos disponibles debajo.

En la lista de técnicas disponibles, las palabras de “Aprender ‘Magia Avanzada’ requiere 30 puntos de habilidad” se encuentran brillando.

“El resto de los otros estudiantes debería aprender de Megumin, y trabajar duro para aprender ¡Magia Avanzada! Ahora entonces ¡comencemos con la lección!”

Ignorando al profesor encargado, mi dedo toca una hilera de palabras en color gris en la lista de habilidades de mi tarjeta...

“Aprender ‘Magia de Explosión’ requiere 50 puntos de habilidad.”

En el Clan de Magos Carmesí, una persona debe aprender magia avanzada para ser oficialmente reconocida como mago, pero esa no es la magia que yo quiero aprender.

El hechizo de destrucción lanzado por aquella persona, está aún vivido en mis recuerdos.

Definitivamente voy a aprender magia de Explosión.

Y entonces un día, voy a dejar que esa persona vea mi magia.

## Parte 2

Durante el descanso, después de que acabara el primer periodo, una mano sonó contra mi pupitre con un fuerte “¡Pat!”

“¡Megumin! Tu entiendes, ¿verdad?”

La persona que está hablándome es Yunyun, quien se sienta a mi lado.

Ella es la hija del jefe del Clan de los Magos Carmesí, una estudiante modelo que además es la jefa de grupo.

“Bien. Por cierto, ¿Qué hay de mi desayuno para hoy? Estoy hambrienta.”

“¿Es así? El almuerzo de hoy fue preparado por mí con todo cariño... ¡No, está mal! ¿Esta mí derrota ya predicha? ¡Hoy definitivamente no perderé! Esta vez, ¡Te mostrare la victoria de la hija del jefe!”

Ella se autoproclamó como mí rival, pero aun así me invita de comer a diario.

Mientras Yunyun hacia su declaración, ella ponía una caja con el almuerzo sobre mi pupitre.

Yo ponía la poción que recibí antes sobre el pupitre también.

“Yo decidiré el contenido de este encuentro. La hija del jefe seguro que puede hacer esa concesión. Ya que una apuesta entre una poción rara y una caja de almuerzo es injusta de cualquier forma.”

“Entiendo. Megumin, puedes decidir el contenido de este encuentro esta vez.”

Ella es tan fácil de engañar.

“Entonces, el encuentro será durante el siguiente examen físico, quien sea más delgada y amigable con el ambiente...”

“¡Eso es trampa! No hay forma de que yo le pueda ganar a Megumin en eso”

¡Que...!

“Aunque yo decidí el tema, aun así, me enfada que lo digas con tanta confianza. Somos de la misma edad, ¡Así que no debería de haber tanta diferencia! ¡Qué tan narcisista puedes ser niña!”

“¡Ouch, Ouch! Para. El contenido del encuentro debería ser que tanto hemos crecido, oh ¿no? Si eres tan energética, ¿porque no tenemos una competencia en la clase de educación física?”

Aún estaba golpeando a Yunyun cuando se escuchó el sonido de “¡Pat!, ¡Pat!, ¡Pat!” y el resto de los alumnos se dirigió a la enfermería.

Acorde con mis largos años de investigación, el dicho que escuche de niña – “Convertirse en Archí-Maga provoca que le crezcan a una los pechos” – puede tener ciertas bases.

Probablemente, la circulación del mana, permite a la sangre fluir mejor, acelerando el crecimiento. La mayoría de las magas poderosas en la villa tienen pechos grandes.

Por tanto, como la mejor estudiante de la clase, yo tendré un gran busto pronto.

Mientras me sumergía en mis fantasías, caminaba asía la enfermería.

Yunyun rápidamente me seguía.

“Hey, ¡Megumin! Si estas tan confiada, ¿Qué te parece si decidimos esto con juegos normales? Ah, ¡no camines tan rápido...!”

Cuando llegamos a la enfermería la revisión ya había comenzado.

Esta era una clase de solo chicas. Y yo era la más bajita.

Yo pienso que es un problema de nutrición.

Gracias a mi padre que es un artesano de artículos mágicos con una sensibilidad artística especial, mi familia está en la pobreza todo el tiempo.

La inconsistencia de las comidas que tengo, es probablemente lo que está afectando mi crecimiento.

“Ah, Arue has crecido. Ya eres la primera de la clase. Dios, la siguiente es... Megumin... Hm. Ya te lo había dicho antes, aunque saques tu pecho es inútil. Uso magia de examinación, así que no cambiaran los resultados, aunque tomes un gran respiro y lo contengas.”

Mi pequeño esfuerzo fue inútil. La profesora de la enfermería usa magia para determinar mis estadísticas físicas verdaderas.

“Hm... Megumin has crecido un poquito más alta. La siguiente es Yunyun.”

“Oh, no, yo si he crecido, definitivamente perderé... Ah, en verdad perdí contra Megumin de nuevo... ¡Ouch, Ouch! ¿Por qué? Yo perdí el encuentro y mi almuerzo fue tomado. Así que ¿Por qué Megumin continúa golpeándome?”

“Pregúntale a tus malditos pechos”

“Megumin, ¡el estrés no es propicio para el crecimiento!”

![](capitulo_1/img/c3.jpg)


***

Me comí el almuerzo que obtuve de Yunyun.

“¡Megumin!, Ahí hay un pudin de alta calidad hecho de Neroid natural. ¡Es lo más adecuado para el postre!”

“Gracias. Ah, no hay cuchara.”

“Ah, lo-lo siento. Por favor espera un momento.”

Comía silenciosamente la comida de Yunyun en mi pupitre, mientras veía como ella rápidamente tomaba la cuchara. En ese momento Yunyun finalmente recobro el sentido y azoto el pudin y la cuchara contra el pupitre.

“¡Esto está mal! Yo intento usar el pudin como apuesta, ¿Por qué tengo que cuidar de Megumin tan diligentemente?”

“Yo me siento como un gatito o cachorrito, siendo alimentada por Yunyun todos los días. Así qué, ¿No va siendo tiempo que me lleves a casa? Cómprame algunas botanas en el camino.”

“¡Eh! ¿En verdad puedo...? No, ¡No es así! ¡Nosotras somos rivales! Además, tu llamado ‘cómprame algunas botanas’ no es más que tu quitándome mi comida.”

¿Cuándo me convertí en su rival?

Lo que sea, le regreso la caja de almuerzo a Yunyun después de acabarme todo el contenido.

“Gracias por tu hospitalidad. El sabor no estuvo mal hoy. Muy sabroso. Mañana, quisiera tener un poco de más proteína.”

“Ah, sí, ¿Así fue? Entonces mañana yo...”

Yunyun tomo su caja del almuerzo felizmente guardándola en su mochila y recobro el sentido.

“Yo, ya te lo había dicho, ¡esto es muy extraño! ¿Por qué debería...?”

“Regresen a sus lugares. La clase está comenzando. Hey, no traigan pudin a la escuela. ¡Confiscado!”

“¡AH!” x2

Inadvertidamente entrando al salón, el profesor encargado confisco el pudin.

El profesor ignoro a Yunyun, quien suavemente murmuraba “Mí pudin...” junto a mí, y entonces inicio la clase.

El profesor encargado escribió algunas líneas de un sistema mágico en el pizarrón, urgiendo a las estudiantes a tomar notas.

Mientras tomábamos notas en silencio, el profesor se comió el pudin descaradamente mientras explicaba.

“El día de hoy les explicare una magia especial. Primeramente, hay tres tipos de magia escritos aquí. Elemental, intermedia y avanzada. No hay necesidad de explicar más acerca de estos.

Ustedes ya saben que la magia avanzada es el nivel más alto de la magia.”

El profesor encargado escribió otros tres tipos más de magia en el pizarrón.

“En este mundo, además de la magia avanzada, existen sistemas especiales de magia, conocidas como: Magia de Impacto, Magia de Detonación y Magia de Explosión. Pese a que estos sistemas mágicos son muy poderosos, son difíciles de controlar y consumen una gran cantidad de mana. Así que muy poca gente los usa.”

Estaba atenta al pudin siendo devorado por el profesor, pero repentinamente reaccione al término ‘Magia de Explosión’.

“Primeramente, la Magia de Impacto. Esta magia puede aplastar inclusive roca firme.

Los magos que la aprenden, son reclutados por el gobierno cuando necesitan un desarrollo civil. Pero aprender magia de Impacto se requieren la misma cantidad de puntos de habilidad que para aprender magia avanzada. Así que a menos de que tú quieras ser un servidor público trabajando en ingeniería civil. Es mejor no aprenderla”

Magia de Impacto, Magia de Impacto... escribía cuidadosamente en mi cuaderno, seriamente escuchando la explicación sin perder una letra.

“La siguiente es Magia de Detonación. Esta es la magia que usaba el legendario Archí-Mago. Que de frente a la descarga de la magia de Detonación, los monstruos que se le oponían eran sepultados sin oponer resistencia. Pero esta magia consume mucha mana.

Un mago ordinario puede usarla solo unas pocas veces. Incluso si tienen confianza en sus reservas de mana, aprender esta magia no es práctico.”

Magia de Detonación -Magia de Detonación-

Y escribo abajo “Detonación, Detonación.”

En este punto, el profesor deja la charla y regresa a comer el pudin.

En verdad, aún no ha explicado el más importante ‘¡Magia de Explosión!’

“Profesor. Que hay acerca de la Magia de Explosión...”

Me levanto y alzo la mano. La atención de todas mis compañeras se fija en mí. El profesor encargado se ríe.

“Olvídense de la magia de Explosión. Requiere una cantidad 
ridícula de puntos de habilidad para aprender. Incluso un 
mago con una gran cantidad de reservas de mana no puede 
lanzarlo con seguridad debido a la inmensa cantidad de 
mana requerido. Y si fuera lo suficientemente afortunado 
para lanzarlo, el inmenso poder que tiene el hechizo, no 
solo derrotaría a los monstruos si no que cambiaría físicamente
el terreno. Si este hechizo fuera lanzado en un calabozo
este colapsaría. El ruido de la explosión también atraería
a los monstruos cercanos. Si, la Magia de Explosión es... 
simplemente inservible.”

## Parte 3

-El tercer periodo es clase de lenguaje.

“Todo mundo, para el Clan de Magos Carmesí, la gramática y el vocabulario son muy importantes. ¿Saben por qué...? ¡Megumin! Por favor podrías explicarnos.”

Después de ser nombrada, me levante.

“Por qué la velocidad al recitar el hechizo y la exactitud al pronunciarlo pueden afectar el control de la magia.”

“Tres puntos, lejos de ser suficiente.”

“¡¿Tre-tres puntos?!”

Solo recibí tres puntos... tres puntos...

Vuelvo a mi lugar desanimada. Yunyun, que está junto a mi es llamada.

“Siguiente, ¡Yunyun! Dime la respuesta correcta.”

“¡Sí!, Los sellos, la magia antigua, están escritos en lenguaje de eras pasadas. Es necesario aprenderlo de manera de que podamos descifrar maldiciones prohibidas y hechizos olvidados.”

“¡Treinta puntos! Buen trabajo al mencionar maldiciones prohibidas y magia sellada, ¡pero el resto está mal!”

“¡¿Treinta puntos...?! ... Treinta puntos...”

Yunyun volvió a su lugar desanimada. El profesor parece decepcionado y suspira profundamente.

“Vaya... ¿En verdad las dos son las mejores de la clase...?”

“¡Ah!” x2

La actitud del profesor causo nuestra exclamación, pero ese molesto profesor nos ignoró y llamo a otra estudiante.

“¡Arue! ¡Podrías decirnos tú, ¿por qué la gramática y el vocabulario son importantes para el Clan de Magos Carmesí?!”

La tercera en la clase, Arue, se levantó, alzo su cabeza y levanto el pecho.

“Para prevenir la aparición de Alias extraños como ‘Usuario de la Flama de la Flama Explosiva’. Además, para presentar un interesante monologo antes de la batalla y mejorar el ambiente.”

“¡Cien puntos! Sí, nuestros alias son muy importantes. Yo personalmente poseo el mejor alias de la villa. Cuando ustedes se gradúen, necesitaran pensar en un alias también. ¡En la siguiente clase de educación física, les daré a todas una demostración!”

***

Llamarlo patio de la escuela es una exageración. Es esencialmente un espacio abierto que crearon usando magia de fuego para quemar la vegetación frente a la escuela.

El profesor vistiendo una túnica, ha estado quemando cosas por un tiempo.

El humo que se levanta parece más espeso ahora que cuando llegue a la escuela. El profesor debe haber llegado a la escuela realmente temprano solo para hacer esto.

Mientras el humo se levanta, el cielo se obscurece.

El profesor probablemente quemo talismanes caros para hacer llover, invocando a las nubes por adelantado.

Cuando el profesor estuvo satisfecho con el tamaño de las nubes moviéndose en el cielo, ella asintió ligeramente.

“Bien. Lo siguiente será, ¡Entrenamiento de combate! Para el Clan de Magos Carmesí, ¿Qué es lo más importante durante el combate? Hm... Yunyun, ¡tú respuesta!”

“¿Y-yo? Yo creo que lo más importante, durante un combate... ¡es la calma! La calma para permanecer tranquila ante cualquier evento es lo más importante.”

“¡Cinco puntos! La siguiente ¡Megumin!”

“¡¿Cinco puntos?!”

Yunyun, después de recibir cinco puntos por la profesora permaneció deprimida, murmurando “Cinco puntos...”

¿Qué es lo más importante en un combate? ¡Eso es obvio!

“¡Poder destructivo! ¡El poder para arrasar con todo! ¡Solo el poder es lo más importante!”

“¡Cincuenta puntos! El poder sin duda es necesario. Si no tuviéramos suficiente poder destructivo el Clan de los Magos Carmesí no podría luchar. Pero no es completamente correcto, ¡así que solo cincuenta puntos!”

“¡¿Obtuve solo 50 puntos...?!”

“Yo solo obtuve cinco puntos...”

El profesor vio nuestras expresiones de desánimo y golpeo el piso, mientras decía, “Las mejores alumnas son tan decepcionantes.”

“Fuaa...”

“¡Ahh...!” x2

Ese odioso profesor ignoro nuestros lamentos y llamo a otra estudiante.

“¡Arue! ¡Seguramente, tú sabrás! Alguien como tú, que cubre su ojo izquierdo con un parche, ¡¿Qué es lo más importante durante un combate!?”

Si mi compañera se quitara el parche que cubría su ojo, ella sería considerada como “verdaderamente hermosa”. Arue, que no parece pertenecer al mismo grado que él resto, dio un paso al frente y usando su dedo índice para levantar el parche de su ojo dijo.

“Ser genial.”

“¡Cien puntos! Nada mal, Arue. ¡Te has ganado una pócima de aumento de habilidad! Nosotros, El Clan de Magos Carmesí, ¡Debemos pelear de manera elegante! Ahora se los demostrare...”

“Llamado de la tormenta de arena”

No sé qué hechizo ha estado recitando el profesor. Además de las nubes cargadas que han estado ahí desde temprano, rayos azulados brillan.

Una poderosa magia ha sido invocada, mientras se escucha el aullido de un viento poco natural.

Mis compañeras sujetan su cabello por los fuertes vientos.

El profesor toma su báculo que había preparado y lo levanta al aire.

“Mío es el nombre de Pucchin, el Archí-Mago que ejerce magia avanzada...”

Después de que el profesor declarara su nombre, un rayo golpeo la punta de su báculo.

Entonces, el profesor tiro de su túnica y esta se agito salvajemente con el viento.

“¡El más fuerte maestro encargado del Clan de Magos Carmesí, y que eventualmente se convertirá en el director...!”

Seguido de esta declaración, un golpe de trueno a un mayor que él anterior, resplandeció.

Con el rayo resplandeciendo a su espalda el profesor mantuvo su pose, sosteniendo su báculo y su túnica agitándose.

“¡Tan genial!” x3

Mientras todas mis compañeras animaban ruidosamente, mire alrededor. Solo Yunyun tenía las manos en sus mejillas mientras temblaba un poco.

Debido a que el profesor se veía tan genial, Yunyun no se atrevía a verlo directamente. Solo la escuchaba murmurar suavemente.

“¡Qu..., que vergonzoso...!”

Parece que el rumor era cierto, esta chica es rara.

He escuchado que después de entrar en la pubertad, algunos niños empiezan a admirar a gente extraña. Este fenómeno es llamado Chuunibyou. Ella probablemente lo tenga. **(Nova: Chuunibyou se traduce como “Síndrome del octavo grado” siguiendo el sistema educativo estadounidense o “Síndrome del segundo año de secundaria” ya que por lo general empieza a desarrollarse entre los 13 y 14 años. No es una enfermedad mental, simplemente es una etapa de la adolescencia, una en la que uno cree que tiene poderes sobrenaturales, es la reencarnación de un dios de la destrucción o cosas por el estilo)**

Con los vientos resoplando, el profesor finalmente se movió, y aplaudió mientras decía, “¡Bien!, ¡Hagan equipo con un compañero que este cerca! Entonces, hagan una genial auto-introducción uno a otro, ¡asegúrense de experimentar con las poses también!”

Escuchando las palabras del profesor, el cuerpo de Yunyun se agito.

Quería saber que estaba mal con ella, así que la observe.

Ella miro a los lados insegura, entonces capto mi mirada.

Seguramente quería hacer equipo conmigo, pero le era difícil preguntar después de haberse declarado como rival... que problemática.

Así que decidí hacer equipo a la fuerza con ella y hacerla llorar con mi genialidad. Pero antes de que eso pasara, alguien más interrumpió.

“Megumin, ¿Tienes pareja? Si no, ¿Quieres hacer equipo conmigo?”

Me gire y me confronte con un gran par de pechos, que no parecían pertenecer a alguien de 12 años. Era como si los mostrara intencionalmente... lo que molestaba aún más.

En ese momento, “ah” -una débil voz se escuchó tras de mí.

No fue necesario voltear. Seguramente era Yunyun.

La chica con el parche en el ojo que me hablo, Arue, parece estar haciendo ejercicios de calentamiento, rotando su cabeza y dando saltos.

Sus pechos rebotan mientras ella salta...

... ¡Ella es una enemiga!

“Bien. Basada en mis datos estadísticos, tú tienes grandes posibilidades de convertirte en una muy poderosa Archí-Maga. Antes de eso, aquí y ahora, ¡decidamos quien es mejor!”

“¿Las estadísticas siquiera pueden analizar eso?”

Yunyun contesto acorde, pero no tengo tiempo para eso.

El profesor entonces anuncio, “¿Ya todos tienen pareja? Debería de haber un estudiante extra, quien quede hará pareja conmigo.”

“¿Eh?, ¡Ah!”

Yunyun miro alrededor nerviosamente, encontrándose sola. Entonces camino hacia el profesor luciendo desanimada.

.......

“Arue, no me siento bien hoy. Quiero descansar durante el resto de la clase de educación física. Quizás la culpa la tenga la comida que obtuve de Yunyun, podría haber traído algo raro en ella.”

“¡¿Ah?!”

Después de escucharme, Yunyun mostró una expresión de gratitud y sorpresa.

“Profesor, no me siento muy bien. ¿Puedo descansar un poco durante la clase?”

“¿Qué? No. Tu nuca has atendido propiamente ni una sola clase de educación física. Y la lección del día de hoy es especialmente importante. Así que no pretendas estar enferma.”

En contra del incesante profesor, gemí y colapsé en el suelo.

“No. No intentes ese truco conmigo...”

“¡Yo-yo no puedo contenerlo más...! si esto sigue así, la que está conmigo, ¡tomara posesión de este cuerpo...!”

“¿Qué? Megumin, ¡tu...! Así que lo que se encuentra sellado en tu cuerpo, ¡¿Está despertando...?! Bien no se puede hacer nada entonces, tienes permiso de ir a descansar a la enfermería. Dile a la profesora encargada que refuerce el sello.”

“Entendido, entonces me retiro ahora.”

“-Bien, ¿Todas tienen pareja? ¡Empiecen!”

Escuche las instrucciones del profesor, mientras caminaba hacia la enfermería.

Si por la clase de educación física gastaba las calorías que tanto trabajo me costó obtener del almuerzo de Yunyun, sería un desperdicio.

Después de recibir una medicina nutricional con poder para reforzar los sellos (la cual también se encuentra disponible en el mercado) de la enfermera, me acosté en una cama.

En la tranquila enfermería, jalé la sabana hasta mis hombros y me puse a pensar sobre lo que menciono el profesor.

-La magia de Explosión es una magia inútil.

Cubrí mi cabeza con la manta llena de resentimiento.

“¡Profesor! ¡Está lloviendo! ...o en lugar de eso, ¡Está lloviendo arena! Ya vimos la genialidad de nuestro profesor, así que, ¿Puede hacer que pare?”

“Los tulipanes cuidadosamente plantados por el director en el jardín se van a volar”

“¡No, no puedo detenerlo! Esto es terrible. La luna que es la fuente de mi poder mágico, hoy está en su punto más alto. ¡Mi limitador de poder ha sido liberado...! ¡Déjenme detener esta lluvia! No se preocupen por mí, ¡Refúgiense en la escuela!”

“¡Profesor! Siendo honestos, usted solo pensó en la actuación de hace un momento y ¡no pensó en como parar esto para nada!”

Mientras escucho las voces desde el patio, cierro lentamente mis ojos-

## Parte 4

“Hey, Megumin, ¿Porque estas frente a mi pupitre, mostrando tu pócima de aumento de habilidad? ¿Hay algo que quieras decir?”

“Nada... Ah sí, Yunyun tu almuerzo luce delicioso el día de hoy.”

“¿Es así? A parte de la caja de almuerzo que me robaste, prepare una porción extra... Ah, ¡No voy a dártela! Esta porción no es para apostar. Si esta caja de almuerzo también es robada, entonces no tendré nada para comer, ¡Así que no pienso retarte!”

“.......”

“Detente. No muevas tu poción así frente a mí. ¡Rápido bébetela ya!”

“.......”

“¡No-no! ¡No te lo daré! Auu-aunque pongas esa cara que luce tan triste... yo solo... solo te daré la mitad...”

Después de comerme el almuerzo que obtuve de Yunyun, la escuela emitió un anuncio.

“Basado en el análisis del profesor Pucchin, la misteriosa lluvia que apareció esta mañana. Sin duda fue causada por el dios maligno de la lluvia y la pereza que se encuentra sellado en alguna parte del Hogar del Clan de los Magos Carmesí. Después de revisar, el director ha confirmado que esta llovizna tiene rastros de influencia mágica, por lo que es artificial. Todos los profesores se encuentran controlando la lluvia, así qué las lecciones de la tarde se cancelan. El clima de afuera actualmente tiene fuertes vientos, truenos y lluvia pesada, lo que lo hace peligroso para que los alumnos que regresen a casa.

Por tanto, quédense a estudiar en la escuela.”

Parece que el profesor le echo la culpa al dios malvado.

Mientras veía como aprovechar esta situación para conseguir algo de comer, algunos de los estudiantes se levantaron.

Probablemente se dirijan a la biblioteca de la escuela a pasar el tiempo.

-Hay algo que yo también quiero investigar.

Salvajemente empujo la comida que tome de Yunyun hacia mi boca...

“¡Te había dicho que la mitad! ¡Solo te había dado la mitad!”

Después de arrojarle la caja del almuerzo a Yunyun, corrí hacia la biblioteca.

Esta escuela esta en el Hogar de los Magos Carmesí, que produce tantos archí-magos, así que la biblioteca está bien surtida. Desde leyendas incalificables hasta dudosos manuales prácticos.

Yunyun, que vino por su parte, veía algo en los estantes de manuales prácticos.

“Manual de abstinencia mágica para hacer amigos”, “Guía: Incluso los caracoles pueden ser buenos en compañeros” ...

Yunyun tomo algunos libros cuyos títulos no pude entender, pero viendo que sus ojos brillaban de emoción, decidí quedarme para mí misma, las palabras que murmuro.

Continúe buscando el libro que quería, arrastrando mi dedo sobre los libros que se encontraban en el estante.

“Orígenes secretos, del Clan de los Magos Carmesí”, “Hasta la caída del Reino Mágico”, “Colección de Duques del Infierno, Volumen 4, El Demonio de las profecías”, “La verdad acerca de los residentes de otro mundo”

.........

Aparecían libros interesantes uno tras de otro. Hasta que encontré lo que estaba buscando.

“Utilidad de la Magia de Explosión” .... Tome el libro y lo revise.

“La Magia de Explosión es el último hechizo de destrucción. Es la más poderosa magia ofensiva que puede herir a cualquier cosa en existencia. Actualmente, la forma de aprender este hechizo se encuentra olvidada y solo es conocida por algunos humanos que se han dedicado varios años a la investigación y a los magos no-humanos. Esta no solo es difícil de aprender, sino que también su uso es limitado. Debido a eso los magos que usan ese hechizo también son conocidos como Magos Mina y frecuentemente son rechazados por los aventureros que buscan miembros para su equipo.”

Después de leer esto mi admiración por la magia de Explosión, ...se vio ligeramente sacudida.

En la escena que atestigüe de niña, era una magia destructiva que lo violaba todo.

Siempre he admirado a esa persona encapuchada y su magia-

“Primeramente, una persona común no puede usarla, inclusive si la aprendió, esto debido a la gran cantidad de mana que consume. Es un misterio él porque es que se desarrolló una magia así. En actualidad solo los magos no humanos que tienen largas vidas y que usan caprichosamente el exceso de puntos de habilidad que tienen, son los que aprenden este hechizo...”

...leer esto es un poco... regreso el libro a la estantería.

Tengo la impresión que si continúo leyendo doy a terminar más devastada.

En ese momento, me percato de un título interesante a un lado de donde deje el libro.

“Rodas el indomable”

Atraída por el extraño título, me acerco y lo tomo.

-La historia era acerca de un rey viejo y senil que acompañado por dos de sus sirvientes viajaban por su territorio con el fin de cambiar el mundo.

Debido a varios incidentes inesperados, los aldeanos descubren la verdadera identidad del viejo y acusan al gobernador de varios crímenes.

El gobernador insiste en su inocencia y afirma que los aldeanos son los que mienten.

El viejo declara que ambas partes son culpables y dicta sentencia tanto a los aldeanos como al gobernador corrupto. Como resultado de esto los aldeanos y el gobernador se unen y derrotan al viejo.

El viejo se enfurece y declara que arrasara ese lugar, mientras que sus sirvientes lo consuelan diciéndole que “es hora de comer” y lo llevan de regreso a casa.

Después de pelear juntos en el mismo lado, los aldeanos y gobernador aprenden de los beneficios de la unidad. No mucho después de eso, ellos construyen ahí una ciudad, sin igual-

... ¿Dónde está el siguiente volumen?

Buscaba la secuela del libro.

“Hey, ¿qué pasa con ese libro? ¡Es súper divertido! Que ¿no tienes amigos?”

El tranquilo ambiente de la biblioteca se ve interrumpido bruscamente.

Busco que es lo que pasa y veo a Yunyun y otra compañera... ¡esta situación es! ....

“Amigo... eso es...”

” No tienes ninguno ¿verdad? Sino porque leerías... ‘Puedes hacerte amigo de los peces’¿...? Oye. Olvídate de ese libro. Al menos escoge uno con mamíferos...”

“¡Alto justo ahí!”

Salto entre Yunyun y esta compañera de clase, mientras los separo severamente.

“Si tu objetivo es primero molestar y jugar con esta chica lamentable, para después tomar ventaja de su corazón roto y pretender ser su amiga en orden de que cumpla toda una serie de demandas irrazonables, ¡¿Verdad?! ¡Incluso si puedes engañar a otros no puedes engañarme a mí!”

“¡¿Ha?!”

Después de ser expuesta por mí, la compañera parece entrar en pánico.

“Hey, espera ¡No sé de qué estás hablando! Yo solo le hacía platica por qué vi a Yunyun sosteniendo un libro interesante.”

“Meg-, Megumin, ¿Que sucede? ¿No estarás siendo influenciada por algún libro extraño que acabaras de leer? He. Solo es alguien conversando conmigo...”

Ambas tratan de explicarme por separado, pero-

“No, yo simplemente detecté un ambiente hostil, así que intervine porque no tenía nada mejor que hacer. Y debido a que no tome la clase anterior, yo fui la única que no practico la auto-introducción, así que estoy un poco insatisfecha.”

“¡Eso es demasiado irrazonable!” x2

Probablemente debido a que alguien escucho los gritos de Yunyun y la compañera, las puertas de la biblioteca se abrieron.

“Hey, ustedes son demasiado ruidosos. Guarden silencio cuando estén en la biblioteca. Ya pensare en alguna forma para detener la lluvia del dios demonio. Con mi poder y el del director tendremos suficiente poder para suprimir al dios maligno de alguna forma...”

“¡Profesor, pero no nos dijo que ese era su poder que se encontraba sellado y fue liberado...! Es muy triste que el dios maligno se lleve la culpa de todo”

Nuestra compañera, dijo todo lo que quería decir sobre nuestro profesor irresponsable.

Pero el profesor solo nos respondió.

“No, los pobladores han revisado la tumba del dios maligno. Parece que algún idiota arruino el sello y lo rompió. Aparentemente, aún faltan algunos fragmentos del sello. El dios maligno y sus sub-alternos que fueron sellados, quizás marchen a la batalla en cualquier momento. Después de todo, el sello fue hecho específicamente para el dios demonio, así que sus sub-alternos (minions) quizá ya hallan escapado. Debido a eso, hasta que sea re-sellado, por favor no vayan a casa solas.”

***

“Hey, sabias que hace como 7 años, cuando aún éramos niños, el sello del dios malvado fue roto. Encontraron un gran oyó frente a la tumba donde esta sellado el dios malvado, ¿Es verdad? Cuentan que es la marca que dejó el mago viajero, que volvió a sellar al dios malvado.”

De regreso al salón mis compañeros discutían sobre ese tema.

En esta villa, inclusive esos rumores tan increíbles, son sujetos de discusión.

No puedo recordar todo lo que me pasó de niña, pero el mago viajero que mencionaron, debe de ser la persona encapuchada que me rescato.

Más tarde debido a que los profesores salieron a investigar lo concerniente al dios malvado, nos dejaron salir temprano de la escuela.

Los alumnos salieron en grupos según el lugar de donde viven. Solo Yunyun y yo permanecemos en el salón.

Mi casa está situada a las orillas del Hogar de los Magos Carmesí, así que nadie más vive cerca.

Esa fue probablemente una de las razones por lo que nadie me invito a acompañarlos.

Un grupo que se dirigía a comprar golosinas se les podía escuchar decir cosas como, “Esto es sabroso” “Esto también está bien”.

Mientras que yo sin ninguna otra opción, decidí ir a casa sola.

“Ah...”

Yunyun, quien también estaba en el salón, estiro su mano hacia mí, mientras murmuraba algo. Parece ser que se dirigía a mí.

¿Qué pasa?

“¿Uh? No, nada, es solo que... la casa de Megumin está de camino a mí casa, así que...”

En realidad, la casa de Yunyun es la casa del jefe de la aldea, la cual está situada justo en medio de la villa.

Si ella quisiera caminar conmigo a mí casa, tendría que tomar una desviación...

“... ¿Quieres que regresemos juntas?

“¿De verdad puedo? ...Ah, pero nosotras somos rivales. Hacer algo así...”

El rostro de Yunyun luce expectante y aun así dice cosas tan problemáticas. Decido ignorarla y salgo rápidamente del salón. Yunyun sale tras de mí.

“¡Espera! ¡A partir de mañana! Regresaremos a ser rivales, ¡a partir de mañana...!”

-Una vez afuera con Yunyun, veo como las nubes se mueven de forma poco natural.

¿De verdad el profesor causo todo este fenómeno solo con el fin de una presentación?

Usar un talismán caro, solo para parecer genial, nuestro profesor es un verdadero miembro de los Magos Carmesí.

Aunque nuestro profesor encargado es inútil en muchos aspectos, esto es algo en lo que tienes que reconocerlo.

Camine a casa sin detenerme. Yunyun quien me seguía, habló algo vacilante.

“Megumin, ¿Tienes algo de tiempo? Mmm, si fuera posible...”

Yunyun me invitó a comprar algunas golosinas en el camino e inclusive dijo que ella las pagaría.

“Por supuesto, no tengo razón para rechazarte, ¿Pero por qué razón sería?”

“¿Eh? Sol-solo por qué tengo un poco de hambre...”

Yunyun parecía avergonzada.

“Bueno, estás en la etapa de la pubertad después de todo. Pero como chica, ¿no es un poco raro que tengas tan gran apetito?”

“¡Espera! Megumin, ¡Tú no tienes derecho a decir eso! ¡Si tengo hambre es por qué tú te comiste mi almuerzo! Además...”

Yunyun repentinamente bajo la voz.

“Comprar golosinas con una amiga e ir de compras de camino a casa... eso es algo... que yo de alguna manera quería...”

“¿Ah? ¿Qué dijiste?”

Yunyun susurró algo, así que yo intencionalmente di la vuelta y puse mi oído a su lado para poder escucharla mejor.

En un principio Yunyun mintió por vergüenza, diciendo que no había dicho nada. Pero después de interrogarla múltiples veces, hasta las lágrimas, hice que repitiera lo que dijo antes claramente.

## Parte 5

“¡Pasen adelante! ¡Sean ustedes bienvenidas a la mejor cafetería del Clan de Magos Carmesí! No eres tú Megumin, la hija de Hyoizaburo, he escuchado que eres muy aplicada en la escuela. Todo mundo dice que eres una de los más grandes genios del Clan de Magos Carmesí. Pocas veces te veo comer fuera, ¿Que te gustaría ordenar?”

“Quisiera algo con bastantes calorías, que te haga sentir llena fácilmente.”

“Megumin, ¡Esa no es la forma en la que una chica deba ordenar comida! Hm. La recomendación del chef...”

Yunyun y yo no sentamos en la terraza de la única cafetería de la villa.

El dueño nos trajo el menú. Parece ser un conocido de mi padre.

“Recomendaciones... ‘Guiso de la bendición del dios oscuro’ además ‘Fideos picantes en aliento del dragón de lava’.”

“Yo quiero los fideos picantes.”

“Para mí, ... esto del menú, ‘emparedado del cordero sacrificado al dios demonio’.”

“¡Muy bien! Fideos picados en aliento del dragón de lava y emparedado de cordero sacrificado al dios demonio. ¡Un momento por favor!”

“¡Ordene fideos picantes!”

Sonrojada, Yunyun corrigió el nombre del plato con un tono serio.

Mientras bebía lentamente el jugo de fruta que ofrecían.

“Megumin, Megumin. Aunque puede ser repentino, ¿Puedo hacerte una pregunta?”

“¿Qué será? Bueno ya que tú invitas, responderé cualquier pregunta normal que me hagas. Si es sobre mi debilidad. Mi debilidad actual son los dulces después de comer. El postre es mi debilidad.”

“¡No estoy preguntándote eso! Además ¿Qué clase de debilidad es esa? No normalmente te lo comerías a grandes bocados.”

“Es un dicho común –’¡Los dulces son los enemigos de las chicas!’... ¿Entonces qué quieres preguntarme?”

Repentinamente, Yunyun duda sobre si continuar. Así que la presiono para que siga.

Ver las expresiones de esta chica hace que despierte mi sadismo.

“Megumin, ¿Hay algún chico que te guste?”

“¡Yunyun está en celo!

Después de escucharla, me levanto impactada. Mientras Yunyun trata de explicarse casi llorando.

“No, ¡No es nada de eso! Piensa un poco acerca de eso. La plática entre chicas suele ser trivial, ¿cierto? ¡Yo simplemente buscaba ese tipo de conversación! ¡Yo no tengo a alguien que me guste!”

Sus palabras me tranquilizaron así que vuelvo a sentarme.

“¿Cómo decirlo? Yunyun, eres una chica extraña para los estándares del Clan de los Magos Carmesí. Escuche que, durante la clase de educación física, no podías hacer una pose genial debido a la vergüenza.”

“¿Yo soy la extraña? Desde niña, yo siempre he creído que los pobladores de esta villa, son un de alguna manera raros...”

Extrañamente Yunyun parece desanimada por lo que le dije.

Ella no tiene amigos en clase, probablemente por su rareza.

“Entonces, ¿Qué tipo de chicos te gustan Yunyun?”

“¿Eh?”

Enfrentándose a mi pregunta, una sonrojada Yunyun pones sus ojos en blanco por el impacto.

“¿No querías hablar cosas triviales? Por cierto, para mí, yo no aceptaría a ningún chico que prestase dinero solo para beneficiarse por ello. Lo mejor sería que tuviera grandes metas, que trabajara duro noche y día y que fuera honesto y serio”

“Alguien serio y gentil, he. Megumin tiene un lado amable y es buena cuidando a otros, así que acabaras atrayendo al tipo opuesto, aquellos vagos insalvables... ¡eso duele, para duele!

¡Solo bromeo! ... A mí me gustan maduros y robustos, del tipo que escuchen lo que me paso durante el día...”

-Una tarde tranquila.

Platique interminablemente con mi auto-proclamado rival, mientras regresábamos a casa.

***

“¡Estoy en casa!”

“¡Bienvenida a casa, hermana!”

Tras llegar a casa mi hermanita, que es unos cuantos años menor que yo, salió a recibirme.

Komekko, qué apenas tiene 5 años de edad, solo es un poco más bajita que yo.

Ella viste mi vieja túnica. Por lo que la parte que le sobraba estaba manchada con lodo.

“Ah... Las orillas de la túnica están enlodadas. Te pedí que cuidaras la casa. ¿Saliste a jugar de nuevo?”

“¡Sí! Derrote al repartidor de periódicos y después salí a jugar.”

“Oh, así que hoy volviste a ganar. En verdad eres mi hermanita.”

“¡Sí! Le dije ‘No he comido ningún alimento solido en 3 días.’ Después de eso, el dejo algunos cupones para comida.”

Komekko me mostró los frutos de su arduo trabajo, con satisfacción.

Mientras le daba palmaditas en la cabeza a mí excelente hermana, Komekko repentinamente detecto algo.

“Percibo cierto aroma en el cuerpo de mi hermana...”

“En verdad eres mi hermana menor. Traje esto conmigo. ¡’Emparedado de cordero sacrificado al dios demonio’! ¡Come hasta explotar!”

“¡Sorprendente! Es tan genial como convertirse en el Rey Demonio. Entonces, la cena que capture para hoy se pasara para el desayuno de mañana.”

Komekko, quien ama los emparedados, dijo repentinamente.

...la cena fue capturada.

Recuerdo que hace algún tiempo Komekko capturo algunas cigarras y sugirió que las friéramos para comerlas. De repente me sentí aterrada.

“Komekko, ¿Qué dijiste que habría de cenar? ¿Qué fue lo que capturaste?”

“¿Quieres ver? Lo derrote después de una dura batalla mortal. ¡Una bestia negra!”

Komekko dejo esta declaración y regreso corriendo hacia la casa.

¡Espero que no sea un insecto! ¡Espero que no sea un insecto!

Mientras esperaba, seguía rezando. Poco después Komekko trajo algo cargando...

“...Miau...”

Era un gato negro que, por alguna razón lucia exhausto.

![](capitulo_1/img/c4.jpg)

“...esta vez atrapaste uno grande.”

“Sí. ¡Trabaje duro! En un principio se defendió, pero se volvió dócil después de que lo mordiera algunas veces.”

“Aunque ganar es algo bueno, no debes ir mordiendo cosas aleatoriamente.”

Komekko asintió obedientemente después de escucharme. Yo tome al gato negro. Él se escondió rápidamente en mis brazos. Parece que paso por una mala experiencia, con temor trataba de poner su cabeza en mi pecho.

Komekko le dio grandes mordidas al emparedado, después tomo una pausa y observo el emparedado aun en su boca para ofréceme un poco.

“... ¿Quieres comer?”

“Yo ya comí. Tú puedes comértelo todo. De cualquier manera, yo me encargare de esta bola de pelo, ¿Esta bien?”

“¡Esta bien!”

Komekko estaba complacida comiendo el emparedado.

-Deje el gato en mi cuarto. Y el cínicamente se enrosco sobre mí cama, yo murmure suavemente...

“¿Qué voy a hacer con este gato?”

Para tener tan poca vergüenza, quizás no sea un simple callejero.

Pero no puedo dejar que se convierta en el desayuno, tal como Komekko desea. Tampoco hay comida de sobra en la casa como para alimentarlo.

Si lo libero y es atrapado por Komekko de nuevo, esta vez definitivamente se volverá la comida de Komekko... Entonces...

## Parte 6


-Esta mañana el salón de clases era muy ruidoso.

“… Megumin…

… Megumin…”

“Buenos días, Yunyun. ¿Por qué tienes esa expresión en tu rostro?”

Yunyun frunció su ceño y me saludo ansiosa.

“… ¿Qué es eso?”

“Mí familiar.”

Yunyun me preguntaba acerca del gato negro que se encontraba acostado en el pupitre mientras jugaba con mis dedos. Se los presentare a todos.

“Este es mi familiar”

“¡¿Familiar?! Creí que solo los magos de los mitos poseían un familiar.”

“¡Miren su cara linda y sin miedo! Ese gato asusta. ¡Debe estar pretendiendo ser puro e inocente mientras observa nuestras cajas de almuerzo para su ama Megumin!”

“¡Eso no es lo que buscaba! Pero- si hay algo de comida…”

Mis compañeras quedaron encantadas por el carisma de mi familiar.

Quizás este gatito en verdad posea el poder de cautivar.

Lo mejor de todo es que parece que puede obtener su propia comida si se queda aquí. Eso me tranquiliza.

“¡Wow, wow, wow! … ¡Es tan suave y blandito…! ¿Cuál es su nombre Megumin? ¿Ya le disté un nombre?”

Los ojos de Yunyun brillaban, probablemente ella quería tocar al gatito. Pero cuando Yunyun estiro su mano, el gato levanto su pata amenazante.

Viéndose rechazada por el gato, Yunyun bajo la mano con un sentimiento de soledad.

“¿Qué pasa con este gato? Parece que no es demasiado amistoso con nadie salvo Megumin.”

Mientras Yunyun decía eso, el gatito acepto el dedo estirado que le ofrecía Arue y disfrutando de las caricias que le daba se le podía ver con los ojos entre cerrados. Tras presenciar esto Yunyun parecía estar a punto de llorar.

“Aún no tiene un nombre. Si lo dejo en casa su vida corre peligro así que pensaba traerlo diario a la escuela.”

Al escuchar esto, mis compañeras lucían preocupadas.

“El gatito es lindo, así que está bien por mí, pero que creen que sea lo que el profesor pensara…”

“Sí, aunque es muy lindo, el profesor probablemente no lo permita. Aunque de verdad es muy lindo.”

Como lo esperaba el profesor encargado será el mayor obstáculo…

“Rechazado.”

Tras entrar eso fue lo primero que pronuncio nuestro profesor encargado.

Viendo esto me dispongo a levantar al gato, el cual puso su mayor esfuerzo para parecer lindo.

“Profesor, este es mí familiar. Se alimenta de mi mana. Si se aleja de mí, es seguro que muera.”

“Aun así es ¡No! En un principio ¿Cómo es posible que alguien que aún no puede usar magia, tenga un familiar? Además ¡La escuela prohíbe traer familiares y bocadillos! Regrésalo a donde lo encontraste.”

Como esperaba, no funciono. ¡Entonces…!

“Profesor, este es mi otro yo. Mí otra mitad que posee mí poder. Aunque la mayoría del poder está conmigo, aun así, es una parte de mí. ¡Somos uno en mente y no podemos ser separadas!”

“…Parece ser que tu otra mitad odia ser cargada por ti y forcejea como si luchara por su vida.”

“Eso es… porque estoy a punto de entrar en mi fase rebelde.”

Pongo en el piso al gato y este empieza a arañar las paredes del salón.

“Ahora, tu otra mitad está aliviando su instinto de afilar sus uñas.”

“El Clan de Magos Carmesí debe estar siempre listo para la batalla, así que por eso afila sus garras. Ya que la mayor parte de la inteligencia y razón esta conmigo, mi otra mitad solo tiene el poder y el instinto de su apariencia animal.”

“Entonces no hay problema que se quede.”

“A primera vista, se parece a una muy linda yo, pero por dentro…”

Usted…

“… ¿lo permite?”

El profesor sorpresivamente lo permitió sin mucho alboroto…

Originalmente, yo aún planeaba explicar como yo y mi otra mitad teníamos un conflicto mortal sobre el control de mi cuerpo.

“Ya que parece muy divertido, que así sea.”

Nuestro profesor es bien conocido por entretenerse con asuntos extraños, pero su declaración me deja intranquila.

“¡Hey, Megumin! Orinar y cagar solo está permitido en el área designada.

Mira, es aquí, ¡aquí! ¡Solo puedes orinar aquí! … ¡Bien hecho!

¡Megumin es realmente impresionante!”

“……”

“¿No olería mal si dejamos los desechos de Megumin aquí? Lo mejor sería dejarlos hasta atrás.”

“……”

“Ah, en verdad, ¡Megumin! ¡No afiles tus garras en cualquier lado!

¡No muestres esa linda expresión, mientras inclinas tu cabeza!

No… Ah, ¡Megumin es demasiado linda!”

“¡AHHHHH!”

“¡Oh no! ¡La falsa Megumin enloqueció repentinamente! Parece ser que no solo es su lindura, ella también finalmente perdió su razón e inteligencia contra su otra mitad”

La compañera que me llamo “Falsa Megumin” anuncio a todos, después de verme voltear mi pupitre.

“¿A quién le llamas falsa? ¡Yo soy la Megumin real! ¡No sigas llamando a eso Megumin una y otra vez!”

“¿Cuál es el problema, Megumin? Tu dijiste que la Megumin que esta por allá es tu otra mitad ¿Cierto? Tu eres la Megumin que posee inteligencia y razón, mientras que la otra Megumin pose el poder y el instinto, ¿Verdad?”

“Megumin, Megumin, Megumin, Megumin – todo el mundo está usando mi nombre, ¡ya no puedo soportarlo! ¡Por favor alguien póngale un nombre a este gato!”

Viendo mi expresión de enojada, Yunyun abrazo a mi otra mitad.

“Aunque digas eso, está decidido que por el día de hoy ‘Esta es la verdadera Megumin’… Mira finalmente empieza a aceptarme. ¡Ahora puedo abrazar a Megumin!

… En lugar de darle un nombre a este pequeño, ¿Por qué no le cambiamos el nombre a la otra Megumin? ¡Ouch, ouch!”

“¡Traidora! ¡No te importa que tu rival cambie de nombre!”

El nombre de Megumin fue dicho el día de hoy más veces ¡que en todos mis días de escuela combinados!

Tras escuchar mis quejas, mis compañeras mostraron una expresión amarga.

“Y ahora que finalmente teníamos a una linda Megumin.”

“Ah… Mí linda Megumin está siendo molestada por la nada linda Megumin…”

“Hey, ¡¿Estás buscando pelea?!”

Como un miembro del Clan de Magos Carmesí, nunca rechazo una pelea, así que me alisto y levanto mi silla, lista para poder arrojársela a mis compañeras en cuanto sea necesario.

“…Norisuke.”

Arue pronuncio palabra por palabra, considerándolo como el nombre alternativo del gato.

“…Perekichi.”

Otra estudiante opina.

“Choisa”, “Marumo”, “Kasuma”.

Pero parece que ninguno de estos nombres fue satisfactorio. El gato desde los brazos de Yunyun hizo un ruido con su nariz, como si estornudara.

Escuchando los incontables nombres alternativos, Yunyun levanto al gato.

“Este gato es hembra…”

“…Como esperaba, el nombre más apropiado sigue siendo Megumin.”

“¡Te matarte!”

Mientras confrontaba a esa compañera, Yunyun grito repentinamente.

“¡Kuro! ¡Kuro!… ¿Qué les parece? Ven, ya que es un gato negro…”

“…….” x2

Todo quedo en silencio.

“Bueno, no esta tan mal. Después todo es más fácil recordar los nombres raros.”

“¡¿Eh?! ¡Seguras, ese nombre es- muy muy raro…!”

…Sí, en verdad era un nombre extraño, pero quizás por eso sea más fácil de recordar.

Después de ser nombrado, el gato aún en los brazos de Yunyun, abrió los ojos y parecía feliz.

“Así será, temporalmente usaremos ese extraño nombre ‘Kuro’. Cuando en verdad se convierta en mi familiar, entonces seriamente le brindare un grandioso nombre.”

“¿Extraño? Hey, en verdad ¡¿Yo soy la rara?! En esta villa, la que es extraña ¿soy yo?”

Ya que Yunyun se quejaba con lágrimas en los ojos. Yo abrase a Kuro.

***

“Hey Megumin. ¿Te gustaría ir conmigo de compras a la tienda de accesorios el día de hoy?”

De camino a casa, de la escuela…

Yunyun afirmo que el regresar juntas estaba limitado a ayer, pero por alguna razón hoy también me está siguiendo.

Pero como en un principio nunca la considere mí rival, esto tampoco me parece mal.

“¿Hay alguna tienda de accesorios en esta villa?”

“El herrero hace accesorios por capricho. Hmm… Megumin…”

“Ir a ver lindos accesorios con alguien más de camino a casa es tu sueño, ¿Verdad? Bien, vayamos entonces.”

Yunyun y yo fuimos a la herrería.

“¡Bienvenidas! Oh, así que esta es la chica rara de la casa del jefe y la hija extraña de Hyoizaburo. ¿Quieren comprar algo? Para niñas como ustedes… ¿Qué les parece esta gran espada? ¡También tenemos hachas y martillos!”

“Rara- chica rara…”

“¿Por qué le quieres dar una delicada chica unas armas tan burdas? De cualquier forma, no hay necesidad que usemos armas.”

Además, es suficientemente extraño que alguien venda armas y armaduras en una villa de magos.

Hacer báculos sería más razonable…

“Porque sería grandioso tener niñas blandiendo armas gigantes. Es contrastantemente lindo.”

“En que mundo habría niñas así… Yunyun, ¿Qué hacemos?”

Para ese momento Yunyun se encontraba echando un vistazo dentro de la tienda.

“Hm. Escuche que este lugar vendía accesorios…”

“Los accesorios están por ahí. Los Miembros del Clan de Magos Carmesí solo buscan armas que parezcan espadas ridículamente largas y de diseños complicados. Así que esas cosas no tienen mucha demanda.”

El herrero señalo con una mueca hacia una esquina.

Hmm… Se puede llamar a esto accesorio o un cuchillo pequeño…

“Normalmente, cuando uno se refiere a accesorios, uno se imagina pequeños objetos usables, no pequeñas armas.”

“Aunque digas eso… ya que no tengo ese tipo de clientes, tendría que cerrar mi tienda si no fuera por esos artículos.”

El tendero debió haberse equivocado al elegir la ubicación de su tienda.

Entonces, ¿Cómo es que esta tienda hace dinero?

“Por sus expresiones ambas deben preguntarse cómo me gano la vida. Después de todo aún soy un archí-mago, por lo que puedo usar la gran cantidad de mana que poseo, para controlar el horno, algo que por medios normales no sería posible. Y de esta forma puedo crear armaduras de la más alta calidad. Entre los amantes de las armaduras, las mías son bastante populares. Y aunque no puedo dar nombres, la hija de cierta familia noble, le encanta usar mis armaduras.”

“¿Por qué la hija una familia noble querría usar armadura?”

“Yunyun, ya casi es hora de…”

Yunyun tomo con cuidado una daga plateada y me volteo a ver.

“… ¿Te gusto eso?”

Yunyun asintió.

De regreso a casa, Komekko tenía su túnica cubierta de lodo como de costumbre.

“¡Bienvenida a casa hermana! ¿Traes algo de comer?”

“No hubo comida hoy. Por cierto ¿A dónde sales siempre? Últimamente, el sello del Dios Malvado de la villa está a punto de romperse, así que sería mejor que no salieras.”

No sé qué tanto de lo que le dije a Komekko realmente escucho. Ella solo parecía observar a Kuro, que estaba en mis brazos.

“… (glup)”

“¡!”

Aterrorizado de ver a Komekko, Kuro trepo hasta mi hombro y trataba de esconderse dentro de mi sombrero.

Y pensar que treparía por el cuerpo de su dueña. Que gata tan audaz.

“Hermana, ¡La cena es carne!”

Tiemblo de ver a mi hermanita, quien trata tanto a mascotas y bichos como comida.

“Komekko, tendrás que esperar un poco más. Este gato es aún muy delgado. No tiene mucha carne para comer. Engordémoslo antes de comerlo.”

“Ya veo, ¡Mi hermana es tan lista!”

Komekko me dirigió una sonrisa de corazón. Aunque aún no sé qué hacia afuera para terminar así de sucia. Limpio su rostro del barro con mi pañuelo, mientras la cuestiono.

“Komekko ¿Has estado jugando afuera de nuevo?”

“Encontré un juguete ¡Así qué esta vez juguemos juntas! ¿Te gustaría jugar hermana?”

¿Jug… juguete?

Por alguna razón ese término no me hizo sentir más tranquila. O más bien hizo que me preocupara aún más.

Sí. Probablemente yo también…

“Hermana, ¡toma un baño! Lava también al gato. Creo que a este paso se le llama ‘Baño astringente’.”

“Komekko, el gato está temblando dentro de mi sombrero, así que no digas más.”

***



Después de tomar un baño con Komekko y Kuro, comimos una cena simple y regrese a mi cuarto.

Había algo de ruido en las escaleras así que probablemente mi madre había regresado.

Mi padre definitivamente se encontraría haciendo herramientas mágicas sin descanso.

Me acuesto sobre la cobija de lana mientras pongo a Kuro sobre mi estómago. Entonces recuerdo.

“Hablando de juguetes. Cuando la conocí, le pedí que me ayudara a recoger mis juguetes.”

Mientras balbuceaba eso en mi cuarto oscuro, levantaba a Kuro frente a mí y lo veía a los ojos.

En esta obscuridad, Kuro parecía entender lo que quería decir, mirándome sin siquiera pestañear.

Con esos grandes, redondos y lindos ojos, sin mostrar nada de temor.

… ¿Por qué?

… Cuando veo a este gato, porque pienso en aquella persona.

Me cubro hasta la cabeza con la cobija. El gato también se cuela bajo la cobija.

“Hey, eres arrogante para ser un gorrón.”

Bajo la cobija, disfruto la sensación de acariciar a Kuro, quien se acurruca sobre mi estómago.

En ese momento, aún pensaba que faltaba mucho tiempo para que dejará la villa –

Pasaba mi vida aprovechando mi tiempo en la escuela y cuidando a mi hermanita, una vida sin cambios.
