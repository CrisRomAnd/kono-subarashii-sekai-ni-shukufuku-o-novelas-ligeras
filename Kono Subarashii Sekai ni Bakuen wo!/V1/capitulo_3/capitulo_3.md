# Capitulo 3: Los Guardianes de la Magia Carmesí

## Parte 1

![](capitulo_3/img/c1.jpg)

“Buenos días, Megumin. ¿Ya desayunaste?”

“Buenos días. Últimamente mi hermanita ha recibido tributos de mucha gente. Las sobras son suficiente como para llenar mi estómago.”

“¿Es que no tienes orgullo como persona?”

Hoy es día festivo para el Clan de Magos Carmesí, así que no hay escuela.

Era un día nublado.

Para ayudar a Buzucoily con su problema, Yunyun y yo nos encontramos temprano por la mañana.

“Fufu, Megumin ¡Mira esto!”

Yunyun saco algo felizmente.

Era algún tipo tablero para jugar.

Aparentemente, es bastante popular en la ciudad imperial.

“¿Cómo es que tienes esto?”

“Es un regalo de mi tío, que fue de viaje a la ciudad imperial, el inclusive dijo algo incompresible como. ‘Esto no se puede jugar solo, así que, con esto, tu definitivamente podrás…’ o algo así, en realidad no comprendí que quería decir…”

… Tal parece que su tío también se preocupa por ella.

“Tal vez lo lleve a la escuela para jugar. Ya que Mr. Buzucoily aún no ha llegado, ¿Qué te parecería un juego primero?”

“… No me importaría. No hay forma de que pierda en un juego basado en el intelecto.”

Jugamos el juego de tablero en el pasto.

“Entonces, ¡yo empiezo primero…!”

-Treinta minutos después.

“¡Ugh! ¡Aquí! Mí ‘Maestro de la Espada’ avanza a este cuadro.”

“‘Archí-Mago’ se tele-trasporta a este cuadro.”

“Megumin, ¡usar teletransportación es muy despreciable!… ¿Uh, podemos bañera al archí-mago?”

“No. Mira, mientras tú te estabas quejando de esto y de aquello, yo ya te tengo en jaque”

“Ah, espera, espera.”

-Una hora más tarde.

“¡Genial! Si esto sigue así, ¡Yo definitivamente ganare…! Megumin, ¡Este es el fin! Solo muevo a mi ‘Cruzado’ a este cuadro…”

“¡Explosión!”

“¡Ah! Megumin eso es muy tramposo. ¡Voltear el tablero es demasiado mezquino!”

“Pero, mira está en el libro de reglas. Ve… en esta parte, ‘Cuando el archí-mago queda solo de tu lado del campo…’”

-Dos horas más tarde.

“¡De nuevo! Megumin ¡Por favor juega conmigo una vez más!”

“Yo ganare sin importar cuantas veces juguemos. Ríndete ya… por cierto, este juego es bastante interesante. Como privilegio del ganador, préstame este juego algunos días.”

“¡Ah!, espera ¡espera!… ¡además las reglas son demasiado extrañas!

¡Qué pasa con la Explosión y la tele-transportación! ¡¿Qué clase de idiota puso este tipo de reglas?!”

Yunyun, que estaba a punto de llorar, le dio un golpecito a la pieza de juego luciendo enojada.

“De cualquier forma, Buzucoily es demasiado lento. ¿Qué está haciendo?”

“… ¿Vamos a llamarlo?”

Tal como Yunyun recomendó, fuimos a la casa de Buzucoily, la cual estaba cerca.

La casa de Buzucoily maneja la mejor zapatería de la villa. Esto era porque… era la única zapatería de la villa, así qué claro que era la mejor.

Tan pronto como entramos vimos al padre de Buzucoily, él es el encargado.

“Sentimos molestar, ¿Esta Buzucoily por aquí?”

“Oh, ¿Pero si no es Megumin? ¡Bienvenida! Mi hijo aún está durmiendo.”

…Hey.

“Lo siento, ¿Podría despertarlo? Él nos dijo, ‘quisiera discutir algo con una linda e ingenua lolita como tú… haha…’.”

“¡Ese bastardo!”

El padre de Buzucoily se lanzó hacia las escaleras de inmediato.

“¡Hey! Aunque es de alguna manera similar a lo que él dijo, hay una pequeña diferencia.”

“Ese NEET hizo una cita con nosotras y el aún se encuentra durmiendo, él debe de ser castigado.”

Escuchamos un rugido y un grito del segundo piso. Un momento más tarde Buzucoily salió corriendo por las escaleras.

“¡Ugh! ¡Megumin! ¡Eso fue demasiado! Mi padre repentinamente me grito, ‘¡Tú bastardo lolicon!’ y me despertó con un golpe.”

“Tú fuiste el que nos citó para discutir algo, y aun así seguías durmiendo después de la hora acordada. Apúrate y vámonos.”

“Ah, ¡espera! ¡déjame cambiarme la ropa primero!”

-Buzucoily se cambió la ropa y fuimos a la única cafetería de la villa con su inusual menú.

“Yunyun, puedes pedir lo que quieras. Buzucoily invita, no es necesario que te contengas. Yo quiero un sundae con la más alta cantidad de calorías.”

“¡Esa debería ser mí línea! De cualquier forma, no tengo mucho dinero…”

“Hm, yo estoy satisfecha, así que con una bebida está bien… Megumin, ¿no dijiste que esta mañana comiste muchísimo?”

Después de ordenar, nos sentamos y escuchamos los problemas de Buzucoily.

“Siento molestarlas el día de hoy. Lo que quiero discutir es… de hecho, yo… tengo a alguien que me gusta.”

“¡¿Eh?!”

“¡¿Pero no eres un NEET?!”

“¡Esto no tiene nada que ver con ser un NEET! Inclusive un NEET tiene que comer y dormir también, ¡así que claro que nos enamoramos también!”

Buzucoily protesto, pero lo ignoramos.

“¡Tema- tema de amor! Megumin, ¡este es un tema de amor!”

“No puedo creer que alguien me consultaría sobre un agridulce tema romántico… como sea, ¿Quién es el objetivo? ¿Es alguien que conocemos? No… no me digas que es una de nosotras…”

“Hey, no digas cosas tan descorteces. Considera tu propia edad. No soy un lolicon… alto, ¡ustedes dos deténganse! Fue mi culpa, por favor no pongan salsa picante en mi café.”

Buzucoily se disculpó rápidamente. Su expresión repentinamente se volvió seria.

“… Bueno, la persona que me gusta es…”

***

-Al poseer una gran cantidad de mana, los miembros del Clan de Magos Carmesí, usualmente tienen un trabajo relacionado con la magia. Por ejemplo, artesano de objetos mágicos, mezclador de pociones y etc.

Aparentemente el interés amoroso de Buzucoily opera una tienda que se dedica a la adivinación. A ella le gusta entrenarse y suele ir a las montañas durante su tiempo libre a practicar sus ‘movimientos finales’. Ella es la típica miembro del clan de magos carmesí que puedes encontrar en cualquier parte.

Dejamos el café y nos dirigimos a su tienda…

“Y pensar que se trataba de Soketto. Tú solo eres un NEET, ¿No es ella demasiado buena para ti?”

“¿A que te refieres con que solo soy un NEET? ¿Es que un NEET no puede tener aspiraciones? Escucha Megumin, los humanos debemos tener muy altas aspiraciones. Es lo mismo con el trabajo. Yo no quiero ser solo un zapatero. ¡Yo quiero tener una carrera más respetable…!”

“Pero si es que quieres salir con una chica, lo mejor es que tengas un trabajo primero…”

Seguimos a Buzucoily mientras él nos explicaba su extraño punto de vista y discutíamos sobre Soketto.

“Mientras que Soketto es la chica más guapa del Clan de Magos Carmesí. Y este otro es solo un NEET sin virtudes y además de flojo hasta para heredar el negocio de su familia.

Esto no tiene futuro…

Buzucoily, hoy jugaremos contigo, así que ¿Porque no te das por vencido con este asunto?”

“¡Por favor no lo analices tan fríamente! ¡Quizás ella sea una rarita que le gustan los tipos inútiles! Lo primero es averiguar cuál es su tipo.”

“Tal parece que entiendes muy bien qué clase de persona eres. Eso mejora la impresión que tengo de ti. Y pues, al fin y al cabo, no tenemos nada mejor que hacer hoy, así que ¡demos lo mejor!”

“Erm. Si ya sabes que eres un inútil, ¿Porque no mejor primero trabajas en ser una persona más responsable? Aunque no es un problema para nosotras preguntarle por cuál es su tipo ideal…”

Buzucoily camino más rápido sin mirar atrás.

Parece que quiere que averigüemos si Soketto tiene novio y que tipo de chicos le gustan.

Esto es lo que Buzucoily quería discutir con nosotras.

“Siento que sería mejor que tú mismo le preguntaras que tipo de chicos le gustan, al menos crearías un tema de conversación.”

“Si tuviera el coraje necesario y la habilidad conversacional, para hacer eso, entonces no sería un NEET… ¡Oh, es aquí adelante!”

Buzucoily con toda confianza dio esos argumentos sin esperanza y se puso a espiar a la tienda de Soketto tras un árbol.

Soketto, la chica más atractiva de todo el clan de Magos Carmesí, se encontraba barriendo la entrada de su negocio de adivinación.

Al tratarse de la hermosa Soketto, la escena de ella haciendo tareas tan normales como esa, era como ver una delicada pintura.

“…Soketto tan hermosa como siempre… desearía ser basura, para poder estar a sus pies y que me barra…”

“¿No, ser un NEET es algo cercano a ser basura?”

“¡Meg- Megumin!”

Mientras platicábamos y observábamos a la distancia. Soketto se estiro y regreso a su negocio.

En ese momento, tuve una epifanía.

“¡Buzucoily! ¡Eso es!”

“¡¿Qué?! ¡¿qué?! ¿Transformarme en basura a sus pies? No importa qué, es mejor dejar ese tipo de ‘juegos’ extraños hasta qué empecemos a salir…”

“¡Pero que idioteces estas diciendo! ¡Eso no! Tuve una buena idea. El trabajo de Soketto es adivinación. Y su adivinación es muy certera, ¡así que haz que ella te diga tu fortuna! ¡Has que ella prediga tu futuro amor Buzucoily!”

“¡Ah! ¡eso suena bien! ¡Y sería aún mejor si ella aparece en su propia predicción! Ahorraría la molestia de la confesión. Si esto funciona, ¡eso sería fantástico! Pero si alguna otra chica aparece, entonces de cualquier forma eso no funcionara sin importar cuanto esfuerzo ponga…”

Comparado con una confesión seguida de un rechazo instantáneo, eso es mucho menos devastador.

Después de que Buzucoily escuchara mi sugerencia…

“No subestimen a un NEET. Si tuviera el dinero, yo iría a su negocio a diario.”

“Parece que ya podemos irnos.”

Buzucoily agacho su cabeza y se disculpó mientras nos preparábamos para regresar a casa.

Pero aún había otro problema.

“Por cierto, La señorita Soketto regreso a su negocio… Hey, si entráramos a su tienda y le preguntáramos sobre su tipo ideal, ¿no sería de alguna manera rudo y repentino…?”

Nosotras somos básicamente extrañas para Soketto. No podríamos hacerle ese tipo de preguntas en nuestro primer encuentro.

Buzucoily abrazo sus brazos con una pinta grave.

“No hay otra forma. ¡Necesito obtener dinero para la adivinación…!”

## Parte 2

Cerca del Hogar del Clan de Magos Carmesí habitan muchos monstruos poderosos. Qué para el aventurero promedio, serían imposibles de derrotar. Inclusive escapar con éxito les resultaría difícil. Dichos monstruos contenían pieles y órganos que podían venderse a altos precios.

Con esos monstruos como objetivo, nosotros entramos al bosque que está aún lado de la villa.

“Megumin… ¿Esto está bien…? Inclusive con la compañía de Buzucoily, si una gran cantidad de monstruos atacan al mismo tiempo…”

“No hay problema. Este NEET casi nunca tiene nada que hacer, así que el viene seguido a este bosque a ganar algo de dinero y puntos de experiencia.”

“Basta ya con lo de NEET esto y NEET aquello, es irritante. Hasta los NEET tienen derechos humanos… Pero hasta el momento no hemos visto ningún monstruo. Es probable que sea porque la gente que tenía tiempo libre en la aldea ya haya acabado con los monstruos poderosos, para que Megumin y el resto pudiera tener su curso de entrenamiento… ¡Oh! ¡encontré uno!”

Buzucoily, que iba a delante, bajo su voz.

Siguiendo su mirada, vimos una criatura negra que cavaba para sacar raíces de árboles.

Este ‘Oso de un golpe’ tiene poderosos brazos que pueden decapitar a un hombre de un solo golpe.

“Su hígado puede ser vendido a un buen precio… Bien.”

Buzucoily inicio el encantamiento, entonces…

“Reflexión de Luz.”

El activo la magia que acababa de terminar de recitar.

Al mismo tiempo Buzucoily desapareció delante nuestro.

El probablemente utilizo algún tipo de hechizo de invisibilidad que utiliza la reflexión de la luz.

A juzgar por las huellas que dejaba en el pasto, su plan era asechar mientras estaba invisible.

En ese momento, el ‘oso de un golpe’ se levantó de improvisto y uso su nariz para olfatear a los alrededores.

Y…

Miro hacia nuestra dirección.

“¡¿Eh?!” x2

Desde nuestro escondite, el oso y nosotras cruzamos miradas. El gruño emocionado por haber descubierto una presa y cargo hacia donde estábamos.

“Yunyun, si no mal recuerdo, tú tienes una daga… ¡Vamos sácala! Y por favor pelea por mí. Mi genial rival Yunyun.”

“Tu usualmente eres bastante apática respecto a eso. Así que no esperes a estos momentos para tratarme como tú rival. ¡¿Entendido?! Además, una daga como esta no funcionaría contra este tipo de monstruos de cualquier forma.”

Ni siquiera podemos escapar debido a que el ‘oso de un golpe’ también es muy rápido.

Por cierto, ¡Buzucoily!

¿A dónde fue Buzucoily?

“¿A dónde demonios fue ese NEET? ¡Rápido derrótalo!”

“Ahhhh, ¡no te acerques!”

Justo cuando el ‘oso de un golpe’ estaba a punto de toparnos.

“¡Sable de luz!”

Repentinamente Buzucoily salió de la nada y corto al monstruo con su brazo mientras gritaba su hechizo.

Una luz resplandeció de su mano. Esa luz atravesó al ‘oso de un golpe’ desde atrás. Desde el hombro hasta el estómago, la luz resplandeció el ‘oso de un golpe’ dio unos cuantos pasos más y callo en dos pedazos frente a nosotras.

“Phew… normalmente, inclusive si me olfateaba, solo volteaba a los alrededores y volvía a relajarse poco después… ¿Están ustedes dos bien? ¿Mi tiempo fue perfecto verdad? Creen que si Soketto estuviera en peligro y yo apareciera así… ¡Ouch! Hey, ¡Esperen! Lo siento, no… ¡Esperar al momento perfecto para aparecer, es el procedimiento estándar para el Clan de Magos Carmesí!”

Yunyun y Yo continuamos golpeando los hombros de Buzucoily sin decir una palabra.

“-En verdad, No bromees en un sitio tan peligroso como este. Si no limpiamos el cuerpo de inmediato, otros monstruos vendrán atraídos por el olor a sangre.”

Tras separarse de nosotras Buzucoily sacudió el polvo de su ropa.

“Tras hacernos enojar, ¿aún tienes el descaro de decir eso? Rápido, saca le el hígado al oso y vámonos de aquí.”

Generalmente cuando un grupo de miembros del Clan de Magos Carmesí sale de cacería, suelen dejar el cuerpo intencionalmente para atraer a otros monstruos. Pero debido a que nosotras solo tenemos a este NEET como fuerza de ataque, lo mejor es no dejar nada.

“… Te dije que.”

En ese momento Yunyun me jalo de mi ropa.

Mire en su dirección. La cara de Yunyun estaba pálida y miraba atrás de nosotros.

Por consiguiente…

“Justo ahí…”

Ella tembló ligeramente y apunto hacía esa dirección.

Tengo un muy mal presentimiento acerca de eso. ¡Volite a ver…!

“¡Corran! Buzucoily, ¡olvida el hígado! ¡La batalla está perdida!”

“¡Waaaaahhhh! ¡espérenme! ¡no se vayan sin mí!”

“¡Grrrrahhhh!” x4

Era un grupo de osos de un solo golpe, los cuales estaban furiosos por la muerte de su compañero.

***

“-Yunyun, ya es tarde. Deberíamos regresar.”

“Si, tienes razón, entonces nos vemos mañana en la escuela.”

“¡Esperen! No me abandonen, ¡ustedes dos! ¡Por favor…!”

Después de escapar de regreso a la villa, intentamos regresar a casa, pero Buzucoily sin ningún tipo de vergüenza nos suplicó que lo acompañáramos.

Debido a que el solo alejo al grupo de ‘osos de un solo golpe’, él ahora estaba cubierto de polvo y lodo.

El realmente se veía sucio y para empeorarlo también estaba llorando patéticamente frente a nosotras.

Tras ver a un NEET mayor que nosotras, actuando así, no podía si no que sentir lastima por el…

“…(Suspire). Muy bien entiendo. Solo que, como adulto, por favor no te arrodilles ante un estudiante. Te aremos compañía un poco más… Pero ¿qué debemos hacer? El plan para que prediga tu futuro amor ya es…”

“Por cierto, ¿Porque razón el oso de un solo golpe formaría grupos? El oso de un solo golpe no es social por naturaleza…”

Buzucoily se quejaba de desesperación.

“Mi padre también menciono que la situación en el bosque ha sido bastante extraña últimamente. ¿Quizás sea debido a la aparición de esos extraños monstruos?”

Yunyun pregunto, mientras que pensaba en lo que sucedió. Pero no había forma de que alguno de nosotros supiera.

“De cualquier forma, es inútil que nos quedemos aquí. Regresemos al negocio de Soketto.”

-Caminamos en dirección de su negocio como sugerí…

“…Hay un letrero que dice que el negocio está cerrado. ¿Habrá salido la señorita Soketto a algún lado?”

Dijo Yunyun.

Buzucoily puso sus manos sobre nuestros hombros y dijo…

![](capitulo_3/img/c2.jpg)

“Yo lo sé todo sobre Soketto, somos bastante cercanos después de todo. Primeramente, Soketto se levanta a las 7am, ¡que saludable estilo de vida! Entonces, ella pone las sabanas en la cesta de ropa sucia y prepara el desayuno. Pero Soketto come fideos de udon todos los días. ¿Qué tan profundo es su amor por el udon? Ella hierve el agua en la olla mientras se cepilla los dientes, que eficiente. Su rostro es hermoso y ella es inteligente también. Soketto es tan astuta. Después de comer los fideos, ella lava los trastes del desayuno y los de la merienda del día anterior. Ella deja remojando en agua los cubiertos del día anterior, es tan brillante. Ella será una buena esposa. Soketto, entonces toma un baño. Para bañarse en la mañana, ella es tan higiénica. Ella se baña en la mañana y en la noche, es por eso que su piel se ve tan suave y encantadora. Después de bañarse ella lava su ropa. Esto es… es un serio problema. Debido a que ella lava toda su ropa inmediatamente, es problemático para mí. No nada problemático. Sí, claro, es verdad, eso no me molesta, de hecho… en verdad no me molesta. Después de todo, no es que yo vaya a hacer algo obsceno. Después de lavar, ella sale a caminar. Su vida es tan saludable. Después de una corta caminata, ella se dirige a su negocio. Tal como ustedes vieron, ella limpia la entrada. Ella en verdad ama la limpieza y es tan buena en las labores del hogar. Después de limpiar, ella entra y permanece en su negocio. Debe ser aburrido estar dentro de la tienda. Si tuviera el dinero, la visitaría todos los días. Si no tienen clientes Soketto se aburre y sale a hacer ejercicios de estiramiento o se asoma a ver si alguien viene, se ve tan linda cuando lo hace. Ella no solo es atractiva, sino que también es linda. Eso es definitivamente trampa. Soketto es tan linda. Ella entonces cierra su negocio sale corriendo hacia algún lugar. Ella es tan libre de espíritu, o debería decir, ella es la libertad personificada. Miren, yo soy un NEET devoto a la libertad. En ese aspecto, somos compatibles. Bien dejemos lo aquí por ahora. Deben faltar aún dos horas antes de que Soketto regrese. Aunque esperarla aquí está bien también… ¿Qué debemos hacer?”

Con los ojos brillantes Buzucoily no dio información intensamente detallada.

“… tal pareciera que siempre la estas observando. Que miedo…

¿Cómo es que sabes tantos detalles?”

“Básicamente, cuando estoy libre, yo vengo a aquí a investigar. Y no lo digo por presumir, pero, soy la persona más libre de la villa.”

Realmente eso no es presumir.

Como sea.

“Eso es lo que se conoce como acosador.”

“Hey Yunyun, si vuelves a decir eso, no te perdonare, aunque seas la hija del jefe de la villa.”

Este tipo… quizás sea más beneficioso para la sociedad si lo noqueamos aquí y lo enterramos vivo.

“En cualquier caso, no hay nada que podamos hacer si ella no está aquí. Así que dejemos lo por el día de hoy…”

Yunyun estuvo de acuerdo con mi sugerencia.

“No importa, se a donde a ido.”

Buzucoily dijo confiado.

….

“Ella en verdad está aquí.”

“Si…”

No se si debería estar feliz por encontrarla o si deberíamos entregar a Buzucoily a la policía por conocer tantos detalles sobre ella.

Guiados por Buzucoily, albergábamos sentimientos encontrados mientras observamos a Soketto desde la distancia. Ella se encontraba tomando algunas cosas en la tienda.

“¿Qué tal eso?… Yo soy alguien que puede lograr grandes cosas si se lo propone. Este nivel de investigación es suficiente.”

Como ya lo había dicho eso no es investigación, es simplemente acoso…

“…como sea, ella no está en su tienda ahora así que… inclusive si nos acercamos y le hablamos, no parecerá poco natural. Yunyun y yo le preguntaremos tranquilamente el tipo de chicos que le gustan. Vamos Yunyun, coordínate conmigo.”

“Entendido. Solo preguntemos le y acabemos con esto de una buena vez.”

Entre junto a una cansada Yunyun a la tienda donde se encontraba Soketto.

“Oye, Yunyun, mira. ¿No te parece lindo?”

“Si- ¡realmente lindo! Si se lo dieras a tu amado, seguramente… ¿Eh? ¿Te refieres a eso? ¡¿La espada de madera con grabados de dragón?!”

Haaaa- hago una perfecta entrada y aun así Yunyun arruina el efecto, con sus innecesarios comentarios.

“Ponte seria Yunyun, ¡coordínate con mi tema!”

“Pe-pero, los gustos de Megumin, ¡Son demasiado raros! No importa como lo vea, esa cosa es demasiado.”

Mientras cuchichiábamos entre nosotras, Soketto se puso a mi lado.

“Ah, es tan lindo. El grabado del dragón es tan hermoso. Te quedaría muy bien si tú lo llevaras en la cintura mientras sales a correr.”

![](capitulo_3/img/c3.jpg)

“¡¿Eh?!”

Tras escuchar lo que dijo Soketto, Yunyun exclamo fuertemente.

“En verdad, es un objeto de gran calidad, que además de practico también es lindo… por cierto, me gustaría preguntarte, aprovechando, cuál es tu tipo de…

¿Pero qué estás haciendo Yunyun?”

Justo cuando estaba a punto de preguntarle de forma natural a Soketto sobre su tipo, Yunyun me interrumpió bruscamente, jalándome del brazo.

“¡Tu no estabas siendo para nada natural! Además, ¿en verdad mis gustos son raros?… Como esperaba, ¿la extraña soy yo?… ¡Por que no puedo ver que tiene de lindo esa espada de madera!”

“Yunyun tú siempre has sido extraña. Tu inclusive le pusiste un nombre raro a mi gato como ‘Kuro’ … ¡Ahaha!”

Mientras que nosotras seguíamos discutiendo en voz baja, Soketto pago por lo que llevaba y salió de la tienda.

“Pero ¿qué es lo que estás haciendo? El desarrollo inicial de hace poco no estuvo mal.”

“¡Pero! ¡Pero!”

Buzucoily se nos acercó mientras seguíamos discutiendo.

“Oigan, ustedes dos, ¡que están haciendo! Soketto ya se marchó”

“Haaa (suspiro). Y pensar que estuvo tan cerca, de no haber sido por la inesperada interferencia… y por qué a una persona peligrosa que gusta de cargar una daga en su cintura todo el día, no le gustaría una espada de madera. Suficiente, ¿Cuánto tiempo más vas a estar enojada? ¡Debemos irnos!”

“¡No compares mi hermosa daga con esa espada de madera!”

“Ustedes dos, si quieren discutir, ¡hagan lo más tarde!”

-Soketto sostenía alegremente la espada de madera, anteriormente bajo duda de ‘muy linda’ en sus manos, mientras caminaba adelante de nosotros.

“… para que Soketto, camine mientras que mueve la espada de madera al mismo tiempo… este comportamiento también es muy lindo…”

“Bajo la mirada de cualquier otro, ella probablemente solo luzca peligrosa.”

Escuche los débiles murmullos de Yunyun, mientras que veía a Soketto, que iba ahitando su espada de madera hacia atrás y adelante con una mano.

Estábamos usando la magia de invisibilidad de Buzucoily para seguirla desde atrás.

“Tal parece que ella en verdad le gusto esa espada de madera. ¡Ah! Miren ella está usando la espada para cortar las hojas que caen del árbol. ¿Es que piensa entrenar?”

Sin saber que la estábamos siguiendo, Soketto usaba la espada de madera para golpear las ramas de los árboles e inclusive los pateaba para que las hojas cayeran.

“Te lo preguntare, ¿Qué es lo que te gusta de la señorita Soketto? Para Sr. Buzucoily ¿ella aún está en el rango de lo que te gusta, pese a que ahora este pateando los árboles?”

“Es su imagen, me gusta la cara y la figura de su cuerpo. Si se trata de una belleza, hasta esas acciones pueden considerarse como lindas.”

Después de escuchar a Buzucoily hablar sin duda- o más bien sin desilusionarse, Yunyun no tenía nada más que decir. En ese momento yo idee un plan.

“Buzucoily. Por qué no pretendes caminar casualmente y la ayudas calmadamente, ¿Qué te parece eso? Tú puedes usar algo de magia de viento para tirar las hojas de los árboles y ayudarla a entrenar.”

“¡Eso es! En verdad eres la mejor genio del Clan de Magos Carmesí.

¡La astucia de Megumin es tan filosa!”

“¡Ah! Yo, ¡yo también pensé en una manera para que Sr. Buzucoily sea popular con las chicas! Por ejemplo, arreglar su desordenado cabello después de levantarse, encontrar trabajo o algo así…”

El ni siquiera escucho a Yunyun que trataba de competir contra mí de nuevo. Y en lugar de eso mantuvo su invisibilidad y se acercó a Soketto.

¡Entonces…!

“¡Tornado!”

Soketto salió volando por los cielos debido al tornado de Buzucoily-

“Es que eres idiota ¡¿es que eres un idiota?!”

“Vamos a enterrarlo. Enterremos a este NEET muerto y ¡olvidémonos del!”

Tras asegurarnos que Soketto estaba bien, nos apresuramos a dejar la escena y estrangule a Buzucoily por el cuello.

“¡Esperen! Ambas ¡Cálmense! También, ¡cállense! O ¡nos descubrirán!”

Después de salir volando por los cielos, Soketto uso su propia magia de viento para mantener su balance y regresar al suelo con algunas dificultades. La palidez de su rostro se podía ver fácilmente inclusive a la distancia.

Ella volteo y miro a los alrededores con los ojos rojos de ira, Tal vez en busca del criminal que uso la magia de viento.

La magia de Buzucoily impone camuflaje óptico en nuestro entorno. Mientras que nuestras acciones no sean demasiado extremas, ella no sería capaz de encontrarnos.

En ese momento, Yunyun tomo fuertemente a Buzucoily del pecho y lo interrogo lentamente.

“¡¿No decías que te gustaba la señorita Soketto?! ¡¿Entonces porque usaste una magia tan letal?!”

“¡No, no! Debido a que yo solo se usar magia avanzada, ¡yo no sé cómo debilitar su efecto…!

De hecho, ¡lo único que quería era hacer que cayeran las hojas! Pero entonces pensé que, si la magia de viento se activara cerca de ella, entonces su falda…”

“Vamos a enterrarlo.”

“Si, enterrémoslo.”

“Esperen, ¡esperen! Por favor, ¡escúchenme!”

Mientras seguíamos discutiendo, Soketto ya se había ido caminando, con una expresión de enojo.

Tal parece que se rindió sobre la búsqueda del criminal.

“Si… lo más importante es que ella no está herida. Y… A hora también sabemos que color le gusta ponerse. Eso es grandioso.”

Yunyun y yo gritamos al mismo tiempo:

“¡Señorita Soketto!”

“¡Señorita Soketto!”

“Hey, oigan, esperen- ¡…!”

## Parte 3

-Ya era casi hora del almuerzo.

Yo tengo que regresar temprano para almorzar con Komekko…

“¡Esperen! Ambas, ¡no me abandonen simplemente así!”

Primero lavare la ropa sucia que sea estado apilando, después creo que jugare con ella por un rato…

“Se los suplico, ¡no me ignoren! ¡eso es demasiado! ¡me esforcé mucho para evitar que ella me descubriera! ¡ella casi ve mi rostro!”

Entonces, me bañare con ella y aprovechare para lavar a la bola de pelo también…

“¡Se los suplico! ¡Por favoooooooor!”

“¡Eres muy ruidoso! ¿Puedes dejar de seguirnos? ¡¿O es que decidiste incluirnos en tu lista de acoso?! ¡Deberías rendirte ahora que aún estas a tiempo y encontrar algún otro objetivo más fácil!”

“Tal y como dijo Megumin, es mejor que te rindas ahora… por cierto, hay un tipo muy lindo de monstruo llamado ‘la chica de la tranquilidad’ cerca del hogar del Clan de Magos Carmesí…”

“¿Por qué me das ese tipo de información? ¡¿Acaso me estás diciendo que me conforme con algún tipo de monstruo?! Y para colmo luces tan honesta cuando lo dices, ¡no puedo creer que seas tan abusiva al hablar!”

El acosador estaba lloriqueando mientras decidíamos ir a casa una vez más.

-Después de dejar de perseguir a Soketto, con algo de dificultad, Buzucoily ha estado molestándonos desde entonces.

“En verdad, es inútil, no importa cuánto supliques. No estamos tan desocupadas como tú. Las estudiantes son diferentes de los NEET, nuestro tiempo de descanso es muy importante.”

“¡Por favor denme un poco más de tiempo! ¡Las invitare a las dos a almorzar!”

Buzucoily agacho su cabeza y junto sus manos como si estuviera orando.

“No somos niñas. No caeremos por ese tipo de trucos, verdad, Megumin… ¿Eh?”

“Llenemos nuestros estómagos mientras que planeamos nuestro siguiente movimiento.”

Yunyun volteo a ver cómo me iba gustosamente con Buzucoily y se quejó.

“Oye, Megumin, ¡¿está bien esto?!… yo, yo también les ayudare, ¡no me dejen atrás!”

-En el único café de la villa.

“En cualquier caso, el asunto principal es crear una oportunidad.”

Levante mi dedo índice, mientras comía mi emparedado de cordero, con grandes mordidas.

Buzucoily nos veía hambriento, mientras que nosotras comíamos nuestro almuerzo.

Este NEET gasto casi todos sus ahorros en invitarnos esta comida.

“Oportunidad.” x2

“Exactamente, actualmente, tú no tienes ningún tipo de relación con Soketto, ¿verdad? Por tanto, Yunyun y yo seremos las responsables de presentarlos. En realidad, la mejor forma seria que fueras un cliente frecuente de su negocio. Pero eso imposible para alguien sin ingresos. Así que tenemos que crear una oportunidad para que se conozcan.”

“¡Ya entiendo!”

“Oye ¿Cómo vas a crear esa oportunidad? ¿Ya tienes algún plan?”

Mientras escuchaba a Yunyun, tomaba mi jugo de frutas para después de comer.

“Muy simple. Primero, Yunyun atacara a Soketto con su daga. Entonces Buzucoily que coincidente mente caminaba por ahí…”

“¡Esta decidido entonces!”

“¡No! ¡¿Es que tienen un hoyo en la cabeza?! ¡¿Es que lo tienen?!”

En ese momento, el encargado de la cafetería se nos acercó, mientras que nosotros seguíamos discutiendo.

“Desde un principio he escuchado el nombre de Soketto. ¿Qué es lo que pasa con ella? ¿Tiene que ver con que saliera al bosque con su espada de madera?”

“No, no es nada… ¿Al bosque?”

Tras escuchar lo que dijo el encargado, Yunyun y yo, nos volteamos a ver.

Quizás el, dándose cuenta de nuestras intenciones. Buzucoily dijo con auto-satisfacción.

![](capitulo_4/img/c4.jpg)

“El bosque he. Soketto va a ese bosque todos los días. Forma parte de su agenda diaria. Ya que a ella le gusta entrenar, ella visita el bosque para cazar monstruos sola. A ella le gustan los ‘Dracos de fuego’, particularmente disfruta congelarlos y reírse de ellos mientras los observa. Hablando de eso, nuestros pasatiempos son bastante similares. Como sabrán yo soy un NEET y cuento con mucho tiempo libre y es ahí donde entra el hielo. Yo suelo congelar el agua y convertirla en hielo, para pasar el día viendo cómo se derrite. Creo que es bastante parecido al pasatiempo de Soketto, ¿verdad? Hmm ¿Tal vez no? Ahh, no importa, lo que sea, de cualquier forma, Soketto, entra al bosque todos los días para entrenar. Su estilo de combate es bastante elegante. A ella le gusta la magia de relámpago. El efecto del relámpago es hermoso después de todo. Yo creo que le va bien a la hermosa Soketto. Lo siento, me desvié del tema de nuevo. Y entonces, si ella entra al bosque a esta hora es definitivamente para entrenar. Ella debería de alcanzar el Nivel 50 pronto. Con eso podría ser considerada una aventurera de primera. Hermosa, linda y poderosa, eso es trampa, Soketto es tan sorprendente. Soketto se ve tan bonita cuando pelea. La Soketto que acaba de terminar de pelear hace que mi corazón se acelere. El sudor que pasa por su hermoso cabello negro a sus delicadas mejillas y de su barbilla a su cuello. Eso es definitivamente trampa, Soketto es tan tramposa, ¿Cómo quieren que no arda en deseo? Desearía que ella tomara responsabilidad por lo que hace, en verdad. Pero olvídenlo, no es que importe. No de hecho, si importa, pero ya está bien. En cualquier caso, entrar al bosque en estos momentos quiere decir que está entrenando. No hay error, ella ahora debería estar riendo y divirtiéndose justo ahora mientras que persigue a un ‘oso de un golpe’ o congelando los pies de un ‘Draco de fuego’. Quisiera poder verla, en verdad quiero verla, ¿Por qué no vamos y echamos un vistazo? Si, está decidido, entonces vamos. Y disfrutemos la sonrisa de Soketto juntos, esta echo entonces. ¡Vamos hagámoslo!”

“En verdad das miedo. Y en lugar de toda esa peligrosa cantidad de información personal, lo importante ahora es … Hace rato cuando entramos al bosque, fuimos atacados por un grupo de ‘osos de un solo golpe’ ¿verdad? He escuchado que el comportamiento de los monstruos ha sido bastante extraño últimamente…”

“Oigan, oigan, esperen. ¡¿en verdad está bien?! Ya que ha habido reportes de ataques de monstruos extraños, porque no vamos y le preguntamos a los aldeanos…”

Mientras que Yunyun y yo conversábamos. Buzucoily tocio suavemente y se levantó.

“Debemos ir…”

Hablando como el protagonista masculino que sabe que la heroína está en peligro.

“¿Sr. Buzucoily…?”

Yunyun se sorprendió de lo serio que se veía Buzucoily mientras que se levantaba.

“¡Entrare al bosque inmediatamente! ¡Y la encontrare lo más pronto posible…!”

Tras escuchar eso, parece que la cara de Yunyun se ruborizo.

“Eso, ¡eso es! ¡eso es! El Sr. Buzucoily de hace un momento… ¡parecía tan…!”

Yunyun cerró su puño y suspiro sentimentalmente.

“Puede que Soketto ¡sea atacada por el grupo de ‘osos de un solo golpe’…! Si hago una entrada dramática de nuevo, ¿Que pasara? Y si la salvo del peligro, ¡¿Llegare a mi meta en un solo movimiento?! ¿…? ¿Que estabas diciendo Yunyun?”

“Nada, solo pensaba que tú mereces volverte la comida de un ‘oso de un solo golpe’.”

***

Entramos en el bosque de nuevo, solo para encontrar que había cambiado completamente.

“… ¿Que paso aquí? Es como si hubiera habido una muy dura e intensa pelea.”

Había signos de un combate a la entrada del bosque.

Los árboles cercanos se encontraban todos quemados, probablemente por el uso de magia de fuego o relámpago.

Y, entre los arboles del fondo, se podía ver salir humo del cuerpo de un ‘oso de un solo golpe’.

“El olor a quemado de los árboles aún no se había disipado del todo. Parece ser que el mago responsable aún se encuentra cerca.”

Dijo Buzucoily después de revisar la escena de la batalla. Elevo su alerta y continuo hacia adelante.

“Por cierto, nosotras aún no podemos usar magia aún, así que aún no somos demasiado confiables. Entonces ¿Por qué debemos seguirlo? Si un ‘oso de un solo golpe’ aparece, lo único que podemos hacer es gritar y correr.”

“Si, para ser honesta, yo en verdad, lo único que quiero es regresar a casa…”

“¿Pero que están diciendo? Si solo estoy yo, no se podría establecer comunicación incluso si me encuentro con Soketto. Para serles honesto, cuando me encuentro con una mujer de mi edad en la villa, no tengo la confianza de poder iniciar una conversación,”

“¿Qué sentido tiene decir eso de una manera tan orgullosa? Yunyun, tú también dile algo… ¿Yunyun?”

“¡¿?! ¡¿Qu-qué?!”

Yunyun estaba impactada después de escuchar que saliera el tema de la barrera de comunicación. Ella sospechosamente miro a otro lado.

… Así que hay otra persona aquí con problemas de comunicación.

-En ese instante.

“… ¿Magia de relámpago?”

Un destello de luz descendió del cielo no muy lejos de aquí.

“Golpe de relámpago”

Después del grito de alguien, un destello de luz desde el cielo ilumino los árboles.

El relámpago descendió con estruendoso sonido.

El ‘oso de un solo golpe’ que fue impactado por este relámpago, cayo con humo saliendo de su cabeza.

Quizás fuera por su instinto, pero el resto de los ‘osos de un solo golpe’ mantuvieron su distancia y temblaron de miedo ante el estruendoso rugido.

Una mujer con flamantes ojos carmesí permanecía en el medio de ellos.

Sosteniendo su espada de madera, la feliz Soketto de ojos rojos, seguía lanzando sus hechizos.

“Golpe de relámpago”

Esta debe ser la magia que incinero los árboles de antes.

Tras dejar de escuchar la voz de Soketto una luz destello desde el cielo, penetrando la cabeza de un ‘oso de un solo golpe’ que se encontraba paralizado por el miedo.

En el momento en el que el segundo oso fue derrotado. Buzucoily salió hacia adelante.

Su expresión era diferente a la del usual NEET.

Él tenía la mirada de un miembro del Clan de Magos Carmesí, determinado a proteger a su amada.

Los ojos de Buzucoily brillaron de un carmesí intenso mientras que liberaba una gran cantidad de mana para lanzar su hechizo.

En eso, los ‘osos de un solo golpe’ notaron la presencia de nuevos oponentes y empezaron a tomar acción.

En ese momento, la magia golpeo con precisión.

“Fuego del Infierno, ¡quema lo todo hasta las cenizas! ¡Inferno!”

El más alto nivel de magia de fuego fue liberado con todo su poder-¡!

…incluso envolviendo a Soketto en su área de efecto.

“Se-Señorita- ¡Señorita Soketto!”

“¡NEET! ¡¿qué es lo que haces?! ¡Rápido! ¡Rápido, apresúrate a rescatarla…!”

Olvídate de los ‘osos de un solo golpe’, incluso los árboles que nos rodeaban estaban engullidos en flamas. Y Soketto, cubierta por completo en una delgada capa de agua, camino despacio hacia nosotros, saliendo del fuego.

Tal parece que ella alcanzo a reaccionar en el último momento y uso un hechizo defensivo de agua en ella misma.

“Que- que bueno que está bien…”

“¡Sí! ¡El corazón casi se me sale…! Buzucoily, rápido despeja el fuego ¡rápido! O de lo contrario todo el bosque se incendiará.”

Después de escucharme, Buzucoily se apresuró a usar magia de agua para extinguir las llamas.

Pese a que es un NEET indecente, el aún es parte del Clan de Magos Carmesí que pueden usar magia avanzada.

El ataque con todo el poder de Buzucoily aniquilo a todo el grupo de ‘osos de un solo golpe’.

Ahora con cenizas en todas partes. Una vez removida su defensa de agua, Soketto miro a Buzucoily con los ojos humedecidos.

¿Fue debido a la batalla? ¿O habrá alguna otra razón?

Las mejillas de Soketto se ruborizaron. Tal parece que tiene problemas en decidir lo que va a decir.

Buzucoily se mostraba nervioso y sonrojado ante la mirada directa de Soketto.

Pero, este NEET que se mostraba confiado y seguro hasta hace un momento fallo en el momento crucial.

Estaba demasiado nervioso, e incapaz de decir nada.

“… Hay algo que Buzucoily quería decirte a Soketto.”

“¡¿?!” x3

Mis palabras hicieron que los otros tres aguantaran su respiración.

Las mejillas de Yunyun se sonrojaron mientras veía este desarrollo.

Buzucoily lucia muy nervioso, como si me acusara por decir cosas innecesarias.

Después de eso-

“Coincidente mente, Buzucoily. Yo también tengo algo que decirte a ti.”

Soketto dijo algo inesperado, sorprendiéndome, a Yunyun e inclusive a Buzucoily.

“Eso, ¡eso es…! No me digas, ¡eso es…!”

“Correcto. Mis sentimientos deben de ser los mismos que los tuyos ahora.”

Soketto sonrío gentilmente.

Tras ver la sonrisa de esta belleza, el rostro de Buzucoily se puso todo rojo como tomate.

¿Habrá sido por haber salvado su vida cuando ella estaba en peligro?

¿Es acaso este el efecto del ‘puente en suspensión’ que hace que la otra parte se enamore con cualquiera que esté presente en el momento de peligro?

Yo sentí que no era justo que este NEET consiguiera el corazón de la chica más hermosa de la villa. Pero tras ver el rostro rojo de Yunyun y sus ojos resplandecientes, pensé que no estaba tan mal.

Buzucoily tomo todo su coraje, cerro su puño y le dijo a Soketto-

“Desde el principio, yo, ¡yo…!”

“Si tu realmente me odias tanto, solo dilo así. En estos momentos, ambos estamos en el bosque ahora. He escuchado que tal como yo, tú también vienes seguido al bosque a entrenar. Por lo que debes de estar calificado para ser mi oponente… Vamos, ¡Terminemos con esto!”

…..…….

“¿Ah?” x3

A excepción de Soketto, el resto de nosotros hicimos un ruido de sorpresa.

“¡No sé por qué me odias tanto! Sé que desde hace tiempo me seguías, ¡Pero lo de hoy fue realmente especial! Olvídate del tornado que casi me mata en la villa. Esta vez, Tu inclusive me hiciste un ataque sorpresa con ‘Inferno’, ¡mientras me estaba preparando para matar esos monstruos! Me impresionaste, tú eres realmente competente. ¡Yo he peleado antes con muchos monstruos, aun así, esta es la primera vez que he acabado en una situación tan difícil!”

Soketto sostuvo su espada de madera e hizo un sonido contundente, “¡Kacha!”

“No, no, no, ¡no! ¡No es así! ¡Estas equivocada! ¡Se supone que esa magia de antes era para rescatarte de ser rodeada por esos monstruos…! Siento mucho que hayas sido envuelta en ella. ¡Simplemente no pude controlarla bien!”

El rostro de Buzucoily se puso blanco y repetidamente juntaba sus manos para explicar. Soketto relajo un poco el mango de la espada de madera y pregunto.

“… Entonces ¿Qué hay del tornado de antes? En ese momento eras invisible ¿verdad? Tu identidad quedo expuesta hace mucho. Después de todo, tu eres la única persona que me acosa todos los días.”

“Sobre eso…”

Buzucoily volteo a vernos. Sus ojos imploraban nuestra ayuda.

Nosotras apuntamos a Buzucoily juntas y le dijimos.

“Él dijo que quería subirte la falda” x2

Soketto elevo su espada de madera y golpeo a Buzucoily.

## Parte 4

Dentro de la tienda, colgaban unas de cortinas violetas pálidas cubriendo todos los lados.

Soketto hablo con resignación.

“Ahh. Parece que ambas la pasaron mal. Debió haber sido difícil haber sido molestadas por este pervertido y ser forzadas a ayudarlo, ¿Verdad?”

Tras regresar del bosque, fuimos a la tienda de Soketto, donde Buzucoily estaba siendo vendado.

“Oigan, no me llamen pervertido. Mis motivos eran puros. Esto no era más que el deseo por conocimiento que caracteriza a los magos, esperando encontrar que color es el que las mujeres del Clan de Magos Carmesí prefieren… lo siento mucho. No fue mi intención. Por favor aleja esa espada de madera.”

Mientras que Buzucoily todavía estaba siendo vendado, este se apresuró a explicarle a Soketto, cuya mano se dirigía a la espada de madera que estaba colocada en la pared.

Buzucoily se encontraba en un terrible estado tras ser sujeto a los ataques de la espada de madera. Quien lo vendaba era Yunyun.

Soketto vio el estado que estaba y suspiro profundamente.

“… En verdad, si lo que querías con tantas fuerzas era una adivinación, tanto que irías a cazar monstruos al bosque para reunir fondos, tu mejor hubieras venido directamente conmigo a preguntarme. Al menos puedo darte un precio especial por ser la primera vez.”

“¡¿En verdad está bien?!”

-Al final este cobarde NEET ni siquiera pudo decir su verdadera razón para entrar al bosque. Solo pudo salir con alguna mala escusa.

“Como quería escuchar mi fortuna, fui al bosque para hacer algo de dinero.”

E inclusive “coincidentemente” encontró a Soketto siendo rodeada por monstruos, entonces “convenientemente” uso magia sobre ella.

“Ignorare la magia de ‘tornado’ y ser casi quemada viva en el bosque por ahora- Pero solo porque querías ayudarme en el bosque a escapar de los monstruos. Bien… ¿Qué clase de adivinación es la que quieres? Solo lo haré una vez.”

Soketto saco una bola de cristal de su cuarto y la puso frente a Buzucoily.

“Lo que quiero saber es, uh… mí futura novia… no, esposa… no, no, ¿la persona que me amará? …Ah ¿Quién podrá ser?”

Buzucoily perdió de vista su objetivo inicial en un momento.

“Básicamente, es sobre tu futuro amor. La bola de cristal nos mostrará a la mujer que se convertirá en tu cónyuge. Este futuro puede cambiar, así que no puedo garantizar que en definitiva sea esa persona… Oh, ¡ahora la veremos pronto…!”

La bola de cristal de Soketto empezó a brillar suavemente.

Hasta que finalmente, ¡el brillo se desvaneció…!

“… no pude ver nada.”

“Eh, ¡¿Ah?!”

Soketto lucia aún más sorprendida que cuando salió volando por los aires por el ‘tornado’. Tanto que ella agito vigorosamente su bola de cristal.

“Espera, espera un momento. No sé que es lo que pasa. Esto no debería ser posible… quien sea, ¡él al menos podría tener un amor potencial…!”

“Tu probablemente deberías de mantener esa dolorosa revelación para ti misma.”

Ya que la bola de cristal no mostró a nadie, eso quería decir que no era posible que Buzucoily y Soketto terminaran juntos.

Soketto volteo a ver a Buzucoily que parecía estar a punto de llorar, con lastima.

“… Uh, no hay problema. Mí adivinación no es 100% segura… una vez cuando era más joven predije el clima y salió que al día siguiente estaría nublado, pero en lugar de eso llovió… por cinco minutos…”

“¡Por favor detente! ¡Ni siquiera puedo saber si tratas de confortarme o simplemente me presumes sobre la exactitud de tu adivinación! ¡que es esto! ¡esto es peor que ser rechazado directamente!”

Yunyun y yo nos mantuvimos alejadas de ellos mientras discutíamos en voz baja.

“Eso fue demasiado lamentable inclusive para un NEET. Si la bola de cristal no mostró nada, eso significa que ni siquiera el monstruo con forma humana de la que Yunyun bromeo antes, la ‘chica de la tranquilidad’ no lo amara…”

“¿Qué debo hacer? Yo, yo no creí que mi broma llegara a ser tan excesiva…”

“¡Ustedes dos! ¡escuche eso! Si en verdad tienen que decirlo, ¡por favor háganlo en silencio!”

-Después de eso Buzucoily salió de la tienda. Parecía estar a punto de llorar.

“Por cierto, ya que estaba ocupada con la adivinación antes, olvide preguntarle esto- Dejando de lado las dos ocasiones del día de hoy, ¿Por qué me ha seguido todos los días?”

“Sobre eso… por favor no preguntes más. Esto es por el bien de Buzucoily.”

Tras escuchar mi respuesta, Soketto inclino su cabeza confundida.

Ella vio cómo se alejaba figura de Buzucoily mientras el arrastraba sus pies para llegar a casa con desesperación.

“Pesé a ser un chico inútil, parece algo interesante. Qué misterioso…”

Dijo Soketto, mientras lanzaba la bola de cristal de su mano.

***

-En el camino de regreso a casa, después de dejar el negocio de Soketto.

“Después de todo, fallo al final. En cualquier caso, yo pienso que Sr. Buzucoily debería de encontrar un trabajo primero…”

“Eran incompatibles para empezar. Después de todo yo he sido su vecina desde pequeña, así que lo conozco bien. Y él es completamente inútil.”

Yunyun que caminaba al lado mío, se detuvo y volteo a verme tras escuchar eso.

“El seria como el hermano mayor de Megumin… Uh, uh, por tanto, ustedes dos podrían ser considerados ‘amigos de la infancia’ ¿verdad? Oye, Megumin, ¿Nunca has estado enamorada de sr. Buzucoily?”

“Nunca.”

Le respondí inmediatamente a Yunyun, antes de que se hiciera de fantasías extrañas.

“Así qué, así es… me he estado preguntando. ¿Acaso es que Megumin solo desea comida? ¿Nunca has tenido algún sentimiento amoroso hacia alguien?”

“Yo aún tengo una misión primordial por completar, Ahora no es el tiempo de entrar en celo.”

Le respondí fríamente, pero Yunyun, no aún no parecía satisfecha con mi respuesta.

“Pero, pero, Megumin se convertirá en aventurera algún día. Y como aventurera serás miembro de un equipo, con el que vivirás y comerás junto a ellos, se ayudarán uno al otro…”

“Es verdad, he escuchado que, entre los aventureros, la posibilidad de que se casen, entre miembros del mismo equipo es bastante alta… pero probablemente eso no me suceda a mí.”

“Tú suenas bastante segura. Bueno tampoco es que pueda imaginar la vergonzosa escena donde Megumin vaya y se le pegue a algún chico por si misma…”

Mientras hablábamos, llegamos a mi casa.

“En cualquier caso, si es que llegara a enamorarme… seria de algún héroe genial, que nunca se rindiera frente la adversidad.”

“Yo siento que, inesperadamente Megumin se casaría con algún chico ordinario…”

Después de despedirme de Yunyun, yo entre a la casa.

“Estoy en casa.”

“Bienvenida a casa, ¡hermana!”

Komekko corrió hacia a mí con Kuro en sus brazos.

Kuro parecía cansado, y al ver como dejaba que ella lo cargara como quisiera. Puedo entender que debió haber pasado por mucho mientras yo estaba afuera.

“Hermana, ¡Vamos a cenar! ¡Hay un montón de comida deliciosa!”

… ¿un montón de comida deliciosa?

“¿Comida deliciosa? ¿Qué sucedió? ¿Volviste a adquirir comida de alguien más de nuevo?”

“Buzucoily la trajo. Él me dijo que, ¡Cuando yo crezca, seré definitivamente una belleza, entonces debo de ir a su casa…!”

Antes de que Komekko terminara de hablar, yo salí disparada hacia la zapatería con el fin de apalear a cierto individuo.

