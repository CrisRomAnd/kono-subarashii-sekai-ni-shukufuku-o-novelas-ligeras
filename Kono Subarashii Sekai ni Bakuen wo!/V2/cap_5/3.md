## Parte 3

-En la parte trasera del carruaje.

Nosotras junto con los aventureros, repelimos el ataque de los gusanos gigantes…

“¡Wow! ¡Así que ese es el Clan de Magos Carmesí! ¡En verdad son fabulosos! ¡Me parece increíble que una sola persona sea capaz de matar tantos gusanos gigantes!”

“Tienes razón. Yo ya había escuchado con anterioridad que los miembros del Clan de Magos Carmesí eran excelentes magos, ¡pero no esperaba que fueran tan poderosos!”

“Y basados en lo que dices, ¿ese ni siquiera es el nivel promedio del Clan de Magos Carmesí? ¡Eso es demasiado poder…!”

Yunyun fue forzada a sentarse en medio del carruaje, mientras que los aventureros y el resto de los pasajeros la elogiaran sin descanso.

Ella agacho la cabeza de la pena.

Yunyun redujo muchos gusanos gigantes a cenizas. Cuando ella recupero la cordura, descubrió que fue ella la que había matado a más de la mitad de los gusanos.

Y ya que los cadáveres podrían atraer a otros monstruos, nosotros partimos sin siquiera descansar.

Y ahora.

Yunyun quien fue la que más contribuyó, estaba siendo objetivo de varios ‘ataques’ y cuestionamientos como ‘¿Cuál es tu destino?’.

“Fuimos afortunados de que estuvieras abordo. Te agradeceremos adecuadamente en cuanto lleguemos a la ciudad de Axel. Después de todo, tú ni siquiera eras una de los guardaespaldas contratados para proteger la caravana. ¡Así que al menos déjanos pagarte por lo que hiciste!”

“No… no hay necesidad… no fue nada…”

Ella lo decía en voz baja.

Después de ser elogiada, Yunyun se sintió aún más avergonzada.

Mientras que yo…

“Onee-san debe de ser más poderosa que la onee-chan que usa moños, ¿verdad? ¡Podemos estar tranquilos si es que monstruos más fuertes llegaran a atacar!”

“… Claro. Cuando monstruos más poderosos aparezcan, yo me encargare. Y en ese momento te dejare contemplar mi magia de muerte segura.”

“¡Estaré esperando por ello!”

Yo estaba sentada aun lado de la ventana, a dos lugares de donde Yunyun estaba y me encontraba escuchando l los elogios de la niña pequeña.

Las cosas no debieron de haber salido así…

Pero claro, había motivos para eso- si yo hubiera usado magia de explosión en ese lugar, los caraguayes y los pasajeros, hubieran salido volando.

Así que dejar las cosas en manos de Yunyun fue la decisión correcta- Claro, yo tenía algo de miedo de esos gusanos gigantes…

“He escuchado que los miembros del Clan de Magos Carmesí aman atraer la atención y las peleas, siempre apareciendo bajo la luz del reflector. ¡Pero tú eres más como una persona normal!”

“Tienes razón. Humilde y seria, ¡Tal parece que tendré que cambiar mi impresión de cómo son aquellos del Clan de Magos Carmesí!”

“Cando escuche que había miembros del Clan de Magos Carmesí en el carruaje, me preocupo que fueran a significar problemas ¡Pero tal parece que me preocupe por nada!”

“Eh, no es así… soy yo la que es la rara del pueblo…”

“¡Tu estas siendo modesta de nuevo! Tu dijiste que solo eras una aficionada, pero probablemente tú seas la más poderosa del Clan de Magos Carmesí!

“Así es. Yo no creo que una persona tan poderosa como tú, sea solo una novata. Yo he escuchado que cuando los miembros del Clan de Magos Carmesí toman el escenario, ellos hacen una extraña auto-presentación. Tú definitivamente calificas para decir ‘soy la maga más poderosa de todo el Clan de Magos Carmesí’, ¿verdad?”

……

“Onee-san, ¿qué te pasa? ¿Estas lastimada? ¿Resultaste herida en la batalla anterior?”

Yo rechine mis dientes del dolor. La chica de enfrente se veía preocupada.

“-La maga más poderosa de todo el Clan de Magos Carmesí, he. Es un poco vergonzoso…”

Yunyun finalmente dejo a esas personas y se acercó a mí.

Mientras que murmuraba emocionada para sí misma.

¡Ahhh…!

“Tu simplemente derrotaste algunos monstruos basura. Si no hubiera habido nadie en el camino, ¡yo los hubiera barrido a todos! ¡Así que no pienses que has ganado!”

“¡Yo no he pensado que gane! Es solo que… es que los pasajeros han reconocido mí fuerza…”

Yunyun se apresuró a explicarse, pero la sonrisa permaneció en su rostro.

…Pero que molesta.

“¡Tengamos un duelo! ¡Veamos quien es la más fuerte de las dos en el próximo descanso!”

“¡Qu-qué! ¡Un duelo! Bien, ¡Acepto el desafío! De cualquier forma, no hemos tenido ningún duelo desde que nos graduamos. Y ya que las dos hemos aprendido magia, ¡llego el momento de decidir, quien es la verdadera ‘maga número uno del Clan de Magos Carmesí!’.”

“Ah, entonces olvídalo. Yo no quiero apostar el título de la ‘maga número uno del Clan de Magos Carmesí’ en este tipo de encuentro.”

“¡Oye, oye, espera! ¡Es que tratas de escapar antes de perder!”

Yo ignore las protestas de Yunyun y voltee a ver a la ventana.

“¿……?”

“Oye, ¡¿Me estas escuchando?! Ya que tú rechazaste el desafío, ¡yo gano por default…! ¡…Megumin! ¿Qué pasa? ¿Es la competencia de las lagartijas corredoras otra vez?”

“…No, probablemente esté viendo cosas.”

Yo le conteste casualmente a Yunyun quien me preguntaba por pura curiosidad.

…Creí haber visto una sombra de nuevo.

Quizás se trate de un ave gigante que se sintió atraída por los cadáveres de los gusanos de tierra y paso de largo.

Para cuando seguí discutiendo con Yunyun yo ya había olvidado aquella sombra-

