## Parte 8

“… ¿Qué sucede?”

“No me preguntes a mí. ¿Cómo se supone que lo sepa?”

Tras ser dejadas atrás, nos quedamos viendo tontamente hacia la dirección por donde Zesta se había ido.

“… Bueno. ¿Qué es lo que planeas hacer ahora, Yunyun? Yo planeo quedarme un tiempo más aquí, hasta que haya juntado suficiente dinero. Después de eso me dirigiré a la ciudad de Axel. Mi meta original era ir a Axel para encontrar compañeros. Y ahora también he escuchado que ahí vive una maga que puede usar la magia de Explosión.”

“Yo… y-yo también había planeado ir a la ciudad de Axel y entrenar con los monstruos débiles que ahí habitan…”

“Ya veo. Pero que coincidencia. Entonces puedes adelantarte, yo te alcanzare tan pronto y cuando haya juntado suficiente dinero.”

“¡¿Eh?!”

Después de escuchar mis planes, Yunyun parecía agobiada.

“¡Pe-pero! No tengo prisa por irme ahora. Creo que puedo recorrer un poco la ciudad antes de irme a Axel…”

Yunyun abrió los ojos y hablo un poco temerosa.

En ese momento, Chomsuke lentamente camino hacia los pies de Yunyun.

Por alguna razón, Chomsuke veía a Yunyun como si le demandara algo, pero Yunyun rápidamente desvió la mirada.

……

“Por cierto, desde que llegamos a esta ciudad, alguien ha estado alimentando a Chomsuke. ¿Sabrás tú algo acerca de eso?”

“¡¿Y-yo no sé?! Chomsuke es una gata después de todo ¡¿Quizás pudo encontrar su propia comida?!”

Ella continúo tartamudeando y desviando la mirada.

Chomsuke continúo a los pies de Yunyun como si esperara algo.

“… Ella continúa viendo a Yunyun con sus ojos llenos de esperanza.”

“¡Quizás quiere que juegue con ella, ya que tenía tiempo que no nos veíamos! En-entonces, ¡regresemos al hotel! Yo me estoy quedando en el hotel que está cerca de la entrada a la ciudad. Si hubiera algo que…”

Como si fuera para remplazar a Yunyun que estaba por irse, Zesta regreso completamente satisfecho y dijo de manera casual.

“-Oh, señorita Megumin. Aún está aquí. Oh cielos, ese demonio es realmente ágil. Ya que era muy agradable ver su cara llena de lágrimas, falle a propósito algunos disparos fatales de magia. Y accidentalmente termino escapando.”

“… Pareces bastante feliz.”

“Pero ¿qué dice? Es lamentable que ella haya escapado, pero, en fin. La señorita Cecily se encuentra ahora buscándola, acompañada por otros miembros del culto… si le es conveniente, señorita Megumin, por favor tome esto.”

Zesta tomo mi mano y puso un pequeño saco en ella.

“¿…? ¿Qué es esto?”

“Es suficiente como para pagar la tarifa desde aquí hasta la ciudad de Axel.”

Me quede congelada tras escuchar las palabras de Zesta.

“Los métodos de reclutamiento que nos enseñó la señorita Megumin nos serán de gran ayuda en un futuro. Aunque no simplemente pienso seguir su fórmula. Sino que planeo experimentar un poco para mejorarlas. ¡Así es! Desarrollaremos nuestros métodos de reclutamiento hasta el punto que sean conocidos como una característica única de esta ciudad. Considere esto como una pequeña muestra de nuestro aprecio.”

Al ver el rostro confiado de Zesta, me empecé a preguntar si lo que hice fue realmente tan sorprendente.

“Entonces, señorita Megumin. ¿Qué planea hacer hoy? ¿Quedarse con el culto a Axis? A esta hora, aún debería de haber caravanas que se dirijan a Axel. ¿Quiere salir en estos momentos hacia Axel?”

Ir hacia Axel ahora mismo.

… yo quiero ir de inmediato.

Quiero llegar a Axel tan pronto como sea posible y reportarle a esa persona.

¡Entonces reunir compañeros y liberar mi magia de explosión, la cual no he podido usar por tanto tiempo!”

“¡Saldré de inmediato!”

“¡¿Eh?!”

Por alguna razón, Yunyun exclamo después de escuchar mi repentina decisión… ah, eso es.

“Yunyun desea recorrer esta ciudad, así que yo me iré adelantando a Axel primero.”

“¡Co-como me harías eso! ¡Y-yo acabo de cambiar de parecer! Si Megumin parte primero a Axel, ¡solo ampliaría la distancia que hay entre nuestro nivel!”

“Ya veo. Yunyun es tan trabajadora.”

“¡¿Sí?!”

Yunyun empezó a actuar ***fuerte*** mientras que desviaba nuestra mirada y sonreía maliciosamente.

Volteé a ver a Zesta, el cual parece ser que entendió lo que quería hacer.

“Señorita Megumin. Ya que es una rara oportunidad, ¿porque no se queda una noche más antes de partir? Después de todo, probablemente aun haya alguien en el culto que quiera despedirse de usted.”

“Ah, lo que dice tiene sentido. ¿Qué debería de hacer?”

“¡¿Eh?!”

-Después de eso, Zesta y yo nos burlamos de Yunyun por un rato más.

Cuando ella finalmente no pudo soportarlo más y estaba a punto de atacarnos, el cielo ya se había oscurecido.

