## Parte 5

No se hacia dónde fue Yunyun.

Pero cuando regreso, me llevo con ella-

“Nunca pensé que Megumin dejaría la villa. Con lo impulsiva y peleonera que eres, ¿Crees que en verdad puedas convertirte en aventurera?”

“¡Tal parece que tu pasaras por problemas para encontrar camaradas!”

Las que dijeron estos rudos comentarios fueron Funifura y Dodonko.

¿Por qué estas dos, siempre dicen cosas así?

“Megumin estará bien. Después de todo, ella es la única a la que nunca le pude ganar.”

Arue, nuestra compañera del parche en el ojo dijo.

“¡¿Eh?! Yo-yo tampoco perdí contra Arue… ¡¡Ah!!”

Yunyun que estaba a punto de decirle algo a Arue, de repente se detuvo al recordar algo.

“En el último examen que tuvimos antes de que ustedes se graduaran, ¿Acaso no fue tu calificación más baja que la mía?”

Tras escuchar eso, Yunyun agacho su cabeza desanimada.

En ese entonces, Yunyun intencionalmente bajo su calificación para que se pudiera graduar junto conmigo.

Bueno tu cosechas lo que siembras.

Pero inesperadamente, Yunyun apretó su puño, como si no quisiera conceder la derrota. Mientras que yo llenaba mi boca con pastel.

Funifura y Dodonko me dijeron sin poder hacer nada.

“… Te diré. En estos últimos momentos aquí, ¡deja de comer tanto y habla con nosotras! ¡¿O es que no tienes ni la más mínima pisca de emociones humanas?!”

“Megumin es una chica después de todo, ¿verdad? En lugar de comer, tu deberías poner más atención a cómo te vistes.”

Ellas pueden decir lo que se les antoje.

Yo ni siquiera sé si tendré una buena comida, después de que inicie mi viaje.

‘Come cuando puedas, duerme cuando puedas’. Ese es el credo de un aventurero…

“… No es que te digamos que no comas, solo que al menos hazlo después de que terminemos con nuestras despedidas.”

Yunyun me dijo, mientras que cargaba a Chomsuke-

Estábamos en la casa más grande de todo el Hogar del Clan de Magos Carmesí, la casa de Yunyun.

Y mi fiesta de despedida se estaba llevando a cabo en estos momentos.

Tal parece que Yunyun solo invito a las compañeras con las que hablaba en la escuela.

Diferentes platillos y el pastel, se encontraban sobre una mesa en el cuarto de Yunyun.

“Bun Abentudedo debe be come buando pueeda, bormir buando bueda…”

“¡Traga antes de hablar!”

Yunyun refunfuñaba. Por alguna razón ella parecía extremadamente emocionada.

“Ah, Arue, ¿Gustas más jugo de frutas? Funifura quieres sabor uva ¿verdad? Megumin, te vas a ahogar si no tomas algo.”

Yunyun entusiasmada, les servía a todas sus bebidas.

Tal parece que ella realmente estaba muy emocionada al tener amigas visitando su casa.

Cuando llegamos, los miembros de la familia de Yunyun quedaron impactados.

Esta era probablemente la primera vez que ella traía amigas a casa.

“Oye, Yunyun, tu deberías de calmarte un poco.”

“Sí, ¿Qué te sucede? Pareces muy emocionada.”

Sus emociones llegaron a tal punto que Funifura y Dodonko comentaron sobre ello.

“¡Lo-lo lamento! Esta es la primera vez que organizó una fiesta…”

“¿Así que así es? Entonces no se puede hacer nada, ¡¿verdad?!”

“¡No se puede evitar! Después, ¡podemos tener una para festejar el cumpleaños de Yunyun también!”

…Yunyun debe de haberse esforzado mucho, para que estas dos se muestren preocupadas por ella.

Arue solo había estado comiéndose su pastal hasta ahora. Pero en ese momento dijo.

“Por cierto, ¿Qué lugar espera usar Megumin como base? Yo siento que Megumin es capaz de tomar áreas con monstruos poderosos desde un inicio.”

“No, Mi intención es iniciar desde lo básico e ir a ‘Axel’ la ciudad de Novatos. Después de todo, yo solo soy una novata, así que sería mejor que formara un grupo con otros novatos.”

“¡Eh! – Incluso tú puedes tener una disposición humilde también.”

“Si hubieras mostrado esa disposición en la escuela antes, tu hubieras hecho más amigas.”

Mientras que pensaba como tratar con los sarcasmos de este dúo…

“Megumin, este es mi regalo para ti.”

Repentinamente, Funifura dijo eso y me entrego un báculo.

“… ¿Uh? ¿Qué es esto? ¿Un regalo de despedida? Yo siento que mi mana fluye realmente suave cuando toco este báculo. ¿Acaso no es caro?”

Para un mago, el báculo era un artículo muy importante, que sirve para elevar nuestro poder mágico.

Y un báculo de esta calidad debería de ser realmente caro.

“No. No pagamos por él. Este báculo lo hizo el padre de Funifura que es un artesano de artículos mágicos. Por cierto, la materia prima para fabricarlo fue reunida por estas dos.”

Después de que Arue termino de explicar, Funifura y Dodonko añadieron orgullosamente.

“Nosotras dos fuimos al bosque que está cerca del pueblo y seleccionamos un árbol que estaba lleno de mana.”

“Así es. De cualquier forma, fue una tarea sencilla… pero si estas agradecida con nosotras, trae algunos chicos apuestos de regreso al pueblo después de tu viaje.”

Arue les hecho una mirada a este par y dijo simplemente.

“Después de todo, estas dos aún no han aprendido magia avanzada, así que yo las acompañaba al bosque. Y cada vez que se encontraban a un monstruo, las dos gritaban…”

“¡Arue!” x2

Después de que Yunyun y yo nos graduamos, Arue fue la siguiente, ella aprendió magia avanzada y se graduó.

Ya que la cantidad de mana que Arue poseía solo estaba atrás de la de Yunyun y la mía, yo pensé que ella se convertirá en artesana de artículos mágicos o algo similar.

Pero ella anuncio que, se convertiría en escritora y se queda en casa todo el día. No sé en qué está pensando.

Yo abrasé el báculo que acababa de recibir y les dije.

“Gracias. Yo lo atesorare. Honestamente no pensé en recibir algo como esto de ustedes dos. ¿Cómo se dice? ¿Acaso esto es a lo que la gente llama ‘tsundere‘?”

“No, ¡no lo es! ¡Yo me sentiría muy mal si me quedara debiéndote algo!”

“La pócima que creo Megumin, fue realmente efectiva. Muy en el fondo, Funifura está realmente agradecida con Megumin.”

“¡Esp-espera!”

…Eh.

“¿Espera? ¿El hermanito de Funifura estaba realmente enfermo en ese momento? Yo creí que solo era una excusa para sacarle algo de dinero a Yunyun para divertirse.”

“Sé que yo no tengo la mejor de las personalidades, ¡pero ni yo haría algo así!”

“Funifura talvez tenga un grave grado de hermanitis, ¡pero no esta tan torcida como para hacer eso!”

“¡¡Oye!!”

Mientras que Funifura y Dodonko discutían, Yunyun permanecía callada.

O más bien, ella quería decir algo, pero no podía.

“Yunyun, ¿No va siendo hora de que lo saques?”

“Sí, después de todo lo que te esforzaste para prepararlo, ¿verdad?”

Entonces Yunyun saco una bolsa de papel. Y me hizo recibirlo a la fuerza.

“Para ti. Megumin ¿Tu aún usas tu uniforme de la escuela como ropa casual ¿verdad? Además, no parece importarte mucho lo que usas, así que yo te compre una túnica de mago para ti. ¿Qué te parece…?”

La bolsa de papel contenía una túnica, una capa y un sombrero.

Sinceramente, esto fue una bendición.

Mi uniforme escolar ya estaba bastante roto y hecho jirones.

“Gracias, lo atesorare.”

Tras escuchar eso, Yunyun suspiro de alivio.

En ese momento, Funifura le dijo a Arue maliciosamente.

“¿Arue? Oye, ¿Tú no preparaste ningún regalo?”

“Sí. Nosotras preparamos el báculo y lo que sea. Es que Arue ¿No preparo nada?”

Como era usual, la personalidad de este dúo es terrible.

Arue asintió y lo reconoció. Hasta ahora, ella había estado concentrada en su comida.

“Entonces, déjenme revelarles este raro ítem que he mantenido oculto.”

Arue dijo eso y se quitó el parche para ojo, que nunca antes se había quitado durante sus días en la escuela.

![](cap_2/img/c2.jpg)

“¡¿Ah?!”

“¡Esta es la primera vez que veo a Arue sin su parche en el ojo!”

Arue ignoro al dúo ruidoso y me entrego el parche para ojo.

“Este es un ítem de la más alta calidad envuelto en gran poder. Al usarlo, uno aumenta su fortaleza mental, sube su resistencia contra el control mental, encanto y magias similares. También puede limitar tu mana. Yo que nací con una vasta cantidad de mana. Para mantener mi poder mágico bajo control, lo he usado desde que era muy joven.”

El pasado de Arue, fue finalmente revelado.

Normalmente, nadie tenía ni idea de que es lo que Arue estaba pensando. La verdad era impensable que Arue tuviera algo como eso.

“¿Es-está realmente bien que le entregues a Megumin algo tan importante?”

“Sí, ¿no te preocupa que tu poder se salga de control?”

Arue le contesto a Funifura y a Dodonko con una sonrisa.

“No hay problema, yo ya no tengo porque usarlo más. Aunque yo aprendí magia, tal y como mi familia deseaba, mi sueño es convertirme en escritora. Yo quiero ser una autora que escriba cosas que hagan a la gente feliz. Y ya que Megumin se convertirá en una aventurera, por favor cuéntame acerca de tus hazañas alguna vez. Yo quiero escribir acerca de las aventuras del pequeño equipo de Megumin.”

Lo que dijo ella fue tan genial.

Tanto que las otras tres personas presentes empezaron a sentirse incomodas.

“Ah, ¿Qué debemos hacer? Nosotras solo le dimos el báculo.”

“Nuestro báculo, uh, fue hecho desde cero, ¡así que podemos considerarlo como que está lleno de buenas intenciones…!”

“¿Qu-que voy a hacer? ¡Yo compre mi regalo directamente de la tienda!”

Yo les dije entonces a las tres personas que murmuraban-

Sonreí y les dije.

“Están bien como están. Yo atesorare todos ellos. Gracias por todo.”

“¡Cierto! …Ah, Arue, si Megumin se pone ese parche en el ojo, ¿no sellaría su mana? ¿eso no reduciría su poder mágico?”

“Ah… si ella quiere usar todo su poder, todo lo que tiene que hacer es quitarse el parche…”

Mientras que ellas hablaban, yo me preparaba para ponerme el parche para ojo…

En ese momento, Arue saco una bella caja envuelta y me dijo sin verme a los ojos.

Esta caja contiene un parche para ojo nuevo.

“Ah, no hay problema. En realidad, no tiene tales efectos. Cuando yo era niña, mi abuelo me lo compro por cómo se veía. Pero ya está gastado ahora, así que compre uno nuevo. ¿No te dije que mi sueño era convertirme en una escritora? Es normal para una autora hacer historias…”

Yo le arroje el parche viejo a Arue, le arrebate la caja que traía el nuevo.
