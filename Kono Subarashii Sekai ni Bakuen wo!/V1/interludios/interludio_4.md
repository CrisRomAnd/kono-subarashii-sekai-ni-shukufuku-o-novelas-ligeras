# Interludio 4

“Ahh. Finalmente resuelve ese rompecabezas y otro más aparece de nuevo, Komekko.”

“No puedo leer las letras que tiene. Señor Hoost, por favor léalas.”

……

“Es mejor que no me llames Señor. Yo te llamare Komekko directamente también.”

“Bien.”

Después de ponernos de acuerdo, Hoost leyó las letras que aparecieron en la tumba.

“ ‘Aquel que desee romper el sello, deberá presentar la oblación de un ave hembra y de su cría. Convide dicha oblación y ore…’ No puedo leer la última parte. Básicamente, necesitamos presentar una ofrenda. Un Ave Hembra y su cría, huh. La ofrenda para romper el sello debería ser algo más formal. Pero será difícil encontrar tales criaturas en este bosque…”

Molesto, Hoost golpeo su puño contra su palma.

“¡Claro! Oye, Komekko, yo te daré el dinero. Ve al pueblo y compra un ave hembra y su cría. ¡Asegúrate de que sean de la más alta calidad!”

“¡Entendido!”

Tome el dinero de Hoost y regrese al pueblo.

-Una hora más tarde.

“¡Ya regresé! Aquí traigo lo que compre.”

“Oh, bien hecho, darte un poco de… uh, ¿Qué es eso?”

Yo estaba sosteniendo con mucho cuidado un plato hondo con mis dos manos para evitar que se derramara. Hoost inclino su cabeza y me volteo a ver confundido.

Yo puse el plato con mucho respeto en el altar.

“Esta es nuestra ofrenda, okyakodon.”

“¡Es que eres idiota!”

Hoost abrió la tapa del plato y miro en su interior…

“Oh… Tienes razón. De verdad tiene que ver con lo que decía el texto de la tumba. Es un ave hembra y su cría. Aunque nunca he escuchado que el okyakodon pueda romper sellos. Y yo que pensé que podría ver la otra parte de la Señora Wolbach…”

“¿Qué es esta Señora Wolbach?”

“¿Uh? La verdadera forma de la Señora Wolbach, es un gigantesco monstruo de color negro. ¡Tú te orinarías en tu ropa si es que la vieras! …en verdad, tal parece que yo mismo debo de preparar la ofrenda. No hay remedio. Presiento que esta ofrenda será problemática, lo más seguro es que me lleve un tiempo…”

Tras decir eso Hoost extendió sus alas. Entonces se volteo rápidamente y me pregunto.

“… Así son las cosas. Quizás me lleve un poco tiempo antes de que nos volvamos a ver. Cuando regrese, jugare contigo de nuevo. No le digas a nadie acerca de mí, ¿entiendes?… Tú puedes comerte ese okyakodon. Nos vemos.”

Después de decir eso, el finalmente salió volando.

-Tiempo después.

Yo me senté en el altar, para comerme el okyakodon. En ese momento.

“¿?”

Repentinamente, escuche un ruido que provenía de los matorrales, así que voltee a ver.

Al final una bestia de color negro emergió de los matorrales.

… su cuerpo era realmente pequeño.

La bestia lentamente se movió hacia donde yo estaba en lo que yo me recuperaba de mi confusión y rápidamente me comí mi okyakodon.

Tal parece que quería comerse mi okyakodon.

Pareciera que él pensaba que este tazón de arroz le pertenecía a él. Pero no, esto es algo que Hoost me entrego a mí.

Me levente para poder comerme mi okyakodon. Esta bestia de color negro me ataco repentinamente.

-Aunque se mantuvo corriendo entre mis piernas y forcejeando por largo tiempo, finalmente se rindió y dejo de moverse.

Incluso cuando lo cargué y mordí su cabeza un par de veces, la bestia ya no se movía.

¡He ganado! –

Después de esta dura batalla, me llevo mi trofeo de guerra a casa.
