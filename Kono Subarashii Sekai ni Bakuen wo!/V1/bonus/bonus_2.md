## Dando mi Primer Paso en el Camino de las Explosiones

Debajo de su capucha, aquella de los grandes pechos parecía temblar.

“¿Qué…. qué dijiste?”

“Dije, por favor, enséñame ese hechizo.”

Se lo repetí a esa mujer.

La onee-san puso una cara más preocupada aún cuando dije que deseaba conquistar el mundo.

Más preocupada que cuando pedí que me creciera el pecho.

Y más que cuando le pedí que me convirtiera en el Rey Demonio.

“Er, ese hechizo…. Lo dije antes, ¿no? Realmente no te recomiendo que aprendas este hechizo”.

………..

“Bien, ¿puedes al menos enseñarme a pescar cangrejos de río? Quiero algo para cenar”.

“¡No… No me mires con tanta frialdad! Puedo enseñarte ese hechizo si realmente lo quieres, pero no lo quiero hacer porque no saldrá nada bueno de ello…”

Miré a la mujer que estaba entrando en pánico delante de mí.

“¿Cuál era el hechizo que acabas de usar?”

“¿Ese hechizo? Se llama Explosión, es llamado el hechizo más fuerte”.

¡El más fuerte!

“Es la fuerza destructiva más poderosa conocida por la humanidad. No importa si el oponente es un dios o un demonio, Explosión es el hechizo ofensivo definitivo que puede destruir cualquier cosa en el mundo”.

¡Definitivo!

“Sin embargo, aprenderlo requiere una enorme cantidad de puntos de habilidad, e incluso si lo aprendes, no podrás lanzarlo si no tienes una gran cantidad de maná. Incluso si puedes lanzarlo, el maná requerido te dejaría sin poder moverte después de un solo disparo… Oye, ¿me estás escuchando?”

“En absoluto. Por favor, enséñame ese hechizo. Quiero lanzar Explosión”.

Me aferré a la túnica de la onee-san y empecé a sacudirla.

“¿Cómo sucedió esto?…. Sólo quería agradecerle… pero en cambio podría haber arruinado su vida…”

La onee-san suspiró profundamente.

-Al día siguiente

La onee-san pidió un día de descanso debido a que ya había lanzado Explosión, así que hoy es el día en que me prometió enseñármelo.

“Jovencita, ¿estás segura de que no quieres reconsiderarlo?”

Nos encontramos en la tumba, antes de que onee-san me llevara a una llanura vacía a cierta distancia de la aldea.

“No, el único hechizo que quiero aprender es Explosión”.

La onee-san suspiró en respuesta.

“Bueno, sólo te estoy enseñando el hechizo. Te llevará unos diez años aprenderlo, así que probablemente te rindas en algún momento”.

“¡Nunca! ¡Lo aprenderé con certeza! ¡Sólo mira!”

Aunque frunció el ceño después de escuchar mi respuesta, la onee-san empezó a recitar lentamente el encantamiento del hechizo Explosión.

“- Sí, ese es el encantamiento de Explosión. Primero veamos si puedes memorizarlo…”

Antes de que la onee-san pudiera terminar sus palabras, moví mi capa y –

“¡El verdadero dios carmesí de la destrucción, Megumin, por la presente ordena! ¡Con mi autoridad, manifiesta el poder de convertir toda la creación en cenizas ante mí!”

“¿Qué clase de encantamiento es ese? ¿¡Qué es eso del dios de la destrucción o de la autoridad o qué que sea!? ¡Definitivamente no dije nada de eso!”

Así, a partir de ese día, pasé mis días aprendiendo magia de aquella onee-san cuyo nombre aún no conozco…

“- Oye, jovencita, no creo que debas añadir tus propias líneas al encantamiento cuando hagas magia. Tus frases suenan geniales, pero eso ya no es Explosión”.

“Soy un Demonio Carmesí, así que quiero añadirle algo del estilo de los Demonios Carmesí”.

Persuadiéndome de no añadir mis propias frases al encantamiento.

“- Jovencita, no hagas una pose. Si haces ese tipo de pose, el hechizo terminará cayendo por todos lados”.

“El hijo del zapatero me dijo que un Demonio Carmesí que logra la victoria sin hacer una pose no es un verdadero Demonio Carmesí.”

“Esa persona esta mal de la cabeza, no le hagas caso. Por favor, no crezcas para ser como él”.

Corrigiendo mi postura mientras canalizo la magia

“- ¡Mira, este es el hechizo mágico avanzado ‘Infierno’! Es un poderoso hechizo que crea una gran llama e incinera toda el área. ¿Qué opinas? ¿Quieres aprender esto mejor?”

“- No, en absoluto. Como sea, date prisa y lanza Explosión. ¡Quiero ver la Explosión de hoy!”

“Jovencita, Explosión no es montón de fuegos artificiales, ¿sabes? Incluso si lo aprendes, no puedes activarlo cuando quieras”.

“No, yo lo haré”.

Con frecuencia, la onee-san se quedaba al borde de las lágrimas cuando intentaba persuadirme a mí, que me aferraba obstinadamente a Explosión.

“-Jovencita, la magia avanzada y el teletransporte es una combinación muy potente que puede lidiar con la mayoría de los enemigos.”
“¡Explosión!”

“Si usas el hechizo Luz de Sable, puedes cortar cualquier cosa si tienes suficiente maná…”

“¡Explosión!”

“Bien, en vez de Explosión, ¿qué tal si aprendes Estallido o Detonación? Ambos son hechizos bastante fuertes por derecho propio…”

“¡Explosión!”

“Te lo ruego, ¡Escúchame!”

La onee-san finalmente se quebró y rompió a llorar mientras practicaba mis frases geniales.

-Después.

“¡No quiero aprender nada más que Explosión! No me importa cuántos años lleve. Aunque tenga que practicar hasta que sea una anciana, no me rendiré hasta que aprenda Explosión”.

“Dime, ¿qué es exactamente lo que te obsesionó tanto de Explosión?”

Ignoré la pregunta de onee-san y le di un trozo de papel.

“Por favor, usa este encantamiento para tu explosión de hoy.”

“… ¿Has vuelto a cambiar el canto? La invocación de un hechizo no es para aumentar su poder, sino para controlarlo. Si lo cambias descuidadamente… suspiro…”

Onee se cubrió la cara con la mano después de leer lo que escribí.

Sin embargo, al final, cedió y usó mi encantamiento para su hechizo–

“¡Explosión!”

Un nuevo cráter apareció en el campo que desde entonces se ha visto atiborrado con cráteres en los últimos días.

“¿Qué diablos…? ¿¡Cómo es que la fuerza del hechizo se incrementó repentinamente!?”

La onee-san se puso nerviosa al ver el poder de su hechizo.

“Por supuesto, un encantamiento que suene genial aumentaría el poder del hechizo.”

“¡La magia no es tan simple!… De acuerdo, entiendo que eres una genio, pero, por favor, no modifiques los hechizos de ahora en adelante, ¿de acuerdo?”

“Me niego.”

Una vez más, la onee-san persona parecía estar al borde de las lágrimas.

Parece ser una mujer muy hermosa, pero por alguna razón creo que sólo la he visto cuando está a punto de llorar.

“- Entonces, antes de que empiece el entrenamiento, hay algo que quería mostrarte. Pero es realmente grande y aterrador, y tú eres una niña, así que no debería forzarte a venir. ¿Quieres hacerlo?”

“Mi mamá da mucho miedo, así que está bien.”

Onee-san se rió finalmente para variar.

“De lo que estaba hablando es varias veces más aterrador que tu madre, ¿sabes?”

“¡¿En serio?! Cuando mi papá salió a pedir dinero prestado para sus extraños inventos, mi mamá congeló su cuerpo de la cabeza hacia abajo y lo arrojó al bosque mientras lloraba. ¿¡Es más aterrador que eso!?”

“…Oh, cierto, tu madre también debería ser un Demonio Carmesí. Si ella es como tú, entonces probablemente será tan aterradora como eso”.

Algo que asusta tanto como mamá….

“¿Vamos a ver al Rey Demonio o a un dios malvado?”

“¿Qué tan aterradora es tu madre? …¿Por qué tus ojos brillan? ¿Estás interesada en el Rey Demonio o en los Dioses Malignos?”

La onee-san dijo eso con una sonrisa astuta, y asentí.

“¡Después de que derrote al Rey Demonio, tomaré su título y me convertiré en el próximo Rey Demonio!”

“¡No puedes hacer eso! ¡El Rey Demonio es el rey de toda la raza demoníaca! No es algo en lo que puedas convertirte simplemente derrotando al actual rey”.

Después de decir eso, la onee-san repentinamente emitió una expresión astuta.

“…Aunque, Rey Demonio aparte, si estamos hablando de dioses malvados, puede que ya hayas conocido a uno sin darte cuenta.”

Dijo, con una extraña sonrisa apareciendo en sus labios.

“Eso es imposible. Si un dios malvado se acercaba a la aldea, los aldeanos lo habrían capturado y sellado para convertirlo en otra atracción turística”.

“¡Creo que los Demonios Carmesí deberían dejar de tratar a los seres divinos como juguetes!”

-En lo más profundo del bosque en las afueras del pueblo.

Este lugar suele estar prohibido para mí, ya que contiene numerosos monstruos fuertes.

Después de que onee-san me trajo aquí….

“¡Jovencita, creo que deberíamos volver! ¡Fue mi culpa, por favor, escúchame!”

“¡El hígado y el estómago de un “oso de un solo golpe” valen mucho dinero! No sé por qué nos tienen tanto miedo, ¡pero es una oportunidad maravillosa! ¡Si atrapamos a uno, podemos comerlo para la cena!”

Terminamos persiguiendo a una manada de cobardes osos de un solo golpe que huían de nosotros con miedo por alguna razón.

Hace tiempo que no como más que cangrejos de río, por lo que la perspectiva de tener un tipo diferente de carne para la cena es realmente atractiva.

“Espera, ¡sólo te traje aquí porque quería enseñarte que la magia avanzada es mucho mejor para tratar con monstruos peligrosos! ¡No he venido aquí a cazar osos de un solo golpe!”

“No hay problema. No siento que vaya a perder contra nadie ahora mismo. ¡Estoy segura de que mis habilidades como dios de la destrucción aparecerán en el fragor de la batalla!”

“¿Por que son los Demonios Carmesí tan… son así desde el momento en que nacen…”

Onee-san empezó a entonar su magia después de suspirar profundamente.

“¡Ráfaga Helada!”

Un polvo blanco surgió de las manos de onee-san y envolvió a todos los osos de un solo golpe, dejándolos inmóviles.

“Oh, creo que esto puede convertirse en una nueva atracción si lo llevamos de vuelta a la aldea.”

“¡De ninguna manera te dejaré hacer eso! ¡Esto es sólo para mostrarte que hay otros, mejores y más fuertes hechizos en el mundo!”

Mirando a la manada congelada de osos de un solo golpe que no podían hacer otra cosa que quejarse patéticamente, onee-san dejó escapar una extraña sonrisa.

“Lo que acabo de usar fue la magia intermedia Ráfaga Helada. ¿Ves? Con suficiente maná, incluso la magia intermedia puede ser un arma poderosa. Esto requiere mucho menos maná que Explosión. Si usas magia avanzada y magia intermedia estratégicamente, estoy segura de que serás capaz de convertirte en una de las mejores magas…. jovencita, ¿¡qué estás haciendo!?”

Ella se movió apresuradamente delante de mí mientras yo me acercaba a la manada con un palo grande.

“Es una rara oportunidad el tener una manada de monstruos de alto nivel indefensos, así que voy a acabar con ellos y ganar algunos puntos de experiencia. La gente de la aldea a menudo utiliza este método para subir de nivel. Los congelan así y los matan, y algunos incluso crían monstruos específicamente para esto”.

“¡Qué demonios! ¡¿L-Los Demonios Carmesí tienen acaso algo de decencia?!”

-Estos días felices continuaron durante bastante tiempo.

Pero con el tiempo, parecía que onee-san finalmente empezaba a inquietarse.

“Jovencita, ¿puedo preguntarte algo?”

“¿Qué cosa? No te diré dónde está el nido de los cangrejos de río, así que no preguntes”.

“No te preocupes, no quiero saber eso. … Los Demonios Carmesí tienen sus tarjetas de aventurero hechas cuando nacen, ¿verdad? Entonces, ¿puede onee-san echar un vistazo a tu tarjeta?”

Escuchando esto, traté de esconder la tarjeta que originalmente estaba en el bolsillo de mi túnica.

Onee-san volvió a suspirar después de ver esto.

“Lo sabía. Explosión ya apareció en tus habilidades aprendidas, ¿no?”

“No, soy un estudiante lenta, así que necesito mucho más tiempo para aprender algo así.”

Onee-san levantó su mano hacia mí.

“Paralizar”.

Después de que ella entonara un extraño hechizo, me di cuenta de que no podía moverme en absoluto.

“¿Qué, quieres hacer lo que quieras conmigo mientras yo no pueda moverme? ¡Pediré ayuda cuando vuelva a la aldea! Cuando vuelva les diré a mi mamá y a mi papá que me secuestró una onee-san de pechos grandes y ojos amarillos. ¡D-Detente! ¡Detente!”

“¡Sólo quería ver tu tarjeta de aventurero! ¡Por favor, no le hables a la gente de mí!”

Por mucho que quisiera resistirme, no podía mover ni un músculo, y me quito fácilmente la tarjeta.

Onee-san removió su hechizo una vez que tomó mi tarjeta, y empezó a leerla en voz baja.

“Explosión está bajo habilidades aprendidas, ¿huh? ¿¡Las otras habilidades que te mostré también están aquí!? ¿¡Cómo puedes llamarte a ti misma una estudiante lenta!? ¡Aprendiste todo esto después de verlos sólo una vez!”

Viéndome cruzar los brazos y mirar hacia otro lado, la onee-san hizo una expresión de preocupación.

“Jovencita, todavía tengo cosas que hacer y lugares que visitar. Ahora que te he enseñado Explosión, ya es hora de que nos separemos”.

“¡No quiero! ¡Si onee-san se va, no podré ver Explosiones nunca más!”

Oyendo eso, se inclinó hasta el nivel de mis ojos.

“¿No dijiste que aprenderías Explosión con certeza? Lo verás de nuevo una vez que lo aprendas.”

La onee-san me dio una sonrisa amable.

“Entonces es hora de que nos separemos. Sería peligroso alejarse más de la aldea, así que no hay necesidad de verme partir…. No estoy de acuerdo con esto, pero al final, es tu vida. No te detendré, depende de ti tomar tus propias decisiones a partir de ahora”.

La onee-san dijo eso mientras miraba el camino que conducía a la salida de la aldea.

No pasamos mucho tiempo juntas, pero disfruté cada día que pasé con onee-san.

Conseguí comer buena comida, cazar monstruos poderosos y ver muchos tipos diferentes de hechizos.

Así que…

“Bien, es hora de decir adiós. Si alguna vez llega el día en que aprendes magia de explosión, me gustaría verlo por mí misma”.

“Entonces no te olvides de mí antes de que llegue ese día.”

“N-No creo que pueda olvidarme de ti aunque quisiera. Tu nombre inusual, todas las cosas que hiciste, y tu aptitud para la magia…. El solo hecho de verte hace que mi corazón se estremezca. Deberías intentar llevar una vida pacífica de ahora en adelante”.

La persona que lanzó ese hechizo para mí tantas veces, el hechizo del que me enamoré….

“Mi objetivo es derrotar al Rey Demonio, así que no puedo prometerte que llevaré una vida pacífica, pero… puedo prometerte que definitivamente te mostraré mi Explosión, así que…”

Como si supiera lo que iba a decir, onee-san me dio una sonrisa irónica.

“¿Podría, por favor, ver la Explosión de onee-san por última vez?”

Definitivamente te mostraré mi Explosión algún día…

**– FIN DEL VOLUMEN 1 –**


![](bonus/img/c1.jpg)
