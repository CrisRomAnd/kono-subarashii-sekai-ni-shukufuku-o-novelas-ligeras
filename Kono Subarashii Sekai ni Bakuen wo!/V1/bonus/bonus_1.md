# Bonus 

## Él Bobo Seguidor de Komekko

¿Qué está pasando?

“Onii-chan, ¡ahora quiero comer hamburguesa de lagartija de las nieves!”

“No hay problema, tú onii-chan es muy fuerte. ¡Lagartos de fuego, lagartijas de las nieves son nada para mí!”

Mientras regresaba a casa de la escuela, yo vi a Buzucoily tratando de seducir a Komekko con comida. Me preguntaba si debía delatarlo con los adultos del pueblo.

Pero, para mi hermanita en crecimiento, este NEET es una rara fuente de alimentos, así que solo pretenderé que no he visto nada….

“Cuando crezca, ¡yo me casaré con mi onii-chan!”

“Maldito, ¡Cómo te atreves a seducir a mi hermana!”

“Oye, espera, espera ¡Megumin! ¡¿Porque me golpeas de Repente?!

-Tras escuchar sus profundas explicaciones, me di cuenta de que la hambrienta Komekko se encontraba corriendo por todos lados y que Buzucoily simplemente la estaba protegiendo.

Si la dejaras correr por donde ella quiera, seguro que ella iría al bosque que está cerca del pueblo para buscar comida.

“Escucha Komekko. Hay monstruos muy peligrosos que viven en el bosque que está a la salida del pueblo, así que tú no debes de ir ahí sola, ¿entendiste?”

Tras escuchar lo que yo le decía, Komekko me contestó.

“¿Puedo ir si el poderoso de Buzucoily me acompaña?”

“No, ese NEET tal-vez tenga poder de batalla, pero estar con él representa otra clase de peligro para ti Komekko.”

“¿Qué es lo que intentas decir? ¡Mis gustos no son tan ‘especiales’!”

Komekko nos volteo a ver mientras que discutíamos.

“Entre mi hermana y Buzucoily, ¿quién es más fuerte?”

“…” x2

Después de un momento de silencio, natural mente ambos nos señalamos a nosotros mismos.

“Pero claro que esa sería yo, la mayor genio del pueblo…”

“Por supuesto que sería yo, después de todo, soy la persona que tiene más tiempo libre para poder cazar monstruos todos los días.”

Y volvimos a discutir enfrente de Komekko.

“… Cuando éramos más jóvenes, yo te aventé al estanque y te pusiste a llorar. Aún deberías poder recordar eso ¿verdad?”

“… Eso fue hace tantos años. Pero ahora ¡yo tengo esta técnica! ¡Refracción de Luz!”

“Agh, ¡y pensar que este tipo en verdad uso magia en una discusión contra una niña menor que el!”

Mientras que me quejaba sobre lo bajo que callo Buzucoily al usar magia de invisibilidad, su voz burlona sonaba por algún lugar.

“Megumin, ¡solo admite tu derrota! Comparado a cuando éramos niños, ahora yo soy diferente, tanto que se me podría considerar como un gran oponente a la armada del rey demonio…”

“-Oh, ¿pero si no son Megumin y su hermanita Komekko?”

El que interrumpió a Buzucoily fue el dueño de la mejor zapatería del pueblo, el padre de Buzucoily.”

“Yo le pedí a ese granuja que cuidara la tienda por un momento, pero en cuanto me distraje el salió corriendo hacia alguna parte. ¿De casualidad ustedes no han visto al tonto de mi hijo?”

Komekko y yo apuntamos hacia un sitio vacío.

“Debería de estar por ahí.” 

“Debería de estar por ahí.” 

Al mismo tiempo, se escuchó el sonido de algo escapando corriendo apresuradamente.

El padre de Buzucoily empezó a recitar un hechizo hacia esa dirección-¡!
