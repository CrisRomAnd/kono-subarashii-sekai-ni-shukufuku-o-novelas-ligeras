# Interludio 3

El rompecabezas fue resuelto al segundo día.

“… como odio a estos miembros del Clan de Magos Carmesí.”

“Yo también soy miembro del Clan de Magos Carmesí.”

“A excepción de ti.”

Hoost y yo estábamos frente a un nuevo rompecabezas. Pensábamos que ya era todo, pero al terminar, uno nuevo apareció al tercer día.

“Ah, no puede ser. No puedo resolver esto. Y por como se ve, no creo que tú tampoco puedas. Algo de este nivel, no hay forma que un niño lo resuelva.”

“Mi hermana dice que, ‘aunque alguien trate de arruinar tu entusiasmo, tú siempre debes seguir adelante’.”

Yo me acosté en el pasto y empecé a agitar mis pies en el aire mientras trabajaba en el rompecabezas al mismo tiempo.

“Oye, eso que haces no es propio. Tus pies están sucios. Ahh…”

Hoost se puso a limpiar mi túnica, mientras que yo me concentraba en el rompecabezas sin ninguna preocupación.

“… Oye… como haces eso… ¡Tú eres bastante competente! ¡¿Debe de haber una manera de solucionarlo así?! Uh, ¿Qué pasa? ¿Te atoraste en alguna parte?”

Entonces yo puse las piezas del rompecabezas a medio resolver en el suelo y cerré mis ojos. Todo había pasado sin contratiempos hasta el momento.

“Estoy cansada de esto.”

“¡Te lo suplico Komekko! Ya has llegado tan lejos ¡No te rindas ahora! ¡¿Quieres comer algo?! ¡Debes de estar hambrienta! Te traeré algo de comer inmediatamente.”

Tras escuchar eso, me levante de nuevo y continúe resolviendo el rompecabezas.

“¡Bien, bien! ¡Mantén esa actitud! Mientras tanto yo iré por algo para comer.”

Hoost desplegó sus alas y voló hacia algún lugar.

“¡Ah! ¡Yo también quiero ir! ¡Llévame contigo!”

“¡Id-idiota! ¡No puedo llevar una mocosa como tú al bosque! ¡Una chiquilla como tú sería comida por los monstruos en un instante!”

“¡Yo quiero ir! ¡Yo quiero ir! Hoost es muy fuerte, ¡por eso ésta bien!”

“Bueno, ¡es verdad que yo soy muy fuerte! Después de todo, cada vez que entro al bosque, ¡Los otros monstruos escapan de mí!”

“¡El señor Hoost es tan genial!”

-Al final Hoost fue persuadido.

“-Ha… ¡Ha…! Tú, ¡tú estás armando demasiado alboroto! Oye, ¡tú estabas a punto de caerte!”

“Oye, ¡por ahí hay una lagartija enorme! Hoost, ¡esa! ¡atrapa esa lagartija!”

“¡Escúchame cuando yo te esté hablando!”

Hoost me llevo al bosque en sus brazos.

“… Viste eso, eso fue tu culpa, Komekko. Se escapó porque gritaste demasiado fuerte. La mayoría de los monstruos huira tras olfatear la esencia de un demonio superior como yo. Ya que he venido al bosque en varias ocasiones para conseguirte comida, es normal que estén atentos a mí y huyan dirigiéndose a la entrada del bosque.”

“… Después de yo crezca y me vuelva fuerte, ¿yo también haré que los monstruos huyan ante mi presencia?”

“¿Quién sabe? Aun qué no importa que tan fuerte te vuelvas una pequeñaja como tu ¡no espantaría a nada! ¡Hahahahaha!”

Hoost se reía mientras que me bajaba. En ese momento, algo se movió en los arboles frente a nosotros.

“Oh, viene hacia nosotros aún tras sentir mi presencia… este debe de ser bastante fuerte.”

“Oye, si yo espanto a ese monstruo aquí, ¿eso querría decir que soy más fuerte que Hoost?”

“¡Hahaha! Oh, oh, no… ¡Mi panza me duele! Hahahaha. Bueno, si en verdad eso pasara, en verdad serias más fuerte que yo. Pero este bosque es el hogar de muchos monstruos aterradores… una vez que los veas, será mejor que no mojes tus calzoncillos.”

“¡Yo no mojare mis calzoncillos! ¡Yo lo ahuyentare!”

Tras escuchar eso, Hoost se rió aún más fuerte.

“Oh, si tú crees que puedes hacerlo, ¡deberías intentarlo entonces! Si tu logras ahuyentarlo, entonces ¡yo te llamare Señorita Komekko a partir de ahora en adelante!”

De entre los árboles que se movían, salía súbitamente un enorme oso.

“… ‘Oso de un solo golpe’. Más bien un grupo. Lidiar con tantos sería difícil sin duda. Tal parece que no hay otra alternativa. Los dejare por esta vez…”

Eso dijo Hoost mientras que daba la vuelta.

En ese momento yo me abalance a los osos.

“¡¡Ugh!!”

“¡¿Komekko?!”

-Hoost grito y se lanzó hacía los osos.
