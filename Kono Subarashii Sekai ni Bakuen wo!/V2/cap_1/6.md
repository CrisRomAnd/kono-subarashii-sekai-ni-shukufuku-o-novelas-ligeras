## Parte 6

“¡Ahhh! Estúpida, ¡estúpida! ¡Soy tan estúpida! Yo ya sabía que Megumin era una idiota, ¡pero hoy yo soy más idiota que ella!”

En el bosque cerca del pueblo, Yunyun se encontraba gritando con lágrimas en los ojos.

“Comparado con una persona ridículamente inteligente ¿No es más adorable de alguna manera un niño estúpido?”

“Eres muy ruidosa, ¡cállate! Ah, en verdad, Megumin ¡sostente bien! ¡o de lo contrario te caerás!”

Yunyun estaba corriendo por el bosque. Y por supuesto, yo estaba en su espalda.

“La espalda de Yunyun es tan amplia. En verdad eres confiable.”

Un monstruo parecido a una lagartija con escamas color rojo-sangre se encontraba persiguiéndonos lleno de ira.

Tal parece que se enfureció, después de haber salido volando, como resultado de mi magia de explosión.

“Y yo que pensé que esto sería un poco mejor que usar magia de explosión sobre el pueblo. ¡Fui una tonta! ¡Me gustaría reprender a mí yo de hace algunos minutos!”

“No te culpes a ti misma, Yunyun. Como alguien que no rechazar a los demás, tu eres realmente linda.”

“¡En verdad yo voy a tirarte aquí! Ah, ¡mami!”

-Si tengo a Yunyun a mi lado, ella puede cargarme a casa.

A partir de ahora Yunyun me acompañara a descargar mi magia de Explosión.

Desafortunadamente, la elección del lugar esta vez fue mala y termine haciendo volar a Draco de fuego que se encontraba durmiendo cerca.

![](cap_1/img/c3.jpg)

Por consiguiente, nos encontramos en esta situación.

Yunyun me cargo en su espalda ya que yo estaba completamente exhausta, mientras que corría ella lloraba por el bosque. Incluso conmigo como peso extra, su paso era rápido.

Ella era por donde lo vieras, una excelente estudiante.

“Algunas veces, yo incluso te confundo con un chico. Tu eres realmente confiable en tiempos de crisis. Y pese a que siempre te quejas, tu aun así soportas todos mis caprichos.”

“¡Cállate! Si hablamos sobre quien es más masculina, ¡tú te pareces más a un chico que yo! Tú tienes el cabello corto y como no te has desarrollado… Ouch, ¡Ouch! ¡No me jales el cabello en estos momentos!”

Tras escucharla mencionar el crecimiento por pubertad, hizo que yo intencionalmente le jalara el cabello, causando que gritara.

Esto probablemente altero su balance, causando que se dirigiera hacia la raíz de un árbol y cayera. Naturalmente, yo también salí volando de su espalda.

Chomsuke, libero sus garras de mi hombro y bajo ágilmente.

“Ouch, Ouch… ¡todo esto es tu culpa Megumin!

“¿Que? ¡Todo es porque Yunyun se burló de las características de mi cuerpo…! Ah, se está acercando, ¡rápido cárgame!”

“… Yo en verdad quiero abandonarte.”

Mientras que Yunyun consideraba eso fríamente, el enojado Draco llego.

“Esto es malo. Yunyun ¡Prepárate para pelear! ¡Solo hay uno y el Draco de fuego es considerado como el más débil de los poderosos monstruos que habitan cerca del pueblo! Mientras puedas evitar su aliento de fuego, ¡Tú puedes ganarle!”

“Aunque tienes razón en eso. Pero verte decirlo, con tu cara en el suelo, es simplemente ¡desesperanzador!”

Yunyun saco su daga y confronto al Draco de fuego.

Con tal de ver la batalla, yo voltee mi cuerpo hacia arriba, no sin muchas dificultades.

Después de voltearme, use un árbol que se encontraba convenientemente cerca para apoyarme y tener mejor vista de la pelea.

El Draco de Fuego es un gran monstruo de aspecto reptiliano, que se arrastra sobre el suelo con cuatro patas.

Este miro a Yunyun y le mostro su lengua roja con intenciones de intimidarla.

No, eso no era su lengua.

Este monstruo en verdad hace honor a su nombre. Podía exhalar fuego, y aunque en pequeña cantidad esta vez, lo hacía para intimidar a su enemigo.

“No nos atacó inmediatamente tras alcanzarnos, así que… Yunyun, el enemigo nos teme.”

Yunyun miro a la lagartija y dijo.

“¿Cómo que ‘nos’? ¡solo es de mí del que es precavido! Pero la culpable de sacarlo volando esta junto a mí incapaz de moverse, ¡así que probablemente no nos deje!”

En este momento, yo repentinamente sentí peso sobre mi estómago. Mire y se trataba de Chomsuke que con calma se acomodó sobre mi cuerpo.

“… En un momento como este y aun así luce tan tranquilo.”

“¡No es justo como su dueña!”

El Draco de fuego se acercó y tomo una posición de pelea.

Yunyun no movió su línea de vista. Ella sostuvo fuerte mente su daga con una mano y preparo su otra mano.

Ella se encontraba a escasos cuatro metros del Draco de fuego. El enemigo podía atacar en cualquier momento.

“¡Ráfaga Congelante!”

Seguido de su encantación, el aire al redor de la lagartija se volvió frio.

Ella uso magia intermedia para crear una fría neblina.

Se formó una capa de hielo sobre sus escamas rojas, cubriéndolo de blanco. Su cola que azotaba el suelo, lentamente dejo de moverse.

Los monstruos del tipo reptiliano son débiles al frio. Por alguna razón desconocida cuando se usa magia de hielo en su contra, ellos se alentaban. Esto es de conocimiento general para los magos.

Pero, aunque los movimientos de la lagartija hayan disminuido por el frio, aun a si esta se acercaba.

Yunyun se percató de esto y preparo otro hechizo para acabarlo… repentinamente, dos refuerzos aparecieron detrás de la lagartija.

“Oh, se dio cuenta de que no tenía oportunidad y llamo por ayuda. Yunyun me tiene a mí y a Chomsuke de su lado, así que al menos en términos de números estamos iguales… Tal parece que no hay necesidad de que yo actué. Yunyun ¡acábalos!”

“¡Deja de decir tonterías mientras que estas tirada en el suelo! ¡Escapa si puedes! Yo aún puedo pelear contra uno, pero contra tres al mismo tiempo, mientras que protejo a la inútil de Megumin, ¡está más allá de lo que puedo hacer…! ¡Ráfaga Congelante! ¡Ráfaga Congelante!… Megumin, ¡escapemos ahora! Mientras que están retenidos, ¡yo puedo escapar aún contigo a mis espaldas!”

Después de que usara su magia sobre los dos que acababan de llegar, Yunyun retrocedió, me tomo de mi brazo y me apoyo con su hombro.

Chomsuke uso sus garras para sujetarse fuertemente a mí y así evitar ser abandonado. Mientras que Yunyun me ponía en su espalda…

Justo cuando estábamos a punto de escapar, una lagartija exhalo una gran flama.

“¡Quema! quema, quema, ¡quema! Yunyun ¡hace mucho calor! ¡Usa magia de hielo rápido! ¡Usa tu magia de hielo en mí! ¡es mi espalda la que se eta quemando!”

“¡Estaremos bien! ¡solo es la orilla de manto el que se quema! Además ¡estamos escapando ahora!”

Después de levantarme, Yunyun salió disparada. Al darse cuenta de nuestras intenciones, las lagartijas exhalaron fuego de nuevo.

Pero las flamas no alcanzaron a Yunyun. Mi espalda por otro lado…

“¡Ahhh! Yunyun, mi espalda, ¡mi espalda!”

Yunyun me sujeto más fuerte y me dijo sin voltear atrás.

“¡No te muevas tanto! ¡Casi me caigo de nuevo! ¡solo un poco más adelante y estaremos bien!”

“¡Yunyun! Aquí vienen, ¡con más refuerzos! Esto es realmente malo. ¡Rápido Huyamos!”

“¡Lo sé! No me aprietes tan fuerte, ¡es difícil correr así! Ah, ¡en verdad! ¡Nunca te volveré a acompañar a hacer esto otra vez!”

